package main

import (
	"encoding/base64"
	"log"
	"math/bits"
	"math/rand"
	"net/http"
	"time"
)

type ShutdownReceiver struct {
	ch    chan struct{}
	token string
}

var shutdownReceiver ShutdownReceiver

func (this *ShutdownReceiver) Init() error {
	if DebugModeShowInit || DebugModeShutdown {
		log.Println("ShutdownReceiver.Init")
	}
	t := time.Now().UnixNano()
	r := int64(bits.Reverse64(uint64(t)))
	rand.Seed(t ^ r)
	var buf [9]byte
	rand.Read(buf[:])
	this.token = base64.URLEncoding.EncodeToString(buf[:])
	this.ch = make(chan struct{})
	return nil
}

func (this *ShutdownReceiver) GetReceiver() <-chan struct{} {
	return this.ch
}

func (this *ShutdownReceiver) GetSender() chan<- struct{} {
	return this.ch
}

func (this *ShutdownReceiver) GetToken() string {
	return this.token
}

func init() {
	// POST
	// token string
	http.Handle("/api/shutdown", &shutdownReceiver)
}

func (this *ShutdownReceiver) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if DebugModeShutdown {
		log.Println("ShutdownReceiver.ServeHTTP")
		defer log.Println("defer ShutdownReceiver.ServeHTTP")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	token := req.PostFormValue("token")
	if token != this.GetToken() {
		badRequest(w, "wrong token")
		return
	}

	ret := struct {
		Result string `json:"result"`
	}{}

	sender := this.GetSender()

	select {
	case sender <- struct{}{}:
		ret.Result = "OK"
	default:
		ret.Result = "DONE"
	}

	writeJson(w, req, &ret)
}
