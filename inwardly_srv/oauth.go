package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

type Consumer struct {
	ConsumerKey    string `json:"oauth_consumer_key"`
	ConsumerSecret string `json:"oauth_consumer_secret"`
}

type RequestToken struct {
	Token             string `json:"oauth_token"`
	TokenSecret       string `json:"oauth_token_secret"`
	CallbackConfirmed bool   `json:"oauth_callback_confirmed"`
}

type AccessToken struct {
	Token       string `json:"oauth_token"`
	TokenSecret string `json:"oauth_token_secret"`
}

func (this *Consumer) TakeRequestToken(u, cb string, form url.Values) (*RequestToken, interface{}, error) {
	if DebugModeOAuth {
		log.Println("Consumer.TakeRequestToken")
		defer log.Println("end of Consumer.TakeRequestToken")
	}

	req, err := http.NewRequest(
		http.MethodPost,
		u,
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return nil, nil, err
	}

	params := NewParamaterList(this.ConsumerKey)
	if cb == "" || cb == "oob" {
		params.SetOAuthCallbackOob()
	} else {
		if _, err = url.Parse(cb); err != nil {
			return nil, nil, err
		}
		params.SetOAuthCallback(cb)
	}

	err = params.SetSignature(req.Method, u, this.ConsumerSecret, "", form)
	if err != nil {
		return nil, nil, err
	}

	authorization := params.HeaderValue()

	req.Header.Set("Authorization", authorization)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	defer func() {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
	}()
	if err != nil {
		if resp != nil {
			log.Println(u)
			log.Println(form)
			log.Println(req.Header)
			log.Println(resp.Status)
			log.Println(resp.Header)
		}
		return nil, nil, err
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}

	if resp.StatusCode != http.StatusOK {
		log.Println(u)
		log.Println(form)
		log.Println(req.Header)
		log.Println(resp.Status)
		log.Println(resp.Header)
		log.Println(string(respBody))
		return nil, respBody, fmt.Errorf(
			"%s %s",
			resp.Status,
			string(respBody),
		)
	}

	contentType := resp.Header.Get("Content-Type")
	if strings.Contains(contentType, "json") {
		var res interface{}
		err := json.Unmarshal(respBody, &res)
		if err != nil {
			return nil, nil, err
		}
		m, ok := res.(map[string]interface{})
		if !ok {
			return nil, respBody, fmt.Errorf("unknown format json")
		}
		reqToken := &RequestToken{}
		if v, ok := m["oauth_token"]; ok {
			if s, ok := v.(string); ok {
				reqToken.Token = s
			}
		}
		if v, ok := m["oauth_token_secret"]; ok {
			if s, ok := v.(string); ok {
				reqToken.TokenSecret = s
			}
		}
		if v, ok := m["oauth_callback_confirmed"]; ok {
			if b, ok := v.(bool); ok {
				reqToken.CallbackConfirmed = b
			} else if s, ok := v.(string); ok {
				reqToken.CallbackConfirmed = s == "true"
			}
		}
		return reqToken, m, nil
	}

	values, err := url.ParseQuery(string(respBody))
	if err != nil {
		return nil, respBody, err
	}

	return &RequestToken{
		Token:             values.Get("oauth_token"),
		TokenSecret:       values.Get("oauth_token_secret"),
		CallbackConfirmed: values.Get("oauth_callback_confirmed") == "true",
	}, values, nil
}

func (this *Consumer) TakeAccessToken(u string, reqToken *RequestToken, verifier string) (*AccessToken, interface{}, error) {
	if DebugModeOAuth {
		log.Println("Consumer.TakeAccessToken")
		defer log.Println("end of Consumer.TakeAccessToken")
	}

	req, err := http.NewRequest(
		http.MethodPost,
		u,
		nil,
	)
	if err != nil {
		return nil, nil, err
	}

	params := NewParamaterList(this.ConsumerKey)
	params.SetOAuthToken(reqToken.Token)
	params.SetOAuthVerifier(verifier)

	err = params.SetSignature(req.Method, u, this.ConsumerSecret, reqToken.TokenSecret, nil)
	if err != nil {
		return nil, nil, err
	}

	authorization := params.HeaderValue()

	req.Header.Set("Authorization", authorization)

	resp, err := http.DefaultClient.Do(req)
	defer func() {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
	}()
	if err != nil {
		if resp != nil {
			log.Println(u)
			log.Println(req.Header)
			log.Println(resp.Status)
			log.Println(resp.Header)
		}
		return nil, nil, err
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}

	if resp.StatusCode != http.StatusOK {
		log.Println(u)
		log.Println(req.Header)
		log.Println(resp.Status)
		log.Println(resp.Header)
		log.Println(string(respBody))
		return nil, respBody, fmt.Errorf(
			"%s %s",
			resp.Status,
			string(respBody),
		)
	}

	contentType := resp.Header.Get("Content-Type")
	if strings.Contains(contentType, "json") {
		var res interface{}
		err := json.Unmarshal(respBody, &res)
		if err != nil {
			return nil, nil, err
		}
		m, ok := res.(map[string]interface{})
		if !ok {
			return nil, respBody, fmt.Errorf("unknown format json")
		}
		accToken := &AccessToken{}
		if v, ok := m["oauth_token"]; ok {
			if s, ok := v.(string); ok {
				accToken.Token = s
			}
		}
		if v, ok := m["oauth_token_secret"]; ok {
			if s, ok := v.(string); ok {
				accToken.TokenSecret = s
			}
		}
		return accToken, m, nil
	}

	values, err := url.ParseQuery(string(respBody))
	if err != nil {
		return nil, respBody, err
	}

	return &AccessToken{
		Token:       values.Get("oauth_token"),
		TokenSecret: values.Get("oauth_token_secret"),
	}, values, nil
}

func (this *Consumer) Get(u string, accToken *AccessToken) ([]byte, http.Header, error) {
	if DebugModeOAuth {
		log.Println("Consumer.Get")
		defer log.Println("end of Consumer.Get")
	}

	req, err := http.NewRequest(
		http.MethodGet,
		u,
		nil,
	)
	if err != nil {
		return nil, nil, err
	}

	params := NewParamaterList(this.ConsumerKey)
	params.SetOAuthToken(accToken.Token)

	err = params.SetSignature(req.Method, u, this.ConsumerSecret, accToken.TokenSecret, nil)
	if err != nil {
		return nil, nil, err
	}

	authorization := params.HeaderValue()

	req.Header.Set("Authorization", authorization)

	resp, err := http.DefaultClient.Do(req)
	defer func() {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
	}()
	if err != nil {
		var h http.Header
		if resp != nil {
			log.Println(u)
			log.Println(req.Header)
			log.Println(resp.Status)
			log.Println(resp.Header)
			h = resp.Header
		}
		return nil, h, err
	}

	if resp.Body == nil {
		return []byte{}, resp.Header, nil
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, resp.Header, err
	}

	if resp.StatusCode != http.StatusOK {
		log.Println(u)
		log.Println(req.Header)
		log.Println(resp.Status)
		log.Println(resp.Header)
		log.Println(string(respBody))
		return respBody, resp.Header, fmt.Errorf(
			"%s %s",
			resp.Status,
			string(respBody),
		)
	}

	return respBody, resp.Header, nil
}

func (this *Consumer) Post(u string, accToken *AccessToken, contentType string, body []byte) ([]byte, http.Header, error) {
	if DebugModeOAuth {
		log.Println("Consumer.Post")
		defer log.Println("end of Consumer.Post")
	}

	req, err := http.NewRequest(
		http.MethodPost,
		u,
		bytes.NewReader(body),
	)
	if err != nil {
		return nil, nil, err
	}

	params := NewParamaterList(this.ConsumerKey)
	params.SetOAuthToken(accToken.Token)

	var form url.Values
	if contentType == "application/x-www-form-urlencoded" {
		form, err = url.ParseQuery(string(body))
		if err != nil {
			return nil, nil, err
		}
	}

	err = params.SetSignature(req.Method, u, this.ConsumerSecret, accToken.TokenSecret, form)
	if err != nil {
		return nil, nil, err
	}

	authorization := params.HeaderValue()

	req.Header.Set("Authorization", authorization)
	req.Header.Set("Content-Type", contentType)

	resp, err := http.DefaultClient.Do(req)
	defer func() {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
	}()
	if err != nil {
		var h http.Header
		if resp != nil {
			log.Println(u)
			log.Println(req.Header)
			log.Println(resp.Status)
			log.Println(resp.Header)
			h = resp.Header
		}
		return nil, h, err
	}

	if resp.Body == nil {
		return []byte{}, resp.Header, nil
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, resp.Header, err
	}

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		log.Println(u)
		log.Println(req.Header)
		log.Println(resp.Status)
		log.Println(resp.Header)
		log.Println(string(respBody))
		return respBody, resp.Header, fmt.Errorf(
			"%s %s",
			resp.Status,
			string(respBody),
		)
	}

	return respBody, resp.Header, nil
}

func (this *Consumer) Put(u string, accToken *AccessToken, contentType string, body []byte) ([]byte, http.Header, error) {
	if DebugModeOAuth {
		log.Println("Consumer.Put")
		defer log.Println("end of Consumer.Put")
	}

	req, err := http.NewRequest(
		http.MethodPut,
		u,
		bytes.NewReader(body),
	)
	if err != nil {
		return nil, nil, err
	}

	params := NewParamaterList(this.ConsumerKey)
	params.SetOAuthToken(accToken.Token)

	err = params.SetSignature(req.Method, u, this.ConsumerSecret, accToken.TokenSecret, nil)
	if err != nil {
		return nil, nil, err
	}

	authorization := params.HeaderValue()

	req.Header.Set("Authorization", authorization)
	req.Header.Set("Content-Type", contentType)

	resp, err := http.DefaultClient.Do(req)
	defer func() {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
	}()
	if err != nil {
		var h http.Header
		if resp != nil {
			log.Println(u)
			log.Println(req.Header)
			log.Println(resp.Status)
			log.Println(resp.Header)
			h = resp.Header
		}
		return nil, h, err
	}

	if resp.Body == nil {
		return []byte{}, resp.Header, nil
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, resp.Header, err
	}

	if resp.StatusCode != http.StatusOK &&
		resp.StatusCode != http.StatusCreated &&
		resp.StatusCode != http.StatusNoContent {
		log.Println(u)
		log.Println(req.Header)
		log.Println(resp.Status)
		log.Println(resp.Header)
		log.Println(string(respBody))
		return respBody, resp.Header, fmt.Errorf(
			"%s %s",
			resp.Status,
			string(respBody),
		)
	}

	return respBody, resp.Header, nil
}

func (this *Consumer) Delete(u string, accToken *AccessToken, contentType string, body []byte) ([]byte, http.Header, error) {
	if DebugModeOAuth {
		log.Println("Consumer.Delete")
		defer log.Println("end of Consumer.Delete")
	}

	req, err := http.NewRequest(
		http.MethodDelete,
		u,
		bytes.NewReader(body),
	)
	if err != nil {
		return nil, nil, err
	}

	params := NewParamaterList(this.ConsumerKey)
	params.SetOAuthToken(accToken.Token)

	err = params.SetSignature(req.Method, u, this.ConsumerSecret, accToken.TokenSecret, nil)
	if err != nil {
		return nil, nil, err
	}

	authorization := params.HeaderValue()

	req.Header.Set("Authorization", authorization)
	if contentType != "" {
		req.Header.Set("Content-Type", contentType)
	}

	resp, err := http.DefaultClient.Do(req)
	defer func() {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
	}()
	if err != nil {
		var h http.Header
		if resp != nil {
			log.Println(u)
			log.Println(req.Header)
			log.Println(resp.Status)
			log.Println(resp.Header)
			h = resp.Header
		}
		return nil, h, err
	}

	if resp.Body == nil {
		return []byte{}, resp.Header, nil
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, resp.Header, err
	}

	if resp.StatusCode != http.StatusOK &&
		resp.StatusCode != http.StatusNoContent &&
		resp.StatusCode != http.StatusAccepted {
		log.Println(u)
		log.Println(req.Header)
		log.Println(resp.Status)
		log.Println(resp.Header)
		log.Println(string(respBody))
		return respBody, resp.Header, fmt.Errorf(
			"%s %s",
			resp.Status,
			string(respBody),
		)
	}

	return respBody, resp.Header, nil
}
