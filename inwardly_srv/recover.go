package main

import (
	"log"
	"net/http"
	"strconv"
	"time"
)

func init() {
	// POST
	//  target  string (non empty && said ID)
	//  index   number (non empty && history index)
	http.HandleFunc("/api/recover", recoverHandleFunc)
}

func recoverHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeRecoverHandler {
		log.Println("recoverHandleFunc")
		defer log.Println("defer recoverHandleFunc")
	}

	now := time.Now().Local()
	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	target := req.PostFormValue("target")
	if target == "" {
		badRequest(w, "missing target parameter")
		return
	}

	indexStr := req.PostFormValue("index")
	if indexStr == "" {
		badRequest(w, "missing index parameter")
		return
	}
	index, err := strconv.Atoi(indexStr)
	if err != nil || index < 0 {
		badRequest(w, "invalid index parameter ("+indexStr+")")
		return
	}

	update := ToDateString(now)
	said, memo, err := dataManager.RecoverSaid(update, target, index)
	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	view := said.View()

	err = view.AttachQuotes()
	if err != nil {
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	response := SayResponse{
		SaidView: view,
		Memo:     memo,
	}

	writeJson(w, req, &response)
}
