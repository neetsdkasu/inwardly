package main

import (
	"fmt"
	"log"
	"strings"
	"time"
)

var _ = log.Println

const (
	TextElementTypeText        = "text"
	TextElementTypeUrl         = "url"
	TextElementTypeInsideQuote = "quote"
	TextElementTypeInsideUrl   = "inside-url"
	TextElementTypeInsideImg   = "inside-img"
)

type TextElement struct {
	Type string `json:"type"`
	Src  string `json:"src"`
}

type Memo struct {
	Id           string        `json:"id"`
	Date         string        `json:"date"`
	TextElements []TextElement `json:"elements"`
	ImageList    []string      `json:"image"`
	Thread       string        `json:"thread"`
}

type MemoList []*Memo

type SaidView struct {
	Id           string        `json:"id"`
	Date         string        `json:"date"`
	Update       string        `json:"update"`
	Deleted      bool          `json:"deleted"`
	Text         string        `json:"text"`
	TextElements []TextElement `json:"elements"`
	ImageList    []string      `json:"image"`
	Parent       string        `json:"parent"`
	Children     []string      `json:"children"`
	QuoteList    []*SaidView   `json:"quote"`
	Thread       string        `json:"thread"`
	QuotedRefs   int           `json:"refs"`
	Secret       bool          `json:"secret"`
}

type PageView struct {
	PageId   string      `json:"id"`
	Next     string      `json:"next"`
	Prev     string      `json:"prev"`
	Timeline []*SaidView `json:"timeline"`
}

type ThreadView struct {
	Id      string               `json:"id"`
	Said    []*SaidView          `json:"said"`
	Setting *ThreadUploadSetting `json:"setting"`
}

type PastSaid struct {
	Date      string   `json:"date"`
	Delete    bool     `json:"delete"`
	Text      string   `json:"text"`
	ImageList []string `json:"image"`
	Secret    bool     `json:"secret"`
}

type Said struct {
	Id           string        `json:"id"`
	Date         string        `json:"date"`
	Update       string        `json:"update"`
	Deleted      bool          `json:"deleted"`
	Text         string        `json:"text"`
	TextElements []TextElement `json:"elements"`
	ImageList    []string      `json:"image"`
	Parent       string        `json:"parent"`
	Children     []string      `json:"children"`
	History      []*PastSaid   `json:"history"`
	Root         string        `json:"root"`
	QuotedRefs   []string      `json:"refs"`
	Secret       bool          `json:"secret"`
}

type SaidList []*Said

type EditHistory struct {
	Said    *SaidView   `json:"said"`
	History []*PastSaid `json:"history"`
}

type QuotedView struct {
	Said *SaidView   `json:"said"`
	Refs []*SaidView `json:"refs"`
}

type DaySaidList struct {
	Update string   `json:"update"`
	Said   SaidList `json:"said"`
}

type Calender struct {
	Option struct {
		FirstDayId string `json:"first"`
		LastDayId  string `json:"last"`
	} `json:"option"`
	Year         int      `json:"year"`
	Month        int      `json:"month"`
	FirstWeekday int      `json:"week"`
	EndOfMonth   int      `json:"end"`
	DayIds       []string `json:"days"`
}

type Foto struct {
	File   string `json:"file"`
	FotoId string `json:"foto_id"`
	Url    string `json:"url"`
	Size   int    `json:"size"`
}

func NewCalender(y, m int) *Calender {
	d := time.Date(y, time.Month(m), 1, 0, 0, 0, 0, time.Local)
	return &Calender{
		Year:         y,
		Month:        m,
		FirstWeekday: int(d.Weekday()),
		EndOfMonth:   d.AddDate(0, 1, 0).AddDate(0, 0, -1).Day(),
	}
}

func (this *Said) Memo() *Memo {
	return &Memo{
		Id:           this.Id,
		Date:         this.Date,
		TextElements: this.TextElements,
		ImageList:    this.ImageList,
		Thread:       this.Root,
	}
}

func (this *Said) View() *SaidView {
	return &SaidView{
		Id:           this.Id,
		Date:         this.Date,
		Update:       this.Update,
		Deleted:      this.Deleted,
		Text:         this.Text,
		TextElements: this.TextElements,
		ImageList:    this.ImageList,
		Parent:       this.Parent,
		Children:     this.Children,
		Thread:       this.Root,
		QuotedRefs:   len(this.QuotedRefs),
		Secret:       this.Secret,
	}
}

func (this *SaidView) AttachQuotes() error {
	for _, te := range this.TextElements {
		if te.Type != TextElementTypeInsideQuote {
			continue
		}
		s := strings.SplitN(te.Src, "\n", 2)
		if len(s) != 2 {
			return fmt.Errorf("BUG? in AttachQuotes ( %+v )", te)
		}
		said, err := dataManager.GetSaidById(s[1])
		if err != nil {
			return err
		}
		this.QuoteList = append(
			this.QuoteList,
			said.View(),
		)
	}
	return nil
}

func (this *DaySaidList) Init() {
	this.Update = ""
	this.Said = SaidList{}
}

func (this *DaySaidList) GetCopy() *DaySaidList {
	said := make(SaidList, len(this.Said))
	copy(said, this.Said[:len(said)])
	return &DaySaidList{
		Update: this.Update,
		Said:   said,
	}
}

func (this *MemoList) Init() {
	//  do nothing
}
