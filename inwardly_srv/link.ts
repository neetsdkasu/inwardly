
const entryNameTitle = `max length: 12
allow character: lower-alphabet, digit, underscore, hyphen`;

function isValidDocumentLinkEntryName(entry: string) {
	return entry.match(/^[a-z0-9_\-]{1,12}$/) !== null;
}

function updateDocumentLinkDigMarkFiles(ds: DocumentLinkState, article: Element) {
	const qafiles = article.querySelector('.qafiles')!;
	const ul = qafiles.querySelector('ul')!;
	ul.innerHTML = '';
	if (ds.marked_files === null || ds.marked_files.length === 0) {
		qafiles.classList.add('toggleDisplayNone');
		return;
	}
	qafiles.classList.remove('toggleDisplayNone');
	const id = ds.id;
	const entryName = ds.entry;
	for (const mf of ds.marked_files) {
		const li = ul.appendChild(document.createElement('li'));
		const btn = li.appendChild(document.createElement('button'));
		btn.classList.add('softbutton');
		btn.textContent = 'RM';
		btn.type = 'button';
		btn.title = `remove mark link ${mf}`;
		btn.addEventListener('click', () => {
			const formData = new FormData();
			formData.set('id', `${id}`);
			formData.set('entry', entryName);
			formData.set('path', mf);
			fetch('/api/link/markfile/remove', {
				method: 'POST',
				body: formData,
			})
			.then(toJson)
			.then( (res: DocumentLink) => {
				if (res.state !== null) {
					updateDocumentLinkDigMarkFiles(res.state, article);
					showMessage(`remove mark link ${mf}`, false);
				}
			})
			.catch( err => {
				showError(err);
			});
		});
		const a = li.appendChild(document.createElement('a'));
		a.target = '_blank';
		a.rel = 'noopener';
		a.href = `/docs/${ds.entry}${mf}`;
		a.textContent = mf;
	}
}

function updateDocumentLinkDigMarkDir(ds: DocumentLinkState, article: Element) {
	const qadirs = article.querySelector('.qadirs')!;
	const ul = qadirs.querySelector('ul')!;
	ul.innerHTML = '';
	if (ds.marked_directories === null || ds.marked_directories.length === 0) {
		qadirs.classList.add('toggleDisplayNone');
		return;
	}
	qadirs.classList.remove('toggleDisplayNone');
	const entryName = ds.entry;
	const id = ds.id;
	for (const md of ds.marked_directories) {
		const li = ul.appendChild(document.createElement('li'));
		const btn = li.appendChild(document.createElement('button'));
		btn.classList.add('softbutton');
		btn.textContent = 'RM';
		btn.type = 'button';
		btn.title = `remove mark dir ${md}`;
		btn.addEventListener('click', () => {
			const formData = new FormData();
			formData.set('id', `${id}`);
			formData.set('entry', entryName);
			formData.set('path', md);
			fetch('/api/link/markdir/remove', {
				method: 'POST',
				body: formData,
			})
			.then(toJson)
			.then( (res: DocumentLink) => {
				if (res.state !== null) {
					updateDocumentLinkDigMarkDir(res.state, article);
					showMessage(`remove mark dir ${md}`, false);
				}
			})
			.catch( err => {
				showError(err);
			});
		});
		const dirbutton = li.appendChild(document.createElement('button'));
		dirbutton.classList.add('dirbutton');
		dirbutton.type = 'button';
		dirbutton.textContent = md;
		dirbutton.addEventListener('click', () => {
			fetch(`/api/link/cd/${entryName}${md}`)
			.then(toJson)
			.then( (res: DocumentLink) => {
				if (res.state !== null) {
					updateDocumentLinkDigCurPath(res.state, article);
					updateDocumentLinkDigDirs(res.state, article);
					updateDocumentLinkDigExtensions(res.state, article);
					updateDocumentLinkDigFiles(res.state, article);
				}
			})
			.catch( err => {
				showError(err);
			});
		});
	}
}

function updateDocumentLinkDigCurPath(ds: DocumentLinkState, article: Element) {
	const cpElm = article.querySelector(':scope > details > .dlcurpath .curpath')!;
	const favBtn = <HTMLButtonElement>article.querySelector(':scope > details > .dlcurpath .softbutton')!;
	cpElm.innerHTML = '';
	if (ds.current === null || ds.current.length === 0) {
		favBtn.disabled = true;
		const btn = cpElm.appendChild(document.createElement('button'));
		btn.classList.add('dirbutton');
		btn.type = 'button';
		btn.textContent = '/';
		btn.title = 'refresh';
		btn.addEventListener('click', () => {
			const entry = (<HTMLInputElement>article.querySelector('input[name="entry"]')!).value;
			fetch(`/api/link/cd/${entry}/`)
			.then(toJson)
			.then( (res: DocumentLink) => {
				if (res.state !== null) {
					updateDocumentLinkDigCurPath(res.state, article);
					updateDocumentLinkDigDirs(res.state, article);
					updateDocumentLinkDigFiles(res.state, article);
				}
			})
			.catch( err => {
				showError(err);
			})
		});
		return;
	}
	favBtn.disabled = false;
	let curPath = '';
	for (const p of ds.current) {
		const btn = cpElm.appendChild(document.createElement('button'));
		btn.classList.add('dirbutton');
		btn.type = 'button';
		btn.textContent = '/';
		cpElm.appendChild(document.createTextNode(` ${p} `));
		const path = curPath;
		curPath += p + '/';
		btn.title = `back to /${path}`;
		btn.addEventListener('click', () => {
			const entry = (<HTMLInputElement>article.querySelector('input[name="entry"]')!).value;
			fetch(`/api/link/cd/${entry}/${path}`)
			.then(toJson)
			.then( (res: DocumentLink) => {
				if (res.state !== null) {
					updateDocumentLinkDigCurPath(res.state, article);
					updateDocumentLinkDigDirs(res.state, article);
					updateDocumentLinkDigFiles(res.state, article);
				}
			})
			.catch( err => {
				showError(err);
			})
		});
	}
}

function updateDocumentLinkDigDirs(ds: DocumentLinkState, article: Element) {
	const dirElm = article.querySelector(':scope > details > .dldirs > .dirs')!;
	dirElm.innerHTML = '';
	if (ds.directories === null) {
		return;
	}
	const curPath = ds.current === null ? '' : ds.current.join('/');
	for (const dn of ds.directories) {
		const btn = dirElm.appendChild(document.createElement('button'));
		btn.classList.add('dirbutton');
		btn.type = 'button';
		btn.textContent = dn;
		dirElm.appendChild(document.createTextNode(', '));
		const path = `${curPath}/${dn}/`;
		btn.title = `move to ${path}`;
		btn.addEventListener('click', () => {
			const entry = (<HTMLInputElement>article.querySelector('input[name="entry"]')!).value;
			fetch(`/api/link/cd/${entry}/${path}`)
			.then(toJson)
			.then( (res: DocumentLink) => {
				if (res.state !== null) {
					updateDocumentLinkDigCurPath(res.state, article);
					updateDocumentLinkDigDirs(res.state, article);
					updateDocumentLinkDigFiles(res.state, article);
				}
			})
			.catch( err => {
				showError(err);
			})
		});
	}
}

function updateDocumentLinkDigExtensions(ds: DocumentLinkState, article: Element) {
	const id = ds.id;
	const extsElm = article.querySelector(':scope > details > .dlfiles .exts')!;
	extsElm.innerHTML = '';
	extsElm.appendChild(document.createElement('span'))
		.textContent = 'file extension: ';
	if (ds.exts !== null) {
		for (const ex of ds.exts) {
			const extBtn = extsElm.appendChild(document.createElement('button'));
			extBtn.classList.add('extbutton');
			extBtn.type = 'button';
			extBtn.textContent = ex;
			extBtn.title = `remove extension ${ex}`;
			extBtn.addEventListener('click', () => {
				const entry = (<HTMLInputElement>article.querySelector('input[name="entry"]')!).value;
				const formData = new FormData();
				formData.set('id', `${id}`);
				formData.set('entry', entry);
				formData.set('extension', ex);
				fetch('/api/link/ext/remove', {
					method: 'POST',
					body: formData,
				})
				.then(toJson)
				.then( (res: DocumentLink) => {
					if (res.state !== null) {
						updateDocumentLinkDigExtensions(res.state, article);
						updateDocumentLinkDigFiles(res.state, article);
						showMessage(`success to remove extension "${ex}"`, false);
					}
				})
				.catch( err => {
					showError(err);
				});
			});
			extsElm.appendChild(document.createTextNode(', '));
		}
	}
	const extInp = extsElm.appendChild(document.createElement('input'));
	extInp.type = 'textbox';
	extInp.placeholder = 'html, pdf, txt ...(etc.)';
	extInp.title = `html, pdf, txt ...(etc.)
asterisk('*') match any file types
dot('.') match no extension file
`;
	const extInpBtn = extsElm.appendChild(document.createElement('button'));
	extInpBtn.classList.add('softbutton');
	extInpBtn.type = 'button';
	extInpBtn.textContent = 'add';
	extInpBtn.title = 'add extensions';
	extInpBtn.addEventListener('click', () => {
		const entry = (<HTMLInputElement>article.querySelector('input[name="entry"]')!).value;
		const exts = extInp.value.trim();
		if (exts === '') {
			showError('extension is empty!');
			return;
		}
		const formData = new FormData();
		formData.set('id', `${id}`)
		formData.set('entry', entry);
		formData.set('extensions', exts);
		fetch('/api/link/ext/add', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then( (res: DocumentLink) => {
			if (res.state !== null) {
				updateDocumentLinkDigExtensions(res.state, article);
				updateDocumentLinkDigFiles(res.state, article);
				showMessage(`success to add extensions "${exts}"`, false);
			}
		})
		.catch( err => {
			showError(err);
		});
	});
}

function updateDocumentLinkDigFiles(ds: DocumentLinkState, article: Element) {
	const filesElm = article.querySelector(':scope > details > .dlfiles .files')!;
	filesElm.innerHTML = '';
	if (ds.files === null) {
		return;
	}
	let curpath = `/docs/${ds.entry}`;
	if (ds.current !== null) {
		curpath += '/' + ds.current.join('/');
	}
	for (const fn of ds.files) {
		const span = filesElm.appendChild(document.createElement('span'));
		const btn = span.appendChild(document.createElement('button'));
		btn.classList.add('softbutton');
		btn.type = 'button';
		btn.textContent = 'M';
		btn.title = `mark link of ${fn}`;
		btn.addEventListener('click', () => {
			const entry = <HTMLInputElement>article.querySelector('input[name="entry"]')!;
			const formData = new FormData();
			formData.set('id', `${ds.id}`);
			formData.set('entry', entry.value);
			formData.set('filename', fn);
			fetch('/api/link/markfile/add', {
				method: 'POST',
				body: formData,
			})
			.then(toJson)
			.then( (res: DocumentLink) => {
				if (res.state !== null) {
					updateDocumentLinkDigMarkFiles(res.state, article);
					showMessage('marked!', false);
				}
			})
			.catch( err => {
				showError(err);
			});
		});
		const a = span.appendChild(document.createElement('a'));
		a.textContent = fn;
		a.href = `${curpath}/${fn}`;
		a.target = '_blank';
		a.rel = 'noopener';
		filesElm.appendChild(document.createTextNode(', '));
	}
}

function updateDocumentLinkDig(dl: DocumentLink, article: Element) {
	const dig = article.querySelector(':scope > details > .dig')!;
	if (dl.state === null) {
		dig.classList.remove('toggleDisplayNone');
		return;
	}
	dig.classList.add('toggleDisplayNone');
	updateDocumentLinkDigCurPath(dl.state, article);
	updateDocumentLinkDigDirs(dl.state, article);
	updateDocumentLinkDigExtensions(dl.state, article);
	updateDocumentLinkDigFiles(dl.state, article);
	updateDocumentLinkDigMarkDir(dl.state, article);
	updateDocumentLinkDigMarkFiles(dl.state, article);
}

function updateDocumentLink(dl: DocumentLink, article: Element) {
	if (dl.setting === null) {
		return;
	}
	const header = <HTMLInputElement>article.querySelector('h5')!;
	const linkentry = <HTMLInputElement>article.querySelector('input[name="linkentry"]')!;
	const entry = <HTMLInputElement>article.querySelector('input[name="entry"]')!;
	const path = <HTMLInputElement>article.querySelector('input[name="path"]')!;
	const baselink = <HTMLAnchorElement>article.querySelector('.baselink > a')!;
	header.textContent = dl.setting.entry;
	linkentry.value = dl.setting.entry;
	entry.value = dl.setting.entry;
	path.value = dl.setting.rootpath + dl.setting.basefile;
	baselink.href = `/docs/${dl.setting.entry}/${dl.setting.basefile}`;
	baselink.textContent = `/docs/${dl.setting.entry}/${dl.setting.basefile}`;
	updateDocumentLinkDig(dl, article);
}

function updateButton(id: number) {
	return () => {
		const article = document.getElementById(`doclink${id}`)!;
		const oldEntry = (<HTMLInputElement>article.querySelector('input[name="linkentry"]')!).value;
		const entry = (<HTMLInputElement>article.querySelector('input[name="entry"]')!).value.trim();
		const path = (<HTMLInputElement>article.querySelector('input[name="path"]')!).value.trim();
		if (entry === '' && path !== '') {
			showError('require Entry Name!');
			return;
		}
		if (entry !== '' && !isValidDocumentLinkEntryName(entry)) {
			showError('invalid Entry Name!');
			return;
		}
		if (path === '' && entry !== '') {
			showError('require Document Path!');
			return;
		}
		const formData = new FormData();
		formData.set('id', `${id}`);
		formData.set('old_entry', oldEntry);
		formData.set('entry', entry);
		formData.set('path', path);
		fetch('/api/link/update', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then( (res: DocumentLink) => {
			const ul = document.querySelector('#quicklinks > ol')!;
			const ql = ul.querySelector(`:scope > li.quicklink${id}`);
			if (res.setting === null) {
				article.parentNode!.removeChild(article);
				if (ql !== null) {
					ul.removeChild(ql);
					if (ul.childElementCount === 0) {
						document.getElementById('quicklinks')!
							.classList.add('toggleDisplayNone');
					}
				}
				showMessage(`deleted the entry "${oldEntry}"!`, false);
				return;
			}
			updateDocumentLink(res, article);
			if (ql !== null){
				updateQuickLink(res, ql);
			}
			showMessage(`updated the entry "${entry}"!`, false);
		})
		.catch( err => {
			showError(err);
		});
	};
}

function updateQuickLink(dl: DocumentLink, li: Element) {
	if (dl.setting === null) {
		return;
	}
	(<HTMLLIElement>li).value = dl.setting.ql_order;
	const index = <HTMLAnchorElement>li.querySelector('a:nth-of-type(1)')!;
	index.href = `#doclink${dl.setting.id}`;
	index.textContent = dl.setting.entry;
	index.title = `jump to detail of ${dl.setting.entry}`;
	const jump = <HTMLAnchorElement>li.querySelector('a:nth-of-type(2)')!;
	jump.href = `/docs/${dl.setting.entry}/${dl.setting.basefile}`;
	jump.textContent = `/docs/${dl.setting.entry}/${dl.setting.basefile}`;
	jump.title = `open document ${dl.setting.entry}`;
}

function makeDocumentQuickLink(dl: DocumentLink) {
	if (dl.setting === null) {
		return;
	}
	const template = <HTMLTemplateElement>document.getElementById('quicklink-template')!;
	const fragment = document.importNode(template.content, true);
	const li = fragment.querySelector('li')!;
	li.classList.add(`quicklink${dl.setting.id}`);
	updateQuickLink(dl, li);
	const ul = document.querySelector('#quicklinks > ol')!;
	let nextElem: Element | null = null;
	const ord = dl.setting.ql_order;
	ul.querySelectorAll(':scope > li')
	.forEach( e => {
		if (nextElem === null) {
			const v = (<HTMLLIElement>e).value;
			if (v <= ord) {
				nextElem = e;
			}
		}
	});
	ul.insertBefore(fragment, nextElem);
}

function updateOrderButton(id: number) {
	return () => {
		const article = document.getElementById(`doclink${id}`)!;
		const entry = (<HTMLInputElement>article.querySelector('input[name="linkentry"]')!).value;
		const order = (<HTMLInputElement>article.querySelector('.qlorder input[name="order"]')!).value;
		const formData = new FormData();
		formData.set('id', `${id}`);
		formData.set('entry', entry);
		formData.set('order', order);
		fetch('/api/link/qlorder/set', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then( (res: DocumentLink) => {
			if (res.setting !== null) {
				const ol = document.querySelector('#quicklinks > ol')!;
				const li = <HTMLLIElement>ol.querySelector(`:scope > li.quicklink${res.setting.id}`)!;
				updateQuickLink(res, li);
				ol.removeChild(li);
				let nextElem: Element | null = null;
				const ord = res.setting.ql_order;
				ol.querySelectorAll(':scope > li')
				.forEach( e => {
					if (nextElem === null) {
						const v = (<HTMLLIElement>e).value;
						if (v <= ord) {
							nextElem = e;
						}
					}
				});
				ol.insertBefore(li, nextElem);
				showMessage(`set new order ${ord} to ${res.setting.entry}`, false);
			}
		})
		.catch(err => {
			showError(err);
		});
	};
}

function makeDocumentLink(dl: DocumentLink) {
	if (dl.setting === null) {
		return;
	}
	const id = dl.setting.id;
	const template = <HTMLTemplateElement>document.getElementById('doclink-template')!;
	const fragment = document.importNode(template.content, true);
	const article = fragment.querySelector('article')!;
	const ql_order_form = <HTMLFormElement>article.querySelector(':scope > .qlorder > form')!;
	const ql_order = <HTMLInputElement>ql_order_form.querySelector('input[type="number"]')!;
	const entry = <HTMLInputElement>article.querySelector('input[name="entry"]')!;
	const update = <HTMLFormElement>article.querySelector(':scope > form')!;
	const dirfav = <HTMLButtonElement>article.querySelector('.dlcurpath .softbutton')!;
	article.id = `doclink${id}`;
	entry.title = entryNameTitle;
	ql_order.value = `${dl.setting.ql_order}`;
	const updateOrderFunc = updateOrderButton(id);
	ql_order_form.addEventListener('submit', e => {
		e.preventDefault();
		updateOrderFunc();
	});
	const updateFunc = updateButton(id);
	update.addEventListener('submit', e => {
		e.preventDefault();
		updateFunc();
	});
	dirfav.title = 'mark current directory';
	dirfav.addEventListener('click', () => {
		const formData = new FormData();
		formData.set('id', `${id}`);
		formData.set('entry', entry.value);
		fetch('/api/link/markdir/add', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then( (res: DocumentLink) => {
			if (res.state !== null) {
				updateDocumentLinkDigMarkDir(res.state, article);
				showMessage('marked!', false);
			}
		})
		.catch( err => {
			showError(err);
		});
	});
	const btn = <HTMLButtonElement>article.querySelector(':scope > details > .dig > button')!;
	btn.addEventListener('click', () => {
		const en = entry.value;
		fetch(`/api/link/dig/${en}`)
		.then(toJson)
		.then( (res: DocumentLink) => {
			updateDocumentLinkDig(res, article);
		})
		.catch( err => {
			showError(err);
		});
	});
	updateDocumentLink(dl, article);
	const hr = document.querySelector('#addlink + hr')!;
	hr.parentNode!.insertBefore(fragment, hr.nextSibling);
}

function addDocumentLink() {
	const entry = <HTMLInputElement>document.getElementById('entry')!;
	const path = <HTMLInputElement>document.getElementById('path')!;
	if (entry.value === '') {
		showError('require Entry Name!');
		return;
	}
	if (!isValidDocumentLinkEntryName(entry.value)) {
		showError('invalid Entry Name!');
		return;
	}
	if (path.value === '') {
		showError('require Document Path!');
		return;
	}
	const formData = new FormData();
	formData.set('entry', entry.value);
	formData.set('path', path.value);
	fetch('/api/link/add', {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then( (res: DocumentLink) => {
		document.getElementById('quicklinks')!
			.classList.remove('toggleDisplayNone');
		makeDocumentLink(res);
		makeDocumentQuickLink(res);
		showMessage(`success to add new entry ${res.setting!.entry}`, false);
		entry.value = '';
		path.value = '';
	})
	.catch( err => {
		showError(err);
	});
}

function loadDocumentLink() {
	fetch('/api/link/list')
	.then(toJson)
	.then( (res: DocumentLink[]) => {
		if (res.length > 0) {
			document.getElementById('quicklinks')!
				.classList.remove('toggleDisplayNone');
		}
		for (const dl of res) {
			makeDocumentLink(dl);
			makeDocumentQuickLink(dl);
		}
	})
	.catch( err => {
		showError(err);
	});
}


////////////////
//
// 初期化処理
//
////////////////

document.querySelector('header > nav button.setting')!
.addEventListener('click', () => {
	window.location.assign('/setting');
});

document.querySelector('header > nav button.home')!
.addEventListener('click', () => {
	window.location.assign('/');
});

document.querySelector('header > nav button.misc')!
.addEventListener('click', () => {
	window.location.assign('/misc');
});

(<HTMLInputElement>document.getElementById('entry')!)
.placeholder = 'java';

(<HTMLInputElement>document.getElementById('entry')!)
.title = entryNameTitle;

(<HTMLInputElement>document.getElementById('path')!)
.placeholder = 'C:\\Program Files\\Java\\jdk\\docs\\index.html';

document.querySelector('#addlink > form')!
.addEventListener('submit', e => {
	e.preventDefault();
	addDocumentLink();
});


window.addEventListener('load', () => {
	loadDocumentLink();
});
