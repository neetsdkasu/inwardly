package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
)

type ThreadPostTemplate struct {
	once sync.Once
	*template.Template
}

var threadPostTemplate ThreadPostTemplate

func routineUploadThread() error {
	if DebugModeRoutine {
		log.Println("routineUploadThread")
		defer log.Println("defer routineUploadThread")
	}

	today := time.Now().Local()
	todayDateTimeValue := ToDateString(today)

	setting := appSetting.GetStoreCopy()
	if !setting.CanUploadThread() {
		return nil
	}

	uploadList := appSetting.GetThreadUploadSettingList()

	for _, tus := range uploadList {

		if !tus.AutoUpload {
			continue
		}

		if strings.Compare(tus.Update, tus.UploadDate) <= 0 {
			continue
		}

		_, err := uploadThread(&setting, tus, todayDateTimeValue)
		if err != nil {
			if _, ok := err.(BadRequestError); !ok {
				return err
			}
		}
	}

	return nil
}

func uploadThread(setting *AppSettingStore, tus *ThreadUploadSetting, todayDateTimeValue string) (*ThreadUploadSetting, error) {

	thread, err := dataManager.GetThread(tus.Id)
	if err != nil {
		return nil, err
	}

	post, err := makeThreadPost(setting, tus, thread)
	if err != nil {
		return nil, err
	}
	head := post[0]
	replies := append(head.Replies, post[1:]...)
	head.Replies = nil

	threadPostTemplate.Init()
	var b bytes.Buffer
	err = threadPostTemplate.ExecuteTemplate(&b, "Z", head)
	if err != nil {
		return nil, err
	}
	_, err = b.WriteString("<p><!-- more --></p>")
	if err != nil {
		return nil, err
	}
	err = threadPostTemplate.ExecuteTemplate(&b, "L", replies)
	if err != nil {
		return nil, err
	}

	title := tus.Title

	t, err := ToTimeFromSaidId(tus.Id)
	if err != nil {
		return nil, err
	}
	date := ToDateString(t)

	threadBody := b.String()

	categories := strings.Split(tus.Categories, ",")
	if DebugMode {
		foundTesting := false
		for _, c := range categories {
			if c == "testing" {
				foundTesting = true
				break
			}
		}
		if !foundTesting {
			categories = append(categories, "testing")
		}
	}

	if DebugModeUploadThread {
		// test
		f, err := os.Create("post_thread_test_" + tus.Id + ".html")
		if err != nil {
			return nil, err
		}
		defer f.Close()
		_, err = f.WriteString(`<!DOCTYPE html><html lang="ja"><head><meta charset="utf-8" /><title>` + title + " (" + date + ")" +
			`</title><style>blockquote { padding-left: 1.5em; border-left: 3px solid blue; background-color: rgba(220,220,220,0.5); }</style></head><body>` +
			"\n" + threadBody + "\n</body></html>")
		if err != nil {
			return nil, err
		}
	}

	if DebugMode {
		log.Println("Upload Thread:", date, tus.Title)
	}

	res, err := setting.UploadBlog(
		tus.BlogPostId,
		title,
		date,
		threadBody,
		categories,
	)
	if err != nil {
		return nil, err
	}
	sleepAfterUpload()

	blogPostId := tus.BlogPostId
	blogUrl := tus.Url
	{
		ts := strings.Split(res.Id, "-")
		id := ts[len(ts)-1]
		if id != "" && blogPostId != id {
			blogPostId = id
		}
	}
	for _, lk := range res.Links {
		if lk.Rel == "edit" {
			ts := strings.Split(lk.Href, "/")
			id := ts[len(ts)-1]
			if id != "" && blogPostId != id {
				blogPostId = id
			}
		} else if lk.Rel == "alternate" {
			u := lk.Href
			if u != "" && blogUrl != u {
				blogUrl = u
			}
		}
	}

	return appSetting.SetThreadUploadSettingUploded(
		tus.Id,
		tus.Update,
		blogPostId,
		blogUrl,
		todayDateTimeValue,
	)
}

func makeThreadPost(setting *AppSettingStore, tus *ThreadUploadSetting, list []*Said) ([]*BlogSaid, error) {
	if DebugModeRoutine {
		log.Println("makeThreadPost")
		defer log.Println("defer makeThreadPost")
	}

	if !tus.SecretUpload && len(list) > 0 && list[0].Secret {
		return nil, BadRequestError{
			"thread root is secret!",
		}
	}

	post := []*BlogSaid{}
	m := map[string]*BlogSaid{}
	imgs := map[string]bool{}
	regimg := func(s *Said) {
		for _, t := range s.TextElements {
			if t.Type == TextElementTypeInsideImg {
				imgs[t.Src] = true
			}
		}
		for _, f := range s.ImageList {
			imgs[f] = true
		}
	}

	secretCount := 0

	for _, s := range list {
		if !tus.SecretUpload && s.Secret {
			bs := new(BlogSaid).InitForThread(s, true, true)
			m[s.Id] = bs
			secretCount++
			continue
		}

		regimg(s)
		bs := new(BlogSaid).InitForThread(s, true, true)
		m[bs.Id] = bs

		// quote
		for _, t := range s.TextElements {
			if t.Type == TextElementTypeInsideQuote {
				id := strings.Fields(t.Src)[1]
				if q, ok := m[id]; ok {
					if tus.SecretUpload || !q.Secret {
						bs.Quoted = append(bs.Quoted, q.GetCopyForQuoted())
					}
				} else {
					qs, err := dataManager.GetSaidById(id)
					if err != nil {
						return nil, err
					}
					if tus.SecretUpload || !qs.Secret {
						regimg(qs)
						q = new(BlogSaid).InitForThread(qs, false, false)
						bs.Quoted = append(bs.Quoted, q)
					}
				}
			}
		}

		// parent
		if s.Parent == "" {
			post = append(post, bs)
		} else {
			if p, ok := m[s.Parent]; ok {
				if !tus.SecretUpload && p.Secret {
					post = append(post, bs)
				} else {
					p.Replies = append(p.Replies, bs)
				}
			} else {
				return nil, fmt.Errorf("[BUG] makeThreadPost: not found parent %s", s.Parent)
			}
		}
	}

	if len(post) == 0 {
		return nil, fmt.Errorf("[BUG] makeThreadPost: invalid post len %d", len(post))
	}

	// images
	for img := range imgs {
		f, err := dataManager.GetFoto(img)
		if err != nil {
			return nil, err
		}
		if f == nil {
			f, err = setting.UploadFoto(img)
			if err != nil {
				return nil, err
			}
			sleepAfterUpload()
			err = dataManager.SetFoto(f)
			if err != nil {
				return nil, err
			}
		}
	}

	return post, nil
}

func (this *ThreadPostTemplate) Init() {
	this.once.Do(func() {
		if DebugModeRoutine {
			log.Println("ThreadPostTemplate.Init")
			defer log.Println("defer ThreadPostTemplate.Init")
		}
		r := regexp.MustCompile(`([}>])\s+([<{])`)
		rps := func(s string) string {
			return r.ReplaceAllString(s, "$1$2")
		}
		t := template.New("B").Funcs(template.FuncMap{
			"first": func(s string) string { return strings.Fields(s)[0] },
			"last":  func(s string) string { x := strings.Fields(s); return x[len(x)-1] },
			"getimginfo": func(s string) []string {
				if f, _ := dataManager.GetFoto(s); f == nil {
					return []string{s, s}
				} else {
					return []string{f.Url, f.FotoId}
				}
			},
			"trim":  func(s string) string { return strings.TrimSuffix(s, "\n") },
			"haslf": func(s string) bool { return strings.HasSuffix(s, "\n") },
		})
		t = template.Must(t.Parse(rps(BlogPostSaidFormat)))
		t = template.Must(t.Parse(rps(BlogPostDate)))
		t = template.Must(t.Parse(rps(BlogPostText)))
		t = template.Must(t.Parse(rps(BlogPostLink)))
		t = template.Must(t.Parse(rps(BlogPostDeleted)))
		t = template.Must(t.Parse(rps(BlogPostImages)))
		t = template.Must(t.Parse(rps(BlogPostQuoted)))
		t = template.Must(t.Parse(rps(ThreadPostSaidList)))
		t = template.Must(t.Parse(rps(ThreadPostToplevel)))
		t = template.Must(t.Parse(rps(BlogPostOmitted)))
		t = template.Must(t.Parse(rps(BlogPostQuotedTweet)))
		this.Template = t
	})
}

const ThreadPostSaidList = `{{define "L"}}
    {{if .}}
        <ul style="list-style: none; padding-left: 1.2em; border-left: 1px solid rgba(127,127,127,0.3);">
        {{range .}}
            {{if .Id}}
                <li id="inwardlythread{{.Id}}">
            {{else}}
                <li>
            {{end}}
            {{template "T" .}}
            </li>
        {{end}}
        </ul>
    {{end}}
{{end}}`

const ThreadPostToplevel = `{{define "Z"}}
    {{if .}}
        <div id="inwardlythread{{.Id}}">
            {{template "T" .}}
        </div>
    {{else}}
        <p>no said</p>
    {{end}}
{{end}}`
