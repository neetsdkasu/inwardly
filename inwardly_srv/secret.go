package main

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"hash/crc32"
	"io"
	"log"
	"mime"
	"mime/multipart"
	"net/http"
	"path"
	"reflect"
	"strconv"
	"strings"
	"time"

	mt "bitbucket.org/neetsdkasu/mersenne_twister_go"
	unko "bitbucket.org/neetsdkasu/unkocrypto_go/v2"
)

type SecretImageSizeInfo struct {
	Width  int   `json:"width"`
	Height int   `json:"height"`
	Length int64 `json:"length"`
}

type SecretImageFileInfo struct {
	Name  string               `json:"name"`
	Ext   string               `json:"ext"`
	Mime  string               `json:"mime"`
	Size  *SecretImageSizeInfo `json:"size"`
	Thumb *SecretImageSizeInfo `json:"thumb"`
}

type SecretInfoStore struct {
	Id        string                 `json:"id"`
	Password1 string                 `json:"password1"`
	Password2 string                 `json:"password2"`
	Files     []*SecretImageFileInfo `json:"files"`
}

type SecretInfo struct {
	store   *SecretInfoStore
	keyword string
	expire  time.Time
	seed    []uint32
}

type MtRng struct {
	*mt.MersenneTwister
}

func (this *MtRng) NextInt() int32 {
	return int32(this.Uint32())
}

func (this *SecretInfo) Keyword() string {
	return this.keyword
}

func (this *SecretInfo) Id() string {
	return this.store.Id
}

func (this *SecretInfoStore) Clone() *SecretInfoStore {
	return &SecretInfoStore{
		Id:        this.Id,
		Password1: this.Password1,
		Password2: this.Password2,
		Files:     this.Files[:],
	}
}

var ErrSecretInfoWrongPassword = errors.New("Wrong Password")
var ErrNotFoundSecretInfoId = errors.New("Not Found ID")
var ErrBrokenSecretInfoPasswordData = errors.New("Broken Password data")
var ErrSecretInfoWrongKeyword = errors.New("Wrong Keyword")
var ErrSecretInfoExpired = errors.New("Expired")
var ErrSecretInfoWrongState = errors.New("Wrong State")
var ErrNotFoundSecretImage = errors.New("Not Found Secret Image")

const secretImageEncryptBlockSize = 1024

type SecretImageCryptor struct {
	seed []uint32
}

func (this *SecretImageCryptor) Encrypt(image []byte) []byte {
	rng := &MtRng{mt.NewMersenneTwister().InitByArray(this.seed)}
	src := bytes.NewReader(image)
	var dst bytes.Buffer
	unko.Encrypt(secretImageEncryptBlockSize, crc32.NewIEEE(), rng, src, &dst)
	return dst.Bytes()
}

func (this *SecretImageCryptor) Decrypt(blob []byte) ([]byte, error) {
	rng := &MtRng{mt.NewMersenneTwister().InitByArray(this.seed)}
	src := bytes.NewReader(blob)
	var dst bytes.Buffer
	_, err := unko.Decrypt(secretImageEncryptBlockSize, crc32.NewIEEE(), rng, src, &dst)
	if err != nil {
		return nil, err
	}
	return dst.Bytes(), nil
}

func (this *SecretInfo) GetCryptor() (cryptor *SecretImageCryptor, err error) {
	if this.seed == nil {
		err = ErrSecretInfoWrongState
		return
	}
	cryptor = &SecretImageCryptor{this.seed}
	return
}

func (this *SecretInfo) AuthByKeyword(keyword string) error {
	if this.keyword != keyword {
		return ErrSecretInfoWrongKeyword
	}
	now := time.Now()
	if now.After(this.expire) {
		return ErrSecretInfoExpired
	}
	this.expire = now.Add(time.Hour)
	return nil
}

func (this *SecretInfo) AuthByPassword(password string) error {
	runePassword := []uint32{}
	for _, r := range []rune(password) {
		runePassword = append(runePassword, uint32(r))
	}
	seed, err := decodeSecretPasswordString(runePassword, this.store.Password1)
	if err != nil {
		return err
	}
	recovery, err := decodeSecretPasswordString(seed, this.store.Password2)
	if err != nil || !reflect.DeepEqual(runePassword, recovery) {
		return ErrBrokenSecretInfoPasswordData
	}
	now := time.Now()
	rng := mt.NewMersenneTwister().Init(uint32(now.Unix()))
	this.keyword = strconv.FormatUint(uint64(rng.Uint32()), 10) +
		strconv.FormatUint(uint64(rng.Uint32()), 10)
	this.expire = now.Add(time.Hour)
	this.seed = seed
	return nil
}

func (this *SecretInfo) ClearAuth() {
	now := time.Now()
	rng := mt.NewMersenneTwister().Init(uint32(now.Unix()))
	this.keyword = strconv.FormatUint(uint64(rng.Uint32()), 10) +
		strconv.FormatUint(uint64(rng.Uint32()), 10)
	this.expire = now.Add(-1)
}

func (this *SecretInfo) ChangerPassword(oldPassword, newPassword string) (*SecretInfo, error) {
	runeOldPassword := []uint32{}
	for _, r := range []rune(oldPassword) {
		runeOldPassword = append(runeOldPassword, uint32(r))
	}
	seed, err := decodeSecretPasswordString(runeOldPassword, this.store.Password1)
	if err != nil {
		return nil, err
	}
	recovery, err := decodeSecretPasswordString(seed, this.store.Password2)
	if err != nil || !reflect.DeepEqual(runeOldPassword, recovery) {
		return nil, ErrBrokenSecretInfoPasswordData
	}

	runeNewPassword := []uint32{}
	for _, r := range []rune(newPassword) {
		runeNewPassword = append(runeNewPassword, uint32(r))
	}

	password1 := encodeSecretPasswordString(runeNewPassword, seed)

	password2 := encodeSecretPasswordString(seed, runeNewPassword)

	newInfo := &SecretInfo{
		store: &SecretInfoStore{
			Id:        this.store.Id,
			Password1: password1,
			Password2: password2,
			Files:     this.store.Files[:],
		},
		keyword: this.keyword,
		expire:  this.expire,
		seed:    this.seed,
	}

	return newInfo, nil
}

func createNewSecretSeed() []uint32 {
	rng := mt.NewMersenneTwister().Init(uint32(time.Now().Unix()))
	seed := make([]uint32, 16)
	for i := range seed {
		seed[i] = rng.Uint32()
	}
	return seed
}

func encodeSecretPasswordString(password, data []uint32) string {
	rng := &MtRng{mt.NewMersenneTwister().InitByArray(password)}
	var buf bytes.Buffer
	for _, b := range data {
		binary.Write(&buf, binary.LittleEndian, b)
	}
	src := bytes.NewReader(buf.Bytes())
	var dst bytes.Buffer
	unko.Encrypt(unko.MinBlockSize, crc32.NewIEEE(), rng, src, &dst)
	return base64.StdEncoding.EncodeToString(dst.Bytes())
}

func decodeSecretPasswordString(password []uint32, dataString string) ([]uint32, error) {
	rng := &MtRng{mt.NewMersenneTwister().InitByArray(password)}
	data, err := base64.StdEncoding.DecodeString(dataString)
	if err != nil {
		return nil, ErrBrokenSecretInfoPasswordData
	}
	src := bytes.NewReader(data)
	var dst bytes.Buffer
	_, err = unko.Decrypt(unko.MinBlockSize, crc32.NewIEEE(), rng, src, &dst)
	if err != nil {
		return nil, ErrSecretInfoWrongPassword
	}
	reader := bytes.NewReader(dst.Bytes())
	result := []uint32{}
	for {
		var b uint32
		err = binary.Read(reader, binary.LittleEndian, &b)
		if err == nil {
			result = append(result, b)
		} else if errors.Is(err, io.EOF) {
			return result, nil
		} else {
			return nil, ErrBrokenSecretInfoPasswordData
		}
	}
}

func NewSecretInfo(id, password string) *SecretInfo {
	runePassword := []uint32{}
	for _, r := range []rune(password) {
		runePassword = append(runePassword, uint32(r))
	}

	seed := createNewSecretSeed()

	password1 := encodeSecretPasswordString(runePassword, seed)

	password2 := encodeSecretPasswordString(seed, runePassword)

	info := &SecretInfo{
		store: &SecretInfoStore{
			Id:        id,
			Password1: password1,
			Password2: password2,
			Files:     []*SecretImageFileInfo{},
		},
	}

	return info
}

func init() {
	// secret APIs
	// /api/secret/{SecretID}/open
	// /api/secret/{SecretID}/close
	// /api/secret/{SecretID}/password
	// /api/secret/{SecretID}/list
	// /api/secret/{SecretID}/add
	// /api/secret/{SecretID}/delete
	// /api/secret/{SecretID}/thumb/{ImageID}
	// /api/secret/{SecretID}/image/{ImageID}
	//
	//  open
	//  method: POST
	//  param: password string (non empty)
	//  response: JSON
	// 	(Response: Set-Cookie: SECRET-KEYWORD-{SecretID}={keyword}; Path=/api/secret/{SecretID}/; HttpOnly)
	//
	//  close
	//  method: POST
	//  response: JSON
	//  (Request: Cookie: SECRET-KEYWORD-{SecretID})
	// 	(Response: Set-Cookie: SECRET-KEYWORD-{SecretID}; Max-Age=-1; Path=/api/secret/{SecretID}/; HttpOnly)
	//
	//  password
	//  method: POST
	//  param: old_password string (non empty)
	//  param: new_password string (non empty)
	//  response: JSON
	//
	//  list
	//  method: GET
	//  response: JSON
	//  (Request: Cookie: SECRET-KEYWORD-{SecretID})
	//
	//  add
	//  method: POST
	//  param: file file (non emtpy)
	//  response: JSON
	//  (Request: Cookie: SECRET-KEYWORD-{SecretID})
	//
	//  delete
	//  method: POST
	//  param: image string (non empty && ImageID)
	//  response: JSON
	//  (Request: Cookie: SECRET-KEYWORD-{SecretID})
	//
	//  thumb
	//  method: GET
	//  response: Binary (Image)
	//  (Request: Cookie: SECRET-KEYWORD-{SecretID})
	//
	//  image
	//  method: GET
	//  response: Binary (Image)
	//  (Request: Cookie: SECRET-KEYWORD-{SecretID})
	//
	//  common handle
	http.Handle("/api/secret/",
		http.StripPrefix("/api/secret/",
			http.HandlerFunc(secretCommonHandleFunc)))
}

func makeSecretInfoCookieName(secretId string) string {
	return "SECRET-KEYWORD-" + secretId
}

func makeSecretInfoCookiePath(secretId string) string {
	return "/api/secret/" + secretId + "/"
}

func setSecretCookie(w http.ResponseWriter, secretId, keyword string) {
	cookie := &http.Cookie{
		Name:     makeSecretInfoCookieName(secretId),
		Value:    keyword,
		Path:     makeSecretInfoCookiePath(secretId),
		HttpOnly: true,
	}
	http.SetCookie(w, cookie)
}

func clearSecretCookie(w http.ResponseWriter, secretId string) {
	cookie := &http.Cookie{
		Name:     makeSecretInfoCookieName(secretId),
		MaxAge:   -1,
		Path:     makeSecretInfoCookiePath(secretId),
		HttpOnly: true,
	}
	http.SetCookie(w, cookie)
}

func secretCommonHandleFunc(w http.ResponseWriter, req *http.Request) {
	p := req.URL.Path
	if ok, _ := path.Match("[A-Z]/thumb/*", p); ok {
		secretThumbHandleFunc(w, req)
	} else if ok, _ = path.Match("[A-Z]/image/*", p); ok {
		secretImageHandleFunc(w, req)
	} else if ok, _ = path.Match("[A-Z]/list", p); ok {
		secretListHandleFunc(w, req)
	} else if ok, _ = path.Match("[A-Z]/add", p); ok {
		secretAddHandleFunc(w, req)
	} else if ok, _ = path.Match("[A-Z]/delete", p); ok {
		secretDeleteHandleFunc(w, req)
	} else if ok, _ = path.Match("[A-Z]/open", p); ok {
		secretOpenHandleFunc(w, req)
	} else if ok, _ = path.Match("[A-Z]/close", p); ok {
		secretCloseHandleFunc(w, req)
	} else if ok, _ = path.Match("[A-Z]/password", p); ok {
		secretPasswordHandleFunc(w, req)
	} else {
		badRequest(w, "Bad Request")
	}
}

type SecretOpenResponse struct {
	Ok     bool   `json:"ok"`
	Detail string `json:"detail"`
}

func secretOpenHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSecretHandler {
		log.Println("secretOpenHandleFunc")
		defer log.Println("defer secretOpenHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	secretId := path.Dir(req.URL.Path)

	secretPassword := req.PostFormValue("secret_password")
	secretPassword = strings.TrimSpace(secretPassword)
	if secretPassword == "" {
		writeJson(w, req, &SecretOpenResponse{
			Ok:     false,
			Detail: "Password is required",
		})
		return
	}

	okDetail := ""
	if !appState.HasSecretInfo(secretId) {
		appState.CreateNewSecretInfo(secretId, secretPassword)
		okDetail = "create new"
	}

	keyword, err := appState.AuthSecretInfo(secretId, secretPassword)
	if err != nil {
		writeJson(w, req, &SecretOpenResponse{
			Ok:     false,
			Detail: err.Error(),
		})
		return
	}

	setSecretCookie(w, secretId, keyword)

	response := SecretOpenResponse{
		Ok:     true,
		Detail: okDetail,
	}

	writeJson(w, req, &response)

	if DebugModeSecretHandler {
		log.Println("Open Secret:", secretId, "(keyword:", keyword, ")")
	}
}

type SecretCloseResponse struct {
	Ok     bool   `json:"ok"`
	Detail string `json:"detail"`
}

func secretCloseHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSecretHandler {
		log.Println("secretCloseHandleFunc")
		defer log.Println("defer secretCloseHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	secretId := path.Dir(req.URL.Path)

	err := appState.ClearSecretAuth(secretId)
	if err != nil {
		badRequest(w, err.Error())
		return
	}

	clearSecretCookie(w, secretId)

	writeJson(w, req, &SecretCloseResponse{
		Ok:     true,
		Detail: "close Secret-ID:" + secretId,
	})
}

type SecretPasswordResponse struct {
	Ok     bool   `json:"ok"`
	Detail string `json:"detail"`
}

func secretPasswordHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSecretHandler {
		log.Println("secretPasswordHandleFunc")
		defer log.Println("defer secretPasswordHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	secretId := path.Dir(req.URL.Path)

	oldPassword := req.PostFormValue("old_password")
	oldPassword = strings.TrimSpace(oldPassword)
	if oldPassword == "" {
		writeJson(w, req, &SecretPasswordResponse{
			Ok:     false,
			Detail: "old_password is required",
		})
		return
	}

	newPassword := req.PostFormValue("new_password")
	newPassword = strings.TrimSpace(newPassword)
	if newPassword == "" {
		writeJson(w, req, &SecretPasswordResponse{
			Ok:     false,
			Detail: "new_password is required",
		})
		return
	}

	err := appState.ChangeSecretPassword(secretId, oldPassword, newPassword)
	if err != nil {
		if errors.Is(err, ErrSecretInfoWrongPassword) {
			writeJson(w, req, &SecretPasswordResponse{
				Ok:     false,
				Detail: "Wrong Old Password",
			})
			return
		}
		errorResponse(w, err)
		return
	}

	writeJson(w, req, &SecretPasswordResponse{
		Ok:     true,
		Detail: "Success to change password of " + secretId,
	})
}

type SecretListResponse struct {
	Ok     bool                   `json:"ok"`
	Detail string                 `json:"detail"`
	Files  []*SecretImageFileInfo `json:"files"`
}

func secretListHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSecretHandler {
		log.Println("secretListHandleFunc")
		defer log.Println("defer secretListHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodHead, http.MethodGet:
		// ok
	}

	secretId := path.Dir(req.URL.Path)

	cookie, err := req.Cookie(makeSecretInfoCookieName(secretId))
	if err != nil {
		writeJson(w, req, &SecretListResponse{
			Ok:     false,
			Detail: "Authentication Required",
		})
		return
	}

	keyword := cookie.Value

	if err = appState.CheckSecretInfoKeyword(secretId, keyword); err != nil {
		if errors.Is(err, ErrSecretInfoExpired) {
			clearSecretCookie(w, secretId)
			appState.ClearSecretAuth(secretId)
		}
		writeJson(w, req, &SecretListResponse{
			Ok:     false,
			Detail: "Authentication Required",
		})
		return
	}

	files, err := appState.GetSecretInfoFiles(secretId)
	if err != nil {
		errorResponse(w, err)
		return
	}

	response := SecretListResponse{
		Ok:    true,
		Files: files,
	}

	writeJson(w, req, &response)
}

type SecretAddResponse struct {
	Ok       bool   `json:"ok"`
	Detail   string `json:"detail"`
	SecretId string `json:"secret_id"`
	FileName string `json:"file_name"`
}

type SecretSaveImageFile struct {
	SecretId string
	File     multipart.File
	Name     string
	Length   int64
}

func secretAddHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSecretHandler {
		log.Println("secretAddHandleFunc")
		defer log.Println("defer secretAddHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	secretId := path.Dir(req.URL.Path)

	cookie, err := req.Cookie(makeSecretInfoCookieName(secretId))
	if err != nil {
		writeJson(w, req, &SecretAddResponse{
			Ok:     false,
			Detail: "Authentication Required",
		})
		return
	}

	keyword := cookie.Value

	if err = appState.CheckSecretInfoKeyword(secretId, keyword); err != nil {
		if errors.Is(err, ErrSecretInfoExpired) {
			clearSecretCookie(w, secretId)
			appState.ClearSecretAuth(secretId)
		}
		writeJson(w, req, &SecretAddResponse{
			Ok:     false,
			Detail: "Authentication Required",
		})
		return
	}

	file, fileHeader, err := req.FormFile("file")
	if err != nil {
		errorResponse(w, err)
		return
	}

	if fileHeader == nil {
		badRequest(w, "missing file")
		return
	}

	ext := path.Ext(fileHeader.Filename)
	fileMime, _ := mime.ExtensionsByType(ext)
	length := fileHeader.Size
	n := appState.SecretImageNumber.GetNext()
	fileName := fmt.Sprintf("%06x", n)

	info := &SecretImageFileInfo{
		Name: fileName,
		Ext:  ext,
		Mime: "image/png",
		Size: &SecretImageSizeInfo{
			Length: length,
		},
		Thumb: &SecretImageSizeInfo{},
	}
	if len(fileMime) > 0 {
		info.Mime = fileMime[0]
	}

	err = appState.AddSecretImage(secretId, info)
	if err != nil {
		errorResponse(w, err)
		return
	}

	appState.Save()

	f := &SecretSaveImageFile{
		SecretId: secretId,
		File:     file,
		Name:     fileName,
		Length:   length,
	}

	fileManager.SaveSecretImage(f)

	response := SecretAddResponse{
		Ok:       true,
		SecretId: secretId,
		FileName: fileName,
	}

	writeJson(w, req, &response)
}

type SecretDeleteResponse struct {
	Ok     bool   `json:"ok"`
	Detail string `json:"detail"`
}

func secretDeleteHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSecretHandler {
		log.Println("secretDeleteHandleFunc")
		defer log.Println("defer secretDeleteHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	secretId := path.Dir(req.URL.Path)

	cookie, err := req.Cookie(makeSecretInfoCookieName(secretId))
	if err != nil {
		writeJson(w, req, &SecretDeleteResponse{
			Ok:     false,
			Detail: "Authentication Required",
		})
		return
	}

	keyword := cookie.Value

	if err = appState.CheckSecretInfoKeyword(secretId, keyword); err != nil {
		if errors.Is(err, ErrSecretInfoExpired) {
			clearSecretCookie(w, secretId)
			appState.ClearSecretAuth(secretId)
		}
		writeJson(w, req, &SecretDeleteResponse{
			Ok:     false,
			Detail: "Authentication Required",
		})
		return
	}

	imageId := req.PostFormValue("image")

	err = appState.DeleteSecretImage(secretId, imageId)
	if err != nil {
		errorResponse(w, err)
		return
	}

	response := &SecretDeleteResponse{
		Ok:     true,
		Detail: "",
	}

	writeJson(w, req, response)
}

func secretThumbHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSecretHandler {
		log.Println("secretThumbHandleFunc")
		defer log.Println("defer secretThumbHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodHead, http.MethodGet:
		// ok
	}

	secretId := path.Dir(path.Dir(req.URL.Path))
	fileName := path.Base(req.URL.Path)

	cookie, err := req.Cookie(makeSecretInfoCookieName(secretId))
	if err != nil {
		badRequest(w, "require Authentication (secretId: "+secretId+")")
		return
	}

	keyword := cookie.Value

	if err = appState.CheckSecretInfoKeyword(secretId, keyword); err != nil {
		if errors.Is(err, ErrSecretInfoExpired) {
			clearSecretCookie(w, secretId)
			appState.ClearSecretAuth(secretId)
		}
		http.Error(w, "missing cookie", http.StatusUnauthorized)
		return
	}

	if !appState.IsReadySecretImage(secretId, fileName) {
		http.Error(w, "now saving image to disk", http.StatusServiceUnavailable)
		return
	}

	etag := secretId + fileName + "THUMBNAIL"

	eTags := strings.Split(req.Header.Get("If-None-Match"), ",")
	for _, value := range eTags {
		if etag == value {
			w.WriteHeader(http.StatusNotModified)
			return
		}
	}

	image, err := fileManager.LoadSecretThumbnail(secretId, fileName)
	if err != nil {
		errorResponse(w, err)
		return
	}

	w.Header().Set("Cache-Control", "public, max-age=604800")
	w.Header().Set("Etag", etag)
	w.Header().Set("Content-Type", "image/png")
	w.Header().Set("Content-Length", strconv.Itoa(len(image)))

	w.Write(image)
}

func secretImageHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSecretHandler {
		log.Println("secretImageHandleFunc")
		defer log.Println("defer secretImageHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodHead, http.MethodGet:
		// ok
	}

	secretId := path.Dir(path.Dir(req.URL.Path))
	fileName := path.Base(req.URL.Path)

	cookie, err := req.Cookie(makeSecretInfoCookieName(secretId))
	if err != nil {
		badRequest(w, "require Authentication (secretId: "+secretId+")")
		return
	}

	keyword := cookie.Value

	if err = appState.CheckSecretInfoKeyword(secretId, keyword); err != nil {
		if errors.Is(err, ErrSecretInfoExpired) {
			clearSecretCookie(w, secretId)
			appState.ClearSecretAuth(secretId)
		}
		http.Error(w, "missing cookie", http.StatusUnauthorized)
		return
	}

	if !appState.IsReadySecretImage(secretId, fileName) {
		http.Error(w, "now saving image to disk", http.StatusServiceUnavailable)
		return
	}

	etag := secretId + fileName

	eTags := strings.Split(req.Header.Get("If-None-Match"), ",")
	for _, value := range eTags {
		if etag == value {
			w.WriteHeader(http.StatusNotModified)
			return
		}
	}

	info, err := appState.GetSecretImageInfo(secretId, fileName)
	if err != nil {
		errorResponse(w, err)
		return
	}

	image, err := fileManager.LoadSecretImage(secretId, fileName)
	if err != nil {
		errorResponse(w, err)
		return
	}

	w.Header().Set("Cache-Control", "public, max-age=604800")
	w.Header().Set("Etag", etag)

	if info.Mime != "" {
		w.Header().Set("Content-Type", info.Mime)
	} else {
		w.Header().Set("Content-Type", "image/png")
	}

	w.Header().Set("Content-Length", strconv.Itoa(len(image)))

	w.Write(image)
}
