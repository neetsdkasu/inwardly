package main

import (
	"fmt"
	"log"
	"sync"
)

type MemoSwap struct {
	Index []int   `json:"index"`
	Memo  []*Memo `json:"memo"`
}

type MemoListStore struct {
	List    MemoList
	History MemoList
	sync.Mutex
}

func (this *MemoListStore) Init() error {
	if DebugModeShowInit || DebugModeMemoListStore {
		log.Println("MemoListStore.Init")
	}
	list := MemoList{}
	if err := fileManager.LoadMemoList(&list); err != nil {
		return err
	}
	this.List = list
	history := MemoList{}
	if err := fileManager.LoadMemoHistory(&history); err != nil {
		return err
	}
	this.History = history
	return nil
}

func (this *MemoListStore) GetList() MemoList {
	if DebugModeMemoListStore {
		log.Println("MemoListStore.GetList")
		defer log.Println("defer MemoListStore.GetList")
	}
	this.Lock()
	defer this.Unlock()
	list := make(MemoList, len(this.List))
	copy(list, this.List)
	return list
}

func (this *MemoListStore) GetHistory() MemoList {
	if DebugModeMemoListStore {
		log.Println("MemoListStore.GetHistory")
		defer log.Println("defer MemoListStore.GetHistory")
	}
	this.Lock()
	defer this.Unlock()
	history := make(MemoList, len(this.History))
	copy(history, this.History)
	return history
}

func (this *MemoListStore) Remove(id string) bool {
	if DebugModeMemoListStore {
		log.Println("MemoListStore.Remove")
		defer log.Println("defer MemoListStore.Remove")
	}
	this.Lock()
	defer this.Unlock()
	list := MemoList{}
	history := MemoList{}
	ok := false
	for _, m := range this.List {
		if m.Id == id {
			ok = true
			history = append(history, m)
		} else {
			list = append(list, m)
		}
	}
	if !ok {
		return false
	}
	this.List = list
	for _, m := range this.History {
		if m.Id != id {
			history = append(history, m)
		}
	}
	if len(history) > 20 {
		history = history[:20]
	}
	this.History = history
	fileManager.SaveMemoList()
	fileManager.SaveMemoHistory()
	return true
}

func (this *MemoListStore) Add(memo *Memo) bool {
	if DebugModeMemoListStore {
		log.Println("MemoListStore.Add")
		defer log.Println("defer MemoListStore.Add")
	}
	this.Lock()
	defer this.Unlock()
	for _, m := range this.List {
		if m.Id == memo.Id {
			return false
		}
	}
	this.List = append(this.List, memo)
	history := make(MemoList, 0, len(this.History))
	for _, m := range this.History {
		if m.Id != memo.Id {
			history = append(history, m)
		}
	}
	this.History = history
	fileManager.SaveMemoList()
	fileManager.SaveMemoHistory()
	return true
}

func (this *MemoListStore) Update(memo *Memo) bool {
	if DebugModeMemoListStore {
		log.Println("MemoListStore.Update")
		defer log.Println("defer MemoListStore.Update")
	}
	this.Lock()
	defer this.Unlock()
	list := make(MemoList, len(this.List))
	found := false
	for i, m := range this.List {
		if m.Id == memo.Id {
			found = true
			list[i] = memo
		} else {
			list[i] = m
		}
	}
	history := make(MemoList, len(this.History))
	for i, m := range this.History {
		if m.Id == memo.Id {
			found = true
			history[i] = memo
		} else {
			history[i] = m
		}
	}
	if !found {
		return false
	}
	this.List = list
	this.History = history
	fileManager.SaveMemoList()
	fileManager.SaveMemoHistory()
	return true
}

func (this *MemoListStore) Move(id string, value int) (*MemoSwap, error) {
	if DebugModeMemoListStore {
		log.Println("MemoListStore.Move")
		defer log.Println("defer MemoListStore.Move")
	}
	this.Lock()
	defer this.Unlock()
	if value != -1 && value != 1 {
		return nil, BadRequestError{
			fmt.Sprint("invalid value: ", value),
		}
	}
	index1 := -1
	for i, m := range this.List {
		if m.Id == id {
			index1 = i
			break
		}
	}
	if index1 < 0 {
		return nil, BadRequestError{
			fmt.Sprint("not found id: ", id),
		}
	}
	index2 := index1 + value
	if index2 < 0 || index2 >= len(this.List) {
		return nil, BadRequestError{
			fmt.Sprint("cannot move memo(id: "+
				id+") (value: ", value, ")"),
		}
	}
	list := make(MemoList, len(this.List))
	copy(list, this.List)
	list[index1], list[index2] = list[index2], list[index1]
	this.List = list
	fileManager.SaveMemoList()
	if index2 < index1 {
		index1, index2 = index2, index1
	}
	return &MemoSwap{
		Index: []int{index1, index2},
		Memo:  []*Memo{list[index1], list[index2]},
	}, nil
}
