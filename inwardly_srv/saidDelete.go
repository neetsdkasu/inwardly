package main

import (
	"log"
	"net/http"
	"time"
)

func init() {
	// POST
	//  target  string (non empty && said ID)
	http.HandleFunc("/api/said/delete", saidDeleteHandleFunc)
}

func saidDeleteHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSaidDeleteHandler {
		log.Println("saidDeleteHandleFunc")
		defer log.Println("defer saidDeleteHandleFunc")
	}
	now := time.Now().Local()
	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}
	target := req.PostFormValue("target")
	if target == "" {
		badRequest(w, "missing target parameter")
		return
	}

	update := ToDateString(now)
	said, memo, err := dataManager.DeleteSaid(update, target)

	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	view := said.View()

	response := SayResponse{
		SaidView: view,
		Memo:     memo,
	}

	writeJson(w, req, &response)
}
