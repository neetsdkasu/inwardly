package main

import (
	"log"
	"net/http"
	"sort"
	"strings"
)

func init() {
	// GET
	// /api/quoted/{saidID}
	http.Handle("/api/quoted/",
		http.StripPrefix("/api/quoted/",
			http.HandlerFunc(quotedRefsHandleFunc)))
}

func quotedRefsHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeQuotedRefsHandler {
		log.Println("quotedRefsHandleFunc")
		defer log.Println("defer quotedRefsHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	target := req.URL.Path
	if target == "" {
		badRequest(w, "missing Said ID")
		return
	}

	said, err := dataManager.GetSaidById(target)
	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	var refs []*SaidView = nil

	if len(said.QuotedRefs) > 0 {
		for _, id := range said.QuotedRefs {
			s, err := dataManager.GetSaidById(id)
			if err != nil {
				if _, ok := err.(BadRequestError); ok {
					badRequest(w, err.Error())
					return
				}
				log.Println(err)
				internalServerError(w, err.Error())
				return
			}
			refs = append(refs, s.View())
		}
		sort.Slice(refs, func(i, j int) bool {
			return strings.Compare(refs[i].Id, refs[j].Id) < 0
		})
	}

	qview := QuotedView{
		Said: said.View(),
		Refs: refs,
	}

	writeJson(w, req, &qview)
}
