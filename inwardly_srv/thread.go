package main

import (
	"log"
	"net/http"
	"strings"
	"time"
)

func init() {
	// GET
	// /api/thread/{SaidID}
	http.Handle("/api/thread/",
		http.StripPrefix("/api/thread/",
			http.HandlerFunc(threadHandleFunc)))

	// POST
	// /api/setting/thread/{SaidID}
	// autoupload   string '1' or '0' (require)
	// secretupload string '1' or '0' (require)
	// title        string (non null, require)
	// categories   string
	http.Handle("/api/setting/thread/",
		http.StripPrefix("/api/setting/thread/",
			http.HandlerFunc(threadSettingHandleFunc)))

	// POST
	// /api/upload/thread/{SaidID}
	// autoupload   string '1' or '0' (require)
	// secretupload string '1' or '0' (require)
	// title        string (non null, require)
	// categories   string
	http.Handle("/api/upload/thread/",
		http.StripPrefix("/api/upload/thread/",
			http.HandlerFunc(threadUploadHandleFunc)))

	// POST
	// /api/deletepost/thread
	// target string (thread ID && require)
	http.Handle("/api/deletepost/thread",
		http.HandlerFunc(deletePostThreadHandleFunc))
}

func threadHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeThreadHandler {
		log.Println("threadHandleFunc")
		defer log.Println("defer threadHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	threadId := req.URL.Path

	said, err := dataManager.GetThread(threadId)
	if err != nil {
		errorResponse(w, err)
		return
	}

	view := make([]*SaidView, len(said))
	for i, s := range said {
		view[i] = s.View()
	}

	thread := ThreadView{
		Id:   threadId,
		Said: view,
	}

	if a := appSetting.GetStoreCopy(); a.CanUploadThread() {
		setting, ok := appSetting.GetThreadUploadSetting(threadId)
		if !ok {
			setting = &ThreadUploadSetting{
				Id: threadId,
			}
		}
		thread.Setting = setting
	}

	for _, view := range thread.Said {
		err = view.AttachQuotes()
		if err != nil {
			log.Println(err)
			internalServerError(w, err.Error())
			return
		}
	}

	writeJson(w, req, thread)
}

func threadSettingHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeThreadHandler {
		log.Println("threadSettingHandleFunc")
		defer log.Println("defer threadSettingHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	autoUpload := req.PostFormValue("autoupload")
	if autoUpload != "0" && autoUpload != "1" {
		badRequest(w, "invalid autoUpload "+autoUpload)
		return
	}

	secretUpload := req.PostFormValue("secretupload")
	if secretUpload != "0" && secretUpload != "1" {
		badRequest(w, "invalid secretupload "+secretUpload)
		return
	}

	title := strings.TrimSpace(req.PostFormValue("title"))
	if title == "" {
		c := strings.TrimSpace(req.PostFormValue("categories"))
		if autoUpload != "0" || c != "" {
			badRequest(w, "missing title")
			return
		}
	}

	categories := []string{}
	for _, s := range strings.Split(req.PostFormValue("categories"), ",") {
		c := strings.TrimSpace(s)
		if c != "" {
			categories = append(categories, c)
		}
	}

	threadId := req.URL.Path

	said, err := dataManager.GetSaidById(threadId)
	if err != nil {
		errorResponse(w, err)
		return
	}

	if said.Root != "" {
		badRequest(w, "invalid ThreadID "+threadId)
		return
	}

	res, err := appSetting.SetThreadUploadSetting(
		threadId,
		title,
		strings.Join(categories, ","),
		autoUpload == "1",
		secretUpload == "1",
	)
	if err != nil {
		errorResponse(w, err)
		return
	}

	writeJson(w, req, res)
}

func threadUploadHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeThreadHandler {
		log.Println("threadUploadHandleFunc")
		defer log.Println("defer threadUploadHandleFunc")
	}

	today := time.Now().Local()
	todayDateTimeValue := ToDateString(today)

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	autoUpload := req.PostFormValue("autoupload")
	if autoUpload != "0" && autoUpload != "1" {
		badRequest(w, "invalid autoUpload "+autoUpload)
		return
	}

	secretUpload := req.PostFormValue("secretupload")
	if secretUpload != "0" && secretUpload != "1" {
		badRequest(w, "invalid secretupload "+secretUpload)
		return
	}

	title := strings.TrimSpace(req.PostFormValue("title"))
	if title == "" {
		badRequest(w, "missing title")
		return
	}

	categories := []string{}
	for _, s := range strings.Split(req.PostFormValue("categories"), ",") {
		c := strings.TrimSpace(s)
		if c != "" {
			categories = append(categories, c)
		}
	}

	threadId := req.URL.Path

	said, err := dataManager.GetSaidById(threadId)
	if err != nil {
		errorResponse(w, err)
		return
	}

	if said.Root != "" {
		badRequest(w, "invalid ThreadID "+threadId)
		return
	}

	setting := appSetting.GetStoreCopy()
	if !setting.CanUploadThread() {
		badRequest(w, "incomplete upload setting!")
		return
	}

	tus, err := appSetting.SetThreadUploadSetting(
		threadId,
		title,
		strings.Join(categories, ","),
		autoUpload == "1",
		secretUpload == "1",
	)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res, err := uploadThread(&setting, tus, todayDateTimeValue)
	if err != nil {
		errorResponse(w, err)
		return
	}

	writeJson(w, req, res)
}

func deletePostThreadHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeThreadHandler {
		log.Println("deletePostThreadHandleFunc")
		defer log.Println("defer deletePostThreadHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	threadId := req.PostFormValue("target")
	if threadId == "" {
		badRequest(w, "missing target "+threadId)
		return
	}

	said, err := dataManager.GetSaidById(threadId)
	if err != nil {
		errorResponse(w, err)
		return
	}

	if said.Root != "" {
		badRequest(w, "invalid ThreadID "+threadId)
		return
	}

	setting := appSetting.GetStoreCopy()
	if !setting.CanUploadThread() {
		badRequest(w, "incomplete upload setting!")
		return
	}

	tus, ok := appSetting.GetThreadUploadSetting(threadId)
	if !ok {
		badRequest(w, "not found setting of ThreadID "+threadId)
		return
	}

	if tus.BlogPostId == "" {
		badRequest(w, "not found blog post")
		return
	}

	err = setting.DeleteBlog(tus.BlogPostId)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res, err := appSetting.SetThreadUploadSettingUploded(
		tus.Id,
		tus.Update,
		"",
		"",
		"",
	)
	if err != nil {
		errorResponse(w, err)
		return
	}

	writeJson(w, req, res)
}
