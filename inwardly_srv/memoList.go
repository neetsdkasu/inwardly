package main

import (
	"log"
	"net/http"
)

func init() {
	// GET
	http.HandleFunc("/api/memo/list", listMemoHandleFunc)

	// POST
	//  target  string (non empty && said ID)
	http.HandleFunc("/api/memo/add", addMemoHandleFunc)

	// POST
	//  target  string (non empty && said ID)
	http.HandleFunc("/api/memo/remove", removeMemoHandleFunc)

	// GET
	http.HandleFunc("/api/memo/history", historyMemoHandleFunc)

	// POST
	//  target  string (non empty && said ID)
	//  value  number (non empty && "-1" or "1")
	http.HandleFunc("/api/memo/move", moveMemoHandleFunc)
}

func listMemoHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeMemoListHandler {
		log.Println("listMemoHandleFunc")
		defer log.Println("defer listMemoHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	list := dataManager.GetMemoList()

	writeJson(w, req, list)
}

func historyMemoHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeMemoListHandler {
		log.Println("historyMemoHandleFunc")
		defer log.Println("defer historyMemoHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	history := dataManager.GetMemoHistory()

	writeJson(w, req, history)
}

func addMemoHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeMemoListHandler {
		log.Println("addMemoHandleFunc")
		defer log.Println("defer addMemoHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	target := req.PostFormValue("target")
	if target == "" {
		badRequest(w, "missing target parameter")
		return
	}

	memo, err := dataManager.AddMemo(target)
	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	writeJson(w, req, memo)
}

func removeMemoHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeMemoListHandler {
		log.Println("removeMemoHandleFunc")
		defer log.Println("defer removeMemoHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	target := req.PostFormValue("target")
	if target == "" {
		badRequest(w, "missing target parameter")
		return
	}

	err := dataManager.RemoveMemo(target)
	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	memo := Memo{
		Id: target,
	}

	writeJson(w, req, &memo)
}

func moveMemoHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeMemoListHandler {
		log.Println("moveMemoHandleFunc")
		defer log.Println("defer moveMemoHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	target := req.PostFormValue("target")
	if target == "" {
		badRequest(w, "missing target parameter")
		return
	}

	valueStr := req.PostFormValue("value")
	if valueStr == "" {
		badRequest(w, "missing value parameter")
		return
	}
	if valueStr != "-1" && valueStr != "1" {
		badRequest(w, "value parameter must be -1 or 1")
		return
	}
	value := 1
	if valueStr == "-1" {
		value = -1
	}

	res, err := dataManager.MoveMemo(target, value)
	if err != nil {
		errorResponse(w, err)
		return
	}

	writeJson(w, req, res)
}
