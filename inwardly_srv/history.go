package main

import (
	"log"
	"net/http"
)

func init() {
	// GET
	// /api/history/{saidID}
	http.Handle("/api/history/",
		http.StripPrefix("/api/history/",
			http.HandlerFunc(historyHandleFunc)))
}

func historyHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeHistoryHandler {
		log.Println("historyHandleFunc")
		defer log.Println("defer historyHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	target := req.URL.Path
	if target == "" {
		badRequest(w, "missing Said ID")
		return
	}

	said, err := dataManager.GetSaidById(target)
	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	hist := EditHistory{
		Said:    said.View(),
		History: said.History,
	}

	writeJson(w, req, &hist)
}
