
interface Dictionary<T> {
	[key: string]: T;
}

interface Memo {
	id: string;
	date: string;
	elements: TextElement[] | null;
	image: string[] | null;
	thread: string;
}

interface MemoSwap {
	index: number[];
	memo: Memo[];
}

type TextElementType = 'text' | 'url' | 'quote' | 'inside-url' | 'inside-img';

interface TextElement {
	type: TextElementType;
	src: string;
}

interface Said {
	id: string;
	date: string;
	update: string;
	deleted: boolean;
	text: string;
	elements: TextElement[] | null;
	image: string[] | null;
	parent: string;
	children: string[] | null;
	quote: Said[] | null;
	thread: string;
	refs: number;
	secret: boolean;
}

interface PageView {
	id: string;
	prev: string;
	next: string;
	timeline: Said[];
}

interface Page {
	page: number;
	prev: number;
	next: number;
	memo: Memo[];
	timeline: Said[];
}

interface ThreadSetting {
	auto_upload: boolean;
	title: string;
	categories: string;
	blog_url: string;
	upload_date: string;
	secret_upload: boolean;
}

interface Thread {
	id: string;
	said: Said[];
	setting: ThreadSetting | null;
}

interface SayResponse {
	memo: Memo | null;
	said: Said;
}

type SayType = 'say' | 'edit' | 'reply';

interface PastSaid {
	date: string;
	delete: boolean;
	text: string;
	image: string[] | null;
	secret: boolean;
}

interface EditHistory {
	said: Said;
	history: PastSaid[] | null;
}

interface QuotedRefs {
	said: Said;
	refs: Said[] | null;
}

interface CalenderData {
	option: {
		first: string;
		last: string;
	};
	year: number;
	month: number;
	week: number;
	end: number;
	days: string[] | null;
}

interface SearchResult {
	id: string;
	query: string;
	progress: number;
	count: number;
	said: Said[] | null;
	error: string;
}

interface HatenaBlog {
	hatena_id: string;
	blog_id: string;
}

interface BlogSettings {
	auto_upload: boolean;
	thread_upload: boolean;
}

interface DebugSetings {
	timeline_upload: string;
	nontargets: boolean;
}

interface Settings {
	has_consumer: boolean;
	consumer_key: string;
	authorized: boolean;
	hatena_blog: HatenaBlog | null;
	blog_settings: BlogSettings | null;
	debug_settings: DebugSetings | null;
}

interface SettingResult {
	ok: boolean;
	message: string;
	value: string;
}

interface DocumentLinkSetting {
	id: number;
	entry: string;
	rootpath: string;
	basefile: string;
	ql_order: number;
}

interface DocumentLinkState {
	id: number;
	entry: string;
	marked_files: string[] | null;
	marked_directories: string[] | null;
	current: string[] | null;
	exts: string[] | null;
	directories: string[] | null;
	files: string[] | null;
}

interface DocumentLink {
	setting: DocumentLinkSetting | null;
	state: DocumentLinkState | null;
}

interface SystemStatus {
	routine_start_time: string;
	routine_end_time: string;
	static_files: string;
	build_version: string;
	shutdown_token: string;
}

interface Status {
	system: SystemStatus | null;
}