package main

import (
	"log"
	"net/http"
	"time"
)

func init() {
	// GET
	// /api/page/
	// or
	// /api/page/{DayID}
	http.Handle("/api/page/",
		http.StripPrefix("/api/page/",
			http.HandlerFunc(pageHandleFunc)))
}

func pageHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModePageHandler {
		log.Println("pageHandleFunc")
		defer log.Println("defer pageHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	dayId := req.URL.Path
	if dayId == "" {
		dayId = GetDayIdFromTime(time.Now().Local())
	}

	pageView, err := dataManager.GetPage(dayId)
	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	for _, view := range pageView.Timeline {
		err = view.AttachQuotes()
		if err != nil {
			log.Println(err)
			internalServerError(w, err.Error())
			return
		}
	}

	writeJson(w, req, pageView)
}
