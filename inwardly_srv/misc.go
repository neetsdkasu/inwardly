package main

import (
	"errors"
	"fmt"
	"html"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"
)

var urlRE = regexp.MustCompile(`https?://\S+`)

var BadSaidIdError = errors.New("Bad said ID")

func MakeSaidIdFromTime(t time.Time) string {
	return strings.Replace(t.Format("060102150405.000"), ".", "", 1)
}

func ToTimeFromSaidId(id string) (time.Time, error) {
	id = id[:12] + "." + id[12:]
	return time.ParseInLocation("060102150405.000", id, time.Local)
}

func ToDateString(t time.Time) string {
	return t.Format("2006-01-02T15:04:05.000-07:00")
}

func FromDateString(d string) (time.Time, error) {
	return time.Parse("2006-01-02T15:04:05.000-07:00", d)
}

func GetDayIdFromSaidId(id string) (string, error) {
	if len(id) != 15 {
		return "", BadSaidIdError
	}
	return id[:6], nil
}

func GetDayIdFromTime(t time.Time) string {
	return t.Format("060102")
}

func MakeDayId(y, m, d int) string {
	return fmt.Sprintf("%02d%02d%02d", y%100, m, d)
}

func ToTimeFromDayId(id string) (time.Time, error) {
	return time.ParseInLocation("060102", id, time.Local)
}

// html.EscapeString済みのurlが与えられてたらやばそう？
//
//	http://example.com/foo?a=0&amp;b=1&amp;c=2
//
// とか (要調査)
func ToSafeUrl(s string) (string, error) {
	s = html.UnescapeString(s)
	u, err := url.Parse(s)
	if err != nil {
		return "", err
	}
	q := u.Query()
	u.RawQuery = q.Encode()
	return u.String(), nil
}

type Counter struct {
	Count      int
	LastAccess time.Time
	Item       interface{}
}

func NewCounter(item interface{}) *Counter {
	return &Counter{Item: item}
}

func (this *Counter) GetItem() interface{} {
	this.Count++
	return this.Item
}

func (this *Counter) Release() {
	this.Count--
	this.LastAccess = time.Now().Local()
}

type TotalNumber struct {
	Count int
	sync.Mutex
}

func (this *TotalNumber) Current() int {
	this.Lock()
	defer this.Unlock()
	v := this.Count
	return v
}

func (this *TotalNumber) GetNext() int {
	this.Lock()
	defer this.Unlock()
	v := this.Count
	v++
	this.Count = v
	return v
}

func (this *TotalNumber) Set(count int) {
	this.Lock()
	defer this.Unlock()
	this.Count = count
}
