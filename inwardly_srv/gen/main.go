package main

import (
	"bufio"
	"crypto/md5"
	"encoding/base64"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
	"unicode"
)

const GeneratorName = "by inwardly/inwardly_srv/gen"

func main() {
	var fsMode bool
	flag.BoolVar(&fsMode, "fs", false, "use file server mode (for debug?)")
	flag.Parse()
	if err := run(fsMode); err != nil {
		log.Panic(err)
	}
	log.Println("done.")
}

func makeName(s string) string {
	a := strings.ReplaceAll(s, "_", ".")
	b := strings.Split(a, ".")
	for i, c := range b {
		if i > 0 {
			first := true
			b[i] = strings.Map(func(r rune) rune {
				if first {
					first = false
					return unicode.ToUpper(r)
				} else {
					return unicode.ToLower(r)
				}
			}, c)
		}
	}
	return strings.Join(b, "")
}

func run(fsMode bool) error {

	if fsMode {
		log.Println("GENERATE: FileServer")
	} else {
		log.Println("GENERATE: Embedded")
	}

	var templateSrc string
	if fsMode {
		templateSrc = FileVerTemplate
	} else {
		templateSrc = BlobVerTemplate
	}

	tmpl, err := template.New("gosource").Parse(templateSrc)
	if err != nil {
		return err
	}

	filelist := []struct {
		file, mime string
	}{
		{
			file: "index.html",
			mime: "text/html",
		},
		{
			file: "style.css",
			mime: "text/css",
		},
		{
			file: "common_utils.js",
			mime: "text/javascript",
		},
		{
			file: "script.js",
			mime: "text/javascript",
		},
		{
			file: "favicon.ico",
			mime: "image/x-icon",
		},
		{
			file: "setting.html",
			mime: "text/html",
		},
		{
			file: "setting.js",
			mime: "text/javascript",
		},
		{
			file: "link.html",
			mime: "text/html",
		},
		{
			file: "link.js",
			mime: "text/javascript",
		},
		{
			file: "misc.html",
			mime: "text/html",
		},
		{
			file: "misc.js",
			mime: "text/javascript",
		},
		{
			file: "secret.html",
			mime: "text/html",
		},
		{
			file: "secret.js",
			mime: "text/javascript",
		},
	}

	for _, f := range filelist {
		log.Printf("%+v\n", f)

		var blob []byte = nil
		var hash string = ""

		if !fsMode {
			blob, err = ioutil.ReadFile(f.file)
			if err != nil {
				return err
			}
			sum := md5.Sum(blob)
			hash = base64.StdEncoding.EncodeToString(sum[:])
		}

		name := makeName(f.file)

		dst, err := os.Create(name + "_gen.go")
		if err != nil {
			return err
		}
		defer dst.Close()

		err = tmpl.Execute(dst, struct {
			GeneratorName string
			FileName      string
			FuncName      string
			BlobName      string
			HandlerName   string
			Blob          []byte
			Mime          string
			Hash          string
		}{
			GeneratorName: GeneratorName,
			FileName:      f.file,
			FuncName:      name + "HandlerFunc",
			BlobName:      name + "Blob",
			HandlerName:   name + "Handler",
			Blob:          blob,
			Mime:          f.mime,
			Hash:          hash,
		})
		if err != nil {
			return err
		}
	}

	{
		log.Println("const_gen.go")
		dst, err := os.Create("const_gen.go")
		if err != nil {
			return err
		}
		defer dst.Close()
		version := readVersion()
		genMode := "Embedded"
		if fsMode {
			genMode = "FileServer"
		}
		_, err = fmt.Fprintf(
			dst,
			ConstGenSrc,
			GeneratorName,
			genMode,
			version,
		)
		if err != nil {
			return err
		}
	}
	return nil
}

func readVersion() (version string) {
	version = "unknown"
	ghfn := filepath.FromSlash("../.git/HEAD")
	gh, err := ioutil.ReadFile(ghfn)
	if err != nil {
		return
	}
	fs := strings.Fields(string(gh))
	if fs[0] != "ref:" {
		version = fs[0]
		return
	}

	r := fs[len(fs)-1]
	rfn := filepath.FromSlash("../.git/" + r)
	rf, err := ioutil.ReadFile(rfn)
	if err == nil {
		version = fmt.Sprintf(
			"%s %s",
			filepath.Base(rfn),
			string(rf),
		)
		return
	}

	prfn := filepath.FromSlash(
		"../.git/packed-refs",
	)
	prf, err := ioutil.ReadFile(prfn)
	if err != nil {
		return
	}
	for len(prf) > 0 {
		a, line, err := bufio.ScanLines(prf, true)
		if err != nil {
			return
		}
		ts := strings.Fields(string(line))
		if ts[len(ts)-1] == r {
			version = fmt.Sprintf(
				"%s %s",
				filepath.Base(rfn),
				ts[0],
			)
			return
		}
		prf = prf[a:]
	}
	return
}

const ConstGenSrc = `// Code generated %s DO NOT EDIT.
package main

const GeneratorMode = %q

const GeneratorBuildVersion = %q
`

const FileVerTemplate = `// Code generated {{.GeneratorName}} DO NOT EDIT.
package main

import (
	"net/http"
)

var {{.HandlerName}} = http.HandlerFunc({{.FuncName}})

func {{.FuncName}}(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, {{printf "%q" .FileName}})
}

func init() {
	http.Handle({{printf "%q" (print "/" .FileName)}}, {{.HandlerName}})
}
`

const BlobVerTemplate = `// Code generated {{.GeneratorName}} DO NOT EDIT.
package main

import (
	"log"
	"net/http"
	"strconv"
	"strings"
)

var {{.HandlerName}} = http.HandlerFunc({{.FuncName}})

var {{.BlobName}} = {{printf "%#v" .Blob}}

func {{.FuncName}}(w http.ResponseWriter, req *http.Request) {

	if DebugMode {
		log.Println(req.Method, req.URL.RequestURI())
	}

	w.Header().Set("Host", req.Host)
	w.Header().Set("Content-Location", {{printf "%q" (print "/" .FileName)}})
	w.Header().Set("Cache-Control", "public, max-age=604800")
	w.Header().Set("ETag", {{printf "%q" (printf "%q" .Hash)}})

	eTags := strings.Split(req.Header.Get("If-None-Match"), ",")
	for _, etag := range eTags {
		if etag == {{printf "%q" (printf "%q" .Hash)}} {
			w.WriteHeader(http.StatusNotModified)
			return
		}
	}

	w.Header().Set("Content-Length", strconv.Itoa(len({{.BlobName}})))
	w.Header().Set("Content-Type", "{{.Mime}}; charset=utf-8")

	if _, err := w.Write({{.BlobName}}); err != nil {
		log.Println(err)
	}

	if f, ok := w.(http.Flusher); ok {
		f.Flush()
	}
}

func init() {
	http.Handle({{printf "%q" (print "/" .FileName)}}, {{.HandlerName}})
}
`
