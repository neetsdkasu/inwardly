//go:generate tsc --build tsconfig.json
//go:generate go run ./gen
package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"fmt"
	"html"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var _ = fmt.Println
var _ = strings.Trim

var Address = "127.8.9.72:8972"

func init() {
	if DebugMode {
		Address = "127.8.8.64:8864"
	}
}

type LogType int

const (
	LogTypeStderr LogType = iota
	LogTypeFile
	LogTypeBoth
	LogTypeNone
)

func (lt *LogType) String() string {
	switch *lt {
	default:
		return ""
	case LogTypeStderr:
		return "stderr"
	case LogTypeFile:
		return "file"
	case LogTypeBoth:
		return "both"
	case LogTypeNone:
		return "none"
	}
}

func (lt *LogType) Set(s string) error {
	switch strings.ToLower(s) {
	default:
		return fmt.Errorf("invalid value")
	case "stderr":
		*lt = LogTypeStderr
	case "file":
		*lt = LogTypeFile
	case "both":
		*lt = LogTypeBoth
	case "none":
		*lt = LogTypeNone
	}
	return nil
}

func logWriterForFile(f *os.File) *bufio.Writer {
	const LogSizeLimit = 1024 * 1024
	r, w := io.Pipe()
	go func() {
		defer r.Close()
		defer w.Close()
		s := bufio.NewScanner(r)
		var total int = 0
		for s.Scan() {
			text := s.Text()
			size, err := fmt.Fprintln(f, text)
			if err != nil {
				if errors.Is(err, os.ErrClosed) {
					return
				}
				panic(err)
			}
			total += size
			if total > LogSizeLimit {
				_, err = f.Seek(0, 0)
				if err != nil {
					if errors.Is(err, os.ErrClosed) {
						return
					}
					panic(err)
				}
				total = 0
			}
		}
		if err := s.Err(); err != nil &&
			!errors.Is(err, io.EOF) &&
			!errors.Is(err, io.ErrClosedPipe) {
			panic(err)
		}
	}()
	return bufio.NewWriter(w)
}

func main() {
	{
		var logType LogType = LogTypeStderr
		var logFile string
		var specialHomeDir string
		defaultLogFile := filepath.Join(os.TempDir(), "inwardly.log")

		flag.Var(&logType, "log", "log type [stderr,file,both,none] (default stderr)")
		flag.StringVar(&logFile, "logfile", defaultLogFile, "logfile path")
		flag.StringVar(&specialHomeDir, "home", "", "home path (for save data)")

		flag.Parse()

		switch logType {
		case LogTypeStderr:
			log.SetOutput(io.MultiWriter(os.Stderr, &memLogger))
		case LogTypeFile:
			if f, err := os.Create(logFile); err != nil {
				panic(err)
			} else {
				defer f.Close()
				w := logWriterForFile(f)
				defer time.Sleep(300 * time.Millisecond)
				defer w.Flush()
				log.SetOutput(io.MultiWriter(w, &memLogger))
			}
		case LogTypeBoth:
			if f, err := os.Create(logFile); err != nil {
				panic(err)
			} else {
				defer f.Close()
				w := logWriterForFile(f)
				defer time.Sleep(300 * time.Millisecond)
				defer w.Flush()
				log.SetOutput(io.MultiWriter(os.Stderr, w, &memLogger))
			}
		case LogTypeNone:
			log.SetOutput(&memLogger)
		}

		if err := homeDir.Init(specialHomeDir); err != nil {
			log.Panic(err)
		}
	}

	log.Println("** inwardly **")
	if err := run(); err != nil {
		log.Panic(err)
	}
	log.Println("finished.")
}

func run() error {
	initList := []interface{ Init() error }{
		&fileManager,
		&appSetting,
		&appState,
		&dataManager,
		&shutdownReceiver,
	}
	for _, x := range initList {
		if err := x.Init(); err != nil {
			return err
		}
	}

	fileSeverConfigs := []struct {
		path, dir string
	}{
		{"/img/", fileManager.imageDir},
		{"/thumb/", fileManager.thumbnailDir},
	}
	for _, cfg := range fileSeverConfigs {
		fs := http.FileServer(http.Dir(cfg.dir))
		http.Handle(
			cfg.path,
			http.StripPrefix(
				cfg.path,
				http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
					if req.Header.Get("If-None-Match") != "" {
						w.WriteHeader(http.StatusNotModified)
						return
					}
					found, err := fileManager.ExistsFile(cfg.dir, req.URL.Path)
					if err != nil {
						badRequest(w, "invalid Path ("+cfg.path+req.URL.Path+")")
						return
					}
					if found {
						w.Header().Set("Cache-Control", "public, max-age=604800, immutable")
						w.Header().Set("ETag", strconv.Quote(html.EscapeString(cfg.path+req.URL.Path)))
					}
					fs.ServeHTTP(w, req)
				}),
			),
		)
	}

	srv := http.Server{Addr: Address}

	idleConnsClosed := make(chan struct{})

	go func() {
		apiShutdown := shutdownReceiver.GetReceiver()

		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		select {
		case <-sigint:
		case <-apiShutdown:
		}
		signal.Stop(sigint)

		if err := srv.Shutdown(context.Background()); err != nil {
			log.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	srv.RegisterOnShutdown(func() {
		log.Println("SHUTDOWN SERVER")
	})

	routineCh := Routine(idleConnsClosed)

	log.Println("START SERVER", Address)

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		return err
	}

	<-routineCh
	<-idleConnsClosed

	return nil
}

func init() {
	list := []struct {
		path    string
		handler http.Handler
	}{
		{path: "/page/"},
		{path: "/thread/"},
		{path: "/mention"},
		{path: "/search"},
		{path: "/result/"},
		{path: "/setting", handler: settingHtmlHandler},
		{path: "/link", handler: linkHtmlHandler},
		{path: "/misc", handler: miscHtmlHandler},
		{path: "/secret", handler: secretHtmlHandler},
	}
	for _, p := range list {
		if p.handler == nil {
			http.Handle(p.path, indexHtmlHandler)
		} else {
			http.Handle(p.path, p.handler)
		}
	}
}

func init() {
	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {

		if DebugMode {
			out, err := httputil.DumpRequest(req, false)
			if err != nil {
				log.Println(err)
				internalServerError(w, err.Error())
				return
			}
			log.Printf("%s\n", out)

			log.Println(req.Method, req.URL.RequestURI())
		}

		if req.URL.RequestURI() != "/" {
			http.NotFound(w, req)
		} else {
			indexHtmlHandler.ServeHTTP(w, req)
		}
	})
}
