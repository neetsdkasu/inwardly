 


////////////////
//
// 初期化処理
//
////////////////

document.querySelector('header > nav button.setting')!
.addEventListener('click', () => {
	window.location.assign('/setting');
});

document.querySelector('header > nav button.link')!
.addEventListener('click', () => {
	window.location.assign('/link');
});

document.querySelector('header > nav button.home')!
.addEventListener('click', () => {
	window.location.assign('/');
});
