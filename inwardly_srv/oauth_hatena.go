package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"mime"
	"net/url"
	"path"
	"path/filepath"
	"strings"
	"time"
)

var _ = log.Println

type (
	HatenaConsumer Consumer

	HatenaRequestToken RequestToken

	HatenaAccessToken struct {
		AccessToken
		UrlName     string `json:"url_name"`
		DisplayName string `json:"display_name"`
	}

	HatenaApplicationsMy struct {
		UrlName         string `json:"url_name"`
		DisplayName     string `json:"display_name"`
		ProfileImageUrl string `json:"profile_image_url"`
	}

	HatenaBlog struct {
		HatenaId string `json:"hatena_id"`
		BlogId   string `json:"blog_id"`
	}

	HatenaBlogEndpoint string
)

const (
	BlogAtom         HatenaBlogEndpoint = "/atom"
	BlogAtomEntry                       = BlogAtom + "/entry"
	BlogAtomCategory                    = BlogAtom + "/category"
)

const (
	BlogTimeFormat = "2006-01-02T15:04:05-07:00"
)

func (this *HatenaConsumer) RequestToken(cb string) (*HatenaRequestToken, error) {
	const u = "https://www.hatena.com/oauth/initiate"

	form := url.Values{}
	form.Add(
		"scope",
		strings.Join(
			[]string{
				"read_public",
				"write_public",
				"read_private",
				"write_private",
			},
			",",
		),
	)

	req, _, err := this.GetConsumer().TakeRequestToken(u, cb, form)

	return (*HatenaRequestToken)(req), err
}

func (this *HatenaConsumer) AccessToken(reqToken *HatenaRequestToken, verifier string) (*HatenaAccessToken, error) {
	const u = "https://www.hatena.com/oauth/token"

	acc, resp, err := this.GetConsumer().TakeAccessToken(u, reqToken.GetRequestToken(), verifier)
	if err != nil {
		return nil, err
	}

	values := resp.(url.Values)

	return &HatenaAccessToken{
		AccessToken: *acc,
		UrlName:     values.Get("url_name"),
		DisplayName: values.Get("display_name"),
	}, nil
}

func (this *HatenaConsumer) ApplicationsMy(accToken *HatenaAccessToken) (*HatenaApplicationsMy, error) {
	const u = "https://n.hatena.com/applications/my.json"

	blob, _, err := this.GetConsumer().Get(u, accToken.GetAccessToken())
	if err != nil {
		return nil, err
	}

	my := new(HatenaApplicationsMy)

	err = json.Unmarshal(blob, my)

	return my, err
}

func (this *HatenaConsumer) FotolifeFeed(accToken *HatenaAccessToken) (interface{}, error) {
	const u = "https://f.hatena.ne.jp/atom/feed"

	blob, h, err := this.GetConsumer().Get(u, accToken.GetAccessToken())
	if err != nil {
		wa := h.Get("WWW-Authenticate")
		if strings.Contains(wa, "WSSE") {
			return "wsse", err
		}
		return nil, err
	}

	err = ioutil.WriteFile("fotolife_feed.xml", blob, 0664)

	return "SAVE TO fotolife_feed.xml", err
}

func (this *HatenaConsumer) FotolifePost(accToken *HatenaAccessToken, filename string, img []byte) (*HatenaFotolifePostResult, error) {
	const u = "https://f.hatena.ne.jp/atom/post"
	contentType := mime.TypeByExtension(".xml")
	if contentType == "" {
		contentType = "application/xml"
	}
	ext := filepath.Ext(filename)
	mediaType := mime.TypeByExtension(ext)
	switch ext {
	default:
		return nil, fmt.Errorf("unsupport file type: '%s' (%s)", ext, mediaType)
	case ".png":
		if mediaType == "" {
			mediaType = "image/png"
		}
	case ".jpg":
		if mediaType == "" {
			mediaType = "image/jpeg"
		}
	}
	content := base64.StdEncoding.EncodeToString(img)
	flp := HatenaFotolifePostBody{}
	flp.Title = filename
	flp.Content.Mode = "base64"
	flp.Content.Type = mediaType
	flp.Content.Value = content
	flp.Generator = "inwardly"
	var b bytes.Buffer
	if _, err := b.WriteString(xml.Header); err != nil {
		return nil, err
	}
	enc := xml.NewEncoder(&b)
	if err := enc.Encode(&flp); err != nil {
		return nil, err
	}
	if err := enc.Flush(); err != nil {
		return nil, err
	}
	body := b.Bytes()

	if DebugModeHatenaAPI {
		err := ioutil.WriteFile("fotolife_post_body.xml", body, 0664)
		if err != nil {
			return nil, err
		}
		log.Println("SAVE TO fotolife_post_body.xml")
	}

	blob, _, err := this.GetConsumer().Post(u, accToken.GetAccessToken(), contentType, body)
	if err != nil {
		return nil, err
	}

	if DebugModeHatenaAPI {
		err = ioutil.WriteFile("fotolife_post_result.xml", blob, 0664)
		if err != nil {
			return nil, err
		}
		log.Println("SAVE TO fotolife_post_result.xml")
	}

	res := &HatenaFotolifePostResult{}
	err = xml.Unmarshal(blob, res)
	if err != nil {
		return nil, err
	}

	return res, err
}

func (this *HatenaConsumer) BlogAtom(accToken *HatenaAccessToken, blog *HatenaBlog) (interface{}, error) {
	u := BlogAtom.Url(blog)

	blob, _, err := this.GetConsumer().Get(u, accToken.GetAccessToken())
	if err != nil {
		return nil, err
	}

	if DebugModeHatenaAPI {
		err = ioutil.WriteFile("blog_atom.xml", blob, 0664)
		if err != nil {
			return nil, err
		}
	}

	res := &HatenaBlogAtomResult{}

	err = xml.Unmarshal(blob, res)
	if err != nil && DebugModeHatenaAPI {
		return "SAVE TO blog_atom.xml", err
	}

	return res, err
}

func (this *HatenaConsumer) BlogAtomCategory(accToken *HatenaAccessToken, blog *HatenaBlog) (interface{}, error) {
	u := BlogAtomCategory.Url(blog)

	blob, _, err := this.GetConsumer().Get(u, accToken.GetAccessToken())
	if err != nil {
		return nil, err
	}

	if DebugModeHatenaAPI {
		err = ioutil.WriteFile("blog_atom_category.xml", blob, 0664)
		if err != nil {
			return nil, err
		}
	}

	res := &HatenaBlogAtomCategoryResult{}

	err = xml.Unmarshal(blob, res)
	if err != nil && DebugModeHatenaAPI {
		return "SAVE TO blog_atom_category.xml", err
	}

	return res, err
}

func (this *HatenaConsumer) BlogAtomEntryGet(accToken *HatenaAccessToken, blog *HatenaBlog, nextPageId string) (interface{}, error) {
	u := BlogAtomEntry.Url(blog)

	if nextPageId != "" {
		u += "?" + nextPageId
	}

	blob, _, err := this.GetConsumer().Get(u, accToken.GetAccessToken())
	if err != nil {
		return nil, err
	}

	if DebugModeHatenaAPI {
		err = ioutil.WriteFile("blog_atom_entry_get.xml", blob, 0664)
		if err != nil {
			return nil, err
		}
	}

	res := &HatenaBlogAtomEntryGetResult{}

	err = xml.Unmarshal(blob, res)
	if err != nil && DebugModeHatenaAPI {
		return "SAVE TO blog_atom_entry_get.xml", err
	}

	return res, err
}

func (this *HatenaConsumer) BlogAtomEntryPost(accToken *HatenaAccessToken, blog *HatenaBlog, title, date, blogBody string, categories []string) (*HatenaBlogEntryResult, error) {
	u := BlogAtomEntry.Url(blog)
	contentType := mime.TypeByExtension(".xml")
	if contentType == "" {
		contentType = "application/xml"
	}

	if date == "" {
		date = time.Now().Local().Format(BlogTimeFormat)
	} else if _, err := time.Parse(BlogTimeFormat, date); err != nil {
		return nil, err
	}

	entry := HatenaBlogAtomEntryPostBody{}
	entry.XmlNS = "http://www.w3.org/2007/app"
	entry.Author = blog.HatenaId
	entry.Title = title
	entry.Updated = date
	for _, c := range categories {
		cat := &HatenaBlogCategory{Term: c}
		entry.Categories = append(entry.Categories, cat)
	}
	entry.Content.Type = "text/plain"
	entry.Content.Value = blogBody
	entry.Draft = "no"

	var b bytes.Buffer
	if _, err := b.WriteString(xml.Header); err != nil {
		return nil, err
	}
	enc := xml.NewEncoder(&b)
	if err := enc.Encode(&entry); err != nil {
		return nil, err
	}
	if err := enc.Flush(); err != nil {
		return nil, err
	}
	body := b.Bytes()

	blob, _, err := this.GetConsumer().Post(u, accToken.GetAccessToken(), contentType, body)
	if err != nil {
		return nil, err
	}

	if DebugModeHatenaAPI {
		err = ioutil.WriteFile("blog_atom_entry_post.xml", blob, 0664)
		if err != nil {
			return nil, err
		}
		log.Println("SAVE TO blog_atom_entry_post.xml")
	}

	res := &HatenaBlogEntryResult{}

	err = xml.Unmarshal(blob, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (this *HatenaConsumer) BlogAtomEntryIdGet(accToken *HatenaAccessToken, blog *HatenaBlog, id string) (interface{}, error) {
	u := BlogAtomEntry.Url(blog, id)

	blob, _, err := this.GetConsumer().Get(u, accToken.GetAccessToken())
	if err != nil {
		return nil, err
	}

	output := fmt.Sprintf("blog_atom_entry_%s_get.xml", id)

	if DebugModeHatenaAPI {
		err = ioutil.WriteFile(output, blob, 0664)
		if err != nil {
			return nil, err
		}
	}

	res := &HatenaBlogEntryResult{}

	err = xml.Unmarshal(blob, res)
	if err != nil && DebugModeHatenaAPI {
		return "SAVE TO " + output, err
	}

	return res, err
}

func (this *HatenaConsumer) BlogAtomEntryIdDelete(accToken *HatenaAccessToken, blog *HatenaBlog, id string) error {
	u := BlogAtomEntry.Url(blog, id)

	blob, _, err := this.GetConsumer().Delete(u, accToken.GetAccessToken(), "", nil)
	if err != nil {
		return err
	}

	if len(blob) == 0 {
		return nil
	}

	if DebugModeHatenaAPI {
		output := fmt.Sprintf("blog_atom_entry_%s_delete.xml", id)
		err = ioutil.WriteFile(output, blob, 0664)
		if err != nil {
			return err
		}
		log.Println("SAVE TO " + output)
	}

	return nil
}

func (this *HatenaConsumer) BlogAtomEntryIdPut(accToken *HatenaAccessToken, blog *HatenaBlog, id, title, date, blogBody string, categories []string) (*HatenaBlogEntryResult, error) {
	u := BlogAtomEntry.Url(blog, id)
	contentType := mime.TypeByExtension(".xml")
	if contentType == "" {
		contentType = "application/xml"
	}

	if date == "" {
		date = time.Now().Local().Format(BlogTimeFormat)
	} else if _, err := time.Parse(BlogTimeFormat, date); err != nil {
		return nil, err
	}

	entry := HatenaBlogAtomEntryPostBody{}
	entry.XmlNS = "http://www.w3.org/2007/app"
	entry.Author = blog.HatenaId
	entry.Title = title
	entry.Updated = date
	for _, c := range categories {
		cat := &HatenaBlogCategory{Term: c}
		entry.Categories = append(entry.Categories, cat)
	}
	entry.Content.Type = "text/plain"
	entry.Content.Value = blogBody
	entry.Draft = "no"

	var b bytes.Buffer
	if _, err := b.WriteString(xml.Header); err != nil {
		return nil, err
	}
	enc := xml.NewEncoder(&b)
	if err := enc.Encode(&entry); err != nil {
		return nil, err
	}
	if err := enc.Flush(); err != nil {
		return nil, err
	}
	body := b.Bytes()

	blob, _, err := this.GetConsumer().Put(u, accToken.GetAccessToken(), contentType, body)
	if err != nil {
		return nil, err
	}

	if DebugModeHatenaAPI {
		output := fmt.Sprintf("blog_atom_entry_%s_put.xml", id)
		err = ioutil.WriteFile(output, blob, 0664)
		if err != nil {
			return nil, err
		}
		log.Println("SAVE TO " + output)
	}

	res := &HatenaBlogEntryResult{}

	err = xml.Unmarshal(blob, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (this *HatenaConsumer) GetConsumer() *Consumer {
	return (*Consumer)(this)
}

func (this *HatenaRequestToken) GetRequestToken() *RequestToken {
	return (*RequestToken)(this)
}

func (this *HatenaAccessToken) GetAccessToken() *AccessToken {
	return &this.AccessToken
}

func (this HatenaBlogEndpoint) Url(blog *HatenaBlog, options ...string) string {
	return "https://" + path.Join(append([]string{"blog.hatena.ne.jp", blog.HatenaId, blog.BlogId, string(this)}, options...)...)
}

func (this *HatenaRequestToken) AuthorizeUrl() string {
	token := PercentEncode(this.Token)
	endPoint := "/oauth/authorize?oauth_token=" + token
	return "https://www.hatena.ne.jp" + endPoint
}
