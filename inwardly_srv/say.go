package main

import (
	"fmt"
	"html"
	"index/suffixarray"
	"log"
	"mime/multipart"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

const (
	DummyAddress = "inwardly"
	DummyBaseUrl = "http://" + DummyAddress + "/"
)

type SayResponse struct {
	SaidView *SaidView `json:"said"`
	Memo     *Memo     `json:"memo"`
}

const (
	SayRequestTypeSay   = "say"
	SayRequestTypeReply = "reply"
	SayRequestTypeEdit  = "edit"
)

type SayImageFile struct {
	Changed  bool
	FileName string
	File     multipart.File
	Header   *multipart.FileHeader
}

type SayRequest struct {
	Time      time.Time
	Type      string
	Target    string
	Text      string
	ImageList []*SayImageFile
	Secret    bool
}

func init() {
	// POST
	//  text    string (non empty)
	http.HandleFunc("/api/say", sayHandleFunc)

	// POST
	//  target  string (non empty && said ID)
	//  text    string (non empty)
	http.HandleFunc("/api/reply", replyHandleFunc)

	// POST
	//  target  string (non empty && said ID)
	//  text    string (non empty)
	http.HandleFunc("/api/said/edit", saidEditHandleFunc)
}

func sayHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSayHandler {
		log.Println("sayHandleFunc")
		defer log.Println("defer sayHandleFunc")
	}
	sayCommon(w, req, SayRequestTypeSay)
}

func replyHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSayHandler {
		log.Println("replyHandleFunc")
		defer log.Println("defer replyHandleFunc")
	}
	sayCommon(w, req, SayRequestTypeReply)
}

func saidEditHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSayHandler {
		log.Println("saidEditHandleFunc")
		defer log.Println("defer saidEditHandleFunc")
	}
	sayCommon(w, req, SayRequestTypeEdit)
}

func sayCommon(w http.ResponseWriter, req *http.Request, sayType string) {

	var sreq SayRequest
	if ok := parseSayRequest(req, &sreq, sayType); !ok {
		defer sreq.Close()
		// TODO 失敗理由ちゃんと取得するべきかも
		badRequest(w, "invalid Form Paramaters")
		return
	}

	said, memo, err := dataManager.Say(&sreq)
	if err != nil {
		defer sreq.Close()
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	view := said.View()

	err = view.AttachQuotes()

	if err != nil {
		defer sreq.Close()
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	fileManager.SaveImages(sreq.ImageList)

	response := SayResponse{
		SaidView: view,
		Memo:     memo,
	}

	writeJson(w, req, &response)
}

func parseSayRequest(req *http.Request, sreq *SayRequest, sayType string) bool {
	if DebugModeSayHandler {
		log.Println("parseSayRequest")
		defer log.Println("defer parseSayRequest")
	}
	now := time.Now().Local()
	if req.Method != http.MethodPost {
		return false
	}
	target := req.PostFormValue("target")
	text := req.PostFormValue("text")
	secret := req.PostFormValue("secret")
	switch sayType {
	default:
		return false
	case SayRequestTypeSay:
		if target != "" {
			return false
		}
	case SayRequestTypeEdit, SayRequestTypeReply:
		if target == "" {
			return false
		}
	}
	sreq.ImageList = []*SayImageFile{}
	for i := 0; i < 4; i++ {
		key := "img" + strconv.Itoa(i)
		changed := true
		if sayType == SayRequestTypeEdit {
			c := req.PostFormValue(key + "changed")
			if c == "0" {
				changed = false
			} else if c != "1" {
				return false
			}
		}
		f, h, err := req.FormFile(key)
		if err != nil {
			if err != http.ErrMissingFile {
				log.Println(err)
				return false
			}
			if sayType == SayRequestTypeEdit {
				sreq.ImageList = append(sreq.ImageList, &SayImageFile{
					Changed: changed,
				})
			}
			continue
		}
		sreq.ImageList = append(sreq.ImageList, &SayImageFile{
			Changed: changed,
			File:    f,
			Header:  h,
		})
	}
	if strings.TrimSpace(text) == "" && len(sreq.ImageList) == 0 {
		return false
	}
	host := req.Host
	if host == "" {
		host = Address
	}
	sreq.Time = now
	sreq.Type = sayType
	sreq.Target = target
	sreq.Secret = secret != ""
	sreq.Text = strings.ReplaceAll(
		strings.ReplaceAll(
			strings.ReplaceAll(text, "\r\n", "\n"),
			"\r", "\n",
		),
		"http://"+host+"/",
		DummyBaseUrl,
	)
	return true
}

func (this *SayRequest) MakeId() string {
	return MakeSaidIdFromTime(this.Time)
}

func (this *SayRequest) Date() string {
	return ToDateString(this.Time)
}

func (this *SayRequest) Compose() *Said {
	if DebugModeSayHandler {
		log.Println("SayRequest.Compose")
		defer log.Println("defer SayRequest.Compose")
	}
	said := new(Said)
	switch this.Type {
	case SayRequestTypeSay:
		said.Id = this.MakeId()
		said.Date = this.Date()
	case SayRequestTypeEdit:
		said.Id = this.Target
		said.Update = this.Date()
	case SayRequestTypeReply:
		said.Id = this.MakeId()
		said.Date = this.Date()
		said.Parent = this.Target
	}
	said.Secret = this.Secret
	said.Text = this.Text
	said.TextElements = this.TextElements()
	imageCount := 0
	for _, sif := range this.ImageList {
		if !sif.Changed {
			said.ImageList = append(said.ImageList, ".")
			continue
		}
		if sif.Header == nil {
			said.ImageList = append(said.ImageList, "")
			continue
		}
		e := filepath.Ext(sif.Header.Filename)
		n := appState.ImageNumber.GetNext()
		imf := fmt.Sprintf("%s_%04x%s", said.Id, n, e)
		said.ImageList = append(said.ImageList, imf)
		sif.FileName = imf
		imageCount++
	}
	if imageCount > 0 {
		appState.Save()
	}
	return said
}

// Lock: dataManager.GetSaidById() in DaySayListSet.GetSaidById
func (this *SayRequest) makeTextElementForInside(p string, lockedList []DaySayListSetEntry) (*TextElement, error) {
	if strings.HasPrefix(p, "img/") {
		fn := strings.TrimPrefix(p, "img/")
		found, err := fileManager.ExistsImg(fn)
		if !found {
			return nil, err
		}
		return &TextElement{
			Type: TextElementTypeInsideImg,
			Src:  fn,
		}, err
	} else if strings.HasPrefix(p, "thumb/") {
		tfn := strings.TrimPrefix(p, "thumb/")
		be := strings.SplitN(tfn, ".", 2)
		if len(be) != 2 {
			return nil, nil
		}
		ifn, err := fileManager.GetImgFilenameByBasename(be[0])
		if ifn == "" {
			return nil, err
		}
		return &TextElement{
			Type: TextElementTypeInsideImg,
			Src:  ifn,
		}, err
	} else if strings.HasPrefix(p, "thread/") {
		ids := strings.SplitN(strings.TrimPrefix(p, "thread/"), "#said", 2)
		id := ids[len(ids)-1]
		if this.Type == SayRequestTypeEdit && strings.Compare(id, this.Target) >= 0 {
			// 未来引用禁止
			return nil, nil
		}
		s, err := DaySayListSet(lockedList).GetSaidById(id)
		if err != nil {
			return nil, err
		}
		if len(ids) == 1 {
			return &TextElement{
				Type: TextElementTypeInsideQuote,
				Src:  "/" + p + "\n" + ids[0],
			}, nil
		}
		if len(ids) == 2 {
			if s.Root != "" && ids[0] == s.Root {
				return &TextElement{
					Type: TextElementTypeInsideQuote,
					Src:  "/" + p + "\n" + ids[1],
				}, nil
			}
			if s.Root == "" && ids[0] == ids[1] {
				return &TextElement{
					Type: TextElementTypeInsideQuote,
					Src:  "/" + p + "\n" + ids[0],
				}, nil
			}
		}
	}
	return nil, nil
}

// Lock: dataManager.GetSaidById() in makeTextElementForInside
func (this *SayRequest) TextElements(lockedList ...DaySayListSetEntry) (ret []TextElement) {
	if DebugModeSayHandler {
		log.Println("SayRequest.TextElements")
		defer log.Println("defer SayRequest.TextElements")
	}
	lines := strings.SplitAfter(this.Text, "\n")
	for _, line := range lines {
		buf := []byte(line)
		index := suffixarray.New(buf)
		offsets := index.FindAllIndex(urlRE, -1)
		pos := 0
		for _, off := range offsets {
			s, e := off[0], off[1]
			display := string(buf[s:e])
			href, err := ToSafeUrl(display)
			if err != nil {
				if !strings.HasPrefix(display, DummyBaseUrl) {
					continue
				}
				href = html.UnescapeString(display)
			}
			if pos < s {
				text := string(buf[pos:s])
				ret = append(ret, TextElement{
					Type: TextElementTypeText,
					Src:  text,
				})
			}
			if strings.HasPrefix(href, DummyBaseUrl) {
				display = strings.TrimPrefix(display, DummyBaseUrl)
				te, err := this.makeTextElementForInside(display, lockedList)
				if err != nil {
					log.Println(err)
				}
				if te != nil {
					ret = append(ret, *te)
				} else {
					href = "/" + strings.TrimPrefix(href, DummyBaseUrl)
					ret = append(ret, TextElement{
						Type: TextElementTypeInsideUrl,
						Src:  href + "\n/" + display,
					})
				}
			} else if href == display {
				ret = append(ret, TextElement{
					Type: TextElementTypeUrl,
					Src:  href,
				})
			} else {
				ret = append(ret, TextElement{
					Type: TextElementTypeUrl,
					Src:  href + "\n" + display,
				})
			}
			pos = e
		}
		if pos < len(buf) {
			text := string(buf[pos:])
			ret = append(ret, TextElement{
				Type: TextElementTypeText,
				Src:  text,
			})
		}
	}
	return
}

func (this *SayRequest) Close() {
	if DebugModeSayHandler {
		log.Println("SayRequest.Close")
		defer log.Println("defer SayRequest.Close")
	}
	for _, img := range this.ImageList {
		f := img.File
		if f != nil {
			err := f.Close()
			if err != nil {
				log.Println(err)
			}
		}
	}
}
