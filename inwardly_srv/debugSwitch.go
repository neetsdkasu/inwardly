package main

const (
	DebugMode = false

	DebugModeFilePath = false && DebugMode

	DebugModeShowInit = false && DebugMode

	DebugModeOAuth = false && DebugMode

	DebugModeRoutine         = false && DebugMode
	DebugModeUploadBlog      = false && DebugMode
	DebugModeReUploadBlogAll = false && DebugMode
	DebugModeUploadThread    = false && DebugMode
	DebugModeCleanCache      = false && DebugMode

	DebugModeHatenaAPI = false && DebugMode

	DebugModeFileManager          = false && DebugMode
	DebugModeFileAccessController = false && DebugMode

	DebugModeDataManager      = false && DebugMode
	DebugModeDaySaidListStore = false && DebugMode
	DebugModeMemoListStore    = false && DebugMode

	DebugModeSayHandler        = false && DebugMode
	DebugModeSaidDeleteHandler = false && DebugMode
	DebugModePageHandler       = false && DebugMode
	DebugModeThreadHandler     = false && DebugMode
	DebugModeMemoListHandler   = false && DebugMode
	DebugModeHistoryHandler    = false && DebugMode
	DebugModeRecoverHandler    = false && DebugMode
	DebugModeCalenderHandler   = false && DebugMode
	DebugModeQuotedRefsHandler = false && DebugMode
	DebugModeDocsHandler       = false && DebugMode
	DebugModeStatusHandler     = false && DebugMode

	DebugModeAppState   = false && DebugMode
	DebugModeAppSetting = false && DebugMode

	DebugModeShutdown = false && DebugMode

	DebugModeMemLogger = false && DebugMode

	DebugModeSecretHandler = false && DebugMode
)
