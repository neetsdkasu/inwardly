package main

import (
	"log"
	"time"
)

func Routine(idleConnsClosed chan struct{}) chan struct{} {
	interval := 5 * time.Minute
	if DebugModeUploadBlog || DebugModeCleanCache {
		interval = time.Minute
	}
	ch := make(chan struct{})
	go func() {
		log.Println("START ROUTINE")
		defer log.Println("STOPPED ROUTINE")
		for {
			appState.SetValue(rtvRoutineEndTime, time.Now().Local())
			select {
			case <-idleConnsClosed:
				close(ch)
				return
			case <-time.After(interval):
				if !(DebugModeUploadBlog || DebugModeCleanCache) {
					interval = time.Hour
				}
			}
			appState.SetValue(rtvRoutineStartTime, time.Now().Local())

			if DebugModeRoutine {
				log.Println("ROUTINE: hourly routine")
			}

			if err := routineUploadBlog(); err != nil {
				log.Println(err)
			}

			if err := routineUploadThread(); err != nil {
				log.Println(err)
			}

			expire := time.Now().Local().AddDate(0, 0, -1)
			if DebugModeCleanCache {
				expire = time.Now().Local().Add(-5 * time.Minute)
			}
			if err := dataManager.CleanCache(expire); err != nil {
				log.Println(err)
			}
			fileManager.CleanCache(expire)
		}
	}()
	return ch
}
