package main

import (
	"fmt"
	"log"
	"net/http"
	"path"
	"strings"
	"sync"
	"time"
)

type DocumentLinkState struct {
	Id                int      `json:"id"`
	EntryName         string   `json:"entry"`
	MarkedFiles       []string `json:"marked_files"`
	MarkedDirectories []string `json:"marked_directories"`
	CurrentPath       []string `json:"current"`
	Extensions        []string `json:"exts"`
	Directories       []string `json:"directories"`
	Files             []string `json:"files"`
}

type DocumentLinkStateList []*DocumentLinkState
type SecretInfoStoreList []*SecretInfoStore

type RuntimeValue int

const (
	rtvRoutineStartTime RuntimeValue = iota
	rtvRoutineEndTime
)

type SystemStatus struct {
	RoutineStartTime string `json:"routine_start_time"`
	RoutineEndTime   string `json:"routine_end_time"`
	BuildVersion     string `json:"build_version"`
	StaticFiles      string `json:"static_files"`
	ShutdownToken    string `json:"shutdown_token"`
}

type Status struct {
	SystemStatus *SystemStatus `json:"system"`
}

type UpgradeFlag uint64

const (
	UpgradeBlogPostDatetimeValue UpgradeFlag = 1 << iota
)

type AppStateStore struct {
	ImageNumber        int    `json:"imagenumber"`
	DocumentLinkNumber int    `json:"documentlinknumber"`
	SecretImageNumber  int    `json:"secretimagenumber"`
	UpgradeFlag        uint64 `json:"upgrade_flag"`
}

func (this *AppStateStore) Init() {
	this.ImageNumber = 0
	this.DocumentLinkNumber = 0
	this.SecretImageNumber = 0
}

type AppState struct {
	ImageNumber            TotalNumber
	DocumentLinkNumber     TotalNumber
	SecretImageNumber      TotalNumber
	DocumentLinkStateStore struct {
		Table map[string]*DocumentLinkState
		sync.Mutex
	}
	UpgradeFlag struct {
		Value uint64
		sync.Mutex
	}
	SecretState struct {
		List []*SecretInfo
		sync.Mutex
	}
	readySecret   sync.Map
	runtimeValues sync.Map
}

var appState AppState

func (*DocumentLinkStateList) Init() {}
func (*SecretInfoStoreList) Init()   {}

func (this *AppState) Init() error {
	if DebugModeShowInit || DebugModeAppState {
		log.Println("AppState.Init")
	}
	var state AppStateStore
	err := fileManager.LoadAppState(&state)
	if err != nil {
		return err
	}
	this.ImageNumber.Set(state.ImageNumber)
	this.DocumentLinkNumber.Set(state.DocumentLinkNumber)
	this.SecretImageNumber.Set(state.SecretImageNumber)
	this.UpgradeFlag.Value = state.UpgradeFlag

	docLinkList := DocumentLinkStateList{}
	err = fileManager.LoadDocumentLinkState(&docLinkList)
	if err != nil {
		return err
	}
	docLinkTable := map[string]*DocumentLinkState{}
	for _, ds := range docLinkList {
		docLinkTable[ds.EntryName] = ds
	}
	this.DocumentLinkStateStore.Table = docLinkTable

	secretInfoStoreList := SecretInfoStoreList{}
	err = fileManager.LoadSecretInfo(&secretInfoStoreList)
	if err != nil {
		return err
	}
	secretInfoList := []*SecretInfo{}
	for _, sis := range secretInfoStoreList {
		si := new(SecretInfo)
		si.store = sis
		secretInfoList = append(secretInfoList, si)
	}
	this.SecretState.List = secretInfoList

	return nil
}

func (this *AppState) Store(store *AppStateStore) {
	store.ImageNumber = this.ImageNumber.Current()
	store.DocumentLinkNumber = this.DocumentLinkNumber.Current()
	store.SecretImageNumber = this.SecretImageNumber.Current()
	store.UpgradeFlag = this.GetUpgradeFlag()
}

func (this *AppState) Save() {
	fileManager.SaveAppState()
}

func (this *AppState) GetUpgradeFlag() uint64 {
	this.UpgradeFlag.Lock()
	defer this.UpgradeFlag.Unlock()
	return this.UpgradeFlag.Value
}

func (this *AppState) GetDocumentLinkState(entry string) (*DocumentLinkState, bool) {
	if DebugModeAppState {
		log.Println("AppState.GetDocumentLinkState")
		defer log.Println("defer AppState.GetDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	ds, ok := this.DocumentLinkStateStore.Table[entry]
	return ds, ok
}

func (this *AppState) GetDocumentLinkStateList() DocumentLinkStateList {
	if DebugModeAppState {
		log.Println("AppState.GetDocumentLinkStateList")
		defer log.Println("defer AppState.GetDocumentLinkStateList")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	docLinkList := make(DocumentLinkStateList, 0, len(this.DocumentLinkStateStore.Table))
	for _, ds := range this.DocumentLinkStateStore.Table {
		docLinkList = append(docLinkList, ds)
	}
	return docLinkList
}

func (this *AppState) AddMarkDirDocumentLinkState(id int, entryName string) (*DocumentLinkState, error) {
	if DebugModeAppState {
		log.Println("AppState.AddMarkDirDocumentLinkState")
		defer log.Println("defer AppState.AddMarkDirDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	oldDs, ok := this.DocumentLinkStateStore.Table[entryName]
	if !ok {
		return nil, BadRequestError{
			fmt.Sprint("not found entry name: ", entryName),
		}
	}
	if oldDs.Id != id {
		return nil, BadRequestError{
			fmt.Sprintf("invalid id: %d (EntryName: %s)", id, entryName),
		}
	}
	if len(oldDs.CurrentPath) == 0 {
		return nil, BadRequestError{
			fmt.Sprintf("root path is not markable (EntryName: %s)", entryName),
		}
	}
	newMarkedDir := "/" + path.Join(oldDs.CurrentPath...)
	for _, md := range oldDs.MarkedDirectories {
		if md == newMarkedDir {
			return nil, BadRequestError{
				fmt.Sprint("already existed: ", newMarkedDir),
			}
		}
	}
	delete(this.DocumentLinkStateStore.Table, entryName)
	defer fileManager.SaveDocumentLinkState()
	newDs := new(DocumentLinkState)
	*newDs = *oldDs
	newDs.MarkedDirectories = append(
		newDs.MarkedDirectories,
		newMarkedDir,
	)
	this.DocumentLinkStateStore.Table[entryName] = newDs
	return newDs, nil
}

func (this *AppState) AddMarkFileDocumentLinkState(id int, entryName, fileName string) (*DocumentLinkState, error) {
	if DebugModeAppState {
		log.Println("AppState.AddMarkFileDocumentLinkState")
		defer log.Println("defer AppState.AddMarkFileDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	oldDs, ok := this.DocumentLinkStateStore.Table[entryName]
	if !ok {
		return nil, BadRequestError{
			fmt.Sprint("not found entry name: ", entryName),
		}
	}
	if oldDs.Id != id {
		return nil, BadRequestError{
			fmt.Sprintf("invalid id: %d (EntryName: %s)", id, entryName),
		}
	}
	found := false
	for _, f := range oldDs.Files {
		if f == fileName {
			found = true
			break
		}
	}
	if !found {
		return nil, BadRequestError{
			fmt.Sprintf("not found file: %s (EntryName: %s)", fileName, entryName),
		}
	}
	newMarkedFile := path.Join("/", path.Join(oldDs.CurrentPath...), fileName)
	for _, mf := range oldDs.MarkedFiles {
		if mf == newMarkedFile {
			return nil, BadRequestError{
				fmt.Sprint("already existed: ", newMarkedFile),
			}
		}
	}
	delete(this.DocumentLinkStateStore.Table, entryName)
	defer fileManager.SaveDocumentLinkState()
	newDs := new(DocumentLinkState)
	*newDs = *oldDs
	newDs.MarkedFiles = append(
		newDs.MarkedFiles,
		newMarkedFile,
	)
	this.DocumentLinkStateStore.Table[entryName] = newDs
	return newDs, nil
}

func (this *AppState) RemoveMarkDirDocumentLinkState(id int, entryName, path string) (*DocumentLinkState, error) {
	if DebugModeAppState {
		log.Println("AppState.RemoveMarkDirDocumentLinkState")
		defer log.Println("defer AppState.RemoveMarkDirDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	oldDs, ok := this.DocumentLinkStateStore.Table[entryName]
	if !ok {
		return nil, BadRequestError{
			fmt.Sprint("not found entry name: ", entryName),
		}
	}
	if oldDs.Id != id {
		return nil, BadRequestError{
			fmt.Sprintf("invalid id: %d (EntryName: %s)", id, entryName),
		}
	}
	found := false
	markedDirectories := make([]string, 0, len(oldDs.MarkedDirectories))
	for i, mf := range oldDs.MarkedDirectories {
		if mf == path {
			found = true
			markedDirectories = append(markedDirectories, oldDs.MarkedDirectories[:i]...)
			markedDirectories = append(markedDirectories, oldDs.MarkedDirectories[i+1:]...)
			break
		}
	}
	if !found {
		return nil, BadRequestError{
			fmt.Sprintf("not found marked dir: %s (EntryName: %s)", path, entryName),
		}
	}
	delete(this.DocumentLinkStateStore.Table, entryName)
	defer fileManager.SaveDocumentLinkState()
	newDs := new(DocumentLinkState)
	*newDs = *oldDs
	newDs.MarkedDirectories = markedDirectories
	this.DocumentLinkStateStore.Table[entryName] = newDs
	return newDs, nil
}

func (this *AppState) RemoveMarkFileDocumentLinkState(id int, entryName, path string) (*DocumentLinkState, error) {
	if DebugModeAppState {
		log.Println("AppState.RemoveMarkFileDocumentLinkState")
		defer log.Println("defer AppState.RemoveMarkFileDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	oldDs, ok := this.DocumentLinkStateStore.Table[entryName]
	if !ok {
		return nil, BadRequestError{
			fmt.Sprint("not found entry name: ", entryName),
		}
	}
	if oldDs.Id != id {
		return nil, BadRequestError{
			fmt.Sprintf("invalid id: %d (EntryName: %s)", id, entryName),
		}
	}
	found := false
	markedFiles := make([]string, 0, len(oldDs.MarkedFiles))
	for i, mf := range oldDs.MarkedFiles {
		if mf == path {
			found = true
			markedFiles = append(markedFiles, oldDs.MarkedFiles[:i]...)
			markedFiles = append(markedFiles, oldDs.MarkedFiles[i+1:]...)
			break
		}
	}
	if !found {
		return nil, BadRequestError{
			fmt.Sprintf("not found marked file: %s (EntryName: %s)", path, entryName),
		}
	}
	delete(this.DocumentLinkStateStore.Table, entryName)
	defer fileManager.SaveDocumentLinkState()
	newDs := new(DocumentLinkState)
	*newDs = *oldDs
	newDs.MarkedFiles = markedFiles
	this.DocumentLinkStateStore.Table[entryName] = newDs
	return newDs, nil
}

func (this *AppState) AddExtensionsDocumentLinkState(id int, entryName, extensions string) (*DocumentLinkState, error) {
	if DebugModeAppState {
		log.Println("AppState.AddExtensionsDocumentLinkState")
		defer log.Println("defer AppState.AddExtensionsDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	oldDs, ok := this.DocumentLinkStateStore.Table[entryName]
	if !ok {
		return nil, BadRequestError{
			fmt.Sprint("not found entry name: ", entryName),
		}
	}
	if oldDs.Id != id {
		return nil, BadRequestError{
			fmt.Sprintf("invalid id: %d (EntryName: %s)", id, entryName),
		}
	}
	exts := append([]string{}, oldDs.Extensions...)
	for _, token := range strings.Split(extensions, ",") {
		for _, newExt := range strings.Fields(token) {
			if newExt == "" {
				continue
			}
			for _, oldExt := range oldDs.Extensions {
				if newExt == oldExt {
					return nil, BadRequestError{
						fmt.Sprint("already existed: ", newExt),
					}
				}
			}
			exts = append(exts, newExt)
		}
	}
	if len(exts) == 0 {
		return nil, BadRequestError{
			fmt.Sprint("missing extensions"),
		}
	}
	delete(this.DocumentLinkStateStore.Table, entryName)
	defer fileManager.SaveDocumentLinkState()
	newDs := new(DocumentLinkState)
	*newDs = *oldDs
	newDs.Extensions = exts
	this.DocumentLinkStateStore.Table[entryName] = newDs
	return newDs, nil
}

func (this *AppState) RemoveExtensionDocumentLinkState(id int, entryName, extension string) (*DocumentLinkState, error) {
	if DebugModeAppState {
		log.Println("AppState.RemoveExtensionDocumentLinkState")
		defer log.Println("defer AppState.RemoveExtensionDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	oldDs, ok := this.DocumentLinkStateStore.Table[entryName]
	if !ok {
		return nil, BadRequestError{
			fmt.Sprint("not found entry name: ", entryName),
		}
	}
	if oldDs.Id != id {
		return nil, BadRequestError{
			fmt.Sprintf("invalid id: %d (EntryName: %s)", id, entryName),
		}
	}
	exts := []string{}
	found := false
	for i, oldExt := range oldDs.Extensions {
		if extension == oldExt {
			found = true
			exts = append(exts, oldDs.Extensions[:i]...)
			exts = append(exts, oldDs.Extensions[i+1:]...)
			break
		}
	}
	if !found {
		return nil, BadRequestError{
			fmt.Sprint("not found extension: ", extension),
		}
	}
	delete(this.DocumentLinkStateStore.Table, entryName)
	defer fileManager.SaveDocumentLinkState()
	newDs := new(DocumentLinkState)
	*newDs = *oldDs
	newDs.Extensions = exts
	this.DocumentLinkStateStore.Table[entryName] = newDs
	return newDs, nil
}

func (this *AppState) UpdateDocumentLinkState(oldDl, newDl *DocumentLinkSetting) (*DocumentLinkState, error) {
	if DebugModeAppState {
		log.Println("AppState.UpdateDocumentLinkState")
		defer log.Println("defer AppState.UpdateDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	oldDs, ok := this.DocumentLinkStateStore.Table[oldDl.EntryName]
	if !ok {
		return nil, nil
	}
	if oldDs.Id != oldDl.Id {
		return nil, fmt.Errorf("[BUG] AppState.ChangeDocumentLinkStateEntryName: invalid id (%v)", oldDl.Id)
	}
	delete(this.DocumentLinkStateStore.Table, oldDl.EntryName)
	defer fileManager.SaveDocumentLinkState()
	if newDl == nil {
		return nil, nil
	}
	newDs := new(DocumentLinkState)
	if oldDl.DocumentRootPath == newDl.DocumentRootPath {
		*newDs = *oldDs
	} else {
		newDs.Id = oldDs.Id
		newDs.Extensions = oldDs.Extensions
	}
	newDs.EntryName = newDl.EntryName
	this.DocumentLinkStateStore.Table[newDs.EntryName] = newDs
	return newDs, nil
}

func (this *AppState) SetDocumentLinkState(newDs *DocumentLinkState) error {
	if DebugModeAppState {
		log.Println("AppState.SetDocumentLinkState")
		defer log.Println("defer AppState.SetDocumentLinkState")
	}
	this.DocumentLinkStateStore.Lock()
	defer this.DocumentLinkStateStore.Unlock()
	oldDs, ok := this.DocumentLinkStateStore.Table[newDs.EntryName]
	if ok {
		if oldDs.Id != newDs.Id {
			return fmt.Errorf(
				"[BUG] AppState.SetDocumentLinkState dupulicate EntryName (%s)",
				newDs.EntryName,
			)
		}
	} else {
		oldDs = nil
		for _, ds := range this.DocumentLinkStateStore.Table {
			if ds.Id == newDs.Id {
				oldDs = ds
				break
			}
		}
		if oldDs != nil {
			delete(this.DocumentLinkStateStore.Table, oldDs.EntryName)
		}
	}
	this.DocumentLinkStateStore.Table[newDs.EntryName] = newDs
	fileManager.SaveDocumentLinkState()
	return nil
}

func (this *AppState) GetValue(rtv RuntimeValue) (interface{}, bool) {
	return this.runtimeValues.Load(rtv)
}

func (this *AppState) SetValue(rtv RuntimeValue, value interface{}) {
	this.runtimeValues.Store(rtv, value)
}

func (this *AppState) ExistsFlag(f interface{}) bool {
	switch f := f.(type) {
	default:
		return false
	case UpgradeFlag:
		this.UpgradeFlag.Lock()
		defer this.UpgradeFlag.Unlock()
		return (uint64(f) & this.UpgradeFlag.Value) != 0
	}
}

func (this *AppState) SetFlag(f interface{}) {
	switch f := f.(type) {
	case UpgradeFlag:
		defer this.Save() // use UpgradeFlag.Lock() (run on other thread)
		this.UpgradeFlag.Lock()
		defer this.UpgradeFlag.Unlock()
		this.UpgradeFlag.Value |= uint64(f)
	}
}

func (this *AppState) GetSecretInfoList() (list SecretInfoStoreList) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		list = append(list, info.store.Clone())
	}
	return
}

func (this *AppState) HasSecretInfo(id string) bool {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			return true
		}
	}
	return false
}

func (this *AppState) GetSecretCryptor(id string) (cryptor *SecretImageCryptor, err error) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			return info.GetCryptor()
		}
	}
	err = ErrNotFoundSecretInfoId
	return
}

func (this *AppState) CreateNewSecretInfo(id, password string) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	info := NewSecretInfo(id, password)
	this.SecretState.List = append(this.SecretState.List, info)
	fileManager.SaveSecretInfo()
}

func (this *AppState) AuthSecretInfo(id, password string) (keyword string, err error) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			if err = info.AuthByPassword(password); err == nil {
				keyword = info.Keyword()
			}
			return
		}
	}
	err = ErrNotFoundSecretInfoId
	return
}

func (this *AppState) ClearSecretAuth(id string) (err error) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			info.ClearAuth()
			return
		}
	}
	err = ErrNotFoundSecretInfoId
	return
}

func (this *AppState) CheckSecretInfoKeyword(id, keyword string) error {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			return info.AuthByKeyword(keyword)
		}
	}
	return ErrNotFoundSecretInfoId
}

func (this *AppState) ChangeSecretPassword(id, oldPassword, newPassword string) error {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	found := false
	newList := []*SecretInfo{}
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			newInfo, err := info.ChangerPassword(oldPassword, newPassword)
			if err != nil {
				return err
			}
			newList = append(newList, newInfo)
			found = true
		} else {
			newList = append(newList, info)
		}
	}
	if !found {
		return ErrNotFoundSecretInfoId
	}
	this.SecretState.List = newList
	fileManager.SaveSecretInfo()
	return nil
}

func (this *AppState) GetSecretImageInfo(id, name string) (*SecretImageFileInfo, error) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() != id {
			continue
		}
		for _, file := range info.store.Files {
			if file.Name == name {
				return file, nil
			}
		}
		return nil, ErrNotFoundSecretImage
	}
	return nil, ErrNotFoundSecretInfoId
}

func (this *AppState) GetSecretInfoFiles(id string) (files []*SecretImageFileInfo, err error) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			files = info.store.Files[:]
			return
		}
	}
	err = ErrNotFoundSecretInfoId
	return

}

func (this *AppState) AddSecretImage(id string, newfile *SecretImageFileInfo) (err error) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			files := info.store.Files
			info.store.Files = append(files, newfile)
			key := id + newfile.Name
			this.readySecret.Store(key, newfile)
			fileManager.SaveSecretInfo()
			return
		}
	}
	err = ErrNotFoundSecretInfoId
	return
}

func (this *AppState) DeleteSecretImage(id, name string) (err error) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	for _, info := range this.SecretState.List {
		if info.Id() == id {
			found := false
			files := []*SecretImageFileInfo{}
			for _, f := range info.store.Files {
				if f.Name == name {
					found = true
				} else {
					files = append(files, f)
				}
			}
			if found {
				info.store.Files = files
				fileManager.SaveSecretInfo()
				fileManager.DeleteSecretImage(id, name)
			} else {
				err = ErrNotFoundSecretImage
			}
			return
		}
	}
	err = ErrNotFoundSecretInfoId
	return
}

func (this *AppState) IsReadySecretImage(id string, name string) bool {
	key := id + name
	_, ok := this.readySecret.Load(key)
	return !ok
}

func (this *AppState) ReadySecretImage(id string, name string, size, thumb *SecretImageSizeInfo) {
	this.SecretState.Lock()
	defer this.SecretState.Unlock()
	key := id + name
	value, loaded := this.readySecret.LoadAndDelete(key)
	if !loaded {
		// BUG?
		log.Println("appState.ReadySecretImage(", id, name, ")", "BUG? load")
		return
	}
	info, ok := value.(*SecretImageFileInfo)
	if !ok || info == nil {
		// BUG?
		log.Println("appState.ReadySecretImage(", id, name, ")", "BUG? cast")
		return
	}
	for _, item := range this.SecretState.List {
		if item.Id() != id {
			continue
		}
		files := []*SecretImageFileInfo{}
		for _, f := range item.store.Files {
			if f.Name == name {
				c := &SecretImageFileInfo{}
				*c = *f
				c.Size = size
				c.Thumb = thumb
				files = append(files, c)
			} else {
				files = append(files, f)
			}
		}
		item.store.Files = files
		fileManager.SaveSecretInfo()
		return
	}
	log.Println("appState.ReadySecretImage(", id, name, ")", ErrNotFoundSecretInfoId)
}

func init() {
	// GET
	// /api/status/get
	http.Handle("/api/status/get",
		http.HandlerFunc(systemStatusHandleFunc))
}

func systemStatusHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeStatusHandler {
		log.Println("systemStatusHandleFunc")
		defer log.Println("defer systemStatusHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	ss := SystemStatus{}

	if v, ok := appState.GetValue(rtvRoutineStartTime); ok {
		ss.RoutineStartTime = ToDateString(v.(time.Time))
	}
	if v, ok := appState.GetValue(rtvRoutineEndTime); ok {
		ss.RoutineEndTime = ToDateString(v.(time.Time))
	}

	ss.BuildVersion = GeneratorBuildVersion
	ss.StaticFiles = GeneratorMode
	ss.ShutdownToken = shutdownReceiver.GetToken()

	res := Status{
		SystemStatus: &ss,
	}

	writeJson(w, req, &res)
}
