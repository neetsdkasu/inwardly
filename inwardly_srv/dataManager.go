package main

import (
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"
	"time"
)

type SearchResultEntry struct {
	LastAccess time.Time
	Result     *SearchResult
}
type DaySayListSetEntry struct {
	DayId string
	*DaySaidList
}

type DaySayListSet []DaySayListSetEntry

type FotoList []*Foto

type FotoStore struct {
	FotoList             `json:"list"`
	LastUploadDate       string `json:"lastupload"`
	TotalSizeOfThisMonth int    `json:"totalsize"`
}

type DataManager struct {
	DaySaidListStore  DaySaidListStore
	MemoListStore     MemoListStore
	SearchResultStore sync.Map
	FotoStore         *FotoStore
	sync.Mutex
}

var dataManager DataManager

func (this *DataManager) Init() error {
	if DebugModeShowInit || DebugModeDataManager {
		log.Println("DataManager.Init")
	}
	if err := this.DaySaidListStore.Init(); err != nil {
		return err
	}
	if err := this.MemoListStore.Init(); err != nil {
		return err
	}
	return nil
}

func (this *DataManager) GetDayIdList() []string {
	return this.DaySaidListStore.GetDayIdList()
}

func (this *DataManager) GetBlogPathSuffix(dayId string) string {
	return this.DaySaidListStore.GetBlogPathSuffix(dayId)
}

func (this *DataManager) GetListCopyWithoutCaching(dayId string) (*DaySaidList, error) {
	return this.DaySaidListStore.GetListCopyWithoutCaching(dayId)
}

func (this *DataManager) GetDaySaidListStatus() []*DaySaidListStatus {
	return this.DaySaidListStore.GetDaySaidListStatus()
}

func (this *DataManager) SetUploadedDaySaidListStatus(i int, status DaySaidListStatus) error {
	return this.DaySaidListStore.SetUploadedDaySaidListStatus(i, status)
}

func (this *DataManager) UpdateDaySaidListStatus(influence map[string]bool) error {
	return this.DaySaidListStore.UpdateDaySaidListStatus(true, true, influence)
}

func (this *DataManager) SetSearchResult(res *SearchResult) {
	id := res.Id
	entry := &SearchResultEntry{
		LastAccess: time.Now().Local(),
		Result:     res,
	}
	this.SearchResultStore.Store(id, entry)
}

func (this *DataManager) GetSearchResult(id string) (*SearchResult, error) {
	this.Lock()
	defer this.Unlock()
	value, ok := this.SearchResultStore.Load(id)
	if !ok {
		return nil, BadRequestError{
			"invalid Result Id (" + id + ")",
		}
	}
	if value == nil {
		return nil, fmt.Errorf("BUG? SearchResultEntry is nil (id=%s)", id)
	}
	entry, ok := value.(*SearchResultEntry)
	if !ok {
		return nil, fmt.Errorf("BUG? not SearchResultEntry (id=%s) (%#v)", id, value)
	}
	entry.LastAccess = time.Now().Local()
	return entry.Result, nil
}

func (this *DataManager) GetCalender(y, m int) (*Calender, error) {
	return this.DaySaidListStore.GetCalender(y, m)
}

func (this *DataManager) GetMemoList() MemoList {
	return this.MemoListStore.GetList()
}

func (this *DataManager) GetMemoHistory() MemoList {
	return this.MemoListStore.GetHistory()
}

func (this *DataManager) MoveMemo(id string, value int) (*MemoSwap, error) {
	return this.MemoListStore.Move(id, value)
}

func (this *DataManager) RemoveMemo(saidId string) error {
	if DebugModeDataManager {
		log.Println("DataManager.RemoveMemo")
		defer log.Println("defer DataManager.RemoveMemo")
	}

	if this.MemoListStore.Remove(saidId) {
		return nil
	}

	return BadRequestError{
		"not found Said ID in Memo (" + saidId + ")",
	}
}

func (this *DataManager) AddMemo(saidId string) (*Memo, error) {
	if DebugModeDataManager {
		log.Println("DataManager.AddMemo")
		defer log.Println("defer DataManager.AddMemo")
	}

	said, err := this.GetSaidById(saidId)
	if err != nil {
		return nil, err
	}

	if said.Deleted {
		return nil, BadRequestError{
			"the said has deleted (Said ID " + saidId + ")",
		}
	}

	memo := said.Memo()
	if this.MemoListStore.Add(memo) {
		return memo, nil
	}

	// saidIdがメモに重複
	return nil, BadRequestError{
		"already exists (Said ID " + saidId + ")",
	}
}

func (this *DataManager) GetSaidById(id string) (*Said, error) {
	// this が　nil　かはチェックしてない…
	// 引数の id が valid format かはチェックしてない…
	dayId, err := GetDayIdFromSaidId(id)
	if err != nil {
		return nil, err
	}
	list, err := this.DaySaidListStore.Get(dayId)
	if err != nil {
		return nil, err
	}
	list.Lock()
	defer list.Unlock()
	for _, said := range list.Said {
		if said.Id == id {
			return said, nil
		}
	}
	return nil, nil
}

func (this *DataManager) Say(sreq *SayRequest) (*Said, *Memo, error) {
	if DebugModeDataManager {
		log.Println("DataManager.Say")
		defer log.Println("defer DataManager.Say")
	}
	switch sreq.Type {
	case SayRequestTypeReply:
		return this.sayReply(sreq)
	case SayRequestTypeEdit:
		return this.editSaid(sreq)
	}
	said := sreq.Compose()
	err := this.DaySaidListStore.AppendSaid(said)
	if err != nil {
		return nil, nil, err
	}
	return said, nil, nil
}

func (this *DataManager) sayReply(sreq *SayRequest) (*Said, *Memo, error) {
	if DebugModeDataManager {
		log.Println("DataManager.sayReply")
		defer log.Println("defer DataManager.sayReply")
	}
	said := sreq.Compose()
	err := this.DaySaidListStore.AppendReply(said)
	if err != nil {
		return nil, nil, err
	}
	{
		threadId := said.Root
		if threadId == "" {
			threadId = said.Id
		}
		appSetting.UpdateThread(threadId, said.Date)
	}
	return said, nil, nil
}

func (this *DataManager) editSaid(sreq *SayRequest) (*Said, *Memo, error) {
	if DebugModeDataManager {
		log.Println("DataManager.editSaid")
		defer log.Println("defer DataManager.editSaid")
	}
	said := sreq.Compose()
	err := this.DaySaidListStore.EditSaid(said)
	if err != nil {
		return nil, nil, err
	}
	{
		threadId := said.Root
		if threadId == "" {
			threadId = said.Id
		}
		appSetting.UpdateThread(threadId, said.Update)
	}
	memo := said.Memo()
	if this.MemoListStore.Update(memo) {
		return said, memo, nil
	}
	return said, nil, nil
}

func (this *DataManager) RecoverSaid(update, id string, index int) (*Said, *Memo, error) {
	if DebugModeDataManager {
		log.Println("DataManager.RecoverSaid")
		defer log.Println("defer DataManager.RecoverSaid")
	}
	said := &Said{
		Id:     id,
		Update: update,
	}
	err := this.DaySaidListStore.RecoverSaid(said, index)
	if err != nil {
		return nil, nil, err
	}
	{
		threadId := said.Root
		if threadId == "" {
			threadId = id
		}
		appSetting.UpdateThread(threadId, update)
	}
	memo := said.Memo()
	if this.MemoListStore.Update(memo) {
		return said, memo, nil
	}
	return said, nil, nil
}

func (this *DataManager) DeleteSaid(update, id string) (*Said, *Memo, error) {
	if DebugModeDataManager {
		log.Println("DataManager.DeleteSaid")
		defer log.Println("defer DataManager.DeleteSaid")
	}
	said := &Said{
		Id:        id,
		Update:    update,
		Deleted:   true,
		ImageList: []string{"", "", "", ""},
	}
	err := this.DaySaidListStore.EditSaid(said)
	if err != nil {
		return nil, nil, err
	}
	{
		threadId := said.Root
		if threadId == "" {
			threadId = id
		}
		appSetting.UpdateThread(threadId, update)
	}
	if this.MemoListStore.Remove(id) {
		memo := &Memo{Id: id}
		return said, memo, nil
	}
	return said, nil, nil
}

func (this *DataManager) GetPage(dayId string) (*PageView, error) {
	if DebugModeDataManager {
		log.Println("DataManager.GetPage")
		defer log.Println("defer DataManager.GetPage")
	}

	prev, next, err := this.DaySaidListStore.GetArround(dayId)
	if err != nil {
		return nil, err
	}

	list, err := this.DaySaidListStore.GetListCopy(dayId)
	if err != nil {
		return nil, err
	}

	timeline := make([]*SaidView, 0, len(list.Said))

	idSet := map[string]bool{}

	for _, said := range list.Said {
		if said.Parent != "" {
			if _, ok := idSet[said.Parent]; !ok {
				// 過去said
				// 過去saidじゃなかった場合デッドロック発生…
				parent, err := this.DaySaidListStore.GetSaid(said.Parent)
				if err != nil {
					return nil, err
				}
				pView := parent.View()
				timeline = append(timeline, pView)
				idSet[parent.Id] = true
			}
		}
		view := said.View()
		timeline = append(timeline, view)
		idSet[said.Id] = true
	}

	pageView := &PageView{
		PageId:   dayId,
		Prev:     prev,
		Next:     next,
		Timeline: timeline,
	}

	return pageView, nil
}

func (this *DataManager) GetThread(rootId string) (SaidList, error) {
	if DebugModeDataManager {
		log.Println("DataManager.GetThread")
		defer log.Println("defer DataManager.GetThread")
	}
	dayId, err := GetDayIdFromSaidId(rootId)
	if err != nil {
		return nil, err
	}
	days := []string{dayId}
	idlists := map[string][]string{}
	idlists[dayId] = []string{rootId}
	said := SaidList{}

	for len(days) > 0 {
		dayId = days[0]
		days = days[1:]
		ids := idlists[dayId]
		list, err := this.DaySaidListStore.GetListCopy(dayId)
		if err != nil {
			return nil, err
		}
		if len(ids) > 1 {
			sort.Strings(ids)
		}
		id := ids[0]
		ids = ids[1:]
		for _, s := range list.Said {
			if s.Id != id {
				continue
			}
			if len(said) == 0 && len(s.Root) != 0 {
				// 引数のrootIdがルートになってない場合に入る
				return nil, BadRequestError{
					"invalid thread ID (" + rootId + ")",
				}
			}
			said = append(said, s)
			idl := len(ids)
			for _, childId := range s.Children {
				dId, err := GetDayIdFromSaidId(childId)
				if err != nil {
					return nil, err
				}
				if dId == dayId {
					ids = append(ids, childId)
				} else {
					if tmp, ok := idlists[dId]; ok {
						idlists[dId] = append(tmp, childId)
					} else {
						days = append(days, dId)
						sort.Strings(days)
						idlists[dId] = []string{childId}
					}
				}
			}
			if len(ids) == 0 {
				break
			} else if len(ids) > 1 && len(ids) != idl {
				sort.Strings(ids)
			}
			id = ids[0]
			ids = ids[1:]
		}
		if len(ids) != 0 {
			return nil, fmt.Errorf("BUG!?!?")
		}
	}

	return said, nil
}

func (this DaySayListSet) GetSaidById(id string) (*Said, error) {
	dayId, err := GetDayIdFromSaidId(id)
	if err != nil {
		return nil, err
	}
	for _, e := range this {
		if e.DayId != dayId {
			continue
		}
		for _, s := range e.Said {
			if s.Id == id {
				return s, nil
			}
		}
		return nil, fmt.Errorf("invalid Said ID (%s)", id)
	}
	return dataManager.GetSaidById(id)
}

func (this *FotoStore) Init() {
	this.FotoList = FotoList{}
	this.LastUploadDate = ToDateString(time.Now().Local())
}

func (this *DataManager) GetFoto(filename string) (*Foto, error) {
	this.Lock()
	defer this.Unlock()
	if this.FotoStore == nil {
		store := new(FotoStore)
		err := fileManager.LoadFotoStore(store)
		if err != nil {
			return nil, err
		}
		this.FotoStore = store
	}
	list := this.FotoStore.FotoList
	p := sort.Search(len(list), func(i int) bool {
		return strings.Compare(list[i].File, filename) >= 0
	})
	if p < len(list) && list[p].File == filename {
		return list[p], nil
	}
	return nil, nil
}

func (this *DataManager) GetUploadedTotalFotoSize() (int, error) {
	this.Lock()
	defer this.Unlock()
	if this.FotoStore == nil {
		store := new(FotoStore)
		err := fileManager.LoadFotoStore(store)
		if err != nil {
			return 0, err
		}
		this.FotoStore = store
	}
	totalSize := this.FotoStore.TotalSizeOfThisMonth
	lastDay, err := FromDateString(this.FotoStore.LastUploadDate)
	if err != nil {
		return 0, err
	}
	y0, m0, _ := lastDay.Date()
	y1, m1, _ := time.Now().Local().Date()
	if y0 != y1 || m0 != m1 {
		totalSize = 0
	}
	return totalSize, nil
}

func (this *DataManager) SetFoto(foto *Foto) error {
	this.Lock()
	defer this.Unlock()
	if this.FotoStore == nil {
		store := new(FotoStore)
		err := fileManager.LoadFotoStore(store)
		if err != nil {
			return err
		}
		this.FotoStore = store
	}
	totalSize := this.FotoStore.TotalSizeOfThisMonth
	today := time.Now().Local()
	{
		lastDay, err := FromDateString(this.FotoStore.LastUploadDate)
		if err != nil {
			return err
		}
		y0, m0, _ := lastDay.Date()
		y1, m1, _ := today.Date()
		if y0 != y1 || m0 != m1 {
			totalSize = 0
		}
	}
	list := this.FotoStore.FotoList
	p := sort.Search(len(list), func(i int) bool {
		return strings.Compare(list[i].File, foto.File) >= 0
	})
	if p < len(list) {
		if list[p].File == foto.File {
			temp := make(FotoList, len(list))
			copy(temp, list)
			temp[p] = foto
			list = temp
		} else {
			temp := make(FotoList, 0, len(list)+1)
			temp = append(temp, list[:p]...)
			temp = append(temp, foto)
			temp = append(temp, list[p:]...)
			list = temp
		}
	} else {
		temp := make(FotoList, 0, len(list)+1)
		temp = append(temp, list...)
		temp = append(temp, foto)
		list = temp
	}
	store := &FotoStore{
		FotoList:             list,
		LastUploadDate:       ToDateString(today),
		TotalSizeOfThisMonth: totalSize + foto.Size,
	}
	err := fileManager.SaveFotoStore(store)
	if err != nil {
		return err
	}
	this.FotoStore = store
	return nil
}

func (this *DataManager) CleanCache(expire time.Time) error {
	if DebugModeDataManager || DebugModeCleanCache {
		log.Println("DataManager.CleanCache")
		defer log.Println("defer DataManager.CleanCache")
	}
	err := this.DaySaidListStore.CleanCache(expire)
	if err != nil {
		return err
	}
	{
		var targets []interface{}
		this.SearchResultStore.Range(func(k, v interface{}) bool {
			if v != nil {
				r := v.(*SearchResultEntry)
				if expire.After(r.LastAccess) {
					targets = append(targets, k)
				}
			}
			return true
		})
		for _, k := range targets {
			this.SearchResultStore.Delete(k)
			if DebugModeCleanCache {
				log.Println("Clean Cache: Search Result", k)
			}
		}
	}
	return nil
}
