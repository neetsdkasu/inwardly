package main

import (
	"bytes"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"
)

type MemLogger struct {
	bufR, bufW, rem []byte
	bufLocker       sync.Mutex
	parseRunning    bool
	linesR, linesW  [][]byte
	linesLocker     sync.Mutex
}

var memLogger MemLogger
var loggerLinesSize = 100

func init() {
	bufSize := 1000
	if DebugMode {
		bufSize = 32000
		loggerLinesSize = 1000
	}
	memLogger.bufR = make([]byte, 0, bufSize)
	memLogger.bufW = make([]byte, 0, bufSize)
	memLogger.rem = make([]byte, 0, bufSize)
	memLogger.linesR = make([][]byte, 0, loggerLinesSize)
	memLogger.linesW = make([][]byte, 0, loggerLinesSize)
}

func (this *MemLogger) Write(p []byte) (n int, err error) {
	this.bufLocker.Lock()
	defer this.bufLocker.Unlock()
	this.bufW = append(this.bufW, p...)
	if !this.parseRunning {
		this.parseRunning = true
		go this.Parse()
	}
	return len(p), nil
}

func (this *MemLogger) SwapBuffer() bool {
	this.bufLocker.Lock()
	defer this.bufLocker.Unlock()
	this.bufR, this.bufW = this.bufW, this.bufR
	if len(this.bufR) == 0 {
		this.parseRunning = false
	}
	return this.parseRunning
}

func (this *MemLogger) SwapLines() {
	this.linesLocker.Lock()
	defer this.linesLocker.Unlock()
	this.linesR, this.linesW = this.linesW, this.linesR
}

func (this *MemLogger) GetLines() [][]byte {
	this.linesLocker.Lock()
	defer this.linesLocker.Unlock()
	lines := make([][]byte, len(this.linesR))
	copy(lines, this.linesR)
	return lines
}

func (this *MemLogger) Parse() {
	sep := []byte("\n")
	for {
		time.Sleep(time.Second)
		if !this.SwapBuffer() {
			return
		}
		this.rem = append(this.rem, this.bufR...)
		this.bufR = this.bufR[:0]
		lines := bytes.SplitAfter(this.rem, sep)
		this.rem = this.rem[:0]
		if len(lines) < loggerLinesSize {
			r := len(this.linesR) - (loggerLinesSize - len(lines))
			if r < 0 {
				r = 0
			}
			if r < len(this.linesR) {
				this.linesW = append(this.linesW, this.linesR[r:]...)
			}
		} else {
			s := len(lines) - loggerLinesSize
			lines = lines[s:]
		}
		for _, line := range lines {
			if len(line) == 0 {
				continue
			}
			if line[len(line)-1] != '\n' {
				this.rem = append(this.rem, line...)
				break
			}
			newLine := make([]byte, len(line))
			copy(newLine, line)
			this.linesW = append(
				this.linesW,
				newLine,
			)
		}
		this.SwapLines()
		for i := range this.linesW {
			this.linesW[i] = nil
		}
		this.linesW = this.linesW[:0]
	}
}

func init() {
	// GET
	http.Handle("/log", &memLogger)
}

func (this *MemLogger) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if DebugModeMemLogger {
		log.Println("MemLogger.ServeHTTP")
		defer log.Println("defer MemLogger.ServeHTTP")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	lines := this.GetLines()

	blob := bytes.Join(lines, nil)

	w.Header().Set("Host", req.Host)

	w.Header().Set("Content-Length", strconv.Itoa(len(blob)))
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")

	if _, err := w.Write(blob); err != nil {
		log.Println(err)
	}

	if f, ok := w.(http.Flusher); ok {
		f.Flush()
	}

}
