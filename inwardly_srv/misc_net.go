package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

type BadRequestError struct {
	Message string
}

func (this BadRequestError) Error() string {
	return this.Message
}

func errorResponse(w http.ResponseWriter, err error) {
	if _, ok := err.(BadRequestError); ok {
		badRequest(w, err.Error())
		return
	}
	log.Println(err)
	internalServerError(w, err.Error())
}

func internalServerError(w http.ResponseWriter, reason string) {
	const code = http.StatusInternalServerError
	http.Error(w, reason, code)
}

func badRequest(w http.ResponseWriter, reason string) {
	const code = http.StatusBadRequest
	http.Error(w, reason, code)
}

func writeJson(w http.ResponseWriter, req *http.Request, target interface{}) {
	blob, err := json.Marshal(target)
	if err != nil {
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	w.Header().Set("Host", req.Host)
	w.Header().Set("Content-Length", strconv.Itoa(len(blob)))
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	if _, err = w.Write(blob); err != nil {
		log.Println(err)
		return
	}

	if f, ok := w.(http.Flusher); ok {
		f.Flush()
	}
}
