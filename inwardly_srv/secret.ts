
interface SecretResponse {
	ok: boolean;
	detail: string;
}

interface SecretImageSizeInfo {
	width: number;
	height: number;
	length: number;
}

interface SecretImageFileInfo {
	name: string;
	ext: string;
	mime: string;
	size: SecretImageSizeInfo;
	thumb: SecretImageSizeInfo;
}

interface SecretListResponse extends SecretResponse {
	files: SecretImageFileInfo[];
}

interface SecretAddResponse extends SecretResponse {
	secret_id: string;
	file_name: string;
}

let currentSecretId = "";

function addSecretThumbnail(id: string, name: string, focus: boolean): void {
	const files = document.getElementById("files")!;
	const thumb = files.appendChild(document.createElement("img"));
	thumb.src = `./api/secret/${id}/thumb/${name}`;
	thumb.width = 100;
	thumb.classList.add("secretThumnail");
	thumb.title = `${id}/${name}`;
	thumb.addEventListener("click", () => {
		const img = document.getElementById("image") as HTMLImageElement;
		img.src = `./api/secret/${id}/image/${name}`;
		document.getElementById("delete_target_name")!.textContent = `${id}/${name}`;
		(document.getElementById("delete_target") as HTMLInputElement).value = name;
		(document.getElementById("delete_button") as HTMLButtonElement).disabled = true;
		const check = (document.getElementById("delete_check") as HTMLInputElement);
		check.disabled = false;
		check.checked = false;
		(document.getElementById("delete_thumbnail") as HTMLImageElement).src = `./api/secret/${id}/thumb/${name}`;
		(document.getElementById("open_to_new_tab") as HTMLAnchorElement).href = `./api/secret/${id}/image/${name}`;
	});
	if (focus) {
		setTimeout(() => {
			thumb.scrollIntoView({block: "end", inline: "end"});
		}, 1000);
	}
}

function showList(): void {
	document.getElementById("id_title")!.textContent = `SECRET-ID: ${currentSecretId}`;
	fetch(`./api/secret/${currentSecretId}/list`, {
		method: "GET"
	})
	.then(toJson)
	.then( (res: SecretListResponse) => {
		if (!res.ok) {
			showMessage(res.detail, true);
			const dialog = document.getElementById("open_secret_dialog") as HTMLDialogElement;
			(document.getElementById("current_secret_id") as HTMLInputElement).value = currentSecretId;
			dialog.showModal();
			return;
		}
		const files = document.getElementById("files")!;
		files.innerHTML = "";
		for (const f of res.files) {
			addSecretThumbnail(currentSecretId, f.name, false);
		}
		(document.getElementById("close_secret_button") as HTMLButtonElement).disabled = false;
		(document.getElementById("change_password_button") as HTMLButtonElement).disabled = false;
	})
	.catch(err => {
		showError(err);
	});
}

function openSecret(id: string, password: string): void {
	const formData = new FormData();
	formData.set("secret_id", id);
	formData.set("secret_password", password);

	fetch(`./api/secret/${id}/open`, {
		method: "POST",
		body: formData
	})
	.then(toJson)
	.then( (res: SecretResponse) => {
		if (!res.ok) {
			currentSecretId = "";
			showError(res.detail);
			return;
		}
		currentSecretId = id;
		window.location.replace(`./secret?id=${id}`);
	})
	.catch( err => {
		showError(err);
	});
}

function addNewSecretThumnail(id: string, name: string): void {
	fetch(`./api/secret/${id}/thumb/${name}`)
	.then( r => {
		if (r.ok) {
			addSecretThumbnail(id, name, true);
			showMessage(`SAVED! ${id}/${name}`, false);
		} else {
			setTimeout(() => addNewSecretThumnail(id, name), 1000);
		}
	})
	.catch( err => {
		showError(err);
	});
}

function addSecretImage(file: File): void {
	const formData = new FormData();
	formData.set("file", file);
	showMessage("PASTE!", false);
	fetch(`./api/secret/${currentSecretId}/add`, {
		method: "POST",
		body: formData
	})
	.then(toJson)
	.then( (res: SecretAddResponse) => {
		if (!res.ok) {
			showError(res.detail);
			return;
		}
		showMessage(`Saving... ${res.secret_id}/${res.file_name}`, false);
		addNewSecretThumnail(res.secret_id, res.file_name);
	})
	.catch( err => {
		showError(err);
	});
}

function deleteSecretImage(id: string, name: string): void {
	const formData = new FormData();
	formData.set("image", name);
	showMessage(`Deleting... ${id}/${name}`, false);
	fetch(`./api/secret/${id}/delete`, {
		method: "POST",
		body: formData
	})
	.then(toJson)
	.then( (res: SecretResponse) => {
		if (!res.ok) {
			showError(res.detail);
			return;
		}
		showList();
		const img = document.getElementById("image") as HTMLImageElement;
		img.src = "./favicon.ico";
		document.getElementById("delete_target_name")!.textContent = "";
		(document.getElementById("delete_target") as HTMLInputElement).value = "";
		(document.getElementById("delete_button") as HTMLButtonElement).disabled = true;
		const check = (document.getElementById("delete_check") as HTMLInputElement);
		check.disabled = true;
		check.checked = false;
		(document.getElementById("delete_thumbnail") as HTMLImageElement).src = "./favicon.ico";
		showMessage(`DELETED! ${id}/${name}`, false);
	})
	.catch( err => {
		showError(err);
	});
}

function closeSecret(id: string): void {
	fetch(`./api/secret/${id}/close`, {
		method: "POST"
	})
	.then(toJson)
	.then( (res: SecretResponse) => {
		showMessage(res.detail, !res.ok);
		if (res.ok && id === currentSecretId) {
			currentSecretId = "";
			window.location.replace(`./secret`);	
		}
	})
	.catch( err => {
		showError(err);
	});
}

function changeSecretPassword(id: string, oldPassword: string, newPassword: string): void {
	const formData = new FormData();
	formData.set("old_password", oldPassword);
	formData.set("new_password", newPassword);
	fetch(`./api/secret/${id}/password`, {
		method: "POST",
		body: formData
	})
	.then(toJson)
	.then( (res: SecretResponse) => {
		showMessage(res.detail, !res.ok);
	})
	.catch( err => {
		showError(err);
	});

}

////////////////
//
// 初期化処理
//
////////////////

document.querySelector('header > nav button.setting')!
.addEventListener('click', () => {
	window.location.assign('/setting');
});

document.querySelector('header > nav button.link')!
.addEventListener('click', () => {
	window.location.assign('/link');
});

document.querySelector('header > nav button.home')!
.addEventListener('click', () => {
	window.location.assign('/');
});

document.querySelector('header > nav button.misc')!
.addEventListener('click', () => {
	window.location.assign('/misc');
});

window.addEventListener("pageshow", () => {
	navigator.storage.estimate().then(e => {
		if ((e.quota ?? 0) < 500_000_000) {
			showMessage("PRIVATE MODE OK", false);
			(document.getElementById("open_secret_button") as HTMLButtonElement).disabled = false;
			const sel = document.getElementById("secret_id") as HTMLSelectElement;
			sel.disabled = false;
			const params = new URLSearchParams(window.location.search);
			if (params.has("id")) {
				currentSecretId = params.get("id")!;
				sel.value = currentSecretId;
				showMessage(`OPEN (SECRET-ID: ${currentSecretId})`, false);
				showList();
			}
		} else {
			showMessage("REQUIRE PRIVATE MODE", true);
			(document.getElementById("open_secret_button") as HTMLButtonElement).disabled = true;
		}
	});
});

document.getElementById("open_secret_button")!.addEventListener("click", () => {
	const id = (document.getElementById("secret_id") as HTMLSelectElement).value;
	currentSecretId = id;
	window.location.replace(`./secret?id=${id}`);
});

document.getElementById("open_secret_dialog")!.addEventListener("close", () => {
	const dialog = document.getElementById("open_secret_dialog") as HTMLDialogElement;
	if (dialog.returnValue !== "ok") {
		return;
	}
	const form = dialog.querySelector(":scope form") as HTMLFormElement;
	const elems = form.elements;
	const id = (elems.namedItem("current_secret_id") as HTMLInputElement).value;
	const password = (elems.namedItem("secret_password") as HTMLInputElement).value.trim();
	if (password === "") {
		showError("Password must not be empty");
		return;
	}
	form.reset();
	openSecret(id, password);
});

document.addEventListener("paste", e => {
	if (currentSecretId === "") {
		return;
	}
	const cd = e.clipboardData;
	if (cd === null) {
		return;
	}
	const items = cd.items;
	for (let i = 0; i < items.length; i++) {
		const item = items[i];
		if (item.kind !== "file") { continue; }
		if (!item.type.match(/^image\//)) { continue; }
		const file = item.getAsFile();
		if (file === null) { continue; }
		addSecretImage(file);
		return;
	}
});

document.getElementById("delete_check")!.addEventListener("change", () => {
	const checked = (document.getElementById("delete_check") as HTMLInputElement).checked;
	(document.getElementById("delete_button") as HTMLButtonElement).disabled = !checked;
});

document.getElementById("delete_button")!.addEventListener("click", () => {
	const name = (document.getElementById("delete_target") as HTMLInputElement).value;
	deleteSecretImage(currentSecretId, name);
});

document.getElementById("close_secret_button")!.addEventListener("click", () => {
	closeSecret(currentSecretId);
});

document.getElementById("change_password_button")!.addEventListener("click", () => {
	const dialog = document.getElementById("change_password_dialog") as HTMLDialogElement;
	const form = dialog.querySelector(":scope form") as HTMLFormElement;
	(form.elements.namedItem("target_secret_id") as HTMLInputElement).value = currentSecretId;
	dialog.showModal();
});

document.getElementById("change_password_dialog")!.addEventListener("close", () => {
	const dialog = document.getElementById("change_password_dialog") as HTMLDialogElement;
	if (dialog.returnValue !== "ok") {
		return;
	}
	const form = dialog.querySelector(":scope form") as HTMLFormElement;
	const elems = form.elements;
	const id = (elems.namedItem("target_secret_id") as HTMLInputElement).value;
	const oldPassword = (elems.namedItem("old_password") as HTMLInputElement).value.trim();
	if (oldPassword === "") {
		showError("Current Password must not be empty");
		return;
	}
	const newPassword = (elems.namedItem("new_password") as HTMLInputElement).value.trim();
	if (newPassword === "") {
		showError("New Password must not be empty");
		return;
	}
	form.reset();
	changeSecretPassword(id, oldPassword, newPassword);
});
