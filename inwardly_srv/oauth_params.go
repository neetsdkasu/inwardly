package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"net/url"
	"sort"
	"strings"
	"time"
)

var percentEncodeTable [256][]byte

func init() {
	for i := range percentEncodeTable {
		switch {
		default:
			b := make([]byte, 0, 3)
			b = append(b, '%')
			b = append(b, []byte(fmt.Sprintf("%02X", i))...)
			percentEncodeTable[i] = b
		case '0' <= i && i <= '9',
			'A' <= i && i <= 'Z', 'a' <= i && i <= 'z',
			i == '-', i == '.', i == '_', i == '~':
			percentEncodeTable[i] = []byte{byte(i)}
		}
	}
}

func PercentEncode(s string) string {
	bs := []byte(s)
	ret := make([]byte, 0, len(bs)*3)
	for _, b := range bs {
		ret = append(ret, percentEncodeTable[b]...)
	}
	return string(ret)
}

type Paramerter struct {
	Key, Value string
}

func NewQuotedParameter(key, value string) *Paramerter {
	return &Paramerter{
		Key:   PercentEncode(key),
		Value: PercentEncode(value),
	}
}

func (this *Paramerter) Quoted() *Paramerter {
	return NewQuotedParameter(this.Key, this.Value)
}

func (this *Paramerter) Pair() string {
	return this.Key + "=" + this.Value
}

func (this *Paramerter) QuotedPair() string {
	return fmt.Sprintf(
		`%s="%s"`,
		PercentEncode(this.Key),
		PercentEncode(this.Value),
	)
}

func (this *Paramerter) Compare(other *Paramerter) int {
	cmp := strings.Compare(this.Key, other.Key)
	if cmp != 0 {
		return cmp
	}
	return strings.Compare(this.Value, other.Value)
}

type ParamerterList []Paramerter

func NewParamaterList(consumerKey string) ParamerterList {
	return ParamerterList{
		{"oauth_signature_method", "HMAC-SHA1"},
		{"oauth_version", "1.0"},
		{"oauth_timestamp", fmt.Sprint(time.Now().UTC().Unix())},
		{"oauth_nonce", GenerateNonce()},
		{"oauth_consumer_key", consumerKey},
	}
}

func (this *ParamerterList) HeaderValue() string {
	tmp := *this
	params := make([]string, 0, len(tmp))
	for i := range tmp {
		params = append(params, tmp[i].QuotedPair())
	}
	return "OAuth " + strings.Join(params, ", ")
}

func (this *ParamerterList) Set(key, value string) {
	tmp := *this
	for i := range tmp {
		if tmp[i].Key == key {
			tmp[i].Value = value
			*this = tmp
			return
		}
	}
	tmp = append(tmp, Paramerter{
		Key:   key,
		Value: value,
	})
	*this = tmp
}

func (this *ParamerterList) SetOAuthToken(token string) {
	this.Set("oauth_token", token)
}

func (this *ParamerterList) SetOAuthVerifier(verifier string) {
	this.Set("oauth_verifier", verifier)
}

func (this *ParamerterList) SetOAuthCallback(callback string) {
	this.Set("oauth_callback", callback)
}

func (this *ParamerterList) SetOAuthCallbackOob() {
	this.SetOAuthCallback("oob")
}

func (this *ParamerterList) SetSignature(method, uri, key1, key2 string, form url.Values) error {
	u, err := url.Parse(uri)
	if err != nil {
		return err
	}

	params := []*Paramerter{}

	for _, p := range *this {
		params = append(params, p.Quoted())
	}

	queries := u.Query()
	for k, vs := range queries {
		if len(vs) == 0 {
			params = append(
				params,
				NewQuotedParameter(k, ""),
			)
		}
		for _, v := range vs {
			params = append(
				params,
				NewQuotedParameter(k, v),
			)
		}
	}

	if form != nil {
		for k, vs := range form {
			if len(vs) == 0 {
				params = append(
					params,
					NewQuotedParameter(k, ""),
				)
			}
			for _, v := range vs {
				params = append(
					params,
					NewQuotedParameter(k, v),
				)
			}
		}
	}

	sort.Slice(params, func(i, j int) bool {
		return params[i].Compare(params[j]) < 0
	})

	pairs := make([]string, 0, len(params))
	for _, p := range params {
		pairs = append(
			pairs,
			p.Pair(),
		)
	}

	u.RawQuery = ""
	u.RawFragment = ""
	baseUri := u.String()

	message := []byte(
		strings.Join(
			[]string{
				PercentEncode(method),
				PercentEncode(baseUri),
				PercentEncode(
					strings.Join(pairs, "&"),
				),
			},
			"&",
		),
	)

	key := []byte(PercentEncode(key1) + "&" + PercentEncode(key2))

	mac := hmac.New(sha1.New, key)
	mac.Write(message)
	signature := base64.StdEncoding.EncodeToString(mac.Sum(nil))

	this.Set("oauth_signature", signature)

	return nil
}
