
let developmentMode = false;
let titleBase = 'inwardly';

(function() {
	const host = window.location.host;
	if (host === '127.8.8.64:8864') {
		// development mode
		developmentMode = true;
		titleBase = '[dev]inwardly';
		document.body!.classList.add('develop');
		document.title = titleBase;
	}
})();

function showMessage(msg: string, isError: boolean) {
	const ul = document.querySelector('#messageboard > ul')!;
	const li = ul.insertBefore(document.createElement('li'), ul.firstChild);
	if (isError) {
		li.classList.add('errorMessage');
	} else {
		li.classList.add('infoMessage');
	}
	const btn = li.appendChild(document.createElement('button'));
	btn.classList.add('softbutton');
	btn.textContent = 'x';
	btn.style.marginRight = '1em';
	btn.type = 'button';
	li.appendChild(document.createTextNode(msg));
	const close = () => {
		if (li.parentNode) {
			li.parentNode.removeChild(li);
		}
	};
	btn.addEventListener('click', close);
	window.setTimeout(close, 10000);
}

function showError(err: any) {
	const d = new Date();
	const hour = leadingZeros2digits(d.getHours());
	const minute = leadingZeros2digits(d.getMinutes());
	const second = leadingZeros2digits(d.getSeconds());
	const ds = `${hour}:${minute}:${second}`;
	const msg = `[${ds}] ${err}`;
	console.log(ds, err);
	showMessage(msg, true);
}

function setTitle(pagetype: string, ext: string) {
	let title = titleBase;
	if (pagetype !== '') {
		title += ` ( ${pagetype} )`;
	}
	ext = ext.replace(/\s+/g, ' ').trim();
	if (ext !== '') {
		if (ext.length > 15) {
			const arr = [...ext];
			if (arr.length > 15) {
				ext = arr.slice(0, 15).join('');
				ext += '...';
			}
		}
		title += ' ' + ext;
	}
	document.querySelector('html > head > title')!.textContent = title;
}

const replaceHostRE = new RegExp('http://inwardly/', 'g');
function replaceHost(u: string) {
	return u.replace(replaceHostRE, `http://${window.location.host}/`);
}

function toJson(resp: Response) {
	if (resp.ok) {
		return resp.json();
	} else {
		const s = `${resp.status} ${resp.statusText}`;
		switch (resp.status) {
		case 400:
		case 500:
			return resp
				.text()
				.then( t => Promise.reject(`${s}: ${t}`) );
		default:
			return Promise.reject(s);
		}
	}
}

function parseDateTime(dtstr: string) {
	const ret = new Date();
	const dt = dtstr.split('T');
	const d = dt[0].split('-');
	const t = dt[1].substring(0, 8).split(':');
	const year = Number(d[0]);
	const month = Number(d[1])-1;
	const date = Number(d[2]);
	ret.setFullYear(year, month, date);
	const hour = Number(t[0]);
	const minute = Number(t[1]);
	const second = Number(t[2]);
	ret.setHours(hour, minute, second);
	return ret;
}

function parseDateTimeFromSaidId(id: string) {
	const ret = new Date();
	const year = 2000 + Number(id.slice(0, 2));
	const month = Number(id.slice(2, 4))-1;
	const date = Number(id.slice(4, 6));
	ret.setFullYear(year, month, date);
	const hour = Number(id.slice(6, 8));
	const minute = Number(id.slice(8, 10));
	const second = Number(id.slice(10, 12));
	const millis = Number(id.slice(12, 15));
	ret.setHours(hour, minute, second, millis);
	return ret;
}

function leadingZeros(minLength: number, num: number) {
	const ns = num.toString();
	return new Array(Math.max(0, minLength - ns.length)).fill('0').join() + ns;
}

function leadingZeros2digits(num: number) {
	return `00${num}`.slice(-2);
}

function toJpYearString(y: number) {
	return `${y}年`;
}

function toJpMonthString(m: number) {
	return `${m}月`;
}

const weekdayName = ['日','月','火','水','木','金','土'];

function toJpYmdStringFromDayId(dayId: string) {
	const year = '20' + dayId.slice(0, 2);
	const month = dayId.slice(2, 4);
	const day = dayId.slice(4, 6);
	const d = new Date();
	d.setFullYear(Number(year), Number(month)-1, Number(day));
	const week = weekdayName[d.getDay()];
	return `${year}年${month}月${day}日 (${week})`;
}

function toJpString(d: Date) {
	const year = d.getFullYear();
	const month = leadingZeros2digits(d.getMonth()+1);
	const date = leadingZeros2digits(d.getDate());
	const hour = leadingZeros2digits(d.getHours());
	const minute = leadingZeros2digits(d.getMinutes());
	const second = leadingZeros2digits(d.getSeconds());
	const week = weekdayName[d.getDay()];
	return `${year}年${month}月${date}日 (${week}) ${hour}:${minute}:${second}`;
}

function toJpDateTimeString(d: Date) {
	const year = d.getFullYear();
	const month = leadingZeros2digits(d.getMonth()+1);
	const date = leadingZeros2digits(d.getDate());
	const hour = leadingZeros2digits(d.getHours());
	const minute = leadingZeros2digits(d.getMinutes());
	const second = leadingZeros2digits(d.getSeconds());
	return `${year}-${month}-${date}T${hour}:${minute}:${second}+09:00`;
}
