///////////////
//
// Utilities
//
///////////////


let saidRepository: Dictionary<Said> = {};

function addToSaidRepository(said: Said) {
	saidRepository[said.id] = said;
}

function getFromSaidRepository(id: string) {
	if (id in saidRepository) {
		return saidRepository[id];
	} else {
		return null;
	}
}

function clearSaidRepoitory() {
	saidRepository = {};
}

/////////////
//
// Image Cache Contorol
//
/////////////

let imageCache: Dictionary<File> = {};

function createImageCache(file: File) {
	const url = URL.createObjectURL(file);
	imageCache[url] = file;
	return url;
}

function deleteImageCache(url: string) {
	if (url in imageCache) {
		URL.revokeObjectURL(url);
		delete imageCache[url];
	}
}

function getImageCacheFile(url: string) {
	if (url in imageCache) {
		return imageCache[url];
	}
	return null;
}

function clearAllImageCache() {
	const a = [];
	for (const url in imageCache) {
		URL.revokeObjectURL(url);
		a.push(url);
	}
	for (const url of a) {
		delete imageCache[url];
	}
	imageCache = {};
}

/////////////
//
// Main
//
/////////////

const currentCalender = {
	year: 0,
	month: 0,
};

let threadMode = ''; // thread ID
let memoButtonsVisible = false;

function showImageViewer(imgsrc: string, detachCallback?: () => void) {
	const iv = document.getElementById('imageviewer')!;
	const cb = <HTMLButtonElement>iv.querySelector(':scope button.closeimageviewer')!;
	cb.disabled = false;
	const di = <HTMLButtonElement>iv.querySelector(':scope button.detachimage')!;
	if (detachCallback) {
		di.disabled = false;
		di.onclick = () => {
			detachCallback();
			cb.click();
		};
	} else {
		di.disabled = true;
	}
	(<HTMLAnchorElement>iv.querySelector(':scope a')!).href = imgsrc;
	(<HTMLImageElement>iv.querySelector(':scope img')!).src = imgsrc;
	iv.classList.remove('toggleDisplayNone');
}

function submitSaid(event: MouseEvent) {
	const sayForm = (<HTMLElement>event.currentTarget!).parentElement!;
	const textarea = <HTMLTextAreaElement>sayForm.querySelector('textarea')!;
	const text = textarea.value;
	if (text === '') {
		const ibs = sayForm.querySelectorAll(':scope button.imagebutton');
		if (ibs.length === 0) {
			showError('empty!');
			return;
		}
	}
	const secret = (<HTMLInputElement>sayForm.querySelector('input[name="secret"]')!).checked;
	// img?chanedとtargetのhidden…
	const hiddens = sayForm.querySelectorAll(':scope > input[name^="img"][type="hidden"]');
	const target = (<HTMLInputElement>sayForm.querySelector('input[name="target"]')!).value;
	let sayType: SayType = 'say';
	let api = '/api/say';
	if (sayForm.classList.contains('edit')) {
		// editの場合、変更なしのケースの検出が欲しいかも… 	// TODO implement!
		sayType = 'edit';
		api = '/api/said/edit';
		const ts = getFromSaidRepository(target);
		if (ts === null) {
			showError('BUG!?');
			return;
		}
		let edited = false;
		edited = edited || (text !== replaceHost(ts.text))
		                || (secret !== ts.secret);
		hiddens.forEach( h => {
			edited = edited || (<HTMLInputElement>h).value !== '0';
		});
		if (!edited) {
			showError('nothing changes');
			return;
		}
	} else if (sayForm.classList.contains('reply')) {
		sayType = 'reply';
		api = '/api/reply';
	}
	// submitボタンとimageボタン…
	const buttons = sayForm.querySelectorAll('button');
	buttons.forEach( btn => { (<HTMLButtonElement>btn).disabled = true; });
	textarea.disabled = true;
	const inputs = sayForm.querySelectorAll('input[type="file"]');
	const formData = new FormData();
	formData.set('target', target);
	formData.set('text', text);
	formData.set('secret', secret ? 'on' : '');
	for (let i = 0; i < inputs.length; i++) {
		if (0 < hiddens.length) {
			const hv = (<HTMLInputElement>hiddens[i]).value;
			formData.set(`img${i}changed`, hv);
			if (hv === '0') {
				continue;
			}
		}
		const btn = <HTMLButtonElement>buttons[i+1];
		if (btn.style.backgroundImage === '') {
			continue;
		}
		const u = btn.style.backgroundImage.slice(5, -2);
		const file = getImageCacheFile(u);
		if (file !== null) {
			formData.set(`img${i}`, file);
		}
	}
	fetch(api, {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then( (sayResp: SayResponse) => {
		const said = sayResp.said;
		addToSaidRepository(said);
		if (sayResp.memo !== null) {
			// editの場合、memoの更新もあるかも(json内でその情報を通知すべき) // TODO implement!
			addMemo(sayResp.memo);
		}
		if (window.location.pathname === '/mention') {
			history.replaceState(null, '', `/thread/${said.id}`);
			threadMode = said.id;
			const thread = document.getElementById('thread')!;
			thread.classList.remove('toggleDisplayNone');
			thread.querySelector(':scope > :first-child')!.textContent = `thread ${said.id}`;
			setThreadTitle(said);
		}
		if (sayType === 'edit') {
			updateSaid(said);
			updateQuote(said);
		} else {
			insertSaid(said);
		}
		if (sayType !== 'edit') {
			textarea.value = '';
		}
		// reply,editのときの投稿フォームを消す
		if (sayType !== 'say' && !sayForm.classList.contains('toggleDisplayNone')) {
			const action = sayForm.parentElement!.querySelector('.action')!;
			switch (sayType) {
			case 'reply':
				(<HTMLButtonElement>action.querySelector('.replybutton')!).click();
				break;
			case 'edit':
				(<HTMLButtonElement>action.querySelector('.editbutton')!).click();
				break;
			}
		}
		inputs.forEach( inp => {
			(<HTMLInputElement>inp).value = '';
		});
		// 画像投稿ボタンのサムネの削除
		buttons.forEach( btn => {
			const elm = <HTMLElement>btn;
			if (!elm.classList.contains('imagebutton')) {
				return;
			}
			elm.classList.add('softbutton');
			elm.classList.remove('imagebutton');
			const url = elm.style.backgroundImage;
			if (url !== '') {
				elm.style.backgroundImage = '';
				deleteImageCache(url.slice(5, -2));
			}
		});
		if (said.image !== null && said.image.length > 0) {
			const f = () => {
				const images = document.querySelectorAll(`#said${said.id} > .images img`);
				images.forEach( e => {
					const img = <HTMLImageElement>e;
					const src = img.src;
					img.src = src; // リロード効果？
				});
			};
			window.setTimeout(f, 2000);
			window.setTimeout(f, 5000);
			window.setTimeout(f, 10000);
		}
		if (threadMode !== '') {
			if (said.id === threadMode) {
				setThreadTitle(said);
			} else if (said.thread === threadMode && sayType === 'reply') {
				window.location.hash = `#said${said.id}`;
			}
		}
	})
	.catch( err => {
		// エラーの想定が必要		// TODO implement!
		showError(err);
	})
	.finally( () => {
		buttons.forEach( btn => { (<HTMLButtonElement>btn).disabled = false; });
		textarea.disabled = false;
		if (sayType === 'say') {
			textarea.focus();
		}
	});
}

function clickImageButton(i: number, btn: HTMLButtonElement, inp: HTMLInputElement) {
	return () => {
		const bi = btn.style.backgroundImage;
		if (bi === '') {
			inp.click();
		} else {
			const u = bi.slice(5, -2);
			showImageViewer(u, () => {
				btn.style.backgroundImage = '';
				btn.classList.remove('imagebutton');
				btn.classList.add('softbutton');
				deleteImageCache(u);
				inp.value = '';
				const h = btn.parentElement!.parentElement!.querySelector(`:scope > input[name="img${i}changed"]`);
				if (h !== null) {
					(<HTMLInputElement>h).value = '1';
				}
			});
		}
	};
}

function changeButtonImage(i: number, btn: HTMLButtonElement, inp: HTMLInputElement) {
	return (files: FileList | null) => {
		const bi = btn.style.backgroundImage;
		const h = btn.parentElement!.parentElement!.querySelector(`:scope > input[name="img${i}changed"]`);
		if (bi !== '') {
			btn.style.backgroundImage = '';
			btn.classList.remove('imagebutton');
			btn.classList.add('softbutton');
			deleteImageCache(bi.slice(5, -2));
			if (h !== null) {
				(<HTMLInputElement>h).value = '1';
			}
		}
		if (files === null) {
			files = inp.files!;
		}
		if (files.length === 0) {
			return;
		}
		if (!files[0].type.startsWith('image/')) {
			return;
		}
		const u = createImageCache(files[0]);
		btn.classList.remove('softbutton');
		btn.classList.add('imagebutton');
		btn.style.backgroundImage = `url("${u}")`;
		if (h !== null) {
			(<HTMLInputElement>h).value = '1';
		}
	};
}

function submitTemplate(placeholder: string) {
	const template = <HTMLTemplateElement>document.getElementById('submit-template')!;
	const fragment = document.importNode(template.content, true);
	const textarea = <HTMLTextAreaElement>fragment.querySelector('textarea')!;
	const submit = <HTMLButtonElement>fragment.querySelector('textarea + button')!;
	textarea.placeholder = placeholder;
	textarea.addEventListener('keydown', event => {
		if (event.ctrlKey && event.key === 'Enter' && !submit.disabled) {
			submit.click();
		}
	});
	submit.addEventListener('click', submitSaid);
	const buttons = fragment.querySelectorAll('.attachimage > button');
	const inputs = fragment.querySelectorAll('.attachimage > input');
	let cbi: ((fl: FileList | null) => void)[] = [];
	for (let i = 0; i < buttons.length; i++) {
		const btn = <HTMLButtonElement>buttons[i];
		const inp = <HTMLInputElement>inputs[i];
		btn.addEventListener('click', clickImageButton(i, btn, inp));
		cbi.push(changeButtonImage(i, btn, inp));
		inp.addEventListener('input', () => {
			cbi[i](null);
		});
	}
	textarea.addEventListener('paste', event => {
		const cd = event.clipboardData;
		if (cd ===  null) {
			return;
		}
		if (cd.files.length !== 1) {
			return;
		}
		if (!cd.files[0].type.startsWith('image/')) {
			return;
		}
		for (let i = 0; i < buttons.length; i++) {
			const btn = <HTMLButtonElement>buttons[i];
			const inp = <HTMLInputElement>inputs[i];
			const bi = btn.style.backgroundImage;
			if (bi !== '') {
				continue;
			}
			cbi[i](cd.files);
			break;
		}
	});
	return fragment;
}

function replyTemplate(id: string) {
	const template = <HTMLTemplateElement>document.getElementById('reply-template')!;
	const fragment = document.importNode(template.content, true);
	fragment.querySelector('.reply')!.appendChild(submitTemplate('reply...'));
	(<HTMLInputElement>fragment.querySelector('input[name="target"]')!).value = id;
	return fragment;
}

function editTemplate(id: string) {
	const template = <HTMLTemplateElement>document.getElementById('edit-template')!;
	const fragment = document.importNode(template.content, true);
	fragment.querySelector('.edit')!.appendChild(submitTemplate(''));
	(<HTMLInputElement>fragment.querySelector('input[name="target"]')!).value = id;
	return fragment;
}

function historyTemplate(del: boolean, content: { id?: string; update?: string; date: string; text: string; image: string[] | null; secret: boolean;}) {
	const template = <HTMLTemplateElement>document.getElementById('history-template')!;
	const fragment = document.importNode(template.content, true);
	const dt = fragment.querySelector('.datetime')!;
	const tm = dt.appendChild(document.createElement('time'));
	let d = content.date;
	if (content.update) {
		if (content.update !== '') {
			d = content.update;
		}
	}
	tm.dateTime = d;
	tm.textContent = toJpString(parseDateTime(d));
	if (content.secret) {
		dt.classList.add('secret');
	}
	const text = fragment.querySelector('.text')!;
	const btn = fragment.querySelector('button')!;
	if (del) {
		text.classList.add('deleted');
		text.textContent = 'delete';
		btn.disabled = true;
		btn.classList.add('toggleDisplayNone');
	} else {
		text.textContent = replaceHost(content.text);
	}
	if (content.id) {
		btn.disabled = true;
		btn.classList.add('toggleDisplayNone');
		dt.appendChild(document.createTextNode(' (Current)'));
	}
	if (content.image) {
		const imageDiv = fragment.querySelector('li')!
			.appendChild(document.createElement('div'));
		updateImages(content.image, imageDiv, null);
	}
	return fragment;
}

function historyTemplateForMemo(hm: Memo) {
	const template = <HTMLTemplateElement>document.getElementById('history-template')!;
	const fragment = document.importNode(template.content, true);
	const dt = fragment.querySelector('.datetime')!;
	const tm = dt.appendChild(document.createElement('time'));
	tm.dateTime = hm.date;
	tm.textContent = toJpString(parseDateTime(hm.date));
	const text = <HTMLElement>fragment.querySelector('.text')!;
	expandElements(hm.elements, text);
	if (hm.image) {
		const imageDiv = fragment.querySelector('li')!
			.appendChild(document.createElement('div'));
		updateImages(hm.image, imageDiv, null);
	}
	const btn = fragment.querySelector('button')!;
	btn.addEventListener('click', recoverMemoFromHistory(hm.id));
	const a = btn.parentNode!.insertBefore(document.createElement('a'), btn);
	a.classList.add('threadlink');
	if (hm.thread !== '') {
		a.href = `/thread/${hm.thread}#said${hm.id}`;
	} else {
		a.href = `/thread/${hm.id}`;
	}
	a.textContent = '#';
	btn.parentNode!.insertBefore(document.createElement('span'), btn).innerHTML = '&nbsp;&nbsp;';
	return fragment;
}

function historyTemplateForQuoted(said: Said) {
	const template = <HTMLTemplateElement>document.getElementById('history-template')!;
	const fragment = document.importNode(template.content, true);
	const dt = fragment.querySelector('.datetime')!;
	const tm = dt.appendChild(document.createElement('time'));
	tm.dateTime = said.date;
	tm.textContent = toJpString(parseDateTime(said.date));
	if (said.secret) {
		dt.classList.add('secret');
	}
	if (said.update !== '') {
		const span = dt.appendChild(document.createElement('span'));
		span.classList.add('update');
		span.appendChild(document.createTextNode('(UPD '));
		const update = span.appendChild(document.createElement('time'));
		span.appendChild(document.createTextNode(')'));
		(<HTMLTimeElement>update).dateTime = said.update;
		update.textContent = toJpString(parseDateTime(said.update));
	}
	const text = <HTMLElement>fragment.querySelector('.text')!;
	const btn = fragment.querySelector('button')!;
	btn.disabled = true;
	btn.classList.add('toggleDisplayNone');
	updateText(said, text);
	if (said.image) {
		const imageDiv = fragment.querySelector('li')!
			.appendChild(document.createElement('div'));
		updateImages(said.image, imageDiv, null);
	}
	const li = fragment.querySelector('li')!;
	let qinfo = li.querySelector('div.quoteinfo'); // always null
	if (said.parent !== '') {
		if (qinfo === null) {
			qinfo = li.appendChild(document.createElement('div'));
			qinfo.classList.add('quoteinfo');
		}
		const span = qinfo.appendChild(document.createElement('span'));
		span.classList.add('parentinfo');
		span.textContent = `to: ${said.parent}`;
	}
	if (said.children !== null) {
		if (qinfo === null) {
			qinfo = li.appendChild(document.createElement('div'));
			qinfo.classList.add('quoteinfo');
		}
		const span = qinfo.appendChild(document.createElement('span'));
		span.classList.add('childreninfo');
		span.textContent = `replies: ${said.children.length}`;
	}
	const a = btn.parentNode!.insertBefore(document.createElement('a'), btn);
	a.classList.add('threadlink');
	if (said.thread !== '') {
		a.href = `/thread/${said.thread}#said${said.id}`;
	} else {
		a.href = `/thread/${said.id}`;
	}
	a.textContent = '#'
	return fragment;
}

function dummyReplyTemplate(threadId: string, saidId: string) {
	const template = <HTMLTemplateElement>document.getElementById('dummy-reply-template')!;
	const fragment = document.importNode(template.content, true);
	fragment.querySelector('li')!.id = saidId;
	const t = fragment.querySelector('.datetime')!.appendChild(document.createElement('time'));
	const d = parseDateTimeFromSaidId(saidId);
	t.dateTime = toJpDateTimeString(d);
	t.textContent = toJpString(d);
	(<HTMLAnchorElement>fragment.querySelector('.threadlink')!).href = `/thread/${threadId}#said${saidId}`;
	return fragment;
}

function quoteTemplate(said: Said) {
	const template = <HTMLTemplateElement>document.getElementById('quote-template')!;
	const fragment = document.importNode(template.content, true);
	const bq = fragment.querySelector('blockquote')!;
	const dt = bq.querySelector('.datetime')!;
	bq.classList.add(`quote${said.id}`);
	const time = dt.appendChild(document.createElement('time'));
	time.dateTime = said.date;
	time.textContent = toJpString(parseDateTime(said.date));
	if (said.secret) {
		dt.classList.add('secret');
	}
	if (said.update !== '') {
		let update = bq.querySelector(':scope > .saidheader .update > time');
		if (update === null) {
			const span = dt.appendChild(document.createElement('span'));
			span.classList.add('update');
			span.appendChild(document.createTextNode('(UPD '));
			update = span.appendChild(document.createElement('time'));
			span.appendChild(document.createTextNode(')'));
		}
		(<HTMLTimeElement>update).dateTime = said.update;
		update.textContent = toJpString(parseDateTime(said.update));
	}
	let threadUrl: string;
	if (isTimelineSaid(said)) {
		if (said.thread === '') {
			threadUrl = `/thread/${said.id}`;
		} else {
			threadUrl = `/thread/${said.thread}#said${said.id}`;
		}
	} else {
		threadUrl = `#said${said.id}`;
	}
	(<HTMLAnchorElement>bq.querySelector('.threadlink')!).href = threadUrl;
	const text = <HTMLElement>bq.querySelector(':scope > .text')!;
	updateText(said, text);
	if (said.image !== null) {
		const imageDiv = bq.appendChild(document.createElement('div'));
		imageDiv.classList.add('images');
		updateImages(said.image, imageDiv, null);
	}
	let qinfo = bq.querySelector('div.quoteinfo'); // always null
	if (said.parent !== '') {
		if (qinfo === null) {
			qinfo = bq.appendChild(document.createElement('div'));
			qinfo.classList.add('quoteinfo');
		}
		const span = qinfo.appendChild(document.createElement('span'));
		span.classList.add('parentinfo');
		span.textContent = `to: ${said.parent}`;
	}
	if (said.children !== null) {
		if (qinfo === null) {
			qinfo = bq.appendChild(document.createElement('div'));
			qinfo.classList.add('quoteinfo');
		}
		const span = qinfo.appendChild(document.createElement('span'));
		span.classList.add('childreninfo');
		span.textContent = `replies: ${said.children.length}`;
	}
	return fragment;
}

function resultTemplate(said: Said) {
	const template = <HTMLTemplateElement>document.getElementById('result-template')!;
	const fragment = document.importNode(template.content, true);
	const li = fragment.querySelector('li')!;
	const time = li.querySelector('.datetime')!.appendChild(document.createElement('time'));
	time.dateTime = said.date;
	time.textContent = toJpString(parseDateTime(said.date));
	if (said.update !== '') {
		let update = li.querySelector(':scope > .saidheader .update > time');
		if (update === null) {
			const span = li.querySelector('.datetime')!
				.appendChild(document.createElement('span'));
			span.classList.add('update');
			span.appendChild(document.createTextNode('(UPD '));
			update = span.appendChild(document.createElement('time'));
			span.appendChild(document.createTextNode(')'));
		}
		(<HTMLTimeElement>update).dateTime = said.update;
		update.textContent = toJpString(parseDateTime(said.update));
	}
	if (said.secret) {
		li.querySelector('.datetime')!.classList.add('secret');
	}
	let threadUrl: string;
	if (isTimelineSaid(said)) {
		if (said.thread === '') {
			threadUrl = `/thread/${said.id}`;
		} else {
			threadUrl = `/thread/${said.thread}#said${said.id}`;
		}
	} else {
		threadUrl = `#said${said.id}`;
	}
	(<HTMLAnchorElement>li.querySelector('.threadlink')!).href = threadUrl;
	const text = <HTMLElement>li.querySelector(':scope > .text')!;
	updateText(said, text);
	if (said.image !== null) {
		const imageDiv = li.appendChild(document.createElement('div'));
		imageDiv.classList.add('images');
		updateImages(said.image, imageDiv, null);
	}
	let qinfo = li.querySelector('div.quoteinfo'); // always null
	if (said.parent !== '') {
		if (qinfo === null) {
			qinfo = li.appendChild(document.createElement('div'));
			qinfo.classList.add('quoteinfo');
		}
		const span = qinfo.appendChild(document.createElement('span'));
		span.classList.add('parentinfo');
		span.textContent = `to: ${said.parent}`;
	}
	if (said.children !== null) {
		if (qinfo === null) {
			qinfo = li.appendChild(document.createElement('div'));
			qinfo.classList.add('quoteinfo');
		}
		const span = qinfo.appendChild(document.createElement('span'));
		span.classList.add('childreninfo');
		span.textContent = `replies: ${said.children.length}`;
	}
	return fragment;
}

function updateQuote(said: Said) {
	const qs = document.querySelectorAll(`.quote${said.id}`);
	qs.forEach( q => {
		if (said.secret) {
			q.querySelector('.datetime')!.classList.add('secret');
		} else {
			q.querySelector('.datetime')!.classList.remove('secret');
		}
		if (said.update !== '') {
			let update = q.querySelector(':scope > .saidheader .update > time');
			if (update === null) {
				const span = q.querySelector('.datetime')!
					.appendChild(document.createElement('span'));
				span.classList.add('update');
				span.appendChild(document.createTextNode('(UPD '));
				update = span.appendChild(document.createElement('time'));
				span.appendChild(document.createTextNode(')'));
			}
			(<HTMLTimeElement>update).dateTime = said.update;
			update.textContent = toJpString(parseDateTime(said.update));
		}
		if (said.children !== null && said.children.length > 0) {
			let qinfo = q.querySelector(':scope > div.quoteinfo');
			if (qinfo === null) {
				qinfo = q.appendChild(document.createElement('div'));
				qinfo.classList.add('quoteinfo');
			}
			let chinfo = qinfo.querySelector('.childreninfo');
			if (chinfo === null) {
				chinfo = qinfo.appendChild(document.createElement('span'));
				chinfo.classList.add('childreninfo');
			}
			chinfo.textContent = `replies: ${said.children.length}`;
		}
		const text = <HTMLElement>q.querySelector(':scope > .text')!;
		updateText(said, text);
		let imageDiv = q.querySelector(':scope > .images');
		if (imageDiv === null) {
			if (said.image === null) {
				return;
			}
			imageDiv = q.appendChild(document.createElement('div'));
			imageDiv.classList.add('images');
		} else if (said.image === null) {
			q.removeChild(imageDiv);
			return;
		}
		const btn = imageDiv.querySelectorAll('button');
		updateImages(said.image, imageDiv, btn);
	});
}

function recoverMemoFromHistory(id: string) {
	return () => {
		const btn = document.querySelectorAll('#historyviewer button.recoverhistory:not([disabled])');
		btn.forEach( b => { (<HTMLButtonElement>b).disabled = true; } );
		const formData = new FormData();
		formData.set('target', id);
		fetch('/api/memo/add', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then(addMemo)
		.then( () => {
			document.querySelectorAll('#memo > :not(:first-child)')
			.forEach( item => {
				item.classList.remove('toggleDisplayNone');
			});
			showMessage('recover memo!', false);
			(<HTMLButtonElement>document.querySelector('#historyviewer button.closehistoryviewer')!).click();
		})
		.catch( err => {
			showError(err);
			btn.forEach( b => {
				if (b) {
					(<HTMLButtonElement>b).disabled = false;
				}
			});
		});
	};
}

function historyMemoButton() {
	fetch('/api/memo/history')
	.then(toJson)
	.then( (hist: Memo[]) => {
		const hisview = document.getElementById('historyviewer')!;
		hisview.classList.remove('toggleDisplayNone');
		hisview.querySelector(':scope > div > h5')!.textContent = 'memo history viewer';

		const ul = hisview.querySelector(':scope ul')!;
		ul.innerHTML = '';

		for (const hm of hist) {
			const temp = historyTemplateForMemo(hm);
			ul.appendChild(temp);
		}
	})
	.catch( err => {
		showError(err);
	});
}

function removeMemoButton(id: string) {
	return () => {
		const formData = new FormData();
		formData.set('target', id);
		fetch('/api/memo/remove', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then( (memo: Memo) => {
			removeMemo(memo.id);
		})
		.catch( err => {
			// エラーの想定が必要		// TODO implement!
			showError(err);
		});
	};
}

function removeMemo(id: string) {
	const memoList = document.querySelector('#memo > ul')!;
	const elm = memoList.querySelector(`:scope > .memo${id}`);
	if (elm !== null) {
		memoList.removeChild(elm);
	}
}

function updateMemo(memo: Memo, li: Element) {
	const time = li.querySelector('.datetime')!;
	(<HTMLTimeElement>time).dateTime = memo.date;
	time.textContent = toJpString(parseDateTime(memo.date));
	const text = <HTMLElement>li.querySelector('.memotext')!;
	expandElements(memo.elements, text);
	let imageDiv = li.querySelector(':scope > .images');
	if (memo.image === null || memo.image.length === 0) {
		if (imageDiv !== null) {
			li.removeChild(imageDiv);
		}
	} else {
		if (imageDiv === null) {
			const action = li.querySelector(':scope > .memoaction');
			imageDiv = li.insertBefore(document.createElement('div'), action);
			imageDiv.classList.add('images');
		}
		const imgs = imageDiv.querySelectorAll('button.thumbnail');
		updateImages(memo.image, imageDiv, imgs);
	}
}

function moveMemo(id: string, value: number) {
	return () => {
		const formData = new FormData();
		formData.set('target', id);
		formData.set('value', `${value}`);
		fetch('/api/memo/move', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then( (res: MemoSwap) => {
			const id1 = res.memo[0].id;
			const id2 = res.memo[1].id;
			const memoList = document.querySelector('#memo > ul')!;
			const elm1 = memoList.querySelector(`:scope > .memo${id1}`)!;
			const elm2 = memoList.querySelector(`:scope > .memo${id2}`)!;
			memoList.removeChild(elm2);
			memoList.insertBefore(elm2, elm1);
		})
		.catch( err => {
			showError(err);
		});
	};
}

function makeMemo(memo: Memo) {
	const template = <HTMLTemplateElement>document.getElementById('memo-template')!;
	const fragment = document.importNode(template.content, true);
	const li = fragment.querySelector('li')!;
	li.classList.add(`memo${memo.id}`);
	li.querySelector('.removebutton')!
		.addEventListener('click', removeMemoButton(memo.id));
	// threadはページ遷移処理なのでanchorタグのほうがいいのか？
	const thbtn = li.querySelector('.threadbutton')!;
	thbtn.addEventListener('click', () => {
		(<HTMLButtonElement>thbtn).disabled = true;
		let u: string;
		if (memo.thread === '') {
			u = `/thread/${memo.id}`;
		} else {
			u = `/thread/${memo.thread}#said${memo.id}`;
		}
		history.pushState(null, '', u);
		load();
		Promise.resolve()
		.then( () => {
			(<HTMLButtonElement>thbtn).disabled = false;
		});
	}); // TODO implement!
	li.querySelector('.memoaction > button:nth-of-type(3)')!
		.addEventListener('click', moveMemo(memo.id, -1));
	li.querySelector('.memoaction > button:nth-of-type(4)')!
		.addEventListener('click', moveMemo(memo.id, 1));
	if (memoButtonsVisible) {
		li.querySelector('.memoaction')!.classList.remove('toggleDisplayNone');
	} else {
		li.querySelector('.memoaction')!.classList.add('toggleDisplayNone');
	}
	updateMemo(memo, li);
	return fragment;
}

function addMemo(memo: Memo) {
	const memoList = document.querySelector('#memo > ul')!;
	let elm = memoList.querySelector(`:scope > .memo${memo.id}`);
	if (elm === null) {
		memoList.insertBefore(makeMemo(memo), memoList.firstChild);
	} else {
		updateMemo(memo, elm);
	}
}

function toggleReplyButton(scope: HTMLElement, id: string) {
	return () => {
		let reply = scope.querySelector(':scope > .reply');
		if (reply === null) {
			const ul = scope.querySelector(':scope > ul');
			scope.insertBefore(replyTemplate(id), ul);
			reply = scope.querySelector(':scope > .reply')!;
		}
		if (!reply.classList.toggle('toggleDisplayNone')) {
			const said = getFromSaidRepository(id);
			if (said !== null) {
				(<HTMLInputElement>reply.querySelector('input[name="secret"]')!)
					.checked = said.secret;
			}
			(<HTMLTextAreaElement>reply.querySelector('textarea')!).focus();
		}
	};
}

function resetEdit(scope: HTMLElement, edit: Element) {
	const target = <HTMLInputElement>edit.querySelector(':scope > input[name="target"]')!;
	const said = getFromSaidRepository(target.value)!;
	// text
	// 改行やリンクをどうするか…textContentのままでは考慮できない…		// TODO implement!
	const text = scope.querySelector(':scope > .text')!;
	let textValue: string;
	if (text.classList.contains('deleted')) {
		textValue = '';
	} else {
		textValue = replaceHost(said.text);
	}
	(<HTMLInputElement>edit.querySelector('input[name="secret"]')!)
		.checked = said.secret;
	(<HTMLTextAreaElement>scope.querySelector(':scope > .edit > textarea')!).value = textValue;
	// images
	const images = scope.querySelectorAll(':scope > .images img');
	const hiddens = edit.querySelectorAll(':scope > input[name^="img"][type="hidden"]');
	for (let i = 0; i < 4; i++) {
		let button = <HTMLElement>scope.querySelector(`:scope > .edit .img${i}`)!;
		(<HTMLInputElement>hiddens[i]).value = '0';
		const bg = button.style.backgroundImage;
		if (i < images.length) {
			const imgsrc = <HTMLImageElement>images[i];
			const url = `url("/img/${imgsrc.alt}")`;
			if (bg !== '' && bg !== url) {
				button.style.backgroundImage = '';
				deleteImageCache(bg.slice(5, -2));
			}
			button.classList.remove('softbutton');
			button.classList.add('imagebutton');
			button.style.backgroundImage = url;
		} else {
			button.classList.add('softbutton');
			button.classList.remove('imagebutton');
			if (bg !== '') {
				button.style.backgroundImage = '';
				deleteImageCache(bg.slice(5, -2));
			}
		}
	}
}

function toggleEditButton(scope: HTMLElement, id: string) {
	return () => {
		let edit = scope.querySelector(':scope > .edit');
		if (edit === null) {
			const ul = scope.querySelector(':scope > ul');
			scope.insertBefore(editTemplate(id), ul);
			edit = scope.querySelector(':scope > .edit')!;
		}
		if (!edit.classList.toggle('toggleDisplayNone')) {
			resetEdit(scope, edit);
			(<HTMLTextAreaElement>edit.querySelector('textarea')!).focus();
		}
	};
}

function expandElements(elements: TextElement[] | null, text: HTMLElement) {
	text.innerHTML = '';
	if (elements !== null) {
		for (const te of elements) {
			switch (te.type) {
			case 'text':
				text.appendChild(document.createTextNode(te.src));
				break;
			case 'url':
				const a = text.appendChild(document.createElement('a'));
				const hd = te.src.split('\n');
				a.href = hd[0];
				a.textContent = hd[hd.length-1];
				a.target = '_blank';
				a.rel = 'noopener noreferrer nofollow external';
				break;
			case 'inside-url':
				const ia = text.appendChild(document.createElement('a'));
				const idd = te.src.split('\n');
				ia.href = idd[0];
				ia.textContent = idd[idd.length-1];
				break;
			case 'inside-img':
				const btn = updateImageButton(te.src, null, null);
				text.appendChild(btn);
				break;
			case 'quote':
				const qa = text.appendChild(document.createElement('a'));
				const qh = te.src.split('\n');
				qa.classList.add('quotelink');
				qa.href = qh[0];
				qa.textContent = qh[1];
				break;
			}
		}
	}
}

function updateText(said: Said, text: HTMLElement) {
	if (said.deleted) {
		text.innerHTML = '';
		text.classList.add('deleted');
		text.appendChild(
			document.createTextNode('deleted')
		);
	} else {
		text.classList.remove('deleted');
		// 改行やリンクをどうするか…				// TODO implement!
		expandElements(said.elements, text);
	}
}

function updateImageButton(imgsrc: string, btn: HTMLButtonElement | null, i: number | null) {
	const src = '/img/' + imgsrc;
	const thumb = '/thumb/' + imgsrc.replace(/\..+$/, '.png');
	let img: HTMLImageElement;
	if (btn !== null) {
		img = <HTMLImageElement>btn.firstChild!;
	} else {
		btn = document.createElement('button');
		btn.classList.add('thumbnail');
		btn.type = 'button';
		img = btn.appendChild(document.createElement('img'));
		if (i !== null) {
			img.classList.add(`img${i}`);
		}
		img.width = 100;
	}
	img.alt = imgsrc;
	img.title = imgsrc;
	img.src = thumb;
	btn.onclick = () => {
		showImageViewer(src);
	};
	return btn;
}

function updateImages(image: string[] | null, imageDiv: Element, imgs: NodeListOf<Element> | null) {
	const imgLen = image!.length;
	const length = imgs !== null ? Math.max(imgLen, imgs.length) : imgLen;
	for (let i = 0; i < length; i++) {
		if (i < imgLen && image !== null) {
			if (imgs !== null && i < imgs.length) {
				let btn = <HTMLButtonElement>imgs[i];
				updateImageButton(image[i], btn, i);
			} else {
				let btn = updateImageButton(image[i], null, i);
				imageDiv.appendChild(btn);
			}
		} else if (imgs !== null) {
			imageDiv.removeChild(imgs[i]);
		}
	}
}

function updateSaid(said: Said, elms?: { li: Element; action: Element; }) {
	const li = elms?.li ?? document.querySelector(`#said${said.id}`)!;
	const text = <HTMLElement>li.querySelector('.text')!;
	updateText(said, text);
	li.querySelectorAll(':scope > blockquote').forEach( q => li.removeChild(q) );
	if (said.quote !== null) {
		const nx = text.nextSibling!;
		for (const q of said.quote) {
			const bq = quoteTemplate(q);
			li.insertBefore(bq, nx);
		}
	}
	const qBtn = <HTMLButtonElement>li.querySelector('.quotedbutton')!;
	if (said.refs > 0) {
		qBtn.disabled = false;
		qBtn.classList.remove('toggleDisplayNone');
	} else {
		qBtn.disabled = true;
		qBtn.classList.add('toggleDisplayNone');
	}
	const dt = li.querySelector(':scope > .saidheader > .datetime')!;
	if (said.secret) {
		dt.classList.add('secret');
	} else {
		dt.classList.remove('secret');
	}
	if (said.update !== '') {
		li.querySelector('.historybutton')!.classList.remove('toggleDisplayNone');
		let update = li.querySelector(':scope > .saidheader .update > time');
		if (update === null) {
			const span = li.querySelector('.datetime')!
				.appendChild(document.createElement('span'));
			span.classList.add('update');
			span.appendChild(document.createTextNode('(UPD '));
			update = span.appendChild(document.createElement('time'));
			span.appendChild(document.createTextNode(')'));
		}
		(<HTMLTimeElement>update).dateTime = said.update;
		update.textContent = toJpString(parseDateTime(said.update));
	}
	let imageDiv = li.querySelector(':scope > .images');
	if (said.image === null || said.image.length === 0) {
		if (imageDiv !== null) {
			li.removeChild(imageDiv);
		}
	} else {
		if (imageDiv === null) {
			const action = elms?.action ?? li.querySelector('.action')!;
			imageDiv = li.insertBefore(document.createElement('div'), action);
			imageDiv.classList.add('images');
		}
		const imgs = imageDiv.querySelectorAll('button.thumbnail');
		updateImages(said.image, imageDiv, imgs);
	}
}

function memoSaidButton(id: string) {
	return () => {
		const said = getFromSaidRepository(id);
		if (said === null) {
			showError('BUG!?');
			return;
		}
		if (said.deleted) {
			showError(`the said has deleted (Said ID ${id})`);
			return;
		}
		const exists = document.querySelector(`#memo > ul > li.memo${id}`);
		if (exists !== null) {
			const formData = new FormData();
			formData.set('target', id);
			fetch('/api/memo/remove', {
				method: 'POST',
				body: formData,
			})
			.then(toJson)
			.then( (memo: Memo) => {
				removeMemo(memo.id);
				showMessage('remove memo!', false);
			})
			.catch( err => {
				showError(err);
			});
			return;
		}
		const formData = new FormData();
		formData.set('target', id);
		fetch('/api/memo/add', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then(addMemo)
		.then( () => {
			document.querySelectorAll('#memo > :not(:first-child)')
			.forEach( item => {
				item.classList.remove('toggleDisplayNone');
			});
			showMessage('add memo!', false);
		})
		.catch( err => {
			// エラーの想定が必要		// TODO implement!
			showError(err);
		});
	};
}

function deleteSaidButton(id: string) {
	return () => {
		const curSaid = getFromSaidRepository(id);
		if (curSaid === null) {
			showError('BUG!?');
			return;
		}
		if (curSaid.deleted) {
			showError('already deleted');
			return;
		}
		const formData = new FormData();
		formData.set('target', id);
		fetch('/api/said/delete', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then( (saidresp: SayResponse) => {
			if (saidresp.memo !== null) {
				// 本当はjsonで来る、memoの更新も要求あるかも // TODO implement!
				removeMemo(saidresp.memo.id);
			}
			const said = saidresp.said;
			addToSaidRepository(said);
			updateSaid(said);
			updateQuote(said);
			if (said.id === threadMode) {
				setThreadTitle(said);
			}
		})
		.catch( err => {
			// エラーの想定が必要		// TODO implement!
			showError(err);
		});
	};
}

function recoverFromHistory(id: string, i: number) {
	return () => {
		const btn = document.querySelectorAll('#historyviewer button.recoverhistory:not([disabled])');
		btn.forEach( b => { (<HTMLButtonElement>b).disabled = true; } );
		const formData = new FormData();
		formData.set('target', id);
		formData.set('index', `${i}`);
		fetch('/api/recover', {
			method: 'POST',
			body: formData,
		})
		.then(toJson)
		.then( (saidresp: SayResponse) => {
			const said = saidresp.said;
			addToSaidRepository(said);
			updateSaid(said);
			updateQuote(said);
			if (said.id === threadMode) {
				setThreadTitle(said);
			}
			(<HTMLButtonElement>document.querySelector('#historyviewer button.closehistoryviewer')!).click();
		})
		.catch( err => {
			showError(err);
			btn.forEach( b => {
				if (b) {
					(<HTMLButtonElement>b).disabled = false;
				}
			});
		});
	};
}

function historySaidButton(id: string) {
	return () => {
		fetch(`/api/history/${id}`)
		.then(toJson)
		.then( (hist: EditHistory) => {
			const hisview = document.getElementById('historyviewer')!;
			hisview.classList.remove('toggleDisplayNone');
			hisview.querySelector(':scope > div > h5')!.textContent = 'history viewer';

			const ul = hisview.querySelector(':scope ul')!;
			ul.innerHTML = '';

			if (hist.history !== null) {
				for (let i = 0; i < hist.history.length; i++) {
					const ps = hist.history[i];
					const temp = historyTemplate(ps.delete, ps);
					if (!ps.delete) {
						temp.querySelector('button')!.addEventListener('click', recoverFromHistory(id, i));
					}
					ul.insertBefore(temp, ul.firstChild);
				}
			}

			const temp = historyTemplate(hist.said.deleted, hist.said);
			ul.insertBefore(temp, ul.firstChild);
		})
		.catch( err => {
			showError(err);
		});
	};
}

function quotedSaidButton(id: string) {
	return () => {
		fetch(`/api/quoted/${id}`)
		.then(toJson)
		.then( (qview: QuotedRefs) => {
			const hisview = document.getElementById('historyviewer')!;
			hisview.classList.remove('toggleDisplayNone');
			hisview.querySelector(':scope > div > h5')!.textContent = 'quoted viewer';

			const ul = hisview.querySelector(':scope ul')!;
			ul.innerHTML = '';

			if (qview.refs !== null) {
				for (const ps of qview.refs) {
					const temp = historyTemplateForQuoted(ps);
					ul.insertBefore(temp, ul.firstChild);
				}
			}

		})
		.catch( err => {
			showError(err);
		});
	};
}

function makeSaid(said: Said, theDayId?: string) {
	const template = <HTMLTemplateElement>document.getElementById('said-template')!;
	const fragment = document.importNode(template.content, true);
	const li = fragment.querySelector('li')!; // fragment.firstChild ?
	li.id = 'said' + said.id;
	const time = li.querySelector('.datetime')!.appendChild(document.createElement('time'));
	time.dateTime = said.date;
	time.textContent = toJpString(parseDateTime(said.date));
	// スレッドの仕様はまだ未解決だからテキトーすぎる…  // TODO implement!
	let threadUrl: string;
	if (isTimelineSaid(said)) {
		if (said.thread === '') {
			threadUrl = `/thread/${said.id}`;
		} else {
			threadUrl = `/thread/${said.thread}#said${said.id}`;
		}
	} else {
		threadUrl = `#said${said.id}`;
	}
	(<HTMLAnchorElement>li.querySelector('.threadlink')!).href = threadUrl;
	const action = li.querySelector('.action')!;
	action.querySelector('.replybutton')!
		.addEventListener('click', toggleReplyButton(li, said.id));
	action.querySelector('.memobutton')!
		.addEventListener('click', memoSaidButton(said.id));
	action.querySelector('.editbutton')!
		.addEventListener('click', toggleEditButton(li, said.id));
	action.querySelector('.deletebutton')!
		.addEventListener('click', deleteSaidButton(said.id));
	action.querySelector('.historybutton')!
		.addEventListener('click', historySaidButton(said.id));
	action.querySelector('.quotedbutton')!
		.addEventListener('click', quotedSaidButton(said.id));
	updateSaid(said, { li: li, action: action });
	if (said.children !== null) {
		// daily-saidモードのとき過去の日のリプへの参照を表示する
		// スレッドモード時はこのブロックに入るべきではない
		if (theDayId) {
			const threadId = said.thread !== '' ? said.thread : said.id;
			const ul = li.querySelector(':scope > ul')!;
			for (const child of said.children) {
				if (child.slice(0, 6) >= theDayId) {
					continue;
				}
				// TODO 過去の日のリプ
				const rep = dummyReplyTemplate(threadId, child);
				ul.insertBefore(rep, ul.firstChild);
			}
		}
	}
	return fragment;
}

function isTimelineSaid(said: Said) {
	return threadMode === '' || (said.id !== threadMode && said.thread !== threadMode);
}

function insertSaid(said: Said, theDayId?: string) {
	const fragment = makeSaid(said, theDayId);
	if (said.parent !== '') {
		const pid = '#said' + said.parent;
		const pList = document.querySelector(`${pid} > ul`);
		if (pList !== null) {
			if (isTimelineSaid(said)) {
				const fc = pList.firstChild;
				pList.insertBefore(fragment, fc);
			} else {
				pList.appendChild(fragment);
			}
			return;
		}
		const li = fragment.querySelector('li')!;
		// const ac = li.querySelector(':scope > .text')!;
		const qi = li.insertBefore(document.createElement('div'), li.firstChild);
		qi.classList.add('quoteinfo');
		qi.textContent = `to: ${said.parent}`;
	}
	if (isTimelineSaid(said)) {
		const timeline = document.querySelector('#timeline > ul')!;
		const fc = timeline.firstChild;
		timeline.insertBefore(fragment, fc);
	} else {
		document.querySelector('#thread > ul')!.appendChild(fragment);
	}
}

function initPage(json: Page) {
	const nav = document.querySelectorAll('header > nav li button');
	(<HTMLButtonElement>nav[0]).disabled = json.prev < 0;
	(<HTMLButtonElement>nav[1]).disabled = json.next < 0;
	document.querySelector('header > nav > output')!.textContent = json.page.toString();
	for (const memo of json.memo) {
		addMemo(memo);
	}
	for (const said of json.timeline) {
		insertSaid(said);
	}
}

function setPageView(view: PageView) {
	threadMode = '';
	const nav = document.querySelectorAll('header > nav li button');
	(<HTMLButtonElement>nav[2]).disabled = false;
	const prev = <HTMLButtonElement>nav[0];
	prev.title = toJpYmdStringFromDayId(view.prev);
	prev.onclick = () => {
		for (let i = 0; i < 3; i++) {
			(<HTMLButtonElement>nav[i]).disabled = true;
		}
		history.pushState(null, '', `/page/${view.prev}`);
		load();
	};
	prev.disabled = view.prev === '';
	const next = <HTMLButtonElement>nav[1];
	next.title = toJpYmdStringFromDayId(view.next);
	next.onclick = () => {
		for (let i = 0; i < 3; i++) {
			(<HTMLButtonElement>nav[i]).disabled = true;
		}
		history.pushState(null, '', `/page/${view.next}`);
		load();
	};
	next.disabled = view.next === '';
	document.getElementById('thread')!.classList.add('toggleDisplayNone');
	document.getElementById('result')!.classList.add('toggleDisplayNone');
	const ymd = toJpYmdStringFromDayId(view.id);
	document.querySelector('#timeline > :first-child')!
		.textContent = `timeline - ${ymd}`;
	setTitle(view.next === '' ? '' : ` ${ymd}`, '');
	for (const said of view.timeline) {
		insertSaid(said, view.id);
		addToSaidRepository(said);
	}
	for (const said of view.timeline) {
		// daily-saidモードのとき未来の日からのリプへの参照を表示する
		// スレッドモード時はこのブロックに入るべきではない
		if (said.children === null || said.children.length === 0) {
			continue
		}
		const threadId = said.thread !== '' ? said.thread : said.id;
		const ul = document.querySelector(`#said${said.id} > ul`)!;
		for (const child of said.children) {
			if (child.slice(0, 6) <= view.id) {
				continue;
			}
			// TODO 未来の日ｋらのリプ
			const rep = dummyReplyTemplate(threadId, child);
			ul.insertBefore(rep, ul.firstChild);
		}
	}
}
function setThreadTitle(said: Said) {
	setTitle(`Thread ${threadMode}`, said.text);
}

function setThreadView(view: Thread) {
	threadMode = view.id;
	const nav = document.querySelectorAll('header > nav li button');
	const prev = (<HTMLButtonElement>nav[0]);
	prev.disabled = true;
	prev.title = '';
	const next = (<HTMLButtonElement>nav[1]);
	next.disabled = true;
	next.title = '';
	const timeline = document.getElementById('timeline')!;
	timeline.querySelector(':scope > :first-child')!.textContent = `timeline`;
	document.getElementById('result')!.classList.add('toggleDisplayNone');
	const thread = document.getElementById('thread')!;
	thread.classList.remove('toggleDisplayNone');
	thread.querySelector(':scope > :first-child')!.textContent = `thread ${view.id}`;
	setThreadTitle(view.said[0]);
	for (const said of view.said) {
		insertSaid(said);
		addToSaidRepository(said);
	}
	let existsPost = false;
	if (view.setting === null) {
		document.querySelector('#thread > details')!.classList.add('toggleDisplayNone');
		(<HTMLButtonElement>document.querySelector('#thread > details button')!).disabled = true;
	} else {
		document.querySelector('#thread > details')!.classList.remove('toggleDisplayNone');
		(<HTMLButtonElement>document.querySelector('#thread > details button')!).disabled = false;
		(<HTMLSelectElement>document.getElementById('thupl')!)
			.value = view.setting.auto_upload ? '1' : '0';
		(<HTMLSelectElement>document.getElementById('thupls')!)
			.value = view.setting.secret_upload ? '1' : '0';
		(<HTMLInputElement>document.getElementById('thuplt')!).value = view.setting.title;
		(<HTMLInputElement>document.getElementById('thuplc')!)
			.value = (view.setting.title !== ''  || view.setting.categories !== '') ? view.setting.categories : 'inwardly';
		const upd = document.querySelector('#thread > details > div.setting > form > div.form:nth-of-type(2) > output')!;
		if (view.setting.upload_date === '') {
			upd.textContent = '-';
		} else {
			upd.textContent = toJpString(parseDateTime(view.setting.upload_date));
		}
		const lk = document.querySelector('#thread > details > div.setting > form > div.form:nth-of-type(1) > output')!;
		lk.innerHTML = '';
		if (view.setting.blog_url === '') {
			existsPost = false;
			lk.textContent = '-';
		} else {
			existsPost = true;
			const a = lk.appendChild(document.createElement('a'));
			a.rel = 'nofollow noopener noreferrer external';
			a.target = '_blank'
			a.href = view.setting.blog_url;
			a.title = view.setting.blog_url;
			a.textContent = (new URL(view.setting.blog_url)).pathname;
		}
	}
	const db = document.querySelector('#thread > details > div.setting > form > button:nth-of-type(3)')!;
	(<HTMLButtonElement>db).disabled = !existsPost;
}

function updateThreadSetting(upload: boolean) {
	if (threadMode === '') {
		showError('not thread mode');
		return;
	}
	const up = (<HTMLSelectElement>document.getElementById('thupl')!).value;
	const sec = (<HTMLSelectElement>document.getElementById('thupls')!).value;
	const title = (<HTMLInputElement>document.getElementById('thuplt')!).value.trim();
	const categories = (<HTMLInputElement>document.getElementById('thuplc')!).value.trim();
	if (title === '') {
		if (upload || up !== '0' || categories !== '') {
			showError('require title');
			return;
		}
	}
	const btn = document.querySelectorAll('#thread > details > div.setting > form > button');
	btn.forEach( b => { (<HTMLButtonElement>b).disabled = true; } );
	const formData = new FormData();
	formData.set('autoupload', up);
	formData.set('secretupload', sec);
	formData.set('title', title);
	formData.set('categories', categories);
	const api = upload
		? `/api/upload/thread/${threadMode}`
		: `/api/setting/thread/${threadMode}`;
	let existsPost = false;
	fetch(api, {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then( (res: ThreadSetting) => {
		(<HTMLSelectElement>document.getElementById('thupl')!)
			.value = res.auto_upload ? '1' : '0';
		(<HTMLSelectElement>document.getElementById('thupls')!)
			.value = res.secret_upload ? '1' : '0';
		(<HTMLInputElement>document.getElementById('thuplt')!).value = res.title;
		(<HTMLInputElement>document.getElementById('thuplc')!)
			.value = (res.title !== '' || res.categories !== '') ? res.categories : 'inwardly';
		const upd = document.querySelector('#thread > details > div.setting > form > div.form:nth-of-type(2) > output')!;
		if (res.upload_date === '') {
			upd.textContent = '-';
		} else {
			upd.textContent = toJpString(parseDateTime(res.upload_date));
		}
		const lk = document.querySelector('#thread > details > div.setting > form > div.form:nth-of-type(1) > output')!;
		lk.innerHTML = '';
		if (res.blog_url === '') {
			existsPost = false;
			lk.textContent = '-';
		} else {
			existsPost = true;
			const a = lk.appendChild(document.createElement('a'));
			a.rel = 'nofollow noopener noreferrer external';
			a.target = '_blank'
			a.href = res.blog_url;
			a.title = res.blog_url;
			a.textContent = (new URL(res.blog_url)).pathname;
		}
		showMessage('success to save thread setting!', false);
		if (upload) {
			showMessage('success to upload thread!', false);
		}
	})
	.catch( err => {
		showError(err);
	})
	.finally( () => {
		btn.forEach( b => { (<HTMLButtonElement>b).disabled = false; } );
		const db = document.querySelector('#thread > details > div.setting > form > button:nth-of-type(3)')!;
		(<HTMLButtonElement>db).disabled = !existsPost;
	});
}

function deleteThreadPost() {
	if (threadMode === '') {
		showError('not thread mode');
		return;
	}
	const btn = document.querySelectorAll('#thread > details > div.setting > form > button');
	btn.forEach( b => { (<HTMLButtonElement>b).disabled = true; } );
	const formData = new FormData();
	formData.set('target', threadMode);
	const api = '/api/deletepost/thread';
	let existsPost = false;
	fetch(api, {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then( (res: ThreadSetting) => {
		(<HTMLSelectElement>document.getElementById('thupl')!)
			.value = res.auto_upload ? '1' : '0';
		(<HTMLSelectElement>document.getElementById('thupls')!)
			.value = res.secret_upload ? '1' : '0';
		(<HTMLInputElement>document.getElementById('thuplt')!).value = res.title;
		(<HTMLInputElement>document.getElementById('thuplc')!)
			.value = (res.title !== '' || res.categories !== '') ? res.categories : 'inwardly';
		const upd = document.querySelector('#thread > details > div.setting > form > div.form:nth-of-type(2) > output')!;
		if (res.upload_date === '') {
			upd.textContent = '-';
		} else {
			upd.textContent = toJpString(parseDateTime(res.upload_date));
		}
		const lk = document.querySelector('#thread > details > div.setting > form > div.form:nth-of-type(1) > output')!;
		lk.innerHTML = '';
		if (res.blog_url === '') {
			existsPost = false;
			lk.textContent = '-';
		} else {
			existsPost = true;
			const a = lk.appendChild(document.createElement('a'));
			a.rel = 'nofollow noopener noreferrer external';
			a.target = '_blank'
			a.href = res.blog_url;
			a.title = res.blog_url;
			a.textContent = (new URL(res.blog_url)).pathname;
		}
		showMessage('success to delete post thread!', false);
	})
	.catch( err => {
		showError(err);
	})
	.finally( () => {
		btn.forEach( b => { (<HTMLButtonElement>b).disabled = false; } );
		const db = document.querySelector('#thread > details > div.setting > form > button:nth-of-type(3)')!;
		(<HTMLButtonElement>db).disabled = !existsPost;
	});
}

function setMentionView() {
	threadMode = '';
	const nav = document.querySelectorAll('header > nav li button');
	const prev = (<HTMLButtonElement>nav[0]);
	prev.disabled = true;
	prev.title = '';
	const next = (<HTMLButtonElement>nav[1]);
	next.disabled = true;
	next.title = '';
	document.getElementById('thread')!.classList.add('toggleDisplayNone');
	document.getElementById('result')!.classList.add('toggleDisplayNone');
	const timeline = document.getElementById('timeline')!;
	timeline.querySelector(':scope > :first-child')!.textContent = `timeline`;
	const params = (new URL(window.location.toString())).searchParams;
	const text = params.get('text');
	let body = '';
	if (text) {
		body = text;
	}
	const url = params.get('url');
	if (url) {
		body += ' ' + url;
	}
	const hashtags = params.get('hashtags');
	if (hashtags) {
		const tags = hashtags.split(',');
		for (const t of tags) {
			body += ' #' + t;
		}
	}
	const via = params.get('via');
	if (via) {
		body += ' via @' + via;
	}
	const ta = <HTMLTextAreaElement>document.querySelector('#say > textarea')!;
	body += ' \n';
	setTitle('Mention', body);
	ta.value = body;
	ta.focus();
}

function updateCalender(cal: CalenderData) {
	const calenderYear = <HTMLSelectElement>document.querySelector('#calender select[name="calenderyear"]')!;
	const calenderMonth = <HTMLSelectElement>document.querySelector('#calender select[name="calendermonth"]')!;
	if (currentCalender.year === 0)
	{
		if (cal.option.first === '' || cal.option.last === '') {
			showError('calender BUG?');
			return;
		}
		const first = Number(cal.option.first.slice(0, 2)) + 2000;
		const last = Number(cal.option.last.slice(0, 2)) + 2000;
		for (let y = first; first <= last && y <= last; y++) {
			const opt = calenderYear.appendChild(document.createElement('option'));
			opt.value = `${y}`;
			opt.textContent = toJpYearString(y);
		}
	}
	let found = false;
	for (let i = 0; i < calenderYear.options.length; i++) {
		const opt =  calenderYear.options[i];
		opt.selected = opt.value === `${cal.year}`;
		found = found || opt.selected;
	}
	if (!found) {
		const last = Number(cal.option.last.slice(0, 2)) + 2000;
		if (last === cal.year) {
			// 新年到達？
			const opt = calenderYear.appendChild(document.createElement('option'));
			opt.value = `${last}`;
			opt.textContent = toJpYearString(last);
			opt.selected = true;
		}
	}
	for (let i = 0; i < calenderMonth.options.length; i++) {
		const opt =  calenderMonth.options[i];
		opt.selected = opt.value === `${cal.month}`;
	}
	currentCalender.year = cal.year;
	currentCalender.month = cal.month;
	const cells = document.querySelectorAll('#calender .table button');
	let di = 0;
	for (let i = 0; i < cells.length; i++) {
		const btn = <HTMLButtonElement>cells[i];
		btn.disabled = true;
		const d = i - cal.week + 1;
		if (d < 1 || cal.end < d) {
			// not
			btn.textContent = '';
			continue;
		}
		btn.textContent = `${d}`;
		if (cal.days) {
			const dayId = leadingZeros2digits(cal.year % 100)
				+ leadingZeros2digits(cal.month)
				+ leadingZeros2digits(d);
			if (di < cal.days.length) {
				if (cal.days[di] === dayId) {
					btn.disabled = false;
					di++;
				}
			}
		}
	}
}

function updateSearchResult(result: SearchResult) {
	const viewSizePerShow = 20;
	const requestInterval = 300;
	const resultHolder = document.getElementById('result')!;
	resultHolder.classList.remove('toggleDisplayNone');
	const header = resultHolder.querySelector(':scope > :first-child')!;
	const progress = `${result.progress}/${result.count}`;
	const query = result.query;
	if (result.progress < 0) {
		setTitle(`Search CANCELED ${progress}`, query);
		header.textContent = `result (CANCELED ${progress}) ${query}`;
	} else if (result.progress < result.count) {
		setTitle(`Searching... ${progress}`, query);
		header.textContent = `result (searching... ${progress}) ${query}`;
	} else {
		setTitle(`Search`, query);
		header.textContent = `result (finished ${progress}) ${query}`;
	}
	if (result.said) {
		const ul = resultHolder.querySelector(':scope > ol')!;
		const len = ul.querySelectorAll(':scope > li').length;
		const min = Math.min(viewSizePerShow, result.said.length);
		for (let i = len; i < min; i++) {
			const fragment = resultTemplate(result.said[i]);
			ul.appendChild(fragment);
		}
		if (result.said.length > viewSizePerShow && len < result.said.length) {
			const btn = <HTMLButtonElement>resultHolder.querySelector(':scope button.resultmore')!;
			btn.textContent = `more (${Math.max(min,len)}/${result.said.length})`;
			btn.onclick = () => {
				btn.blur();
				const ul = document.querySelector('#result > ol')!;
				const len = ul.querySelectorAll(':scope > li').length;
				const min = Math.min(len + viewSizePerShow, result.said!.length);
				for (let i = len; i < min; i++) {
					const fragment = resultTemplate(result.said![i]);
					ul.appendChild(fragment);
				}
				if (min < result.said!.length) {
					btn.textContent = `more (${min}/${result.said!.length})`;
				} else {
					btn.textContent = 'more';
					btn.disabled = true;
					btn.classList.add('toggleDisplayNone');
				}
			};
			btn.classList.remove('toggleDisplayNone');
			btn.disabled = false;
		}
	}
	if (result.progress < 0) {
		showError(result.error);
	} else if (result.progress < result.count) {
		const id = result.id;
		window.setTimeout(() => {
			fetch(`/api/result/${id}`)
			.then(toJson)
			.then(updateSearchResult)
			.catch( err => {
				showError(err);
			});
		}, requestInterval);
	} else {
		if (window.location.pathname === '/search') {
			const url = `/result/${result.id}`;
			history.pushState(null, '', url);
		}
		let rescount = 0;
		if (result.said) {
			rescount = result.said.length;
		}
		setTitle(`Result ${rescount} hits`, query);
		header.textContent = `result (${rescount} hits) ${query}`;
	}
}

function callSearch() {
	threadMode = '';
	const nav = document.querySelectorAll('header > nav li button');
	const prev = (<HTMLButtonElement>nav[0]);
	prev.disabled = true;
	prev.title = '';
	const next = (<HTMLButtonElement>nav[1]);
	next.disabled = true;
	next.title = '';
	document.getElementById('thread')!.classList.add('toggleDisplayNone');
	const params = (new URL(window.location.toString())).searchParams;
	const query = params.get('q');
	if (query === null || query.trim() === '')
	{
		showError('no search keyword');
		return;
	}
	const resultHolder = document.getElementById('result')!;
	resultHolder.querySelector(':scope > :first-child')!.textContent = `result (searching...) ${query}`;
	resultHolder.classList.remove('toggleDisplayNone');
	setTitle('Searching...', query);
	// ここでクエリを解析するのもアリかもしれないが
	const formData = new FormData();
	formData.set('query', query);
	fetch('/api/search', {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then(updateSearchResult)
	.catch( err => {
		showError(err);
	});
}

function load() {
	/*
		基本的にパスによって処理は変わるが
		memo listの取得
	    pageの取得
		など

		history.stateにパスと同等の情報を載せておくと
		パスのparseが不要で楽かも？
		nullの場合もあるかも
	*/
	// console.log('called load()');
	let path = window.location.pathname;
	if (threadMode !== '') {
		if (path.startsWith(`/thread/${threadMode}`)) {
			// たぶんhash変化
			// console.log('hash change?');
			return;
		}
	}
	clearAllImageCache();
	clearSaidRepoitory();
	(<HTMLElement>document.querySelector('#timeline > ul')!).innerHTML = '';
	(<HTMLElement>document.querySelector('#thread > ul')!).innerHTML = '';
	(<HTMLElement>document.querySelector('#result > ol')!).innerHTML = '';
	if (path === '/' || path === '' || path === '/page') {
		path = '/page/';
	}
	let callback;
	if (path.startsWith('/page/')) {
		callback = setPageView;
	} else if (path.startsWith('/thread/')) {
		callback = setThreadView;
	} else if (path === '/mention') {
		setMentionView();
		return;
	} else if (path === '/search') {
		callSearch();
		return;
	} else if (path.startsWith('/result/')) {
		callback = updateSearchResult;
	} else {
		// TODO ここどうすんの
		showError(`bad path '${path}'`);
		return;
	}
	fetch(`/api${path}`)
	.then(toJson)
	.then(callback)
	.then( () => {
		const hash = window.location.hash;
		if (hash !== '') {
			window.location.hash = '';
			window.location.hash = hash;
		} else {
			window.scrollTo({ top: 0 });
		}
	})
	.catch( err => {
		showError(err);
	})
}

function loadMemoList() {
	const path = window.location.pathname;
	if (path.startsWith('/thread/')
			|| path === '/mention'
			|| path === '/search'
			|| path.startsWith('/result/')) {
		document.querySelectorAll('#memo > :not(:first-child)')
		.forEach( item => {
			item.classList.add('toggleDisplayNone');
		});
	}
	fetch('/api/memo/list')
	.then(toJson)
	.then( (memoList: Memo[]) => memoList.forEach(addMemo) )
	.catch( err => {
		showError(err);
	});
}

function loadCalender(year?: string, month?: string) {
	let api = '/api/calender/';
	if (year) {
		api += `${year}/${month}`;
	}
	fetch(api)
	.then(toJson)
	.then(updateCalender)
	.catch( err => {
		showError(err);
	});
}

////////////////
//
// 初期化処理
//
////////////////

(calenderMonth => {
	for (let i = 1; i <= 12; i++) {
		const opt = calenderMonth.appendChild(document.createElement('option'));
		opt.value = `${i}`;
		opt.textContent = toJpMonthString(i);
	}
})(document.querySelector('#calender select[name="calendermonth"]')!);

(calenderTable => {
	for (let i = 0; i < 6; i++) {
		const tablerow = calenderTable.appendChild(document.createElement('div'));
		tablerow.classList.add('tablerow');
		for (let j = 0; j < 7; j++) {
			const btn = tablerow.appendChild(document.createElement('button'));
			const d = i * 7 + j + 1 - 6;
			if (1 <= d && d <= 31) {
				btn.textContent = `${d}`;
			} else {
				btn.textContent = 'x';
			}
			btn.type = 'button';
			btn.disabled = true;
			btn.onclick = () => {
				const url = '/page/'
					+ leadingZeros2digits(currentCalender.year % 100)
					+ leadingZeros2digits(currentCalender.month)
					+ leadingZeros2digits(Number(btn.textContent!));
				history.pushState(null, '', url);
				load();
			};
		}
	}
})(document.querySelector('#calender .table')!);

document.getElementById('say')!.appendChild(submitTemplate('say...'));

document.querySelector('footer > button.totopbutton')!
.addEventListener('click', () => {
	const btn = <HTMLButtonElement>document.querySelector('footer > button.totopbutton')!;
	btn.blur();
	window.scrollTo({ top: 0 });
});

document.querySelector('#say > h5 > input')!
.addEventListener('change', () => {
	document.getElementById('say')!.classList.toggle('fixedsay');
});

document.querySelector('#memo > button')!
.addEventListener('click', () => {
	document.querySelectorAll('#memo > :not(:first-child)')
	.forEach( item => {
		item.classList.toggle('toggleDisplayNone');
	});
});

document.querySelector('#memo > div > button.toggler')!
.addEventListener('click', () => {
	memoButtonsVisible = !memoButtonsVisible;
	document.querySelectorAll('#memo > ul .memoaction')
	.forEach( item => {
		item.classList.toggle('toggleDisplayNone');
	});
});

document.querySelector('#memo > div > button:nth-of-type(2)')!
.addEventListener('click', historyMemoButton);

document.querySelector('#calender > button')!
.addEventListener('click', () => {
	const cal = document.querySelector('#calender > .calenderbody');
	if (cal !== null) {
		cal.classList.toggle('toggleDisplayNone');
	}
});

document.querySelector('#calender form')!
.addEventListener('submit', e => {
	e.preventDefault();
	const calenderYear = <HTMLSelectElement>document.querySelector('#calender select[name="calenderyear"]')!;
	const calenderMonth = <HTMLSelectElement>document.querySelector('#calender select[name="calendermonth"]')!;
	loadCalender(calenderYear.value, calenderMonth.value);
});

document.querySelector('#calender button.prev')!
.addEventListener('click', () => {
	let year = currentCalender.year;
	let month = currentCalender.month;
	if (1 < month) {
		month--;
	} else {
		year--;
		month = 12;
	}
	loadCalender(`${year}`, `${month}`);
});

document.querySelector('#calender button.next')!
.addEventListener('click', () => {
	let year = currentCalender.year;
	let month = currentCalender.month;
	if (month < 12) {
		month++;
	} else {
		year++;
		month = 1;
	}
	loadCalender(`${year}`, `${month}`);
});

document.querySelector('#imageviewer button.closeimageviewer')!
.addEventListener('click', () => {
	const iv = document.getElementById('imageviewer')!;
	iv.classList.add('toggleDisplayNone');
	const buttons = iv.querySelectorAll(':scope button');
	buttons.forEach( e => {
		const btn = <HTMLButtonElement>e;
		btn.disabled = true;
		if (btn.classList.contains('detachimage')) {
			btn.onclick = () => {};
		}
	});
	(<HTMLAnchorElement>iv.querySelector(':scope a')!).href = '';
	(<HTMLImageElement>iv.querySelector(':scope img')!).src = '';
});

document.querySelector('#historyviewer button.closehistoryviewer')!
.addEventListener('click', () => {
	const hv = document.getElementById('historyviewer')!;
	hv.classList.add('toggleDisplayNone');
	hv.querySelector(':scope ul')!.innerHTML = '';
});

document.querySelector('#thread > details > div.setting > form > button:nth-of-type(1)')!
.addEventListener('click', () => updateThreadSetting(true) );

document.querySelector('#thread > details > div.setting > form')!
.addEventListener('submit', e => {
	e.preventDefault();
	updateThreadSetting(false);
});

document.querySelector('#thread > details > div.setting > form > button:nth-of-type(3)')!
.addEventListener('click', () => deleteThreadPost() );

document.querySelectorAll('#thread > details > div.setting > div.form:nth-of-type(5) > *')
.forEach( e => { (<HTMLElement>e).title = 'categories separate by comma'; });

document.querySelector('header > nav button.setting')!
.addEventListener('click', () => {
	window.location.assign('/setting');
});

document.querySelector('header > nav button.link')!
.addEventListener('click', () => {
	window.location.assign('/link');
});

document.querySelector('header > nav button.misc')!
.addEventListener('click', () => {
	window.location.assign('/misc');
});

document.querySelector('header > nav button.home')!
.addEventListener('click', () => {
	const nav = document.querySelectorAll('header > nav li button');
	for (let i = 0; i < 3; i++) {
		(<HTMLButtonElement>nav[i]).disabled = true;
	}
	const p = window.location.pathname;
	if (p !== '/' && p !== '') {
		history.pushState(null, '', '/');
	}
	load();
});

window.addEventListener('load', () => {
	// memoリストのロードが必要
	loadMemoList();
	loadCalender();
	load();
});
window.addEventListener('popstate', load);

(<HTMLInputElement>document.querySelector('#searchform > input[type="search"]')!)
.title = `:case: ... must match cases
:since:2020-10-01 ... set oldest search position
:until:2021-06-29 ... set newest search position
:limit:15 ... most number of result (default 200)
:space:+ ... replace the char to space char
 (example: [:space:@@ "abcd@@efg"] -> ["abcd efg"])
:not:keyword ... exclude said with keyword
 (example: [foo :not:bar baz] -> include foo&baz and not include bar)
:image: ... attached images  (con :image:not)
:edited: ... edited (con :edited:not)
:order:old ... search oldest first order (default :order:new)
:update: ... :since: and :until: use update date (unorderd)
:root: ... root of reply tree (con :root:not)
:leaf: ... leaf of reply tree (con :leaf:not)
:reply: ... reply said (con :reply:not)
:quoted: ... quoted said (con :quoted:not)
:unquote: ... trim double-quotations
 (example: [:unquote: "abcd" ""efg""] -> [abcd "efg"])
:week:134 ... filter weekday (Sun-0)
:hour:6-8,11,13,19-21 ... hour range
:secret: ... secret said (con :secret:not)
:deleted: ... deleted
:history: ... edit history
`;
