package main

/*
   encoding/xml
   https://github.com/golang/go/issues?q=is%3Aissue+is%3Aopen+xml

   挙動が色々怪しい…
   https://github.com/golang/go/issues/9519

   XMLのNameSpaceの動作が…MarshalもUnmarshalも…ダメ
   MarshalとUnmarshalでNameSpaceの扱いが違って一貫性がない…
   Marshal時は
   プリフィクスNameSpaceの追加は`xml:"xmlns:dc"`で出力したり
   プリフィクス付きの<dc:subject>タグは`xml:"dc:subject"`で出力したり
   Unmarshal時
   <dc:subject>を読み取るなら
   `xml:"subject"`
   か
   `xml:"http://purl.org/dc/elements/1.1/ subject"`
   のどちらかにしないと読めない
   `xml:"dc:subject"`にすると読み込めない…
   `xml:"http://purl.org/dc/elements/1.1/ dc:subject"`でも読み込めない…

   Marshalのomitemptyはシンプルなstring定義以外はomitしてくんない…
*/

import (
	"encoding/xml"
)

// title未指定だとIDがtitleに割り当てられるぽい
// titleはフォトライフ上で閲覧するときに表示されるだけ
// ディレクトリ(dc:subject)が指定されているとそこに保存される
// ディレクトリが指定されておらず、かつジェネレータ名(generator)が指定されてるとジェネレータ名のディレクトリに保存される
// 保存先が指定されない場合は公開ディレクトリに保存される
// 保存先が指定されｔる場合、保存先が存在しない場合は指定した名前の非公開ディレクトリが作成される
// 保存先ディレクトリはあくまでフォトライフ内での管理上の分類、直リンURLには影響しない
type HatenaFotolifePostBody struct {
	XMLName xml.Name `xml:"http://purl.org/atom/ns# entry"`
	XmlNS   string   `xml:"xmlns:dc,attr,omitempty"`
	Title   string   `xml:"title,omitempty"`
	Content struct {
		Mode  string `xml:"mode,attr"`
		Type  string `xml:"type,attr"`
		Value string `xml:",chardata"`
	} `xml:"content"`
	Subject   string `xml:"dc:subject,omitempty"`
	Generator string `xml:"generator,omitempty"`
}

// ドキュメントにはない　hatena:imageurlmedium タグが取得xmlに存在…
// hatena:imageurlmedium は縦か横のどちらかサイズ(おそらく長辺)が120pxとして縮小（正方形の画像でテストしたから分からん…)
// hatena:imageurlsmall は 60px に縮小
// なおブログで貼り付けられてる画像では縮小版は使われておらず
// ブログの記事一覧ページのアイキャッチ画像にもこの縮小版は使われていない…(アイキャッチ画像は一辺が500px前後に伸縮される)
// 縮小版は存在が謎すぎる…容量の無駄では…？
// ブログの画像のimgタグのtitle属性についてるの
//
//	xmlのsyntaxの末尾の:image部分を:plainに置き換えてる感じぽい
//	         f:id:neetsdkasu:XXXXXXXXXXXXXXp:image
//	id末尾の記号がファイルタイプを表すぽい
//	 jpegだと f:id:neetsdkasu:XXXXXXXXXXXXXXj:plain
//	 pngだと  f:id:neetsdkasu:XXXXXXXXXXXXXXp:plain
type HatenaFotolifePostResult struct {
	XMLName xml.Name `xml:"entry"`
	Title   string   `xml:"title"`
	Links   []struct {
		Rel   string `xml:"rel,attr"`
		Type  string `xml:"type,attr"`
		Href  string `xml:"href,attr"`
		Title string `xml:"title,attr"`
	} `xml:"link"`
	Issued    string `xml:"issued"`
	Author    string `xml:"author>name"`
	Generator struct {
		Url    string `xml:"url,attr"`
		Vesion string `xml:"version,attr"`
		Value  string `xml:",chardata"`
	} `xml:"generator"`
	Subject        string `xml:"subject"`
	Id             string `xml:"id"`
	ImageUrl       string `xml:"imageurl"`
	ImageUrlMedium string `xml:"imageurlmedium"`
	ImageUrlSmall  string `xml:"imageurlsmall"`
	Syntax         string `xml:"syntax"`
}

type HatenaBlogAtomResult struct {
	XMLName    xml.Name `xml:"service"`
	Title      string   `xml:"workspace>title"`
	Collection struct {
		Href   string `xml:"href,attr"`
		Title  string `xml:"title"`
		Accept string `xml:"accept"`
	} `xml:"workspace>collection"`
}

type HatenaBlogCategory struct {
	XMLName xml.Name `xml:"category"`
	Term    string   `xml:"term,attr"`
}

type HatenaBlogAtomCategoryResult struct {
	XMLName    xml.Name              `xml:"categories"`
	Fixed      string                `xml:"fixed,attr"`
	Categories []*HatenaBlogCategory `xml:"category"`
}

type HatenaBlogEntryResult struct {
	XMLName xml.Name `xml:"entry"`
	Id      string   `xml:"id"`
	Links   []struct {
		Rel  string `xml:"rel,attr"`
		Href string `xml:"href,attr"`
	} `xml:"link"`
	Author    string `xml:"author>name"`
	Title     string `xml:"title"`
	Updated   string `xml:"updated"`
	Published string `xml:"published"`
	Edited    string `xml:"edited"`
	Summary   struct {
		Type  string `xml:"type,attr"`
		Value string `xml:",chardata"`
	} `xml:"summary"`
	Content struct {
		Type  string `xml:"type,attr"`
		Value string `xml:",chardata"`
	} `xml:"content"`
	FormattedContent struct {
		Type  string `xml:"type,attr"`
		Value string `xml:",chardata"`
	} `xml:"formatted-content"`
	Categories []*HatenaBlogCategory `xml:"category"`
	Draft      string                `xml:"control>draft"`
}

type HatenaBlogAtomEntryGetResult struct {
	XMLName xml.Name `xml:"feed"`
	Links   []struct {
		Rel  string `xml:"rel,attr"`
		Href string `xml:"href,attr"`
	} `xml:"link"`
	Title     string `xml:"title"`
	SubTitle  string `xml:"subtitle"`
	Updated   string `xml:"updated"`
	Author    string `xml:"author>name"`
	Generator struct {
		Uri    string `xml:"uri,attr"`
		Vesion string `xml:"version,attr"`
		Value  string `xml:",chardata"`
	} `xml:"generator"`
	Id      string                   `xml:"id"`
	Entries []*HatenaBlogEntryResult `xml:"entry"`
}

// app:draft は yes か no
type HatenaBlogAtomEntryPostBody struct {
	XMLName xml.Name `xml:"http://www.w3.org/2005/Atom entry"`
	XmlNS   string   `xml:"xmlns:app,attr"`
	Title   string   `xml:"title"`
	Author  string   `xml:"author>name"`
	Content struct {
		Type  string `xml:"type,attr"`
		Value string `xml:",chardata"`
	} `xml:"content"`
	Updated    string `xml:"updated,omitempty"`
	Categories []*HatenaBlogCategory
	Draft      string `xml:"app:control>app:draft"`
}
