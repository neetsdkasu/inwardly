package main

import (
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

type AppSettingStore struct {
	Consumer      *HatenaConsumer     `json:"consumer"`
	RequestToken  *HatenaRequestToken `json:"request_token"`
	AccessToken   *HatenaAccessToken  `json:"access_token"`
	Blog          *HatenaBlog         `json:"blog"`
	BlogSettings  *BlogSettings       `json:"blog_settings"`
	DebugSettings *DebugSettings      `json:"debug_settings"`
}

type DebugSettings struct {
	TimelineUploadTarget string `json:"timeline_upload"`
	DeleteUploadedPosts  bool   `json:"nontargets"`
}

type BlogSettings struct {
	AutoUpload   bool `json:"auto_upload"`
	ThreadUpload bool `json:"thread_upload"`
}

type ThreadUploadSetting struct {
	Id           string `json:"id"`
	AutoUpload   bool   `json:"auto_upload"`
	Title        string `json:"title"`
	Categories   string `json:"categories"`
	BlogPostId   string `json:"blogpost_id"`
	Url          string `json:"blog_url"`
	Update       string `json:"update"`
	UploadDate   string `json:"upload_date"`
	SecretUpload bool   `json:"secret_upload"`
}

type ThreadUploadSettingList []*ThreadUploadSetting

type ThreadUploadSettingStore map[string]*ThreadUploadSetting

type DocumentLinkSetting struct {
	Id               int    `json:"id"`
	EntryName        string `json:"entry"`
	DocumentRootPath string `json:"rootpath"`
	BaseFile         string `json:"basefile"`
	QuickLinkOrder   int    `json:"ql_order"`
}

type DocumentLinkSettingList []*DocumentLinkSetting

type AppSetting struct {
	AppSettingStore
	ThreadUploadSettingStore
	DocumentLinkSettingStore struct {
		Table map[string]*DocumentLinkSetting
		sync.Mutex
	}
	sync.Mutex
}

type Setting struct {
	HasConsumer   bool           `json:"has_consumer"`
	ConsumerKey   string         `json:"consumer_key"`
	Authorized    bool           `json:"authorized"`
	Blog          *HatenaBlog    `json:"hatena_blog"`
	BlogSettings  *BlogSettings  `json:"blog_settings"`
	DebugSettings *DebugSettings `json:"debug_settings"`
}

type SettingResult struct {
	Ok      bool   `json:"ok"`
	Message string `json:"message"`
	Value   string `json:"value"`
}

var appSetting AppSetting

func (*AppSettingStore) Init() {}

func (this *AppSettingStore) CanUploadBlog() bool {
	return this.HasConsumer() &&
		this.Authorized() &&
		this.HasBlogProfile()
}

func (this *AppSettingStore) AutoBlogUpload() bool {
	return this.CanUploadBlog() &&
		this.BlogSettings != nil &&
		this.BlogSettings.AutoUpload
}

func (this *AppSettingStore) CanUploadThread() bool {
	return this.CanUploadBlog() &&
		this.BlogSettings != nil &&
		this.BlogSettings.ThreadUpload
}

func (this *AppSettingStore) HasBlogProfile() bool {
	return this.Blog != nil &&
		this.Blog.HatenaId != "" &&
		this.Blog.BlogId != ""
}

func (this *AppSettingStore) HasConsumer() bool {
	return this.Consumer != nil &&
		this.Consumer.ConsumerKey != "" &&
		this.Consumer.ConsumerSecret != ""
}

func (this *AppSettingStore) Authorized() bool {
	return this.AccessToken != nil &&
		this.AccessToken.Token != "" &&
		this.AccessToken.TokenSecret != ""
}

func (this *DocumentLinkSetting) DocumentPath() string {
	return filepath.Join(this.DocumentRootPath, this.BaseFile)
}

func (*ThreadUploadSettingList) Init() {}

func (*DocumentLinkSettingList) Init() {}

func (this *AppSetting) Init() error {
	if DebugModeShowInit || DebugModeAppSetting {
		log.Println("AppSetting.Init")
	}

	threadList := ThreadUploadSettingList{}
	err := fileManager.LoadThreadUploadSetting(&threadList)
	if err != nil {
		return err
	}
	threadStore := ThreadUploadSettingStore{}
	for _, s := range threadList {
		threadStore[s.Id] = s
	}
	this.ThreadUploadSettingStore = threadStore

	docLinkList := DocumentLinkSettingList{}
	err = fileManager.LoadDocumentLinkSetting(&docLinkList)
	if err != nil {
		return err
	}
	docLinkTable := map[string]*DocumentLinkSetting{}
	for _, dl := range docLinkList {
		docLinkTable[dl.EntryName] = dl
		if dl.QuickLinkOrder == 0 { // fix for inwardly v0.1
			dl.QuickLinkOrder = dl.Id
		}
	}
	this.DocumentLinkSettingStore.Table = docLinkTable

	return fileManager.LoadAppSetting(&this.AppSettingStore)
}

func (this *AppSetting) GetDocumentLinkSettingList() DocumentLinkSettingList {
	if DebugModeAppSetting {
		log.Println("AppSetting.GetDocumentLinkSettingList")
		defer log.Println("defer AppSetting.GetDocumentLinkSettingList")
	}
	this.DocumentLinkSettingStore.Lock()
	defer this.DocumentLinkSettingStore.Unlock()
	docLinkList := make(DocumentLinkSettingList, 0, len(this.DocumentLinkSettingStore.Table))
	for _, dl := range this.DocumentLinkSettingStore.Table {
		docLinkList = append(docLinkList, dl)
	}
	return docLinkList
}

func isValidDocumentLinkEntryName(entry string) bool {
	if len(entry) == 0 || len(entry) > 12 {
		return false
	}
	for _, r := range entry {
		switch {
		default:
			return false
		case 'a' <= r && r <= 'z':
		case '0' <= r && r <= '9':
		case r == '_', r == '-':
		}
	}
	return true
}

func (this *AppSetting) AddDocumentLinkSetting(entry, path string) (*DocumentLinkSetting, error) {
	if DebugModeAppSetting {
		log.Println("AppSetting.AddDocumentLinkSetting")
		defer log.Println("defer AppSetting.AddDocumentLinkSetting")
	}
	this.DocumentLinkSettingStore.Lock()
	defer this.DocumentLinkSettingStore.Unlock()
	if entry == "" {
		return nil, fmt.Errorf("[BUG] AppSetting.AddDocumentLinkSetting: entry is empty")
	}
	if path == "" {
		return nil, fmt.Errorf("[BUG] AppSetting.AddDocumentLinkSetting: path is empty")
	}
	if _, ok := this.DocumentLinkSettingStore.Table[entry]; ok {
		return nil, BadRequestError{"entry is existed : " + entry}
	}
	if !isValidDocumentLinkEntryName(entry) {
		return nil, BadRequestError{"invalid entry : " + entry}
	}
	dir, base, err := fileManager.SplitBaseFile(path)
	if err != nil {
		return nil, err
	}
	id := appState.DocumentLinkNumber.GetNext()
	appState.Save()
	dl := &DocumentLinkSetting{
		Id:               id,
		EntryName:        entry,
		DocumentRootPath: dir,
		BaseFile:         base,
		QuickLinkOrder:   id,
	}
	this.DocumentLinkSettingStore.Table[entry] = dl
	fileManager.SaveDocumentLinkSetting()
	return dl, nil
}

func (this *AppSetting) UpdateQLOrderDocumentLinkSetting(id int, entry string, order int) (*DocumentLinkSetting, error) {
	if DebugModeAppSetting {
		log.Println("AppSetting.UpdateQLOrderDocumentLinkSetting")
		defer log.Println("defer AppSetting.UpdateQLOrderDocumentLinkSetting")
	}
	this.DocumentLinkSettingStore.Lock()
	defer this.DocumentLinkSettingStore.Unlock()
	if entry == "" {
		return nil, fmt.Errorf("[BUG] AppSetting.AddDocumentLinkSetting: entry is empty")
	}
	oldDl, ok := this.DocumentLinkSettingStore.Table[entry]
	if !ok {
		return nil, BadRequestError{"not found entry : " + entry}
	}
	if oldDl.Id != id {
		return nil, BadRequestError{fmt.Sprint("invalid id : ", id)}
	}
	newDl := new(DocumentLinkSetting)
	*newDl = *oldDl
	newDl.QuickLinkOrder = order
	this.DocumentLinkSettingStore.Table[entry] = newDl
	fileManager.SaveDocumentLinkSetting()
	return newDl, nil
}

func (this *AppSetting) UpdateDocumentLinkSetting(id int, oldEntry, entry, path string) (*DocumentLinkSetting, *DocumentLinkSetting, error) {
	if DebugModeAppSetting {
		log.Println("AppSetting.UpdateDocumentLinkSetting")
		defer log.Println("defer AppSetting.UpdateDocumentLinkSetting")
	}
	this.DocumentLinkSettingStore.Lock()
	defer this.DocumentLinkSettingStore.Unlock()
	if entry == "" && path != "" {
		return nil, nil, fmt.Errorf("[BUG] AppSetting.AddDocumentLinkSetting: entry is empty")
	}
	if path == "" && entry != "" {
		return nil, nil, fmt.Errorf("[BUG] AppSetting.AddDocumentLinkSetting: path is empty")
	}
	if entry != oldEntry {
		if _, ok := this.DocumentLinkSettingStore.Table[entry]; ok {
			return nil, nil, BadRequestError{"entry is existed : " + entry}
		}
	}
	oldDl, ok := this.DocumentLinkSettingStore.Table[oldEntry]
	if !ok {
		return nil, nil, BadRequestError{"not found old_entry : " + oldEntry}
	}
	if oldDl.Id != id {
		return nil, nil, BadRequestError{fmt.Sprint("invalid id : ", id)}
	}
	if entry == "" {
		delete(this.DocumentLinkSettingStore.Table, oldEntry)
		fileManager.SaveDocumentLinkSetting()
		return oldDl, nil, nil
	}
	if !isValidDocumentLinkEntryName(entry) {
		return nil, nil, BadRequestError{"invalid entry : " + entry}
	}
	dir, base, err := fileManager.SplitBaseFile(path)
	if err != nil {
		return nil, nil, err
	}
	newDl := &DocumentLinkSetting{
		Id:               oldDl.Id,
		EntryName:        entry,
		DocumentRootPath: dir,
		BaseFile:         base,
		QuickLinkOrder:   oldDl.QuickLinkOrder,
	}
	delete(this.DocumentLinkSettingStore.Table, oldEntry)
	this.DocumentLinkSettingStore.Table[entry] = newDl
	fileManager.SaveDocumentLinkSetting()
	return oldDl, newDl, nil
}

func (this *AppSetting) GetDocumentLinkSetting(entry string) (*DocumentLinkSetting, bool) {
	if DebugModeAppSetting {
		log.Println("AppSetting.GetDocumentLinkSetting")
		defer log.Println("defer AppSetting.GetDocumentLinkSetting")
	}
	this.DocumentLinkSettingStore.Lock()
	defer this.DocumentLinkSettingStore.Unlock()
	dl, ok := this.DocumentLinkSettingStore.Table[entry]
	return dl, ok
}

func (this *AppSetting) UpdateThread(threadId, update string) {
	if DebugModeAppSetting {
		log.Println("AppSetting.UpdateThread")
		defer log.Println("defer AppSetting.UpdateThread")
	}
	this.Lock()
	defer this.Unlock()
	oldSetting, ok := this.ThreadUploadSettingStore[threadId]
	if ok && (oldSetting.Update == "" || strings.Compare(oldSetting.Update, update) < 0) {
		newSetting := new(ThreadUploadSetting)
		*newSetting = *oldSetting
		newSetting.Update = update
		this.ThreadUploadSettingStore[threadId] = newSetting
		fileManager.SaveThreadUploadSetting()
	}
}

func (this *AppSetting) SetThreadUploadSettingUploded(threadId, update, blogPostId, url, uploadDate string) (*ThreadUploadSetting, error) {
	if DebugModeAppSetting {
		log.Println("AppSetting.SetThreadUploadSettingUploded")
		defer log.Println("defer AppSetting.SetThreadUploadSettingUploded")
	}
	this.Lock()
	defer this.Unlock()
	oldSetting, ok := this.ThreadUploadSettingStore[threadId]
	if !ok {
		return nil, fmt.Errorf("[BUG] AppSetting.SetThreadUploadSettingUploded: invalid threadId %s", threadId)
	}
	deleted := blogPostId == "" && url == "" && uploadDate == ""
	if oldSetting.UploadDate == "" || deleted ||
		strings.Compare(oldSetting.UploadDate, uploadDate) < 0 {
		newSetting := new(ThreadUploadSetting)
		*newSetting = *oldSetting
		newSetting.BlogPostId = blogPostId
		newSetting.Url = url
		if !deleted && oldSetting.Update != update {
			t, err := FromDateString(oldSetting.Update)
			if err != nil {
				return nil, err
			}
			uploadDate = ToDateString(t.Add(-time.Second))
		}
		newSetting.UploadDate = uploadDate
		this.ThreadUploadSettingStore[threadId] = newSetting
		fileManager.SaveThreadUploadSetting()
		return newSetting, nil
	}
	return oldSetting, nil
}

func (this *AppSetting) SetThreadUploadSetting(threadId, title, categories string, autoUpload, secretUpload bool) (*ThreadUploadSetting, error) {
	if DebugModeAppSetting {
		log.Println("AppSetting.SetThreadUploadSetting")
		defer log.Println("defer AppSetting.SetThreadUploadSetting")
	}
	this.Lock()
	defer this.Unlock()
	oldSetting, ok := this.ThreadUploadSettingStore[threadId]
	if title == "" && categories == "" && autoUpload == false {
		if !ok {
			return nil, fmt.Errorf("[BUG] AppSetting.SetThreadUploadSetting: not found thread setting %s", threadId)
		}
		if oldSetting.BlogPostId != "" {
			return nil, BadRequestError{
				"do 'delete post' beforehand",
			}
		}
		delete(this.ThreadUploadSettingStore, threadId)
		fileManager.SaveThreadUploadSetting()
		return &ThreadUploadSetting{
			Id: threadId,
		}, nil
	}
	newSetting := new(ThreadUploadSetting)
	if ok {
		*newSetting = *oldSetting
	}
	newSetting.Id = threadId
	newSetting.AutoUpload = autoUpload
	newSetting.Title = title
	newSetting.Categories = categories
	newSetting.SecretUpload = secretUpload
	if ok {
		if oldSetting.Title != newSetting.Title ||
			oldSetting.Categories != newSetting.Categories {
			newSetting.Update = ToDateString(time.Now().Local())
		}
	}
	this.ThreadUploadSettingStore[threadId] = newSetting
	fileManager.SaveThreadUploadSetting()
	return newSetting, nil
}

func (this *AppSetting) GetThreadUploadSettingList() ThreadUploadSettingList {
	if DebugModeAppSetting {
		log.Println("AppSetting.GetThreadUploadSettingList")
		defer log.Println("defer AppSetting.GetThreadUploadSettingList")
	}
	this.Lock()
	defer this.Unlock()
	list := make(ThreadUploadSettingList, 0, len(this.ThreadUploadSettingStore))
	for _, s := range this.ThreadUploadSettingStore {
		list = append(list, s)
	}
	return list
}

func (this *AppSetting) GetThreadUploadSetting(threadId string) (*ThreadUploadSetting, bool) {
	if DebugModeAppSetting {
		log.Println("AppSetting.GetThreadUploadSetting")
		defer log.Println("defer AppSetting.GetThreadUploadSetting")
	}
	this.Lock()
	defer this.Unlock()
	setting, ok := this.ThreadUploadSettingStore[threadId]
	if !ok {
		setting = nil
	}
	return setting, ok
}

func (this *AppSetting) SetDebugModeSettings(settings *DebugSettings) error {
	if DebugModeAppSetting {
		log.Println("AppSetting.SetDebugModeSettings")
		defer log.Println("defer AppSetting.SetDebugModeSettings")
	}

	for _, t := range strings.Split(settings.TimelineUploadTarget, ",") {
		xs := strings.Split(t, ":")
		if len(xs) > 2 {
			return BadRequestError{
				"syntax error in timeline_upload",
			}
		}
		for _, x := range xs {
			if x == "" {
				continue
			}
			_, err := time.Parse("2006-01-02", x)
			if err != nil {
				return BadRequestError{
					"syntax error in timeline_upload",
				}
			}
		}
	}

	this.Lock()
	defer this.Unlock()
	tmp := this.AppSettingStore
	tmp.DebugSettings = settings
	err := fileManager.SaveAppSetting(&tmp)
	if err != nil {
		return err
	}
	this.AppSettingStore = tmp
	return nil
}

func (this *AppSetting) SetConsumer(consumer *HatenaConsumer) error {
	this.Lock()
	defer this.Unlock()
	tmp := this.AppSettingStore
	tmp.Consumer = consumer
	tmp.RequestToken = nil
	tmp.AccessToken = nil
	err := fileManager.SaveAppSetting(&tmp)
	if err != nil {
		return err
	}
	this.AppSettingStore = tmp
	return nil
}

func (this *AppSetting) SetBlog(blog *HatenaBlog, settings *BlogSettings) error {
	this.Lock()
	defer this.Unlock()
	tmp := this.AppSettingStore
	tmp.Blog = blog
	tmp.BlogSettings = settings
	err := fileManager.SaveAppSetting(&tmp)
	if err != nil {
		return err
	}
	this.AppSettingStore = tmp
	return nil
}

func (this *AppSetting) GetStoreCopy() AppSettingStore {
	this.Lock()
	defer this.Unlock()
	ret := AppSettingStore{}
	ret = this.AppSettingStore
	return ret
}

func (this *AppSetting) GetSetting(setting *Setting) {
	if DebugModeAppSetting {
		log.Println("AppSetting.GetSetting")
		defer log.Println("defer AppSetting.GetSetting")
	}
	this.Lock()
	defer this.Unlock()

	if setting.HasConsumer = this.HasConsumer(); setting.HasConsumer {
		setting.ConsumerKey = this.Consumer.ConsumerKey
	}

	setting.Authorized = this.Authorized()

	setting.Blog = this.Blog
	setting.BlogSettings = this.BlogSettings

	if DebugMode {
		setting.DebugSettings = this.DebugSettings
		if setting.DebugSettings == nil {
			setting.DebugSettings = new(DebugSettings)
		}
	}
}

func (this *AppSetting) GetAuthorizeUrl(cb string) (string, error) {
	if DebugModeAppSetting {
		log.Println("AppSetting.GetAuthorizeUrl")
		defer log.Println("defer AppSetting.GetAuthorizeUrl")
	}
	this.Lock()
	defer this.Unlock()
	if !this.HasConsumer() {
		return "", BadRequestError{
			"Consumer is empty",
		}
	}
	rq, err := this.Consumer.RequestToken(cb)
	if err != nil {
		return "", err
	}
	tmp := this.AppSettingStore
	tmp.RequestToken = rq
	err = fileManager.SaveAppSetting(&tmp)
	if err != nil {
		return "", err
	}
	this.AppSettingStore = tmp
	return this.RequestToken.AuthorizeUrl(), nil
}

func (this *AppSetting) Verify(verifier string) error {
	if DebugModeAppSetting {
		log.Println("AppSetting.Verify")
		defer log.Println("defer AppSetting.Verify")
	}
	this.Lock()
	defer this.Unlock()
	if !this.HasConsumer() {
		return fmt.Errorf("Consumer is empty")
	}
	if this.RequestToken == nil {
		return fmt.Errorf("RequestToken is empty")
	}
	ac, err := this.Consumer.AccessToken(this.RequestToken, verifier)
	if err != nil {
		return err
	}
	tmp := this.AppSettingStore
	tmp.RequestToken = nil
	tmp.AccessToken = ac
	err = fileManager.SaveAppSetting(&tmp)
	if err != nil {
		return err
	}
	this.AppSettingStore = tmp
	return nil
}

func (this *AppSettingStore) DeleteBlog(id string) error {
	if DebugModeAppSetting {
		log.Println("AppSettingStore.DeleteBlog")
		defer log.Println("defer AppSettingStore.DeleteBlog")
	}
	if id == "" {
		return fmt.Errorf("missing id")
	}
	if !this.HasConsumer() {
		return fmt.Errorf("no consumer")
	}
	if !this.Authorized() {
		return fmt.Errorf("no authorized")
	}
	if !this.HasBlogProfile() {
		return fmt.Errorf("no blog profile")
	}
	return this.Consumer.BlogAtomEntryIdDelete(
		this.AccessToken,
		this.Blog,
		id,
	)
}

func (this *AppSettingStore) UploadBlog(id, title, date, blogBody string, categories []string) (*HatenaBlogEntryResult, error) {
	if DebugModeAppSetting {
		log.Println("AppSettingStore.UploadBlog")
		defer log.Println("defer AppSettingStore.UploadBlog")
	}
	if !this.HasConsumer() {
		return nil, fmt.Errorf("no consumer")
	}
	if !this.Authorized() {
		return nil, fmt.Errorf("no authorized")
	}
	if !this.HasBlogProfile() {
		return nil, fmt.Errorf("no blog profile")
	}
	if id == "" {
		return this.Consumer.BlogAtomEntryPost(
			this.AccessToken,
			this.Blog,
			title, date, blogBody,
			categories,
		)
	} else {
		return this.Consumer.BlogAtomEntryIdPut(
			this.AccessToken,
			this.Blog,
			id, title, date, blogBody,
			categories,
		)
	}
}

func (this *AppSettingStore) UploadFoto(filename string) (*Foto, error) {
	if DebugModeAppSetting {
		log.Println("AppSettingStore.UploadFoto")
		defer log.Println("defer AppSettingStore.UploadFoto")
	}
	if !this.HasConsumer() {
		return nil, fmt.Errorf("no consumer")
	}
	if !this.Authorized() {
		return nil, fmt.Errorf("no authorized")
	}
	blob, err := fileManager.LoadImageFile(filename)
	if err != nil {
		return nil, err
	}
	totalSize, err := dataManager.GetUploadedTotalFotoSize()
	if err != nil {
		return nil, err
	}
	if totalSize+len(blob) >= 250_000_000 {
		return nil, fmt.Errorf("reached fotolife limit")
	}
	res, err := this.Consumer.FotolifePost(this.AccessToken, filename, blob)
	if err != nil {
		return nil, err
	}
	fotoId := strings.Replace(res.Syntax, ":image", ":plain", 1)
	return &Foto{
		File:   filename,
		FotoId: fotoId,
		Url:    res.ImageUrl,
		Size:   len(blob),
	}, nil
}

func init() {
	// GET
	http.HandleFunc("/api/setting/get", settingGetHandleFunc)

	// POST
	// consumer_key string (require)
	// consumer_secret string (require)
	http.HandleFunc("/api/setting/consumer", settingSaveConsumerHandleFunc)

	// POST
	http.HandleFunc("/api/setting/authorize", settingAuthorizeHandleFunc)

	// POST
	// hatena_id string (require)
	// blog_id string (require)
	// auto_upload number (require 0 or 1)
	http.HandleFunc("/api/setting/blog", settingSaveBlogHandleFunc)

	// GET
	// oauth_verifier string (require)
	http.HandleFunc("/callback/hatena", callbackHatenaHandleFunc)

	// POST
	// timeline_upload string
	// nontargets      number (require 0 or 1, 0-keep, 1-delete)
	http.HandleFunc("/api/setting/debugmode", settingDebugModeHandleFunc)
}

func settingGetHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeAppSetting {
		log.Println("settingGetHandleFunc")
		defer log.Println("defer settingGetHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	setting := Setting{}

	appSetting.GetSetting(&setting)

	writeJson(w, req, &setting)
}

func settingSaveConsumerHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeAppSetting {
		log.Println("settingSaveConsumerHandleFunc")
		defer log.Println("defer settingSaveConsumerHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	key := req.PostFormValue("consumer_key")
	if key == "" {
		badRequest(w, "missing consumer_key")
		return
	}

	secret := req.PostFormValue("consumer_secret")
	if secret == "" {
		badRequest(w, "missing consumer_secret")
		return
	}

	consumer := &HatenaConsumer{
		ConsumerKey:    key,
		ConsumerSecret: secret,
	}

	err := appSetting.SetConsumer(consumer)
	if err != nil {
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	setting := Setting{}

	appSetting.GetSetting(&setting)

	writeJson(w, req, &setting)
}

func settingAuthorizeHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeAppSetting {
		log.Println("settingAuthorizeHandleFunc")
		defer log.Println("defer settingAuthorizeHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	cb := "http://" + req.Host + "/callback/hatena"

	u, err := appSetting.GetAuthorizeUrl(cb)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := SettingResult{
		Ok:      true,
		Message: "access to " + u,
		Value:   u,
	}

	writeJson(w, req, &res)
}

func settingSaveBlogHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeAppSetting {
		log.Println("settingSaveBlogHandleFunc")
		defer log.Println("defer settingSaveBlogHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	hid := req.PostFormValue("hatena_id")
	if hid == "" {
		badRequest(w, "missing hatena_id")
		return
	}

	bid := req.PostFormValue("blog_id")
	if bid == "" {
		badRequest(w, "missing blog_id")
		return
	}

	autoUpload := req.PostFormValue("auto_upload")
	if autoUpload == "" {
		badRequest(w, "missing auto_upload")
		return
	}
	if autoUpload != "0" && autoUpload != "1" {
		badRequest(w, "invalid auto_upload: "+autoUpload)
		return
	}

	threadUpload := req.PostFormValue("thread_upload")
	if threadUpload == "" {
		badRequest(w, "missing thread_upload")
		return
	}
	if threadUpload != "0" && threadUpload != "1" {
		badRequest(w, "invalid thread_upload: "+threadUpload)
		return
	}

	blog := &HatenaBlog{
		HatenaId: hid,
		BlogId:   bid,
	}

	settings := &BlogSettings{
		AutoUpload:   autoUpload == "1",
		ThreadUpload: threadUpload == "1",
	}

	err := appSetting.SetBlog(blog, settings)
	if err != nil {
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	setting := Setting{}

	appSetting.GetSetting(&setting)

	writeJson(w, req, &setting)
}

func callbackHatenaHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeAppSetting {
		log.Println("callbackHatenaHandleFunc")
		defer log.Println("defer callbackHatenaHandleFunc")
	}

	verifier := req.FormValue("oauth_verifier")

	err := appSetting.Verify(verifier)
	if err != nil {
		internalServerError(w, err.Error())
		return
	}

	http.Redirect(w, req, "/setting", http.StatusSeeOther)
}

func settingDebugModeHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeAppSetting {
		log.Println("settingDebugModeHandleFunc")
		defer log.Println("defer settingDebugModeHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	timelineUpload := req.PostFormValue("timeline_upload")

	nonTargets := req.PostFormValue("nontargets")
	if nonTargets == "" {
		badRequest(w, "missing nontargets")
		return
	}
	if nonTargets != "0" && nonTargets != "1" {
		badRequest(w, "invalid nontargets: "+nonTargets)
		return
	}

	err := appSetting.SetDebugModeSettings(
		&DebugSettings{
			TimelineUploadTarget: timelineUpload,
			DeleteUploadedPosts:  nonTargets == "1",
		},
	)
	if err != nil {
		errorResponse(w, err)
		return
	}

	setting := Setting{}

	appSetting.GetSetting(&setting)

	writeJson(w, req, &setting)
}
