module bitbucket.org/neetsdkasu/inwardly/inwardly_srv

go 1.16

require (
	bitbucket.org/neetsdkasu/mersenne_twister_go v1.2.0
	bitbucket.org/neetsdkasu/unkocrypto_go/v2 v2.0.0
)
