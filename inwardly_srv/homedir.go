package main

import (
	"log"
	"os"
	"path/filepath"
)

type HomeDir struct {
	dir string
}

var homeDir HomeDir

func (this *HomeDir) initDefaultDir() error {
	if DebugModeShowInit {
		log.Println("HomeDir.initDefaultDir")
	}
	homedir, err := os.UserHomeDir()
	if err != nil {
		return err
	}
	if DebugModeFilePath {
		homedir, err = os.Getwd()
		if err != nil {
			return err
		}
	}
	this.dir = homedir
	return nil
}

func (this *HomeDir) Init(specialHomeDir string) error {
	if DebugModeShowInit {
		log.Println("HomeDir.Init")
	}

	if specialHomeDir == "" {
		return this.initDefaultDir()
	}

	abs, err := filepath.Abs(specialHomeDir)
	if err != nil {
		return err
	}

	this.dir = abs

	return nil
}

func (this *HomeDir) Get() string {
	return this.dir
}
