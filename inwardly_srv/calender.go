package main

import (
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func init() {
	// GET
	// /api/calender/
	// or
	// /api/calender/{Year}/{Month}
	http.Handle("/api/calender/",
		http.StripPrefix("/api/calender/",
			http.HandlerFunc(calenderHandleFunc)))
}

func calenderHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeCalenderHandler {
		log.Println("calenderHandleFunc")
		defer log.Println("defer calenderHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	t := time.Now().Local()
	year := t.Year()
	month := int(t.Month())
	target := strings.SplitN(req.URL.Path, "/", 2)
	ok := false
	if len(target) == 2 {
		tmpYear, err := strconv.Atoi(target[0])
		if err == nil && 2020 <= tmpYear && tmpYear <= year {
			ok = true
			year = tmpYear
		}
		tmpMonth, err := strconv.Atoi(target[1])
		if err == nil && 1 <= tmpMonth && tmpMonth <= 12 {
			month = tmpMonth
		} else {
			ok = false
		}
	} else if len(target) == 1 && target[0] == "" {
		ok = true
	}
	if !ok {
		badRequest(w, "invalid path ("+req.URL.Path+")")
		return
	}

	cal, err := dataManager.GetCalender(year, month)
	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	writeJson(w, req, cal)
}
