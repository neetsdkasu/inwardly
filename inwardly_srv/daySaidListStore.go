package main

import (
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"
	"time"
)

type DaySaidListStatus struct {
	Id             string `json:"id"`
	Update         string `json:"update"`
	UploadDate     string `json:"upload"`
	BlogPostId     string `json:"blogpost_id"`
	BlogPathSuffix string `json:"blogpath_suffix"`
}

type DaySaidListWithLocker struct {
	DaySaidList
	time   time.Time
	locker sync.Mutex
}

// DaySaidListStoreのLockと
// DaySaidListWithLockerのLockは
// 同時に取得する場合はDaySaidListWithLockerのLockを先にする
type DaySaidListStore struct {
	Cache               map[string]*DaySaidListWithLocker
	Status              []*DaySaidListStatus
	BlogPathSuffixStore sync.Map
	sync.Mutex
}

type QuotedRefsForUpdate = [][]string
type QuotedRefsMapForUpdate = map[string]QuotedRefsForUpdate

func (this *DaySaidListWithLocker) Lock() {
	this.locker.Lock()
}

func (this *DaySaidListWithLocker) Unlock() {
	this.time = time.Now().Local()
	this.locker.Unlock()
}

func (this *DaySaidListStore) Init() error {
	if DebugModeShowInit || DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.Init")
	}
	this.Cache = make(map[string]*DaySaidListWithLocker)
	this.Status = []*DaySaidListStatus{}
	err := fileManager.LoadDaySaidListStatus(&this.Status)
	if err != nil {
		return err
	}
	for _, s := range this.Status {
		if s.BlogPathSuffix != "" {
			this.BlogPathSuffixStore.Store(s.Id, s.BlogPathSuffix)
		}
	}
	return nil
}

func (this *DaySaidListStore) GetDayIdList() []string {
	this.Lock()
	defer this.Unlock()
	list := make([]string, len(this.Status))
	for i, s := range this.Status {
		list[i] = s.Id
	}
	return list
}

func (this *DaySaidListStore) GetBlogPathSuffix(dayId string) string {
	if s, ok := this.BlogPathSuffixStore.Load(dayId); ok {
		return s.(string)
	} else {
		return ""
	}
}

func (this *DaySaidListStore) GetCalender(y, m int) (*Calender, error) {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.GetCalender")
		defer log.Println("defer DaySaidListStore.GetCalender")
	}
	this.Lock()
	defer this.Unlock()
	t := time.Now().Local()
	today := GetDayIdFromTime(t)
	if len(this.Status) == 0 {
		if y != t.Year() {
			return nil, BadRequestError{
				fmt.Sprint("invalid Year (", y, ")"),
			}
		}
		if m != int(t.Month()) {
			return nil, BadRequestError{
				fmt.Sprint("invalid Month (", m, ")"),
			}
		}
		cal := NewCalender(y, m)
		cal.Option.FirstDayId = today
		cal.Option.LastDayId = today
		return cal, nil
	}
	cal := NewCalender(y, m)
	cal.Option.FirstDayId = this.Status[0].Id
	cal.Option.LastDayId = today
	head := MakeDayId(y, m, 0)
	tail := MakeDayId(y, m, 99)
	p := sort.Search(len(this.Status), func(i int) bool {
		return strings.Compare(this.Status[i].Id, head) >= 0
	})
	days := []string{}
	for _, s := range this.Status[p:] {
		if strings.Compare(s.Id, tail) >= 0 {
			break
		}
		days = append(days, s.Id)
	}
	cal.DayIds = days
	return cal, nil
}

func (this *DaySaidListStore) GetArround(dayId string) (prev, next string, err error) {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.GetArround")
		defer log.Println("defer DaySaidListStore.GetArround")
	}
	this.Lock()
	defer this.Unlock()
	p := sort.Search(len(this.Status), func(i int) bool {
		return strings.Compare(this.Status[i].Id, dayId) >= 0
	})
	if p == len(this.Status) {
		// 今日が未saidならthis.Statusには無いため
		today := GetDayIdFromTime(time.Now().Local())
		if today != dayId {
			// dayIdが未来を指してる
			err = BadRequestError{
				"invalid Day ID (" + dayId + ")",
			}
			return
		}
	} else if this.Status[p].Id != dayId {
		// saidしなかった日付のdayIDがリクエストされてる
		err = BadRequestError{
			"not found Day ID (" + dayId + ")",
		}
		return
	}
	if p > 0 {
		prev = this.Status[p-1].Id
	}
	if p+1 < len(this.Status) {
		next = this.Status[p+1].Id
	} else {
		todayId := GetDayIdFromTime(time.Now().Local())
		if todayId != dayId {
			next = todayId
		}
	}
	return
}

func (this *DaySaidListStore) GetListCopyWithoutCaching(dayId string) (*DaySaidList, error) {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.GetListCopyWithoutCaching")
		defer log.Println("defer DaySaidListStore.GetListCopyWithoutCaching")
	}
	list, err := func() (interface{}, error) {
		this.Lock()
		defer this.Unlock()
		if listWithLocker, ok := this.Cache[dayId]; ok {
			return listWithLocker, nil
		}
		list := &DaySaidList{}
		err := fileManager.LoadDaySaidList(dayId, list)
		if err != nil {
			return nil, err
		}
		return list, nil
	}()
	if err != nil {
		return nil, err
	}
	if listWithLocker, ok := list.(*DaySaidListWithLocker); ok {
		listWithLocker.Lock()
		defer listWithLocker.Unlock()
		return listWithLocker.DaySaidList.GetCopy(), nil
	}
	return list.(*DaySaidList), nil
}

func (this *DaySaidListStore) Get(dayId string) (*DaySaidListWithLocker, error) {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.Get")
		defer log.Println("defer DaySaidListStore.Get")
	}
	this.Lock()
	defer this.Unlock()
	list, ok := this.Cache[dayId]
	if ok {
		return list, nil
	}
	list = new(DaySaidListWithLocker)
	err := fileManager.LoadDaySaidList(dayId, &list.DaySaidList)
	if err != nil {
		return nil, err
	}
	list.time = time.Now().Local()
	this.Cache[dayId] = list
	return list, nil
}

func (this *DaySaidListStore) GetSaid(saidId string) (*Said, error) {
	dayId, err := GetDayIdFromSaidId(saidId)
	if err != nil {
		return nil, err
	}
	list, err := this.Get(dayId)
	if err != nil {
		return nil, err
	}
	list.Lock()
	defer list.Unlock()
	p := sort.Search(len(list.Said), func(i int) bool {
		return strings.Compare(list.Said[i].Id, saidId) >= 0
	})
	if p == len(list.Said) || list.Said[p].Id != saidId {
		// 存在しないsaidIdがリクエストされた
		return nil, BadRequestError{
			"not found Said ID (" + saidId + ")",
		}
	}
	return list.Said[p], nil
}

func (this *DaySaidListStore) GetListCopy(dayId string) (*DaySaidList, error) {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.GetListCopy")
		defer log.Println("defer DaySaidListStore.GetListCopy")
	}
	list, err := this.Get(dayId)
	if err != nil {
		return nil, err
	}
	list.Lock()
	defer list.Unlock()
	return list.DaySaidList.GetCopy(), nil
}

func (this *DaySaidListStore) GetDaySaidListStatus() []*DaySaidListStatus {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.GetDaySaidListStatus")
		defer log.Println("defer DaySaidListStore.GetDaySaidListStatus")
	}
	this.Lock()
	defer this.Unlock()
	status := make([]*DaySaidListStatus, len(this.Status))
	copy(status, this.Status[:len(status)])
	return status
}

func (this *DaySaidListStore) SaveDaySaidListStatus(needLock bool) {
	if needLock {
		this.Lock()
		defer this.Unlock()
	}
	status := make([]*DaySaidListStatus, len(this.Status))
	copy(status, this.Status[:len(status)])
	fileManager.SaveDaySaidListStatus(status)
}

func (this *DaySaidListStore) SaveDaySaidList(dayId string, lockedList, temp *DaySaidList) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.SaveDaySaidList")
		defer log.Println("defer DaySaidListStore.SaveDaySaidList")
	}
	this.Lock()
	defer this.Unlock()
	err := fileManager.SaveDaySaidList(dayId, temp)
	if err != nil {
		return err
	}
	lockedList.Update = temp.Update
	lockedList.Said = temp.Said
	defer this.SaveDaySaidListStatus(false)
	if len(this.Status) > 0 {
		lastItem := this.Status[len(this.Status)-1]
		if lastItem.Id == dayId {
			lastItem.Update = temp.Update
			return nil
		}
		p := sort.Search(len(this.Status), func(i int) bool {
			return strings.Compare(this.Status[i].Id, dayId) >= 0
		})
		if p < len(this.Status) {
			item := this.Status[p]
			if item.Id != dayId {
				return fmt.Errorf("[BUG] not found DayId: %s", dayId)
			}
			newItem := new(DaySaidListStatus)
			*newItem = *item
			newItem.Id = dayId
			newItem.Update = temp.Update
			this.Status[p] = newItem
			return nil
		}
	}
	this.Status = append(this.Status, &DaySaidListStatus{
		Id:         dayId,
		Update:     temp.Update,
		UploadDate: "",
		BlogPostId: "",
	})
	return nil
}

func (this *DaySaidListStore) UpdateDaySaidListStatus(needLock, needSave bool, influence map[string]bool) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.UpdateDaySaidListStatus")
		defer log.Println("defer DaySaidListStore.UpdateDaySaidListStatus")
	}
	if needLock {
		this.Lock()
		defer this.Unlock()
	}
	now := ToDateString(time.Now().Local())
	status := this.Status
	for did := range influence {
		p := sort.Search(len(status), func(i int) bool {
			return strings.Compare(status[i].Id, did) >= 0
		})
		if p >= len(status) || status[p].Id != did {
			return fmt.Errorf("DaySaidListStore.UpdateDaySaidListStatus: invalid ID (%s)", did)
		}
		oldSt := status[p]
		newSt := new(DaySaidListStatus)
		*newSt = *oldSt
		newSt.Update = now
		status[p] = newSt
	}
	this.Status = status
	if needSave {
		this.SaveDaySaidListStatus(false)
	}
	return nil
}

func (this *DaySaidListStore) AppendSaid(said *Said) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.AppendSaid")
		defer log.Println("defer DaySaidListStore.AppendSaid")
	}
	dayId, err := GetDayIdFromSaidId(said.Id)
	if err != nil {
		return err
	}
	list, err := this.Get(dayId)
	if err != nil {
		return err
	}
	list.Lock()
	defer list.Unlock()
	temp := DaySaidList{
		Update: ToDateString(time.Now().Local()),
		Said:   append(list.Said, said),
	}
	qrMap, err := quotedRefsForUpdate(said, nil)
	if err != nil {
		return err
	}
	if qr, ok := qrMap[dayId]; ok {
		err = this.UpdateQuotedRefs(said.Id, dayId, qr, &temp)
		if err != nil {
			return err
		}
		delete(qrMap, dayId)
	}
	err = this.SaveDaySaidList(dayId, &list.DaySaidList, &temp)
	if err != nil {
		return err
	}
	for did, qr := range qrMap {
		err = this.UpdateQuotedRefs(said.Id, did, qr, nil)
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *DaySaidListStore) AppendReply(said *Said) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.AppendReply")
		defer log.Println("defer DaySaidListStore.AppendReply")
	}
	saidDayId, err := GetDayIdFromSaidId(said.Id)
	if err != nil {
		return fmt.Errorf("[DaySaidListStore.AppendReply] %w: said.Id = %s", err, said.Id)
	}
	parentDayId, err := GetDayIdFromSaidId(said.Parent)
	if err != nil {
		return fmt.Errorf("[DaySaidListStore.AppendReply] %w: said.Parent = %s", err, said.Parent)
	}
	if parentDayId == saidDayId {
		return this.appendReplyInSameDay(said, saidDayId)
	}
	list, err := this.Get(saidDayId)
	if err != nil {
		return err
	}
	parentList, err := this.Get(parentDayId)
	if err != nil {
		return err
	}
	// list.Lock()とparentList.Lock()の異なるタイミングでのロック
	// 別のLock呼び出しでデッドロック発生しうる気がする…
	// Lockの呼び出し箇所やタイミングの図式化の整理をする必要あるかも…
	list.Lock()
	defer list.Unlock()
	parentList.Lock()
	defer parentList.Unlock()

	// 二分探索をしているがID順に保存されている保証はない…
	// (goルーチン(軽量スレッド)の切り替えタイミング次第ではID順にならなくなる)
	// (現実的な利用の仕方ではID順になっていることが予想はされる)
	p := sort.Search(len(parentList.Said), func(i int) bool {
		return strings.Compare(parentList.Said[i].Id, said.Parent) >= 0
	})
	if p == len(parentList.Said) || parentList.Said[p].Id != said.Parent {
		// 存在しないsaidへのリプ
		return BadRequestError{
			"invalid Parent Said ID (" + said.Parent + ")",
		}
	}
	oldParent := parentList.Said[p]
	newParent := new(Said)
	*newParent = *oldParent
	children := make([]string, 0, len(oldParent.Children)+1)
	children = append(children, oldParent.Children...)
	children = append(children, said.Id)
	newParent.Children = children
	if oldParent.Root == "" {
		said.Root = oldParent.Id
	} else {
		said.Root = oldParent.Root
	}
	influence := map[string]bool{}
	for _, cid := range children {
		did, err := GetDayIdFromSaidId(cid)
		if err != nil {
			return err
		}
		if did != parentDayId && did != saidDayId {
			influence[did] = true
		}
	}

	tempParentList := DaySaidList{
		Update: ToDateString(time.Now().Local()),
		Said:   make(SaidList, len(parentList.Said)),
	}
	copy(tempParentList.Said, parentList.Said[:len(tempParentList.Said)])
	tempParentList.Said[p] = newParent

	tempList := DaySaidList{
		Update: tempParentList.Update,
		Said:   make(SaidList, len(list.Said), len(list.Said)+1),
	}
	copy(tempList.Said, list.Said)
	tempList.Said = append(tempList.Said, said)

	qrMap, err := quotedRefsForUpdate(said, nil)
	if err != nil {
		return err
	}

	if qr, ok := qrMap[saidDayId]; ok {
		err = this.UpdateQuotedRefs(said.Id, saidDayId, qr, &tempList)
		if err != nil {
			return err
		}
		delete(qrMap, saidDayId)
	}
	if qr, ok := qrMap[parentDayId]; ok {
		err = this.UpdateQuotedRefs(said.Id, parentDayId, qr, &tempParentList)
		if err != nil {
			return err
		}
		delete(qrMap, parentDayId)
	}

	err = this.SaveDaySaidList(saidDayId, &list.DaySaidList, &tempList)
	if err != nil {
		return err
	}
	// parent側の更新に失敗した場合の対処がない…
	err = this.SaveDaySaidList(parentDayId, &parentList.DaySaidList, &tempParentList)
	if err != nil {
		return err
	}
	if len(influence) > 0 {
		err = this.UpdateDaySaidListStatus(true, true, influence)
		if err != nil {
			return err
		}
	}
	for did, qr := range qrMap {
		err = this.UpdateQuotedRefs(said.Id, did, qr, nil)
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *DaySaidListStore) appendReplyInSameDay(said *Said, dayId string) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.appendReplyInSameDay")
		defer log.Println("defer DaySaidListStore.appendReplyInSameDay")
	}
	list, err := this.Get(dayId)
	if err != nil {
		return err
	}
	list.Lock()
	defer list.Unlock()
	foundParent := false
	temp := DaySaidList{
		Update: ToDateString(time.Now().Local()),
		Said:   make(SaidList, len(list.Said), len(list.Said)+1),
	}
	copy(temp.Said[:len(list.Said)], list.Said)
	influence := map[string]bool{}
	// 1日に多くて何個投稿するかだけど
	// (多くてせいぜい300？、利用頻度不明だが平均100くらい？)
	// 二分探索するほどの量じゃないよね？
	// (二分探索だと300個なら最大9回くらいの探索で終わるが？)
	// (文字列比較は重いかも？頭6文字は常に一致だし…)
	for i, s := range temp.Said {
		if s.Id != said.Parent {
			continue
		}
		foundParent = true
		newParent := new(Said)
		*newParent = *s
		children := make([]string, 0, len(s.Children)+1)
		children = append(children, s.Children...)
		children = append(children, said.Id)
		newParent.Children = children
		temp.Said[i] = newParent
		if s.Root == "" {
			said.Root = s.Id
		} else {
			said.Root = s.Root
		}
		for _, cid := range children {
			did, err := GetDayIdFromSaidId(cid)
			if err != nil {
				return err
			}
			if did != dayId {
				influence[did] = true
			}
		}
		break // 2つ以上同じIDが見つかった場合(invalidなlistの場合)の処理ないよね、生じるか知らんけど
	}
	if !foundParent {
		// 存在しないsaidへのリプ
		return BadRequestError{
			"invalid Parent Said ID (" + said.Parent + ")",
		}
	}
	temp.Said = append(temp.Said, said)

	qrMap, err := quotedRefsForUpdate(said, nil)
	if err != nil {
		return err
	}

	if qr, ok := qrMap[dayId]; ok {
		err = this.UpdateQuotedRefs(said.Id, dayId, qr, &temp)
		if err != nil {
			return err
		}
		delete(qrMap, dayId)
	}

	err = this.SaveDaySaidList(dayId, &list.DaySaidList, &temp)
	if err != nil {
		return err
	}
	if len(influence) > 0 {
		err = this.UpdateDaySaidListStatus(true, true, influence)
		if err != nil {
			return err
		}
	}
	for did, qr := range qrMap {
		err = this.UpdateQuotedRefs(said.Id, did, qr, nil)
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *DaySaidListStore) EditSaid(said *Said) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.EditSaid")
		defer log.Println("defer DaySaidListStore.EditSaid")
	}
	dayId, err := GetDayIdFromSaidId(said.Id)
	if err != nil {
		return err
	}
	list, err := this.Get(dayId)
	if err != nil {
		return err
	}
	list.Lock()
	defer list.Unlock()
	temp := DaySaidList{
		Update: ToDateString(time.Now().Local()),
		Said:   make(SaidList, len(list.Said)),
	}
	copy(temp.Said, list.Said)
	var qrMap QuotedRefsMapForUpdate
	influence := map[string]bool{}
	found := false
	for i, s := range temp.Said {
		if s.Id != said.Id {
			continue
		}
		if s.Deleted && said.Deleted {
			// 削除済みのsaidをまた削除しようとした
			return BadRequestError{
				"already deleted (Said ID " + said.Id + ")",
			}
		}
		changeImage := false
		found = true
		said.Date = s.Date
		said.Parent = s.Parent
		said.Children = s.Children
		said.QuotedRefs = s.QuotedRefs
		said.Root = s.Root
		var imageList []string = nil
		for i, x := range said.ImageList {
			imf := x
			if x == "." {
				if i < len(s.ImageList) {
					imf = s.ImageList[i]
				} else {
					continue
				}
			} else {
				changeImage = true
			}
			if imf != "" {
				imageList = append(imageList, imf)
			}
		}
		if !changeImage && said.Text == s.Text && said.Secret == s.Secret {
			// 変更がないのに変更のリクエストがきた
			return BadRequestError{
				"nothing changes (Said Id " + said.Id + ")",
			}
		}
		said.ImageList = imageList
		if said.Deleted {
			said.Secret = s.Secret
		}
		past := new(PastSaid)
		past.Text = s.Text
		past.ImageList = s.ImageList
		past.Delete = s.Deleted
		past.Secret = s.Secret
		if len(s.History) == 0 {
			past.Date = s.Date
		} else {
			past.Date = s.Update
		}
		said.History = append(s.History, past)
		temp.Said[i] = said
		qrMap, err = quotedRefsForUpdate(said, s)
		if err != nil {
			return err
		}
		for _, cid := range said.QuotedRefs {
			did, err := GetDayIdFromSaidId(cid)
			if err != nil {
				return err
			}
			if did == dayId {
				continue
			}
			influence[did] = true
			qs, err := this.GetSaid(cid)
			if err != nil {
				return err
			}
			for _, cid2 := range qs.Children {
				did2, err := GetDayIdFromSaidId(cid2)
				if err != nil {
					return err
				}
				if did2 != dayId {
					influence[did2] = true
				}
			}
		}
		for _, cid := range said.Children {
			did, err := GetDayIdFromSaidId(cid)
			if err != nil {
				return err
			}
			if did != dayId {
				influence[did] = true
			}
		}
		break
	}
	if !found {
		// 存在しないsaidIdに対する変更リクエストが来た
		return BadRequestError{
			"invalid Said ID (" + said.Id + ")",
		}
	}
	if qr, ok := qrMap[dayId]; ok {
		err = this.UpdateQuotedRefs(said.Id, dayId, qr, &temp)
		if err != nil {
			return err
		}
		delete(qrMap, dayId)
	}
	err = this.SaveDaySaidList(dayId, &list.DaySaidList, &temp)
	if err != nil {
		return err
	}
	if len(influence) > 0 {
		err = this.UpdateDaySaidListStatus(true, true, influence)
		if err != nil {
			return err
		}
	}
	for did, qr := range qrMap {
		err = this.UpdateQuotedRefs(said.Id, did, qr, nil)
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *DaySaidListStore) RecoverSaid(said *Said, index int) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.RecoverSaid")
		defer log.Println("defer DaySaidListStore.RecoverSaid")
	}
	dayId, err := GetDayIdFromSaidId(said.Id)
	if err != nil {
		return err
	}
	list, err := this.Get(dayId)
	if err != nil {
		return err
	}
	list.Lock()
	defer list.Unlock()
	lockedList := DaySayListSetEntry{dayId, &list.DaySaidList}
	temp := DaySaidList{
		Update: ToDateString(time.Now().Local()),
		Said:   make(SaidList, len(list.Said)),
	}
	copy(temp.Said, list.Said)
	var qrMap QuotedRefsMapForUpdate
	influence := map[string]bool{}
	found := false
	for i, s := range temp.Said {
		if s.Id != said.Id {
			continue
		}
		if index >= len(s.History) {
			// 削除済みのsaidをまた削除しようとした
			return BadRequestError{
				fmt.Sprint("invalid history index (", index, ")"),
			}
		}
		hist := s.History[index]
		if s.Text == hist.Text && len(s.ImageList) == len(hist.ImageList) && s.Secret == hist.Secret {
			same := true
			for i := range s.ImageList {
				if s.ImageList[i] != hist.ImageList[i] {
					same = false
					break
				}
			}
			if same {
				return BadRequestError{
					"nothing changes (Said Id " + said.Id + ")",
				}
			}
		}
		found = true
		said.Date = s.Date
		said.Parent = s.Parent
		said.Children = s.Children
		said.QuotedRefs = s.QuotedRefs
		said.Root = s.Root
		said.Secret = hist.Secret
		said.Text = hist.Text
		sreq := SayRequest{
			Type:   SayRequestTypeEdit,
			Target: said.Id,
			Text:   hist.Text,
		}
		said.TextElements = sreq.TextElements(lockedList)
		said.ImageList = hist.ImageList
		past := new(PastSaid)
		past.Text = s.Text
		past.ImageList = s.ImageList
		past.Delete = s.Deleted
		past.Secret = s.Secret
		if len(s.History) == 0 {
			past.Date = s.Date
		} else {
			past.Date = s.Update
		}
		said.History = append(s.History, past)
		temp.Said[i] = said
		qrMap, err = quotedRefsForUpdate(said, s)
		if err != nil {
			return err
		}
		for _, cid := range said.QuotedRefs {
			did, err := GetDayIdFromSaidId(cid)
			if err != nil {
				return err
			}
			if did == dayId {
				continue
			}
			influence[did] = true
			qs, err := this.GetSaid(cid)
			if err != nil {
				return err
			}
			for _, cid2 := range qs.Children {
				did2, err := GetDayIdFromSaidId(cid2)
				if err != nil {
					return err
				}
				if did2 != dayId {
					influence[did2] = true
				}
			}
		}
		for _, cid := range said.Children {
			did, err := GetDayIdFromSaidId(cid)
			if err != nil {
				return err
			}
			if did != dayId {
				influence[did] = true
			}
		}
		break
	}
	if !found {
		// 存在しないsaidIdに対する変更リクエストが来た
		return BadRequestError{
			"invalid Said ID (" + said.Id + ")",
		}
	}
	if qr, ok := qrMap[dayId]; ok {
		err = this.UpdateQuotedRefs(said.Id, dayId, qr, &temp)
		if err != nil {
			return err
		}
		delete(qrMap, dayId)
	}
	err = this.SaveDaySaidList(dayId, &list.DaySaidList, &temp)
	if err != nil {
		return err
	}
	if len(influence) > 0 {
		err = this.UpdateDaySaidListStatus(true, true, influence)
		if err != nil {
			return err
		}
	}
	for did, qr := range qrMap {
		err = this.UpdateQuotedRefs(said.Id, did, qr, nil)
		if err != nil {
			return err
		}
	}
	return nil
}

func quotedRefsForUpdate(newSaid, oldSaid *Said) (QuotedRefsMapForUpdate, error) {
	tmp := map[string]int{}
	if newSaid != nil && len(newSaid.TextElements) > 0 {
		for _, t := range newSaid.TextElements {
			if t.Type != TextElementTypeInsideQuote {
				continue
			}
			id := strings.SplitN(t.Src, "\n", 2)[1]
			tmp[id] = tmp[id] | 2
		}
	}
	if oldSaid != nil && len(oldSaid.TextElements) > 0 {
		for _, t := range oldSaid.TextElements {
			if t.Type != TextElementTypeInsideQuote {
				continue
			}
			id := strings.SplitN(t.Src, "\n", 2)[1]
			tmp[id] = tmp[id] | 1
		}
	}
	ret := QuotedRefsMapForUpdate{}
	for id, v := range tmp {
		if v == 3 {
			continue
		}
		dayId, err := GetDayIdFromSaidId(id)
		if err != nil {
			return nil, err
		}
		x := ret[dayId]
		if len(x) != 2 {
			x = make(QuotedRefsForUpdate, 2)
		}
		if v == 1 {
			x[0] = append(x[0], id) // delete
		} else {
			x[1] = append(x[1], id) // add
		}
		ret[dayId] = x
	}
	return ret, nil
}

// list: editting list if it exists
func (this *DaySaidListStore) UpdateQuotedRefs(saidId, targetDayId string, targets QuotedRefsForUpdate, list *DaySaidList) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.UpdateQuotedRefs")
		defer log.Println("defer DaySaidListStore.UpdateQuotedRefs")
	}
	var lockedList *DaySaidListWithLocker = nil
	if list == nil {
		var err error
		lockedList, err = this.Get(targetDayId)
		if err != nil {
			return err
		}
		lockedList.Lock()
		defer lockedList.Unlock()
		list = &DaySaidList{
			Update: ToDateString(time.Now().Local()),
			Said:   make(SaidList, len(lockedList.Said)),
		}
		copy(list.Said, lockedList.Said)
	}
	for _, id := range targets[0] { // delete
		found := false
		for i, s := range list.Said {
			if s.Id != id {
				continue
			}
			qr := make([]string, 0, len(s.QuotedRefs))
			for _, r := range s.QuotedRefs {
				if r != saidId {
					qr = append(qr, r)
				} else {
					found = true
				}
			}
			if len(qr) == 0 {
				qr = nil
			}
			temp := new(Said)
			*temp = *s
			temp.QuotedRefs = qr
			list.Said[i] = temp
			break
		}
		if !found {
			// QuotedRefsの無かったときの古いsaidデータでこのエラーに到達する
			// まぁそれはテスト用saidデータだから別にいいんだけどさぁ
			return fmt.Errorf("BUG? delete from target %s", id)
		}
	}
	for _, id := range targets[1] { // add
		found := false
		for i, s := range list.Said {
			if s.Id != id {
				continue
			}
			qr := make([]string, 0, len(s.QuotedRefs)+1)
			for _, r := range s.QuotedRefs {
				if r == saidId {
					found = true
				}
				qr = append(qr, r)
			}
			if !found {
				found = true
				qr = append(qr, saidId)
			}
			temp := new(Said)
			*temp = *s
			temp.QuotedRefs = qr
			list.Said[i] = temp
			break
		}
		if !found {
			return fmt.Errorf("BUG? add to target %s", id)
		}
	}
	if lockedList == nil {
		return nil
	}
	return this.SaveDaySaidList(targetDayId, &lockedList.DaySaidList, list)
}

func (this *DaySaidListStore) SetUploadedDaySaidListStatus(i int, status DaySaidListStatus) error {
	if DebugModeDaySaidListStore {
		log.Println("DaySaidListStore.SetUploadedDaySaidListStatus")
		defer log.Println("defer DaySaidListStore.SetUploadedDaySaidListStatus")
	}
	this.Lock()
	defer this.Unlock()
	if i < 0 || i >= len(this.Status) {
		return fmt.Errorf("[BUG] DaySaidListStore.SetUploadedDaySaidListStatus: invalid index (%d)", i)
	}
	newStatus := new(DaySaidListStatus)
	*newStatus = status
	if this.Status[i].Id != newStatus.Id {
		i = sort.Search(len(this.Status), func(k int) bool {
			return strings.Compare(this.Status[k].Id, newStatus.Id) >= 0
		})
		if i >= len(this.Status) || this.Status[i].Id != newStatus.Id {
			return fmt.Errorf("[BUG] DaySaidListStore.SetUploadedDaySaidListStatus: invalid id (%s)", newStatus.Id)
		}
	}
	oldStatus := this.Status[i]
	if strings.Compare(oldStatus.Update, newStatus.Update) > 0 {
		if strings.Compare(oldStatus.Update, newStatus.UploadDate) <= 0 {
			t, err := FromDateString(oldStatus.Update)
			if err != nil {
				return err
			}
			newStatus.UploadDate = ToDateString(t.Add(-time.Second))
		}
		newStatus.Update = oldStatus.Update
	}
	this.Status[i] = newStatus
	this.SaveDaySaidListStatus(false)
	if newStatus.BlogPathSuffix != "" {
		this.BlogPathSuffixStore.Store(newStatus.Id, newStatus.BlogPathSuffix)
	} else if oldStatus.BlogPathSuffix != "" {
		this.BlogPathSuffixStore.Delete(newStatus.Id)
	}
	return nil
}

func (this *DaySaidListStore) CleanCache(expire time.Time) error {
	if DebugModeDaySaidListStore || DebugModeCleanCache {
		log.Println("DaySaidListStore.CleanCache")
		defer log.Println("defer DaySaidListStore.CleanCache")
	}
	dayIdList := func() []string {
		this.Lock()
		defer this.Unlock()
		list := make([]string, 0, len(this.Cache))
		for dayId := range this.Cache {
			list = append(list, dayId)
		}
		return list
	}()
	yesterday := GetDayIdFromTime(time.Now().Local().AddDate(0, 0, -1))
	for _, dayId := range dayIdList {
		if strings.Compare(dayId, yesterday) >= 0 {
			// 今日と昨日の分は除外
			continue
		}
		err := func(dayId string) error {
			list, err := this.Get(dayId)
			if err != nil {
				return err
			}
			list.locker.Lock()
			defer list.locker.Unlock()
			if expire.After(list.time) {
				this.Lock()
				defer this.Unlock()
				delete(this.Cache, dayId)
				if DebugModeCleanCache {
					log.Println("Clean Cache: DaySaidList", dayId)
				}
			}
			return nil
		}(dayId)
		if err != nil {
			return err
		}
	}
	return nil
}
