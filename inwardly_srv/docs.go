package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"sync"
)

type DocumentLink struct {
	Setting *DocumentLinkSetting `json:"setting"`
	State   *DocumentLinkState   `json:"state"`
}

var docLinkHandlers struct {
	Table map[string]http.Handler
	sync.Mutex
}

func init() {
	docLinkHandlers.Table = map[string]http.Handler{}

	// GET
	// /docs/{EntryName}/[some path...]
	http.Handle("/docs/",
		http.StripPrefix("/docs/",
			http.HandlerFunc(docsHandleFunc)))

	// GET
	// /api/link/list
	http.Handle("/api/link/list",
		http.HandlerFunc(listLinkHandleFunc))

	// POST
	// /api/link/add
	// entry string (not null, require)
	// path      string (non null, require)
	http.Handle("/api/link/add",
		http.HandlerFunc(addLinkHandleFunc))

	// POST
	// /api/link/update
	// id        number (not null, require)
	// old_entry string (not null, require)
	// entry     string (not null, require)
	// path      string (non null, require)
	http.Handle("/api/link/update",
		http.HandlerFunc(updateLinkHandleFunc))

	// GET
	// /api/link/dig/{EntryName}
	http.Handle("/api/link/dig/",
		http.StripPrefix("/api/link/dig/",
			http.HandlerFunc(digLinkHandleFunc)))

	// GET
	// /api/link/cd/{EntryName}/{some path...}
	http.Handle("/api/link/cd/",
		http.StripPrefix("/api/link/cd",
			http.HandlerFunc(cdLinkHandleFunc)))

	// POST
	// mark current dir
	// /api/link/markdir/add
	// id        number (not null, require)
	// entry     string (not null, require)
	http.Handle("/api/link/markdir/add",
		http.HandlerFunc(addMarkDirLinkHandleFunc))

	// POST
	// mark file on current dir
	// /api/link/markfile/add
	// id        number (not null, require)
	// entry     string (not null, require)
	// filename  string (non null, require)
	http.Handle("/api/link/markfile/add",
		http.HandlerFunc(addMarkFileLinkHandleFunc))

	// POST
	// reove mark dir
	// /api/link/markdir/remove
	// id        number (not null, require)
	// entry     string (not null, require)
	// path      string (non null, require)
	http.Handle("/api/link/markdir/remove",
		http.HandlerFunc(removeMarkDirLinkHandleFunc))

	// POST
	// remove mark file
	// /api/link/markfile/remove
	// id        number (not null, require)
	// entry     string (not null, require)
	// path      string (non null, require)
	http.Handle("/api/link/markfile/remove",
		http.HandlerFunc(removeMarkFileLinkHandleFunc))

	// POST
	// add extentions
	// /api/link/ext/add
	// id         number (not null, require)
	// entry      string (not null, require)
	// extensions string (non null, require, comma or space separated)
	http.Handle("/api/link/ext/add",
		http.HandlerFunc(addExtensionsLinkHandleFunc))

	// POST
	// remove extention
	// /api/link/ext/remove
	// id         number (not null, require)
	// entry      string (not null, require)
	// extension  string (non null, require)
	http.Handle("/api/link/ext/remove",
		http.HandlerFunc(removeExtensionLinkHandleFunc))

	// POST
	// set quick link order
	// /api/link/qlorder/set
	// id     number (not null, require)
	// entry  string (not null, require)
	// order  number (non null, require)
	http.Handle("/api/link/qlorder/set",
		http.HandlerFunc(setQuickLinkOrderHandleFunc))
}

func deleteDocLinkHandler(entry string) {
	docLinkHandlers.Lock()
	defer docLinkHandlers.Unlock()
	if _, ok := docLinkHandlers.Table[entry]; ok {
		delete(docLinkHandlers.Table, entry)
	}
}

func getDocLinkHandler(entry string) (http.Handler, bool) {
	docLinkHandlers.Lock()
	defer docLinkHandlers.Unlock()

	handler, ok := docLinkHandlers.Table[entry]

	if !ok {
		if DebugModeDocsHandler {
			log.Println("getDocLinkHandler: make handler", entry)
			defer log.Println("defer getDocLinkHandler: make handler", entry)
		}

		dl, ok := appSetting.GetDocumentLinkSetting(entry)
		if !ok {
			return nil, false
		}
		handler = http.StripPrefix(
			dl.EntryName,
			http.FileServer(http.Dir(dl.DocumentRootPath)),
		)
		docLinkHandlers.Table[dl.EntryName] = handler
	}

	return handler, true
}

func docsHandleFunc(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	entry := strings.SplitN(req.URL.Path, "/", 2)[0]

	if len(entry) == 0 {
		http.NotFound(w, req)
		return
	}

	handler, ok := getDocLinkHandler(entry)

	if ok {
		handler.ServeHTTP(w, req)
	} else {
		http.NotFound(w, req)
	}

}

func listLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("listLinkHandleFunc")
		defer log.Println("defer listLinkHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	list := appSetting.GetDocumentLinkSettingList()
	sort.Slice(list, func(i, j int) bool {
		return list[i].Id < list[j].Id
	})

	res := make([]DocumentLink, len(list))
	for i, dl := range list {
		res[i].Setting = dl
	}

	writeJson(w, req, res)
}

func addLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("addLinkHandleFunc")
		defer log.Println("defer addLinkHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	entryName := strings.TrimSpace(req.PostFormValue("entry"))
	docPath := strings.TrimSpace(req.PostFormValue("path"))

	if entryName == "" {
		badRequest(w, "missing entry")
		return
	}

	if docPath == "" {
		badRequest(w, "missing path")
		return
	}

	dl, err := appSetting.AddDocumentLinkSetting(entryName, docPath)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := DocumentLink{
		Setting: dl,
	}

	writeJson(w, req, &res)
}

func updateLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("updateLinkHandleFunc")
		defer log.Println("defer updateLinkHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	idStr := strings.TrimSpace(req.PostFormValue("id"))
	if idStr == "" {
		badRequest(w, "missing id")
		return
	}

	id, err := strconv.Atoi(idStr)
	if err != nil {
		badRequest(w, "invalid id: "+idStr)
		return
	}

	oldEntryName := strings.TrimSpace(req.PostFormValue("old_entry"))
	if oldEntryName == "" {
		badRequest(w, "missing old_entry")
		return
	}

	entryName := strings.TrimSpace(req.PostFormValue("entry"))
	docPath := strings.TrimSpace(req.PostFormValue("path"))

	if entryName == "" && docPath != "" {
		badRequest(w, "missing entry")
		return
	}

	if docPath == "" && entryName != "" {
		badRequest(w, "missing path")
		return
	}

	oldDl, newDl, err := appSetting.UpdateDocumentLinkSetting(id, oldEntryName, entryName, docPath)
	if err != nil {
		errorResponse(w, err)
		return
	}

	ds, err := appState.UpdateDocumentLinkState(oldDl, newDl)
	if err != nil {
		errorResponse(w, err)
		return
	}

	deleteDocLinkHandler(oldDl.EntryName)

	res := DocumentLink{
		Setting: newDl,
		State:   ds,
	}

	writeJson(w, req, &res)
}

func digLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("digLinkHandleFunc")
		defer log.Println("defer digLinkHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	entryName := req.URL.Path
	if entryName == "" {
		badRequest(w, "missing entryName")
		return
	}

	dl, ok := appSetting.GetDocumentLinkSetting(entryName)
	if !ok {
		badRequest(w, "invalid entryName: "+entryName)
		return
	}

	ds, ok := appState.GetDocumentLinkState(entryName)

	if !ok {
		var err error
		ds, err = dl.Glob(nil, nil)
		if err != nil {
			errorResponse(w, err)
			return
		}

		err = appState.SetDocumentLinkState(ds)
		if err != nil {
			errorResponse(w, err)
			return
		}
	}

	res := DocumentLink{
		Setting: dl,
		State:   ds,
	}

	writeJson(w, req, &res)
}

func cdLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("cdLinkHandleFunc")
		defer log.Println("defer cdLinkHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	p := path.Clean(req.URL.Path)
	p = strings.TrimPrefix(p, "/")
	ps := strings.Split(p, "/")

	entryName := ps[0]
	newPath := ps[1:]

	dl, ok := appSetting.GetDocumentLinkSetting(entryName)
	if !ok {
		badRequest(w, "invalid Entry Name ("+entryName+")")
		return
	}

	oldDs, _ := appState.GetDocumentLinkState(entryName)

	newDs, err := dl.Glob(oldDs, newPath)
	if err != nil {
		errorResponse(w, err)
		return
	}

	err = appState.SetDocumentLinkState(newDs)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := DocumentLink{
		Setting: dl,
		State:   newDs,
	}

	writeJson(w, req, &res)
}

func addMarkDirLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("addMarkDirLinkHandleFunc")
		defer log.Println("defer addMarkDirLinkHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	idStr := strings.TrimSpace(req.PostFormValue("id"))
	if idStr == "" {
		badRequest(w, "missing id")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		badRequest(w, "invalid id: "+idStr)
		return
	}

	entryName := strings.TrimSpace(req.PostFormValue("entry"))
	if entryName == "" {
		badRequest(w, "missing entry")
		return
	}

	ds, err := appState.AddMarkDirDocumentLinkState(id, entryName)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := DocumentLink{
		State: ds,
	}

	writeJson(w, req, &res)
}

func addMarkFileLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("addMarkFileLinkHandleFunc")
		defer log.Println("defer addMarkFileLinkHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	idStr := strings.TrimSpace(req.PostFormValue("id"))
	if idStr == "" {
		badRequest(w, "missing id")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		badRequest(w, "invalid id: "+idStr)
		return
	}

	entryName := strings.TrimSpace(req.PostFormValue("entry"))
	if entryName == "" {
		badRequest(w, "missing entry")
		return
	}

	fileName := strings.TrimSpace(req.PostFormValue("filename"))
	if fileName == "" {
		badRequest(w, "missing filename")
		return
	}

	ds, err := appState.AddMarkFileDocumentLinkState(id, entryName, fileName)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := DocumentLink{
		State: ds,
	}

	writeJson(w, req, &res)
}

func removeMarkDirLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("removeMarkDirLinkHandleFunc")
		defer log.Println("defer removeMarkDirLinkHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	idStr := strings.TrimSpace(req.PostFormValue("id"))
	if idStr == "" {
		badRequest(w, "missing id")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		badRequest(w, "invalid id: "+idStr)
		return
	}

	entryName := strings.TrimSpace(req.PostFormValue("entry"))
	if entryName == "" {
		badRequest(w, "missing entry")
		return
	}

	path := strings.TrimSpace(req.PostFormValue("path"))
	if path == "" {
		badRequest(w, "missing path")
		return
	}

	ds, err := appState.RemoveMarkDirDocumentLinkState(id, entryName, path)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := DocumentLink{
		State: ds,
	}

	writeJson(w, req, &res)
}

func removeMarkFileLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("removeMarkFileLinkHandleFunc")
		defer log.Println("defer removeMarkFileLinkHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	idStr := strings.TrimSpace(req.PostFormValue("id"))
	if idStr == "" {
		badRequest(w, "missing id")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		badRequest(w, "invalid id: "+idStr)
		return
	}

	entryName := strings.TrimSpace(req.PostFormValue("entry"))
	if entryName == "" {
		badRequest(w, "missing entry")
		return
	}

	path := strings.TrimSpace(req.PostFormValue("path"))
	if path == "" {
		badRequest(w, "missing path")
		return
	}

	ds, err := appState.RemoveMarkFileDocumentLinkState(id, entryName, path)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := DocumentLink{
		State: ds,
	}

	writeJson(w, req, &res)
}

func addExtensionsLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("addExtensionsLinkHandleFunc")
		defer log.Println("defer addExtensionsLinkHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	idStr := strings.TrimSpace(req.PostFormValue("id"))
	if idStr == "" {
		badRequest(w, "missing id")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		badRequest(w, "invalid id: "+idStr)
		return
	}

	entryName := strings.TrimSpace(req.PostFormValue("entry"))
	if entryName == "" {
		badRequest(w, "missing entry")
		return
	}

	extensions := strings.TrimSpace(req.PostFormValue("extensions"))
	if extensions == "" {
		badRequest(w, "missing extensions")
		return
	}

	dl, ok := appSetting.GetDocumentLinkSetting(entryName)
	if !ok {
		badRequest(w, "invalid Entry Name ("+entryName+")")
		return
	}

	oldDs, err := appState.AddExtensionsDocumentLinkState(id, entryName, extensions)
	if err != nil {
		errorResponse(w, err)
		return
	}

	newDs, err := dl.Glob(oldDs, nil)
	if err != nil {
		errorResponse(w, err)
		return
	}

	err = appState.SetDocumentLinkState(newDs)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := DocumentLink{
		Setting: dl,
		State:   newDs,
	}

	writeJson(w, req, &res)
}

func removeExtensionLinkHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("removeExtensionLinkHandleFunc")
		defer log.Println("defer removeExtensionLinkHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	idStr := strings.TrimSpace(req.PostFormValue("id"))
	if idStr == "" {
		badRequest(w, "missing id")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		badRequest(w, "invalid id: "+idStr)
		return
	}

	entryName := strings.TrimSpace(req.PostFormValue("entry"))
	if entryName == "" {
		badRequest(w, "missing entry")
		return
	}

	extension := strings.TrimSpace(req.PostFormValue("extension"))
	if extension == "" {
		badRequest(w, "missing extension")
		return
	}

	dl, ok := appSetting.GetDocumentLinkSetting(entryName)
	if !ok {
		badRequest(w, "invalid Entry Name ("+entryName+")")
		return
	}

	oldDs, err := appState.RemoveExtensionDocumentLinkState(id, entryName, extension)
	if err != nil {
		errorResponse(w, err)
		return
	}

	newDs, err := dl.Glob(oldDs, nil)
	if err != nil {
		errorResponse(w, err)
		return
	}

	err = appState.SetDocumentLinkState(newDs)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := DocumentLink{
		Setting: dl,
		State:   newDs,
	}

	writeJson(w, req, &res)
}

func setQuickLinkOrderHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeDocsHandler {
		log.Println("setQuickLinkOrderHandleFunc")
		defer log.Println("defer setQuickLinkOrderHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	idStr := strings.TrimSpace(req.PostFormValue("id"))
	if idStr == "" {
		badRequest(w, "missing id")
		return
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		badRequest(w, "invalid id: "+idStr)
		return
	}

	entry := strings.TrimSpace(req.PostFormValue("entry"))
	if entry == "" {
		badRequest(w, "missing entry")
		return
	}

	orderStr := strings.TrimSpace(req.PostFormValue("order"))
	if orderStr == "" {
		badRequest(w, "missing order")
		return
	}
	order, err := strconv.Atoi(orderStr)
	if err != nil {
		badRequest(w, "invalid order: "+orderStr)
		return
	}

	dl, err := appSetting.UpdateQLOrderDocumentLinkSetting(id, entry, order)
	if err != nil {
		errorResponse(w, err)
		return
	}

	res := &DocumentLink{
		Setting: dl,
	}

	writeJson(w, req, &res)
}

func (this *DocumentLinkSetting) Glob(base *DocumentLinkState, newPath []string) (*DocumentLinkState, error) {
	p := this.DocumentRootPath
	if newPath != nil {
		p = filepath.Join(p, filepath.Join(newPath...))
	} else if base != nil {
		p = filepath.Join(p, filepath.Join(base.CurrentPath...))
	}
	d, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer d.Close()
	if s, err := d.Stat(); err != nil {
		return nil, err
	} else if !s.IsDir() {
		return nil, fmt.Errorf("%s is not directory", p)
	}
	infos, err := d.Readdir(0)
	if err != nil {
		return nil, err
	}
	dirs := []string{}
	files := []string{}
	for _, fi := range infos {
		if fi.IsDir() {
			dirs = append(dirs, fi.Name())
		} else {
			e := filepath.Ext(fi.Name())
			if base != nil {
				for _, te := range base.Extensions {
					if te == "*" {
						files = append(files, fi.Name())
						break
					} else if te == "." && e == "" {
						files = append(files, fi.Name())
						break
					} else if e == "."+te {
						files = append(files, fi.Name())
						break
					}
				}
			} else if e == ".html" {
				files = append(files, fi.Name())
			}
		}
	}
	ret := new(DocumentLinkState)
	if base != nil {
		*ret = *base
	} else {
		ret.Id = this.Id
		ret.EntryName = this.EntryName
		ret.Extensions = append(ret.Extensions, "html")
	}
	if newPath != nil {
		ret.CurrentPath = newPath
	}
	ret.Directories = dirs
	ret.Files = files
	return ret, nil
}
