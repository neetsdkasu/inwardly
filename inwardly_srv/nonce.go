package main

import (
	"encoding/base64"
	"math/rand"
	"strings"
	"sync"
	"time"
	"unicode"
)

var nonceNoise = struct {
	count int64
	sync.Mutex
}{
	count: 97531,
}

func getNonceNoise() int64 {
	nonceNoise.Lock()
	defer nonceNoise.Unlock()
	ret := nonceNoise.count
	nonceNoise.count = (ret + 100007) % 1000000007
	return ret
}

func GenerateNonce() string {
	src := make([]byte, 32)
	noise := getNonceNoise()
	rand.Seed(time.Now().Unix() + noise)
	for i := 1 + (noise % 5); i > 0; i-- {
		rand.Read(src)
	}
	return strings.Join(
		strings.FieldsFunc(
			base64.StdEncoding.EncodeToString(src),
			func(c rune) bool {
				return !unicode.IsLetter(c) && !unicode.IsDigit(c)
			},
		),
		"",
	)
}
