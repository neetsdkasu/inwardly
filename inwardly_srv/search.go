package main

import (
	"fmt"
	"log"
	"net/http"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

func init() {
	// POST
	// query string (non empty)
	http.HandleFunc("/api/search", searchHandleFunc)

	// GET
	// /api/result/{ResultId}
	http.Handle("/api/result/",
		http.StripPrefix("/api/result/",
			http.HandlerFunc(resultHandleFunc)))
}

func searchHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSaidDeleteHandler {
		log.Println("searchHandleFunc")
		defer log.Println("defer searchHandleFunc")
	}

	if req.Method != http.MethodPost {
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	}

	host := req.Host
	if host == "" {
		host = Address
	}

	query := req.PostFormValue("query")
	if strings.TrimSpace(query) == "" {
		badRequest(w, "missing query parameter")
		return
	}

	id := MakeSaidIdFromTime(time.Now().Local())

	list := dataManager.GetDayIdList()

	result := &SearchResult{
		Id:    id,
		Query: query,
		Count: len(list),
	}

	dataManager.SetSearchResult(result)

	go SearchSaid(host, id, query, list)

	writeJson(w, req, result)
}

func resultHandleFunc(w http.ResponseWriter, req *http.Request) {
	if DebugModeSaidDeleteHandler {
		log.Println("resultHandleFunc")
		defer log.Println("defer resultHandleFunc")
	}

	switch req.Method {
	default:
		badRequest(w, "invalid Method ("+req.Method+")")
		return
	case http.MethodGet, http.MethodHead:
		// ok
	}

	resultId := req.URL.Path

	result, err := dataManager.GetSearchResult(resultId)
	if err != nil {
		if _, ok := err.(BadRequestError); ok {
			badRequest(w, err.Error())
			return
		}
		log.Println(err)
		internalServerError(w, err.Error())
		return
	}

	writeJson(w, req, result)
}

type SearchResult struct {
	Id       string      `json:"id"`
	Query    string      `json:"query"`
	Progress int         `json:"progress"`
	Count    int         `json:"count"`
	Said     []*SaidView `json:"said"`
	Error    string      `json:"error"`
}

func SearchSaid(host, resultId, query string, dayIdList []string) {
	if DebugModeSaidDeleteHandler {
		log.Println("SearchSaid", "id:", resultId, "query:", "[[", query, "]]")
		defer log.Println("defer SearchSaid")
	}

	hostBaseUrl := "http://" + host + "/"

	caseMatch := false
	sinceToken := ""
	untilToken := ""
	resultLimit := 200
	spaceToken := ""
	unquote := false
	attachedImage := false
	notAttachedImage := false
	deletedSaid := false
	quotedSaid := false
	notQuotedSaid := false
	repliedRoot := false
	notRepliedRoot := false
	leafSaid := false
	notLeafSaid := false
	reply := false
	notReply := false
	history := false
	update := false
	edited := false
	notEdited := false
	oldestOrder := false
	weekFilter := (1 << 8) - 1
	hourFilter := (1 << 24) - 1
	secretSaid := false
	notSecretdSaid := false

	tokens := strings.Fields(query)
	keywords := make([]string, 0, len(tokens))
	excludeKeywords := []string{}
	for _, tk := range tokens {
		tc := strings.ToLower(tk)

		if strings.HasPrefix(tc, ":case:") {
			caseMatch = !strings.HasPrefix(tc, ":case:off")

		} else if strings.HasPrefix(tc, ":since:") {
			sinceToken = strings.TrimPrefix(tc, ":since:")

		} else if strings.HasPrefix(tc, ":until:") {
			untilToken = strings.TrimPrefix(tc, ":until:")

		} else if strings.HasPrefix(tc, ":limit:") {
			tt := strings.TrimPrefix(tc, ":limit:")
			fmt.Sscan(tt, &resultLimit)
			if resultLimit <= 0 {
				resultLimit = 200
			}

		} else if strings.HasPrefix(tc, ":space:") {
			spaceToken = strings.TrimPrefix(tc, ":space:")

		} else if strings.HasPrefix(tc, ":unquote:") {
			unquote = !strings.HasPrefix(tc, ":unquote:off")

		} else if strings.HasPrefix(tc, ":image:") {
			attachedImage = !strings.HasPrefix(tc, ":image:off")
			notAttachedImage = strings.HasPrefix(tc, ":image:not")

		} else if strings.HasPrefix(tc, ":deleted:") {
			deletedSaid = !strings.HasPrefix(tc, ":deleted:off")

		} else if strings.HasPrefix(tc, ":quoted:") {
			quotedSaid = !strings.HasPrefix(tc, ":quoted:off")
			notQuotedSaid = strings.HasPrefix(tc, ":quoted:not")

		} else if strings.HasPrefix(tc, ":root:") {
			repliedRoot = !strings.HasPrefix(tc, ":root:off")
			notRepliedRoot = strings.HasPrefix(tc, ":root:not")

		} else if strings.HasPrefix(tc, ":leaf:") {
			leafSaid = !strings.HasPrefix(tc, ":leaf:off")
			notLeafSaid = strings.HasPrefix(tc, ":leaf:not")

		} else if strings.HasPrefix(tc, ":reply:") {
			reply = !strings.HasPrefix(tc, ":reply:off")
			notReply = strings.HasPrefix(tc, ":reply:not")

		} else if strings.HasPrefix(tc, ":history:") {
			history = !strings.HasPrefix(tc, ":history:off")

		} else if strings.HasPrefix(tc, ":update:") {
			update = !strings.HasPrefix(tc, ":update:off")

		} else if strings.HasPrefix(tc, ":edited:") {
			edited = !strings.HasPrefix(tc, ":edited:off")
			notEdited = strings.HasPrefix(tc, ":edited:not")

		} else if strings.HasPrefix(tc, ":order:") {
			oldestOrder = strings.HasPrefix(tc, ":order:old")

		} else if strings.HasPrefix(tc, ":week:") {
			weekFilter = 0
			for _, w := range []rune(strings.TrimPrefix(tc, ":week:")) {
				if wn, err := strconv.Atoi(string(w)); err == nil {
					weekFilter |= 1 << wn
				}
			}

		} else if strings.HasPrefix(tc, ":hour:") {
			hourFilter = 0
			for _, token := range strings.Split(strings.TrimPrefix(tc, ":hour:"), ",") {
				hs := strings.Split(token, "-")
				sh, eh := 0, 23
				if len(hs) == 1 {
					if v, err := strconv.Atoi(hs[0]); err == nil && 0 <= v && v < 24 {
						sh = v
						eh = v
					}
				} else {
					if v, err := strconv.Atoi(hs[0]); err == nil && 0 <= v && v < 24 {
						sh = v
					}
					if v, err := strconv.Atoi(hs[1]); err == nil && 0 <= v && v < 24 {
						eh = v
						if sh > eh {
							eh += 24
						}
					}
				}
				for h := sh; h <= eh; h++ {
					hourFilter |= 1 << (h % 24)
				}
			}

		} else if strings.HasPrefix(tc, ":secret:") {
			secretSaid = !strings.HasPrefix(tc, ":secret:off")
			notSecretdSaid = strings.HasPrefix(tc, ":secret:not")

		} else if strings.HasPrefix(tc, ":not:") {
			nk := tk[5:]
			if nk != "" {
				if strings.HasPrefix(nk, hostBaseUrl) {
					nk = DummyBaseUrl +
						strings.TrimPrefix(nk, hostBaseUrl)
				}
				excludeKeywords = append(excludeKeywords, nk)
			}

		} else {
			if strings.HasPrefix(tk, hostBaseUrl) {
				tk = DummyBaseUrl + strings.TrimPrefix(tk, hostBaseUrl)
			}
			keywords = append(keywords, tk)
		}
	}

	if !caseMatch {
		for i, k := range excludeKeywords {
			excludeKeywords[i] = strings.ToLower(k)
		}
		for i, k := range keywords {
			keywords[i] = strings.ToLower(k)
		}
	}

	if spaceToken != "" {
		for i, k := range excludeKeywords {
			excludeKeywords[i] = strings.ReplaceAll(k, spaceToken, " ")
		}
		for i, k := range keywords {
			keywords[i] = strings.ReplaceAll(k, spaceToken, " ")
		}
	}

	if unquote {
		for i, k := range excludeKeywords {
			if strings.HasPrefix(k, `"`) &&
				strings.HasSuffix(k, `"`) {
				excludeKeywords[i] = strings.TrimPrefix(
					strings.TrimSuffix(k, `"`),
					`"`,
				)
			}
		}
		for i, k := range keywords {
			if strings.HasPrefix(k, `"`) &&
				strings.HasSuffix(k, `"`) {
				keywords[i] = strings.TrimPrefix(
					strings.TrimSuffix(k, `"`),
					`"`,
				)
			}
		}
	}

	if !update && sinceToken != "" {
		if ok, err := regexp.MatchString(`^\d{4}-\d{2}-\d{2}$`, sinceToken); ok {
			did := sinceToken[2:4] + sinceToken[5:7] + sinceToken[8:10]
			p := sort.Search(len(dayIdList), func(i int) bool {
				return strings.Compare(dayIdList[i], did) >= 0
			})
			dayIdList = dayIdList[p:]
		} else if err != nil {
			log.Panic(err)
		}
	}

	if !update && untilToken != "" {
		if ok, err := regexp.MatchString(`^\d{4}-\d{2}-\d{2}$`, untilToken); ok {
			did := untilToken[2:4] + untilToken[5:7] + untilToken[8:10]
			p := sort.Search(len(dayIdList), func(i int) bool {
				return strings.Compare(dayIdList[i], did) >= 0
			})
			if p < len(dayIdList) {
				p++
			}
			dayIdList = dayIdList[:p]
		} else if err != nil {
			// unreachable
			log.Panic(err)
		}
	}

	saidList := []*SaidView{}

	for i := range dayIdList {
		result := &SearchResult{
			Id:       resultId,
			Query:    query,
			Progress: i + 1,
			Count:    len(dayIdList),
			Said:     saidList,
		}

		index := i
		if !oldestOrder {
			index = len(dayIdList) - 1 - i
		}
		dayId := dayIdList[index]

		list, err := dataManager.GetListCopyWithoutCaching(dayId)
		if err != nil {
			log.Println(
				"Error", err,
				"in SearchSaid",
				"id:", resultId,
				"query:", "[[", query, "]]",
			)
			result.Progress = -result.Progress
			result.Error = err.Error()
			dataManager.SetSearchResult(result)
			return
		}

	listLoop:
		for e := range list.Said {
			sIndex := e
			if !oldestOrder {
				sIndex = len(list.Said) - 1 - e
			}
			said := list.Said[sIndex]

			if notSecretdSaid {
				if said.Secret {
					continue
				}
			} else if secretSaid {
				if !said.Secret {
					continue
				}
			}

			dt, err := FromDateString(said.Date)
			if err != nil {
				// unreachable
				log.Panic(err)
			}
			week := 1 << int(dt.Weekday())
			if (weekFilter & week) == 0 {
				continue
			}

			hour := 1 << dt.Hour()
			if (hourFilter & hour) == 0 {
				continue
			}

			if notRepliedRoot {
				if said.Root == "" && len(said.Children) != 0 {
					continue
				}
			} else if repliedRoot {
				if said.Root != "" || len(said.Children) == 0 {
					continue
				}
			}

			if notLeafSaid {
				if said.Parent != "" && len(said.Children) == 0 {
					continue
				}
			} else if leafSaid {
				if said.Parent == "" || len(said.Children) != 0 {
					continue
				}
			}

			if notReply {
				if said.Parent != "" {
					continue
				}
			} else if reply {
				if said.Parent == "" {
					continue
				}
			}

			if notQuotedSaid {
				if len(said.QuotedRefs) != 0 {
					continue
				}
			} else if quotedSaid {
				if len(said.QuotedRefs) == 0 {
					continue
				}
			}

			if notEdited {
				if said.Update != "" {
					continue
				}
			} else if edited {
				if said.Update == "" {
					continue
				}
			}

			if !history && update {
				if said.Update == "" {
					continue
				}
				if sinceToken != "" {
					if strings.Compare(said.Update[:10], sinceToken) < 0 {
						continue
					}
				}
				if untilToken != "" {
					if strings.Compare(said.Update[:10], untilToken) > 0 {
						continue
					}
				}
			}

			if history {
				if len(said.History) == 0 {
					continue
				}
				found := false
			historyLoop:
				for _, h := range said.History {
					if update {
						if sinceToken != "" {
							if strings.Compare(h.Date[:10], sinceToken) < 0 {
								continue
							}
						}
						if untilToken != "" {
							if strings.Compare(h.Date[:10], untilToken) > 0 {
								continue
							}
						}
					}

					if deletedSaid {
						if h.Delete {
							found = true
							break
						}
					} else {
						if notAttachedImage {
							if len(h.ImageList) != 0 {
								continue
							}
						} else if attachedImage {
							if len(h.ImageList) == 0 {
								continue
							}
						}
						text := h.Text
						if !caseMatch {
							text = strings.ToLower(text)
						}
						for _, nk := range excludeKeywords {
							if strings.Contains(text, nk) {
								continue historyLoop
							}
						}
						for _, k := range keywords {
							if !strings.Contains(text, k) {
								continue historyLoop
							}
						}
						found = true
						break
					}
				}
				if !found {
					continue
				}
			} else if deletedSaid {
				if !said.Deleted {
					continue
				}
			} else {
				if notAttachedImage {
					if len(said.ImageList) != 0 {
						continue
					}
				} else if attachedImage {
					if len(said.ImageList) == 0 {
						continue
					}
				}

				text := said.Text
				if !caseMatch {
					text = strings.ToLower(text)
				}
				for _, nk := range excludeKeywords {
					if strings.Contains(text, nk) {
						continue listLoop
					}
				}
				for _, k := range keywords {
					if !strings.Contains(text, k) {
						continue listLoop
					}
				}
			}
			saidList = append(saidList, said.View())
			if len(saidList) >= resultLimit {
				break
			}
		}

		if len(saidList) >= resultLimit {
			result.Progress = result.Count
		}

		result.Said = saidList
		dataManager.SetSearchResult(result)

		if len(saidList) >= resultLimit {
			break
		}
	}
}
