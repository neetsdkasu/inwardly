package main

import (
	"bytes"
	"encoding/json"
	"image"
	"image/color"
	_ "image/gif"
	_ "image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const (
	AppDirName       = ".inwardly"
	SaidDirName      = "said"
	ImageDirName     = "img"
	ThumbnailDirName = "thumb"
	SecretDirName    = "secret"
)

const (
	AppStateFileName            = "appstate.json"
	AppSettingFileName          = "appsetting.json"
	DaySaidListStatusFileName   = "status.json"
	MemoListFileName            = "memo.json"
	MemoHistoryFileName         = "memohistory.json"
	FotoListFileName            = "fotolist.json"
	ThreadUploadSettingFileName = "threadsetting.json"
	DocumentLinkSettingFileName = "doclinksetting.json"
	DocumentLinkStateFileName   = "doclinkstate.json"
	SecretInfoFileName          = "secretinfo.json"
)

type FileManager struct {
	baseDir            string
	saidDir            string
	imageDir           string
	thumbnailDir       string
	secretImageDir     string
	secretThumbnailDir string

	appStateFile            string
	appSettingFile          string
	daySaidListStatusFile   string
	memoListFile            string
	memoHistoryFile         string
	fotoListFile            string
	threadUploadSettingFile string
	documentLinkSettingFile string
	documentLinkStateFile   string
	secretInfoFile          string

	fac FileAccessController
}

var fileManager FileManager

func (*FileManager) SplitBaseFile(p string) (string, string, error) {
	p = filepath.Clean(p)
	s, err := os.Stat(p)
	if err != nil {
		return "", "", err
	}
	if s.IsDir() {
		return p, "", nil
	}
	dir, base := filepath.Split(p)
	return dir, base, nil
}

func (*FileManager) ExistsFile(elem ...string) (bool, error) {
	_, err := os.Stat(filepath.Join(elem...))
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func (this *FileManager) ExistsImg(filename string) (bool, error) {
	_, err := os.Stat(filepath.Join(this.imageDir, filename))
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func (this *FileManager) GetImgFilenameByBasename(basename string) (string, error) {
	ms, err := filepath.Glob(filepath.Join(this.imageDir, basename+".*"))
	if err != nil {
		return "", err
	}
	if len(ms) != 1 {
		return "", nil
	}
	filename := filepath.Base(ms[0])
	return filename, nil
}

func (this *FileManager) Save(file string, blob []byte) chan *SaveResult {
	return this.fac.Save(file, blob)
}

func (this *FileManager) Load(file string) chan *LoadResult {
	return this.fac.Load(file)
}

func (this *FileManager) Delete(file string) chan *DeleteResult {
	return this.fac.Delete(file)
}

func (this *FileManager) GetDaySaidListFilePath(id string) string {
	return filepath.Join(this.saidDir, id+".json")
}

func (this *FileManager) Init() error {
	if DebugModeShowInit || DebugModeFileManager {
		log.Println("FileManager.Init")
	}
	this.fac.Init()
	homedir := homeDir.Get()
	baseDir := filepath.Join(homedir, AppDirName)
	saidDir := filepath.Join(baseDir, SaidDirName)
	imageDir := filepath.Join(baseDir, ImageDirName)
	thumbnailDir := filepath.Join(baseDir, ThumbnailDirName)
	secretImageDir := filepath.Join(baseDir, SecretDirName, ImageDirName)
	secretThumbnailDir := filepath.Join(baseDir, SecretDirName, ThumbnailDirName)
	dirList := []string{
		baseDir, saidDir,
		imageDir, thumbnailDir,
		secretImageDir, secretThumbnailDir,
	}
	for _, dir := range dirList {
		err := os.MkdirAll(dir, os.ModeDir)
		if err != nil {
			return err
		}
	}

	this.baseDir = baseDir
	this.saidDir = saidDir
	this.imageDir = imageDir
	this.thumbnailDir = thumbnailDir
	this.secretImageDir = secretImageDir
	this.secretThumbnailDir = secretThumbnailDir

	this.appStateFile = filepath.Join(baseDir, AppStateFileName)
	this.appSettingFile = filepath.Join(baseDir, AppSettingFileName)
	this.daySaidListStatusFile = filepath.Join(saidDir, DaySaidListStatusFileName)
	this.memoListFile = filepath.Join(baseDir, MemoListFileName)
	this.memoHistoryFile = filepath.Join(baseDir, MemoHistoryFileName)
	this.fotoListFile = filepath.Join(baseDir, FotoListFileName)
	this.threadUploadSettingFile = filepath.Join(baseDir, ThreadUploadSettingFileName)
	this.documentLinkSettingFile = filepath.Join(baseDir, DocumentLinkSettingFileName)
	this.documentLinkStateFile = filepath.Join(baseDir, DocumentLinkStateFileName)
	this.secretInfoFile = filepath.Join(baseDir, SecretInfoFileName)
	return nil
}

func (this *FileManager) LoadBinaryFile(file string) ([]byte, error) {
	if DebugModeFileManager {
		log.Println("FileManager.LoadBinaryFile")
		defer log.Println("defer FileManager.LoadBinaryFile")
	}
	loader := this.Load(file)
	result := <-loader
	close(loader)
	blob := result.Blob
	err := result.Error
	return blob, err
}

func (this *FileManager) LoadJsonFile(file string, store interface{}) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadJsonFile")
		defer log.Println("defer FileManager.LoadJsonFile")
	}
	loader := this.Load(file)
	result := <-loader
	close(loader)
	blob := result.Blob
	err := result.Error
	if err != nil {
		if os.IsNotExist(err) {
			if x, ok := store.(interface{ Init() }); ok {
				x.Init()
				return nil
			}
		}
		return err
	}
	return json.Unmarshal(blob, store)
}

func (this *FileManager) LoadImageFile(filename string) ([]byte, error) {
	if DebugModeFileManager {
		log.Println("FileManager.LoadImageFile")
		defer log.Println("defer FileManager.LoadImageFile")
	}
	file := filepath.Join(this.imageDir, filename)
	return this.LoadBinaryFile(file)
}

func (this *FileManager) SaveMemoList() {
	if DebugModeFileManager {
		log.Println("FileManager.SaveMemoList")
		defer log.Println("defer FileManager.SaveMemoList")
	}
	ch := this.fac.DelaySave(this.memoListFile, func() ([]byte, error) {
		list := dataManager.GetMemoList()
		return json.Marshal(list)
	})
	go func() {
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(result.Error)
		}
	}()
}

func (this *FileManager) LoadMemoList(list *MemoList) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadMemoList")
		defer log.Println("defer FileManager.LoadMemoList")
	}
	return this.LoadJsonFile(this.memoListFile, list)
}

func (this *FileManager) SaveMemoHistory() {
	if DebugModeFileManager {
		log.Println("FileManager.SaveMemoHistory")
		defer log.Println("defer FileManager.SaveMemoHistory")
	}
	ch := this.fac.DelaySave(this.memoHistoryFile, func() ([]byte, error) {
		list := dataManager.GetMemoHistory()
		return json.Marshal(list)
	})
	go func() {
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(result.Error)
		}
	}()
}

func (this *FileManager) LoadMemoHistory(list *MemoList) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadMemoHistory")
		defer log.Println("defer FileManager.LoadMemoHistory")
	}
	return this.LoadJsonFile(this.memoHistoryFile, list)
}

func (this *FileManager) SaveAppState() {
	if DebugModeFileManager {
		log.Println("FileManager.SaveAppState")
		defer log.Println("defer FileManager.SaveAppState")
	}
	ch := this.fac.DelaySave(this.appStateFile, func() ([]byte, error) {
		var state AppStateStore
		appState.Store(&state)
		return json.Marshal(&state)
	})
	go func() {
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(result.Error)
		}
	}()
}

func (this *FileManager) LoadAppState(state *AppStateStore) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadAppState")
		defer log.Println("defer FileManager.LoadAppState")
	}
	return this.LoadJsonFile(this.appStateFile, state)
}

func (this *FileManager) SaveAppSetting(setting *AppSettingStore) error {
	if DebugModeFileManager {
		log.Println("FileManager.SaveAppSetting")
		defer log.Println("defer FileManager.SaveAppSetting")
	}
	blob, err := json.Marshal(setting)
	if err != nil {
		return err
	}
	saver := this.Save(this.appSettingFile, blob)
	result := <-saver
	close(saver)
	return result.Error
}

func (this *FileManager) LoadAppSetting(setting *AppSettingStore) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadAppSetting")
		defer log.Println("defer FileManager.LoadAppSetting")
	}
	return this.LoadJsonFile(this.appSettingFile, setting)
}

func (this *FileManager) SaveThreadUploadSetting() {
	if DebugModeFileManager {
		log.Println("FileManager.SaveThreadUploadSetting")
		defer log.Println("defer FileManager.SaveThreadUploadSetting")
	}
	ch := this.fac.DelaySave(this.threadUploadSettingFile, func() ([]byte, error) {
		list := appSetting.GetThreadUploadSettingList()
		return json.Marshal(&list)
	})
	go func() {
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(result.Error)
		}
	}()
}

func (this *FileManager) LoadThreadUploadSetting(setting *ThreadUploadSettingList) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadThreadUploadSetting")
		defer log.Println("defer FileManager.LoadThreadUploadSetting")
	}
	return this.LoadJsonFile(this.threadUploadSettingFile, setting)
}

func (this *FileManager) SaveDocumentLinkSetting() {
	if DebugModeFileManager {
		log.Println("FileManager.SaveDocumentLinkSetting")
		defer log.Println("defer FileManager.SaveDocumentLinkSetting")
	}
	ch := this.fac.DelaySave(this.documentLinkSettingFile, func() ([]byte, error) {
		list := appSetting.GetDocumentLinkSettingList()
		return json.Marshal(&list)
	})
	go func() {
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(result.Error)
		}
	}()
}

func (this *FileManager) LoadDocumentLinkSetting(setting *DocumentLinkSettingList) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadDocumentLinkSetting")
		defer log.Println("defer FileManager.LoadDocumentLinkSetting")
	}
	return this.LoadJsonFile(this.documentLinkSettingFile, setting)
}

func (this *FileManager) SaveDocumentLinkState() {
	if DebugModeFileManager {
		log.Println("FileManager.SaveDocumentLinkState")
		defer log.Println("defer FileManager.SaveDocumentLinkState")
	}
	ch := this.fac.DelaySave(this.documentLinkStateFile, func() ([]byte, error) {
		list := appState.GetDocumentLinkStateList()
		return json.Marshal(&list)
	})
	go func() {
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(result.Error)
		}
	}()
}

func (this *FileManager) LoadDocumentLinkState(setting *DocumentLinkStateList) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadDocumentLinkState")
		defer log.Println("defer FileManager.LoadDocumentLinkState")
	}
	return this.LoadJsonFile(this.documentLinkStateFile, setting)
}

func (this *FileManager) SaveSecretInfo() {
	if DebugModeFileManager {
		log.Println("FileManager.SaveSecretInfo")
		defer log.Println("defer FileManager.SaveSecretInfo")
	}
	ch := this.fac.DelaySave(this.secretInfoFile, func() ([]byte, error) {
		list := appState.GetSecretInfoList()
		return json.Marshal(&list)
	})
	go func() {
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(result.Error)
		}
	}()
}

func (this *FileManager) LoadSecretInfo(list *SecretInfoStoreList) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadSecretInfo")
		defer log.Println("defer FileManager.LoadSecretInfo")
	}
	return this.LoadJsonFile(this.secretInfoFile, list)
}

func (this *FileManager) SaveDaySaidListStatus(status interface{}) {
	if DebugModeFileManager {
		log.Println("FileManager.SaveDaySaidListStatus")
		defer log.Println("defer FileManager.SaveDaySaidListStatus")
	}
	ch := this.fac.DelaySave(this.daySaidListStatusFile, func() ([]byte, error) {
		return json.Marshal(status)
	})
	go func() {
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(result.Error)
		}
	}()
}

func (this *FileManager) LoadDaySaidListStatus(status interface{}) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadDaySaidListStatus")
		defer log.Println("defer FileManager.LoadDaySaidListStatus")
	}
	err := this.LoadJsonFile(this.daySaidListStatusFile, status)
	if err != nil && os.IsNotExist(err) {
		return nil
	}
	return err
}

func (this *FileManager) SaveDaySaidList(id string, list *DaySaidList) error {
	if DebugModeFileManager {
		log.Println("FileManager.SaveDaySaidList")
		defer log.Println("defer FileManager.SaveDaySaidList")
	}
	file := this.GetDaySaidListFilePath(id)
	blob, err := json.Marshal(list)
	if err != nil {
		return err
	}
	saver := this.Save(file, blob)
	result := <-saver
	close(saver)
	return result.Error
}

func (this *FileManager) LoadDaySaidList(id string, list *DaySaidList) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadDaySaidList")
		defer log.Println("defer FileManager.LoadDaySaidList")
	}
	file := this.GetDaySaidListFilePath(id)
	loader := this.Load(file)
	result := <-loader
	close(loader)
	blob := result.Blob
	err := result.Error
	if err != nil {
		if os.IsNotExist(err) {
			list.Init()
			return nil
		}
		return err
	}
	return json.Unmarshal(blob, list)
}

func (this *FileManager) LoadFotoStore(store *FotoStore) error {
	if DebugModeFileManager {
		log.Println("FileManager.LoadFotStore")
		defer log.Println("defer FileManager.LoadFotoStore")
	}
	return this.LoadJsonFile(this.fotoListFile, store)
}

func (this *FileManager) SaveFotoStore(store *FotoStore) error {
	if DebugModeFileManager {
		log.Println("FileManager.SaveFotoStore")
		defer log.Println("defer FileManager.SaveFotoStore")
	}
	blob, err := json.Marshal(store)
	if err != nil {
		return err
	}
	saver := this.Save(this.fotoListFile, blob)
	result := <-saver
	close(saver)
	return result.Error
}

func (this *FileManager) SaveImages(imageList []*SayImageFile) {
	if DebugModeFileManager {
		log.Println("FileManager.SaveImages")
		defer log.Println("defer FileManager.SaveImages")
	}
	for i := range imageList {
		img := imageList[i]
		if img.File == nil {
			continue
		}
		go func() {
			blob, err := ioutil.ReadAll(img.File)
			img.File.Close()
			if err != nil {
				log.Println(img.FileName, err)
				return
			}
			file := filepath.Join(this.imageDir, img.FileName)
			ch := this.Save(file, blob)
			result := <-ch
			close(ch)
			if result.Error != nil {
				log.Println(file, result.Error)
				return
			}
			err = this.makeThumbnail(blob, img.FileName)
			if err != nil {
				log.Println(err)
			}
		}()
	}
}

func (this *FileManager) saveThumbnail(blob []byte, name string) error {
	filename := filepath.Join(this.thumbnailDir, name)
	ch := this.Save(filename, blob)
	result := <-ch
	close(ch)
	return result.Error
}

func (this *FileManager) makeThumbnail(blob []byte, name string) error {
	name = strings.SplitN(name, ".", 2)[0] + ".png"
	orig, ft, err := image.Decode(bytes.NewReader(blob))
	if err != nil {
		return err
	}
	enc := png.Encoder{CompressionLevel: png.BestCompression}
	origBounds := orig.Bounds()
	if origBounds.Dx() <= 100 {
		if ft == "png" {
			return this.saveThumbnail(blob, name)
		}
		var b bytes.Buffer
		err = enc.Encode(&b, orig)
		if err != nil {
			return err
		}
		return this.saveThumbnail(b.Bytes(), name)
	}
	height := origBounds.Dy() * 100 / origBounds.Dx()
	if height == 0 {
		height = 1
	}
	rect := image.Rect(0, 0, 100, height)
	thumb := image.NewNRGBA64(rect)
	rect = thumb.Bounds()
	model := thumb.ColorModel()
	size := rect.Dx() * rect.Dy()
	cnt := make([]int, size)
	Rs := make([]int, size)
	Gs := make([]int, size)
	Bs := make([]int, size)
	As := make([]int, size)
	for y := origBounds.Min.Y; y < origBounds.Max.Y; y++ {
		yy := (y * rect.Dy() / origBounds.Dy()) * rect.Dx()
		for x := origBounds.Min.X; x < origBounds.Max.X; x++ {
			c := model.Convert(orig.At(x, y)).(color.NRGBA64)
			p := yy + x*rect.Dx()/origBounds.Dx()
			cnt[p]++
			Rs[p] += int(c.R)
			Gs[p] += int(c.G)
			Bs[p] += int(c.B)
			As[p] += int(c.A)
		}
	}

	for p, y := 0, rect.Min.Y; y < rect.Max.Y; y++ {
		for x := rect.Min.X; x < rect.Max.X; x++ {
			c := color.NRGBA64{
				uint16(Rs[p] / cnt[p]),
				uint16(Gs[p] / cnt[p]),
				uint16(Bs[p] / cnt[p]),
				uint16(As[p] / cnt[p]),
			}
			thumb.SetNRGBA64(x, y, c)
			p++
		}
	}

	var buf bytes.Buffer
	err = enc.Encode(&buf, thumb)
	if err != nil {
		return err
	}
	return this.saveThumbnail(buf.Bytes(), name)
}

func (this *FileManager) CleanCache(expire time.Time) {
	this.fac.CleanCache(expire)
}

func (this *FileManager) LoadSecretImage(id, name string) ([]byte, error) {
	if DebugModeFileManager {
		log.Println("FileManager.LoadSecretImage")
		defer log.Println("defer FileManager.LoadSecretImage")
	}
	file := filepath.Join(this.secretImageDir, id, name)
	blob, err := this.LoadBinaryFile(file)
	if err != nil {
		return nil, err
	}
	cryptor, err := appState.GetSecretCryptor(id)
	if err != nil {
		return nil, err
	}
	return cryptor.Decrypt(blob)
}

func (this *FileManager) LoadSecretThumbnail(id, name string) ([]byte, error) {
	if DebugModeFileManager {
		log.Println("FileManager.LoadSecretThumbnail")
		defer log.Println("defer FileManager.LoadSecretThumbnail")
	}
	file := filepath.Join(this.secretThumbnailDir, id, name)
	blob, err := this.LoadBinaryFile(file)
	if err != nil {
		return nil, err
	}
	cryptor, err := appState.GetSecretCryptor(id)
	if err != nil {
		return nil, err
	}
	return cryptor.Decrypt(blob)
}

func (this *FileManager) SaveSecretImage(file *SecretSaveImageFile) {
	if DebugModeFileManager {
		log.Println("FileManager.SaveSecretImage")
		defer log.Println("defer FileManager.SaveSecretImage")
	}
	go func() {
		image, err := ioutil.ReadAll(file.File)
		file.File.Close()
		if err != nil {
			log.Println(file.SecretId, file.Name, err)
			return
		}
		cryptor, err := appState.GetSecretCryptor(file.SecretId)
		if err != nil {
			log.Println(file.SecretId, file.Name, err)
			return
		}
		blob := cryptor.Encrypt(image)
		dir := filepath.Join(this.secretImageDir, file.SecretId)
		err = os.MkdirAll(dir, os.ModeDir)
		if err != nil {
			log.Println(file.SecretId, file.Name, err)
		}
		fp := filepath.Join(dir, file.Name)
		ch := this.Save(fp, blob)
		result := <-ch
		close(ch)
		if result.Error != nil {
			log.Println(file.SecretId, file.Name, result.Error)
			return
		}
		size, thumb, err := this.makeSecretThumbnail(image, file.SecretId, file.Name)
		if err != nil {
			log.Println(err)
			return
		}
		appState.ReadySecretImage(file.SecretId, file.Name, size, thumb)
		appState.Save()
	}()
}

func (this *FileManager) saveSecretThumbnail(image []byte, id, name string) error {
	cryptor, err := appState.GetSecretCryptor(id)
	if err != nil {
		return err
	}
	blob := cryptor.Encrypt(image)
	dir := filepath.Join(this.secretThumbnailDir, id)
	err = os.MkdirAll(dir, os.ModeDir)
	if err != nil {
		return err
	}
	filename := filepath.Join(dir, name)
	ch := this.Save(filename, blob)
	result := <-ch
	close(ch)
	return result.Error
}

func (this *FileManager) makeSecretThumbnail(blob []byte, id, name string) (*SecretImageSizeInfo, *SecretImageSizeInfo, error) {
	orig, ft, err := image.Decode(bytes.NewReader(blob))
	if err != nil {
		return nil, nil, err
	}
	enc := png.Encoder{CompressionLevel: png.BestCompression}
	origBounds := orig.Bounds()
	if origBounds.Dx() <= 100 {
		if ft == "png" {
			err = this.saveSecretThumbnail(blob, id, name)
			if err != nil {
				return nil, nil, err
			}
			return &SecretImageSizeInfo{
					Width:  origBounds.Dx(),
					Height: origBounds.Dy(),
					Length: int64(len(blob)),
				}, &SecretImageSizeInfo{
					Width:  origBounds.Dx(),
					Height: origBounds.Dy(),
					Length: int64(len(blob)),
				}, nil
		}
		var b bytes.Buffer
		err = enc.Encode(&b, orig)
		if err != nil {
			return nil, nil, err
		}
		bBlob := b.Bytes()
		err = this.saveSecretThumbnail(bBlob, id, name)
		if err != nil {
			return nil, nil, err
		}
		return &SecretImageSizeInfo{
				Width:  origBounds.Dx(),
				Height: origBounds.Dy(),
				Length: int64(len(blob)),
			}, &SecretImageSizeInfo{
				Width:  origBounds.Dx(),
				Height: origBounds.Dy(),
				Length: int64(len(bBlob)),
			}, nil
	}
	height := origBounds.Dy() * 100 / origBounds.Dx()
	if height == 0 {
		height = 1
	}
	rect := image.Rect(0, 0, 100, height)
	thumb := image.NewNRGBA64(rect)
	rect = thumb.Bounds()
	model := thumb.ColorModel()
	size := rect.Dx() * rect.Dy()
	cnt := make([]int, size)
	Rs := make([]int, size)
	Gs := make([]int, size)
	Bs := make([]int, size)
	As := make([]int, size)
	for y := origBounds.Min.Y; y < origBounds.Max.Y; y++ {
		yy := (y * rect.Dy() / origBounds.Dy()) * rect.Dx()
		for x := origBounds.Min.X; x < origBounds.Max.X; x++ {
			c := model.Convert(orig.At(x, y)).(color.NRGBA64)
			p := yy + x*rect.Dx()/origBounds.Dx()
			cnt[p]++
			Rs[p] += int(c.R)
			Gs[p] += int(c.G)
			Bs[p] += int(c.B)
			As[p] += int(c.A)
		}
	}

	for p, y := 0, rect.Min.Y; y < rect.Max.Y; y++ {
		for x := rect.Min.X; x < rect.Max.X; x++ {
			c := color.NRGBA64{
				uint16(Rs[p] / cnt[p]),
				uint16(Gs[p] / cnt[p]),
				uint16(Bs[p] / cnt[p]),
				uint16(As[p] / cnt[p]),
			}
			thumb.SetNRGBA64(x, y, c)
			p++
		}
	}

	var buf bytes.Buffer
	err = enc.Encode(&buf, thumb)
	if err != nil {
		return nil, nil, err
	}
	thumbBlob := buf.Bytes()
	err = this.saveSecretThumbnail(thumbBlob, id, name)
	if err != nil {
		return nil, nil, err
	}
	return &SecretImageSizeInfo{
			Width:  origBounds.Dx(),
			Height: origBounds.Dy(),
			Length: int64(len(blob)),
		}, &SecretImageSizeInfo{
			Width:  rect.Dx(),
			Height: rect.Dy(),
			Length: int64(len(thumbBlob)),
		}, nil
}

func (this *FileManager) DeleteSecretImage(id, name string) {
	image := filepath.Join(this.secretImageDir, id, name)
	thumb := filepath.Join(this.secretThumbnailDir, id, name)
	ch1 := this.Delete(image)
	ch2 := this.Delete(thumb)
	go func() {
		res1 := <-ch1
		close(ch1)
		if res1.Error != nil {
			log.Println(id, name, res1.Error)
		}
	}()
	go func() {
		res2 := <-ch2
		close(ch2)
		if res2.Error != nil {
			log.Println(id, name, res2.Error)
		}
	}()
}
