package main

import (
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"
)

type Blob []byte

type LoadResult struct {
	Blob  []byte
	Error error
}

type DeleteResult struct {
	Error error
}

type SaveResult struct {
	Error error
}

type SaveArgument struct {
	Blob     Blob
	ResultCh chan<- *SaveResult
}

type SaveDelayArgument struct {
	BlobGetter func() ([]byte, error)
	ResultCh   chan<- *SaveResult
}

type FileAccess struct {
	File   string
	Access interface{}
}

type FileAccessController struct {
	Queue      []*FileAccess
	FileLocker map[string]*Counter
	Alive      bool
	sync.Mutex
}

func (this *FileAccessController) Init() {
	if DebugModeShowInit || DebugModeFileAccessController {
		log.Println("FileAccessController.Init")
	}
	this.Queue = []*FileAccess{}
	this.FileLocker = make(map[string]*Counter)
	this.Alive = false
}

func (this *FileAccessController) getFrontFileAccess() (*FileAccess, sync.Locker) {
	if DebugModeFileAccessController {
		log.Println("FileAccessController.getFrontFileAccess")
		defer log.Println("defer FileAccessController.getFrontFileAccess")
	}
	this.Lock()
	defer this.Unlock()
	size := len(this.Queue)
	if size == 0 {
		this.Alive = false
		return nil, nil
	}
	fa := this.Queue[0]
	for i, x := range this.Queue[1:] {
		this.Queue[i] = x
	}
	this.Queue[size-1] = nil
	this.Queue = this.Queue[:size-1]
	counter, ok := this.FileLocker[fa.File]
	if !ok {
		counter = NewCounter(&sync.Mutex{})
		this.FileLocker[fa.File] = counter
	}
	return fa, counter.GetItem().(sync.Locker)
}

func (this *FileAccessController) releaseFileAccess(fa *FileAccess) {
	if DebugModeFileAccessController {
		log.Println("FileAccessController.releaseFileAccess")
		defer log.Println("defer FileAccessController.releaseFileAccess")
	}
	this.Lock()
	defer this.Unlock()
	counter := this.FileLocker[fa.File]
	counter.Release()
}

func accsessFile(fa *FileAccess, locker sync.Locker) bool {
	if DebugModeFileAccessController {
		log.Println("accsessFile")
		defer log.Println("defer accsessFile")
	}
	if fa == nil {
		return false
	}
	locker.Lock()
	defer locker.Unlock()
	switch acc := fa.Access.(type) {
	case SaveArgument:
		// save
		if DebugModeFileAccessController {
			log.Println("SAVE:", fa.File)
		}
		err := ioutil.WriteFile(fa.File, acc.Blob, 0666)
		acc.ResultCh <- &SaveResult{err}
	case SaveDelayArgument:
		// delay save
		if DebugModeFileAccessController {
			log.Println("SAVE(DELAY):", fa.File)
		}
		blob, err := acc.BlobGetter()
		if err == nil {
			err = ioutil.WriteFile(fa.File, blob, 0666)
		}
		acc.ResultCh <- &SaveResult{err}
	case chan *LoadResult:
		// load
		if DebugModeFileAccessController {
			log.Println("LOAD:", fa.File)
		}
		blob, err := ioutil.ReadFile(fa.File)
		acc <- &LoadResult{blob, err}
	case chan *DeleteResult:
		// delete
		if DebugModeFileAccessController {
			log.Println("DELETE:", fa.File)
		}
		err := os.Remove(fa.File)
		acc <- &DeleteResult{err}
	default:
		log.Panic("BUG in accsessFile")
	}
	return true
}

func (this *FileAccessController) Loop() {
	if DebugModeFileAccessController {
		log.Println("FileAccessController.Loop")
		defer log.Println("defer FileAccessController.Loop")
	}
	for {
		fa, locker := this.getFrontFileAccess()
		if fa == nil {
			return
		}
		accsessFile(fa, locker)
		this.releaseFileAccess(fa)
	}
}

func (this *FileAccessController) Add(fa *FileAccess) {
	if DebugModeFileAccessController {
		log.Println("FileAccessController.Add")
		defer log.Println("defer FileAccessController.Add")
	}
	this.Lock()
	defer this.Unlock()
	if !this.Alive {
		this.Alive = true
		go this.Loop()
	}
	this.Queue = append(this.Queue, fa)
}

func (this *FileAccessController) Save(file string, blob []byte) chan *SaveResult {
	if DebugModeFileAccessController {
		log.Println("FileAccessController.Save")
		defer log.Println("defer FileAccessController.Save")
	}
	ch := make(chan *SaveResult, 1)
	this.Add(&FileAccess{
		File: file,
		Access: SaveArgument{
			Blob:     Blob(blob),
			ResultCh: ch,
		},
	})
	return ch
}

func (this *FileAccessController) DelaySave(file string, getter func() ([]byte, error)) chan *SaveResult {
	if DebugModeFileAccessController {
		log.Println("FileAccessController.DelaySave")
		defer log.Println("defer FileAccessController.DelaySave")
	}
	ch := make(chan *SaveResult, 1)
	this.Add(&FileAccess{
		File: file,
		Access: SaveDelayArgument{
			BlobGetter: getter,
			ResultCh:   ch,
		},
	})
	return ch
}

func (this *FileAccessController) Load(file string) chan *LoadResult {
	if DebugModeFileAccessController {
		log.Println("FileAccessController.Load")
		defer log.Println("defer FileAccessController.Load")
	}
	ch := make(chan *LoadResult, 1)
	this.Add(&FileAccess{
		File:   file,
		Access: ch,
	})
	return ch
}

func (this *FileAccessController) Delete(file string) chan *DeleteResult {
	if DebugModeFileAccessController {
		log.Println("FileAccessController.Delete")
		defer log.Println("defer FileAccessController.Delete")
	}
	ch := make(chan *DeleteResult, 1)
	this.Add(&FileAccess{
		File:   file,
		Access: ch,
	})
	return ch
}

func (this *FileAccessController) CleanCache(expire time.Time) {
	if DebugModeFileAccessController || DebugModeCleanCache {
		log.Println("FileAccessController.CleanCache")
		defer log.Println("defer FileAccessController.CleanCache")
	}
	files := func() []string {
		this.Lock()
		defer this.Unlock()
		files := make([]string, 0, len(this.FileLocker))
		for fn := range this.FileLocker {
			files = append(files, fn)
		}
		return files
	}()
	for _, fn := range files {
		func(fn string) {
			this.Lock()
			defer this.Unlock()
			counter := this.FileLocker[fn]
			if counter.Count > 0 {
				return
			}
			if expire.After(counter.LastAccess) {
				delete(this.FileLocker, fn)
				if DebugModeCleanCache {
					log.Println("Clean Cache: FileLocker", fn)
				}
			}
		}(fn)
	}
}
