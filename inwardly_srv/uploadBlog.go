package main

import (
	"bytes"
	"html/template"
	"log"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
)

const (
	// for Debug
	TimelineBlogTimeValueForDebug = "T00:00:02+09:00"
	TimelineBlogPathEndForDebug   = "000002"

	// for Release
	TimelineBlogTimeValue = "T00:00:01+09:00"
	TimelineBlogPathEnd   = "000001"
)

type BlogSaid struct {
	Id                  string
	DateTimeValue       string
	Date                string
	UpdateDateTimeValue string
	UpdateDate          string
	Link                string
	Deleted             bool
	TextElements        []TextElement
	Images              []string
	Quoted              []*BlogSaid
	Replies             []*BlogSaid
	Omitted             bool
	OmittedTitle        string
	Secret              bool
	Tweets              []string
}

type DayIdRange struct {
	Since, Until string
}

type DayIdRangeList []*DayIdRange

type BlogPostTemplate struct {
	once sync.Once
	*template.Template
}

var blogPostTemplate BlogPostTemplate

func sleepAfterUpload() {
	time.Sleep(5 * time.Second)
}

func routineUploadBlog() error {
	if DebugModeRoutine {
		log.Println("routineUploadBlog")
		defer log.Println("defer routineUploadBlog")
	}

	today := time.Now().Local()
	todayDateTimeValue := ToDateString(today)

	categories := []string{"inwardly"}
	if DebugMode {
		categories = append(categories, "testing")
	}

	setting := appSetting.GetStoreCopy()
	if !setting.AutoBlogUpload() {
		return nil
	}

	deletePost := func(i int, status *DaySaidListStatus) (bool, error) {
		if status.BlogPostId == "" {
			return false, nil
		}
		if DebugMode {
			log.Println("delete post", status.Id)
		}
		err := setting.DeleteBlog(status.BlogPostId)
		if err != nil {
			return false, err
		}
		sleepAfterUpload()
		err = dataManager.SetUploadedDaySaidListStatus(
			i,
			DaySaidListStatus{
				Id:             status.Id,
				Update:         status.Update,
				UploadDate:     "",
				BlogPostId:     "",
				BlogPathSuffix: "",
			},
		)
		if err != nil {
			return false, err
		}

		status.UploadDate = ""
		status.BlogPostId = ""
		return true, nil
	}

	var dayIdRangeList DayIdRangeList = nil
	var deleteNontargets bool = false

	if DebugMode {
		if setting.DebugSettings != nil {
			var err error
			dayIdRangeList, err = dayIdRangeList.Parse(
				setting.DebugSettings.TimelineUploadTarget,
			)
			if err != nil {
				return err
			}
			deleteNontargets =
				setting.DebugSettings.DeleteUploadedPosts
		}
	}

	// Postパス変更の伝達のための変数
	didMap := map[string]bool{} // for timeline
	tidMap := map[string]bool{} // for thread

	statusList := dataManager.GetDaySaidListStatus()

	if !appState.ExistsFlag(UpgradeBlogPostDatetimeValue) {
		log.Println("start UpgradeBlogPostDatetimeValue")
		var deletedKeeps []string
		for i, statusRef := range statusList {
			if statusRef.BlogPostId == "" {
				continue
			}
			if DebugMode {
				if !deleteNontargets {
					if !dayIdRangeList.In(statusRef.Id) {
						dayIdRangeList = append(
							dayIdRangeList,
							&DayIdRange{
								Since: statusRef.Id,
								Until: statusRef.Id,
							},
						)
						deletedKeeps = append(
							deletedKeeps,
							statusRef.Id,
						)
					}
				}
			}
			status := *statusRef
			if _, err := deletePost(i, &status); err != nil {
				if DebugMode {
					// DebugModeのエラー離脱時に
					//　keep相当の記事が消えたままになる事態になる
					if len(deletedKeeps) > 0 {
						log.Println("DELETED keep-posts without recovering", deletedKeeps)
					}
				}
				return err
			}
		}
		statusList = dataManager.GetDaySaidListStatus()
	}

	for i, statusRef := range statusList {

		if DebugMode {
			// 指定時期以外のものは一切アップロードテストには使用しない
			if !dayIdRangeList.In(statusRef.Id) {
				if statusRef.BlogPostId == "" {
					continue
				}
				if deleteNontargets {
					status := *statusRef
					ok, err := deletePost(i, &status)
					if err != nil {
						return err
					}
					if ok {
						for k := i - 1; k >= 0; k-- {
							id := statusList[k].Id
							if dayIdRangeList.In(id) {
								didMap[id] = true
								break
							}
						}
						for k := i + 1; k < len(statusList); k++ {
							id := statusList[k].Id
							if dayIdRangeList.In(id) {
								didMap[id] = true
								break
							}
						}
					}
					continue
				}
			}
		}

		// 安全のための念のためのコピー
		status := *statusRef

		if DebugModeReUploadBlogAll {
			if _, err := deletePost(i, &status); err != nil {
				return err
			}
		}

		if status.UploadDate != "" {
			if strings.Compare(status.Update, status.UploadDate) <= 0 {
				nx := i + 1
				if DebugMode {
					nx = len(statusList)
					for k := i + 1; k < len(statusList); k++ {
						if statusList[k].BlogPostId != "" {
							nx = k
							break
						}
					}
				}
				// 次の日付が未アップロードならリンク生成のためアップロードし
				// そうでない場合はスキップする
				if nx >= len(statusList) ||
					statusList[nx].BlogPostId != "" {
					continue
				}
			}
		}

		list, err := dataManager.GetListCopyWithoutCaching(status.Id)
		if err != nil {
			return err
		}

		post, err := makeBlogPost(&setting, status.Id, list)
		if err != nil {
			return err
		}

		dt, err := ToTimeFromDayId(status.Id)
		if err != nil {
			return err
		}

		data := struct {
			Said              []*BlogSaid
			Date              string
			DateTimeValue     string
			PrevLink          string
			PrevDate          string
			PrevDateTimeValue string
			NextLink          string
			NextDate          string
			NextDateTimeValue string
		}{
			Said:          post,
			Date:          transBlogTitleDate(status.Id),
			DateTimeValue: ToDateString(dt),
		}

		if i > 0 {
			id := statusList[i-1].Id
			if DebugMode {
				for k := i - 1; k >= 0; k-- {
					tmp := statusList[k].Id
					if !deleteNontargets {
						if statusList[k].BlogPostId != "" {
							id = tmp
							break
						}
					}
					if dayIdRangeList.In(tmp) {
						id = tmp
						break
					}
				}
			}
			data.PrevLink = makeBlogPathFromId(id)
			data.PrevDate = transBlogTitleDate(id)
			dt, err = ToTimeFromDayId(id)
			if err != nil {
				return err
			}
			data.PrevDateTimeValue = ToDateString(dt)

			if DebugMode {
				if !dayIdRangeList.In(id) {
					data.PrevLink = ""
					data.PrevDate = ""
					data.PrevDateTimeValue = ""
				}
			}
		}

		if i+1 < len(statusList) {
			id := statusList[i+1].Id
			if DebugMode {
				for k := i + 1; k < len(statusList); k++ {
					tmp := statusList[k].Id
					if !deleteNontargets {
						if statusList[k].BlogPostId != "" {
							id = tmp
							break
						}
					}
					if dayIdRangeList.In(tmp) {
						id = tmp
						break
					}
				}
			}
			data.NextLink = makeBlogPathFromId(id)
			data.NextDate = transBlogTitleDate(id)
			dt, err = ToTimeFromDayId(id)
			if err != nil {
				return err
			}
			data.NextDateTimeValue = ToDateString(dt)

			if DebugMode {
				if !dayIdRangeList.In(id) {
					data.NextLink = ""
					data.NextDate = ""
					data.NextDateTimeValue = ""
				}
			}
		}

		mainMessage := struct {
			Date          string
			DateTimeValue string
			Count         int
		}{
			Date:          data.Date,
			DateTimeValue: data.DateTimeValue,
			Count:         len(list.Said),
		}

		blogPostTemplate.Init()
		var b bytes.Buffer
		err = blogPostTemplate.ExecuteTemplate(&b, "M", &mainMessage)
		if err != nil {
			return err
		}
		b.WriteString("<p><!-- more --></p>")
		err = blogPostTemplate.ExecuteTemplate(&b, "Z", &data)
		if err != nil {
			return err
		}

		title := makeBlogTitle(status.Id)
		date, err := makePostDate(status.Id)
		if err != nil {
			return err
		}
		blogBody := b.String()

		if DebugModeUploadBlog {
			// test
			dayId := status.Id
			f, err := os.Create("post_test_" + dayId + ".html")
			if err != nil {
				return err
			}
			defer f.Close()
			_, err = f.WriteString(`<!DOCTYPE html><html lang="ja"><head><meta charset="utf-8" /><title>` + title + " (" + date + ")" +
				`</title><style>blockquote { padding-left: 1.5em; border-left: 3px solid blue; background-color: rgba(220,220,220,0.5); }</style></head><body>` +
				"\n" + blogBody + "\n</body></html>")
			if err != nil {
				return err
			}
		}

		if DebugMode {
			log.Println("Upload Blog:", date)
		}

		res, err := setting.UploadBlog(
			status.BlogPostId,
			title,
			date,
			blogBody,
			categories,
		)
		if err != nil {
			return err
		}
		sleepAfterUpload()

		blogPostId := status.BlogPostId
		blogPathSuffix := status.BlogPathSuffix
		{
			ts := strings.Split(res.Id, "-")
			id := ts[len(ts)-1]
			if id != "" && blogPostId != id {
				blogPostId = id
			}
		}
		for _, lk := range res.Links {
			if lk.Rel == "edit" {
				ts := strings.Split(lk.Href, "/")
				id := ts[len(ts)-1]
				if id != "" && blogPostId != id {
					blogPostId = id
				}
			} else if lk.Rel == "alternate" {
				u := strings.Split(lk.Href, "_")
				if len(u) > 1 {
					suffix := "_" + u[len(u)-1]
					if suffix != blogPathSuffix {
						blogPathSuffix = suffix
					}
				}
			}
		}

		err = dataManager.SetUploadedDaySaidListStatus(
			i,
			DaySaidListStatus{
				Id:             status.Id,
				Update:         status.Update,
				UploadDate:     todayDateTimeValue,
				BlogPostId:     blogPostId,
				BlogPathSuffix: blogPathSuffix,
			},
		)
		if err != nil {
			return err
		}

		if status.BlogPathSuffix != blogPathSuffix {
			if DebugMode {
				log.Println("BlogPathSuffix:", date, blogPathSuffix)
			}
			if i > 0 {
				if DebugMode {
					for k := i - 1; k >= 0; k-- {
						id := statusList[k].Id
						if !deleteNontargets {
							if statusList[k].BlogPostId != "" {
								didMap[id] = true
								break
							}
						}
						if dayIdRangeList.In(id) {
							didMap[id] = true
							break
						}
					}
				} else {
					didMap[statusList[i-1].Id] = true
				}
			}
			if i+1 < len(statusList) {
				if DebugMode {
					for k := i + 1; k < len(statusList); k++ {
						id := statusList[k].Id
						if !deleteNontargets {
							if statusList[k].BlogPostId != "" {
								didMap[id] = true
								break
							}
						}
						if dayIdRangeList.In(id) {
							didMap[id] = true
							break
						}
					}
				} else {
					didMap[statusList[i+1].Id] = true
				}
			}
			for _, s := range list.Said {
				if s.Parent != "" {
					did, err := GetDayIdFromSaidId(s.Parent)
					if err != nil {
						return err
					}
					didMap[did] = true
				}
				for _, q := range s.QuotedRefs {
					did, err := GetDayIdFromSaidId(q)
					if err != nil {
						return err
					}
					didMap[did] = true
					ts, err := dataManager.GetSaidById(q)
					if err != nil {
						return err
					}
					if ts.Root != "" {
						tidMap[ts.Root] = true
					} else {
						tidMap[ts.Id] = true
					}
				}
				for _, c := range s.Children {
					did, err := GetDayIdFromSaidId(c)
					if err != nil {
						return err
					}
					didMap[did] = true
				}
			}
		}
		if _, ok := didMap[status.Id]; ok {
			delete(didMap, status.Id)
		}

	}

	if !appState.ExistsFlag(UpgradeBlogPostDatetimeValue) {
		appState.SetFlag(UpgradeBlogPostDatetimeValue)
		log.Println("completed UpgradeBlogPostDatetimeValue")
	}

	if len(didMap) > 0 {
		err := dataManager.UpdateDaySaidListStatus(didMap)
		if err != nil {
			return err
		}
	}

	if len(tidMap) > 0 {
		now := ToDateString(time.Now().Local())
		for tid := range tidMap {
			appSetting.UpdateThread(tid, now)
		}
	}

	return nil
}

func makeBlogPost(setting *AppSettingStore, dayId string, list *DaySaidList) ([]*BlogSaid, error) {
	if DebugModeRoutine {
		log.Println("makeBlogPost")
		defer log.Println("defer makeBlogPost")
	}

	post := []*BlogSaid{}
	m := map[string]*BlogSaid{}
	imgs := map[string]bool{}
	regimg := func(s *Said) {
		for _, t := range s.TextElements {
			if t.Type == TextElementTypeInsideImg {
				imgs[t.Src] = true
			}
		}
		for _, f := range s.ImageList {
			imgs[f] = true
		}
	}
	replies := []string{}

	for _, s := range list.Said {
		if s.Secret {
			bs := new(BlogSaid).Init(s, false, false)
			m[s.Id] = bs
			continue
		}

		regimg(s)
		bs := new(BlogSaid).Init(s, true, true)
		m[bs.Id] = bs

		// replies
		for _, c := range s.Children {
			did, err := GetDayIdFromSaidId(c)
			if err != nil {
				return nil, err
			}
			switch strings.Compare(did, dayId) {
			case -1:
				r := new(BlogSaid).Ref(c)
				if !r.Secret {
					bs.Replies = append(bs.Replies, r)
				}
			case +1:
				// upload対象外のreplyも追加されてしまう(today&yesterday)
				// 除外後の更新は難しいため仕方ないが
				replies = append(replies, bs.Id)
				replies = append(replies, c)
			}
		}

		// quote
		for _, t := range s.TextElements {
			if t.Type == TextElementTypeInsideQuote {
				id := strings.Fields(t.Src)[1]
				if q, ok := m[id]; ok {
					if !q.Secret {
						bs.Quoted = append(bs.Quoted, q.GetCopyForQuoted())
					}
				} else {
					qs, err := dataManager.GetSaidById(id)
					if err != nil {
						return nil, err
					}
					if !qs.Secret {
						regimg(qs)
						q = new(BlogSaid).Init(qs, false, false)
						bs.Quoted = append(bs.Quoted, q)
					}
				}
			}
		}

		// parent
		if s.Parent == "" {
			post = append(post, bs)
		} else {
			if p, ok := m[s.Parent]; ok {
				if p.Secret {
					post = append(post, bs)
				} else {
					p.Replies = append(p.Replies, bs)
				}
			} else {
				ps, err := dataManager.GetSaidById(s.Parent)
				if err != nil {
					return nil, err
				}
				if ps.Secret {
					post = append(post, bs)
				} else {
					regimg(ps)
					p = new(BlogSaid).Init(ps, false, false)
					m[ps.Id] = p
					for _, c := range ps.Children {
						did, err := GetDayIdFromSaidId(c)
						if err != nil {
							return nil, err
						}
						switch strings.Compare(did, dayId) {
						case -1:
							r := new(BlogSaid).Ref(c)
							if !r.Secret {
								p.Replies = append(p.Replies, r)
							}
						case +1:
							// upload対象外のreplyも追加されてしまう(today&yesterday)
							// 除外後の更新は難しいため仕方ないが
							replies = append(replies, p.Id)
							replies = append(replies, c)
						}
					}
					// quote
					for _, t := range ps.TextElements {
						if t.Type == TextElementTypeInsideQuote {
							id := strings.Fields(t.Src)[1]
							if q, ok := m[id]; ok {
								if !q.Secret {
									p.Quoted = append(p.Quoted, q.GetCopyForQuoted())
								}
							} else {
								qs, err := dataManager.GetSaidById(id)
								if err != nil {
									return nil, err
								}
								if !qs.Secret {
									regimg(qs)
									q = new(BlogSaid).Init(qs, false, false)
									p.Quoted = append(p.Quoted, q)
								}
							}
						}
					}
					p.Replies = append(p.Replies, bs)
					post = append(post, p)
				}
			}
		}
	}

	// replies
	for i := 0; i < len(replies); i += 2 {
		id := replies[i]
		c := replies[i+1]
		if p, ok := m[id]; ok {
			r := new(BlogSaid).Ref(c)
			if !r.Secret {
				p.Replies = append(p.Replies, r)
			}
		}
	}

	// images
	for img := range imgs {
		f, err := dataManager.GetFoto(img)
		if err != nil {
			return nil, err
		}
		if f == nil {
			f, err = setting.UploadFoto(img)
			if err != nil {
				return nil, err
			}
			sleepAfterUpload()
			err = dataManager.SetFoto(f)
			if err != nil {
				return nil, err
			}
		}
	}

	return post, nil
}

func transBlogDate(date string) string {
	t, err := FromDateString(date)
	if err != nil {
		log.Println("transBlogDate:", date, err)
		return date
	}
	return t.Format("2006年01月02日(Mon) 15:04:05")
}

func transBlogTitleDate(dayId string) string {
	t, err := ToTimeFromDayId(dayId)
	if err != nil {
		log.Println("transBlogTitleDate:", dayId, err)
		return dayId
	}
	return t.Format("2006年01月02日(Mon)")
}

func makeBlogTitle(dayId string) string {
	return transBlogTitleDate(dayId) + "の独り言"
}

func toHashId(id string) string {
	return "#inwardly" + id
}

func toHashThreadId(id string) string {
	return "#inwardlythread" + id
}

func makePostDate(dayId string) (string, error) {
	dt, err := ToTimeFromDayId(dayId)
	if err != nil {
		return "", err
	}
	if DebugModeFilePath {
		return dt.Format("2006-01-02") + TimelineBlogTimeValueForDebug, nil
	}
	return dt.Format("2006-01-02") + TimelineBlogTimeValue, nil
}

func makeBlogPath(p string) string {
	// p ... /2006/01/02/
	if DebugModeFilePath {
		return "/entry" + p + TimelineBlogPathEndForDebug
	}
	return "/entry" + p + TimelineBlogPathEnd
}

func makeBlogPathFromId(id string) string {
	suffix := dataManager.GetBlogPathSuffix(id[:6])
	return makeBlogPath("/20"+id[:2]+
		"/"+id[2:4]+"/"+id[4:6]+"/") + suffix
}

func makeBlogPathFromTime(t time.Time) string {
	id := GetDayIdFromTime(t)
	suffix := dataManager.GetBlogPathSuffix(id)
	return makeBlogPath(t.Format("/2006/01/02/")) + suffix
}

func makeBlogPathFromDate(date string) string {
	if t, err := FromDateString(date); err == nil {
		return makeBlogPathFromTime(t)
	} else {
		log.Println("makeBlogPathFromDate:", err)
		return makeBlogPath("/2106/01/02/")
	}
}

func (this *BlogSaid) Ref(id string) *BlogSaid {
	this.Omitted = true
	if s, err := dataManager.GetSaidById(id); err == nil {
		if s.Secret {
			this.Secret = true
			this.OmittedTitle = "(omitted)"
			return this
		}
		this.DateTimeValue = s.Date
		this.Date = transBlogDate(s.Date)
		if s.Update != "" {
			this.UpdateDateTimeValue = s.Update
			this.UpdateDate = transBlogDate(s.Update)
		}
		this.Link = makeBlogPathFromDate(s.Date) + toHashId(id)
		this.OmittedTitle = s.Text
		return this
	} else {
		// たぶんここにこないと思うけど
		log.Println("[BUG?] BlogSaid.Ref:", err)
		if t, err := ToTimeFromSaidId(id); err == nil {
			this.DateTimeValue = ToDateString(t)
			this.Date = transBlogDate(this.DateTimeValue)
			this.Link = makeBlogPathFromTime(t) + toHashId(id)
			return this
		} else {
			log.Println("[BUG??] BlogSaid.Ref:", err)
			this.DateTimeValue = id
			this.Date = id
		}
	}
	this.Link = makeBlogPathFromId(id) + toHashId(id)
	return this
}

func (this *BlogSaid) GetCopyForQuoted() *BlogSaid {
	ret := new(BlogSaid)
	*ret = *this
	ret.Id = ""
	ret.Quoted = nil
	ret.Replies = nil
	ret.Tweets = nil
	return ret
}

func (this *BlogSaid) Init(s *Said, today, useId bool) *BlogSaid {
	if s.Secret {
		this.Secret = true
		this.Omitted = true
		this.OmittedTitle = "(omitted)"
		return this
	}
	if useId {
		this.Id = s.Id
	}
	this.DateTimeValue = s.Date
	this.Date = transBlogDate(s.Date)
	if s.Update != "" {
		this.UpdateDateTimeValue = s.Update
		this.UpdateDate = transBlogDate(s.Update)
	}
	if today {
		this.Link = toHashId(s.Id)
	} else {
		this.Link = makeBlogPathFromDate(s.Date) + toHashId(s.Id)
	}
	this.Deleted = s.Deleted
	this.TextElements = s.TextElements
	this.Images = s.ImageList
	for _, te := range s.TextElements {
		if te.Type == TextElementTypeUrl {
			tokens := strings.Split(te.Src, "/")
			if len(tokens) < 6 {
				continue
			}
			if tokens[2] != "twitter.com" {
				continue
			}
			if tokens[4] != "status" {
				continue
			}
			this.Tweets = append(this.Tweets, te.Src)
		}
	}
	return this
}

func (this *BlogSaid) InitForThread(s *Said, thisThread, useId bool) *BlogSaid {
	this.Secret = s.Secret
	if useId {
		this.Id = s.Id
	}
	this.DateTimeValue = s.Date
	this.Date = transBlogDate(s.Date)
	if s.Update != "" {
		this.UpdateDateTimeValue = s.Update
		this.UpdateDate = transBlogDate(s.Update)
	}
	if thisThread {
		this.Link = toHashThreadId(s.Id)
	} else {
		this.Link = makeBlogPathFromDate(s.Date) + toHashId(s.Id)
	}
	this.Deleted = s.Deleted
	this.TextElements = s.TextElements
	this.Images = s.ImageList
	for _, te := range s.TextElements {
		if te.Type == TextElementTypeUrl {
			tokens := strings.Split(te.Src, "/")
			if len(tokens) < 6 {
				continue
			}
			if tokens[2] != "twitter.com" {
				continue
			}
			if tokens[4] != "status" {
				continue
			}
			this.Tweets = append(this.Tweets, te.Src)
		}
	}
	return this
}

func (this *DayIdRange) Parse(dayRange string) (*DayIdRange, error) {
	xs := strings.Split(dayRange, ":")
	if xs[0] != "" {
		t0, err := time.Parse("2006-01-02", xs[0])
		if err != nil {
			return nil, err
		}
		this.Since = GetDayIdFromTime(t0)
	}
	if xs[len(xs)-1] != "" {
		t1, err := time.Parse("2006-01-02", xs[len(xs)-1])
		if err != nil {
			return nil, err
		}
		this.Until = GetDayIdFromTime(t1)
	}
	return this, nil
}

func (this *DayIdRange) In(dayId string) bool {
	if this.Since != "" && strings.Compare(this.Since, dayId) > 0 {
		return false
	}
	return this.Until == "" ||
		strings.Compare(this.Until, dayId) >= 0
}

func (this DayIdRangeList) Parse(s string) (DayIdRangeList, error) {
	targets := strings.Split(s, ",")
	for _, t := range targets {
		drng, err := new(DayIdRange).Parse(t)
		if err != nil {
			return nil, err
		}
		this = append(this, drng)
	}
	return this, nil
}

func (this DayIdRangeList) In(dayId string) bool {
	for _, drng := range this {
		if drng.In(dayId) {
			return true
		}
	}
	return false
}

func (this *BlogPostTemplate) Init() {
	this.once.Do(func() {
		if DebugModeRoutine {
			log.Println("BlogPostTemplate.Init")
			defer log.Println("defer BlogPostTemplate.Init")
		}
		r := regexp.MustCompile(`([}>])\s+([<{])`)
		rps := func(s string) string {
			return r.ReplaceAllString(s, "$1$2")
		}
		t := template.New("B").Funcs(template.FuncMap{
			"first": func(s string) string { return strings.Fields(s)[0] },
			"last":  func(s string) string { x := strings.Fields(s); return x[len(x)-1] },
			"getimginfo": func(s string) []string {
				if f, _ := dataManager.GetFoto(s); f == nil {
					return []string{s, s}
				} else {
					return []string{f.Url, f.FotoId}
				}
			},
			"trim":  func(s string) string { return strings.TrimSuffix(s, "\n") },
			"haslf": func(s string) bool { return strings.HasSuffix(s, "\n") },
		})
		t = template.Must(t.Parse(rps(BlogPostSaidFormat)))
		t = template.Must(t.Parse(rps(BlogPostDate)))
		t = template.Must(t.Parse(rps(BlogPostText)))
		t = template.Must(t.Parse(rps(BlogPostLink)))
		t = template.Must(t.Parse(rps(BlogPostDeleted)))
		t = template.Must(t.Parse(rps(BlogPostImages)))
		t = template.Must(t.Parse(rps(BlogPostQuoted)))
		t = template.Must(t.Parse(rps(BlogPostSaidList)))
		t = template.Must(t.Parse(rps(BlogPostToplevel)))
		t = template.Must(t.Parse(rps(BlogPostToplevelLink)))
		t = template.Must(t.Parse(rps(BlogPostOmitted)))
		t = template.Must(t.Parse(rps(BlogPostMainMessage)))
		t = template.Must(t.Parse(rps(BlogPostQuotedTweet)))
		this.Template = t
	})
}

const BlogPostSaidFormat = `{{define "T"}}
    <div style="font-family: monospace; font-size: 0.8em; color: grey;">
        {{template "D" .}}&nbsp;{{template "A" .Link}}
    </div>
    {{template "O" .}}
    {{template "X" .Deleted}}
    {{template "S" .TextElements}}
    {{template "Q" .Quoted}}
    {{template "TW" .Tweets}}
    {{template "I" .Images}}
    {{template "L" .Replies}}
{{end}}`

const BlogPostLink = `{{define "A"}}
    {{if .}}
        <a href="{{.}}">#</a>
    {{end}}
{{end}}`

const BlogPostDate = `{{define "D"}}
    {{if .Date}}
        <time datetime="{{.DateTimeValue}}">{{.Date}}</time>
        {{if .UpdateDateTimeValue}}
            <span style="font-size: 0.8em;">&nbsp;(UPD&nbsp;<time datetime="{{.UpdateDateTimeValue}}">{{.UpdateDate}}</time>)</span>
        {{end}}
    {{end}}
{{end}}`

const BlogPostOmitted = `{{define "O"}}
    {{if .Omitted}}
        <p {{if .OmittedTitle}}title="{{.OmittedTitle}}" {{end}}style="color: rgba(127,127,127,0.5); font-size: 0.64em; font-style: italic;">(omitted)</p>
    {{end}}
{{end}}`

const BlogPostDeleted = `{{define "X"}}
    {{if .}}
        <p style="font-size: 0.64em; font-style: italic;">deleted</p>
    {{end}}
{{end}}`

const BlogPostText = `{{define "S"}}
    {{if .}}
        <p style="font-family: monospace; white-space: pre-wrap; overflow-wrap: break-word; line-height: 1.2;">
        {{range .}}
            {{if (eq .Type "` + TextElementTypeText + `")}}
                {{trim .Src}}
                {{if (haslf .Src)}}<br />{{end}}
            {{else if (eq .Type "` + TextElementTypeUrl + `")}}
                <a href="{{first .Src}}" target="_blank" rel="noopener noreferrer nofollow external">{{last .Src}}</a>
            {{else if (eq .Type "` + TextElementTypeInsideQuote + `")}}
                <span style="font-size: 0.8em; text-decoration: underline; color: rgba(127,127,127,0.5);">{{last .Src}}</span>
            {{else if (eq .Type "` + TextElementTypeInsideUrl + `")}}
                <span style="font-size: 0.8em; text-decoration: underline; color: rgba(127,127,127,0.5);">inwardly:/{{first .Src}}</span>
            {{else if (eq .Type "` + TextElementTypeInsideImg + `")}}
                {{with $s := (getimginfo .Src)}}
                    <img alt="{{index $s 1}}" title="{{index $s 1}}" src="{{index $s 0}}" width="100" class="hatena-fotolife" itemprop="image" />
                {{end}}
            {{end}}
        {{end}}
        </p>
    {{end}}
{{end}}`

const BlogPostImages = `{{define "I"}}
    {{if .}}
        <div>
        {{range .}}
            {{with $s := (getimginfo .)}}
                <img alt="{{index $s 1}}" title="{{index $s 1}}" src="{{index $s 0}}" width="100" class="hatena-fotolife" itemprop="image" />
            {{end}}
        {{end}}
        </div>
    {{end}}
{{end}}`

const BlogPostSaidList = `{{define "L"}}
    {{if .}}
        <ul style="list-style: none; padding-left: 1.2em; border-left: 1px solid rgba(127,127,127,0.3);">
        {{range .}}
            {{if .Id}}
                <li id="inwardly{{.Id}}">
            {{else}}
                <li>
            {{end}}
            {{template "T" .}}
            </li>
        {{end}}
        </ul>
    {{end}}
{{end}}`

const BlogPostToplevelLink = `{{define "W"}}
    <div style="font-size: 0.64em; margin: 3em auto;">
        {{if .PrevLink}}&nbsp;&lt;&nbsp;<a href="{{.PrevLink}}">
            <time datetime="{{.PrevDateTimeValue}}">
                {{.PrevDate}}
            </time>の独り言</a>&nbsp;|{{end}}&nbsp;<time datetime="{.DateTimeValue}}">
                {{.Date}}
            </time>の独り言&nbsp;{{if .NextLink}}|&nbsp;<a href="{{.NextLink}}">
            <time datetime="{{.NextDateTimeValue}}">
                {{.NextDate}}
            </time>の独り言</a>&nbsp;&gt;&nbsp;{{end}}
    </div>
{{end}}`

const BlogPostToplevel = `{{define "Z"}}
    {{template "W" .}}
    {{if .Said}}
        <ul style="list-style: none; padding-left: 0px;">
        {{range .Said}}
            {{if .Id}}
                <li id="inwardly{{.Id}}">
            {{else}}
                <li>
            {{end}}
            {{template "T" .}}
            </li>
        {{end}}
        </ul>
    {{else}}
        <p>no said</p>
    {{end}}
    {{template "W" .}}
{{end}}`

const BlogPostQuoted = `{{define "Q"}}
    {{if .}}
        {{range .}}
            <blockquote>{{template "T" .}}</blockquote>
        {{end}}
    {{end}}
{{end}}`

const BlogPostQuotedTweet = `{{define "TW"}}
    {{if .}}
        {{range .}}
            <blockquote class="twitter-tweet" data-conversation="none" data-theme="dark" data-width="300" style="display:none;">
                <a href="{{.}}">&nbsp;</a>
            </blockquote>
        {{end}}
    {{end}}
{{end}}`

const BlogPostMainMessage = `{{define "M"}}
    <p><time datetime="{{.DateTimeValue}}">{{.Date}}</time>の呟きは {{.Count}}</p>
{{end}}`
