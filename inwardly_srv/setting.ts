
function authorizeHatena() {
	const authorizeBtn = <HTMLButtonElement>document.querySelector('#authorization > form > button')!;
	authorizeBtn.disabled = true;
	const api = '/api/setting/authorize';
	fetch(api, {
		method: 'POST',
	})
	.then(toJson)
	.then( (resp: SettingResult) => {
		if (!resp.ok) {
			showError(resp.message);
			return;
		}
		const authorization = document.querySelector('#authorization > .status')!;
		authorization.innerHTML = '';
		const a = authorization.appendChild(document.createElement('a'));
		a.href = resp.value;
		a.textContent = resp.value;
		showMessage(resp.message, false);
	})
	.catch( err => {
		showError(err);
	})
	.finally( () => {
		authorizeBtn.disabled = false;
	});
}

function saveApiSetting() {
	const ckey = (<HTMLInputElement>document.getElementById('ckey')!).value.trim();
	if (ckey === '') {
		showError('ConsumerKey is empty');
		return
	}
	const csec = (<HTMLInputElement>document.getElementById('csec')!).value.trim();
	if (csec === '') {
		showError('ConsumerSecret is empty');
		return
	}
	const formData = new FormData();
	formData.set('consumer_key', ckey);
	formData.set('consumer_secret', csec);
	const api = '/api/setting/consumer';
	fetch(api, {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then(updateSettings)
	.then( () => {
		showMessage('saved api settings!', false);
	})
	.catch( err => {
		showError(err);
	});
}

function saveBlogSetting() {
	const hid = (<HTMLInputElement>document.getElementById('hid')!).value.trim();
	if (hid === '') {
		showError('Hatena Id is empty');
		return
	}
	const bid = (<HTMLInputElement>document.getElementById('bid')!).value.trim();
	if (bid === '') {
		showError('Blog Id is empty');
		return
	}
	const autoUpload = (<HTMLSelectElement>document.getElementById('autoupload')!).value.trim();
	if (autoUpload !== '0' && autoUpload !== '1') {
		showError(`invalid Auto Upload value "${autoUpload}"`);
		return
	}
	const threadUpload = (<HTMLSelectElement>document.getElementById('threadupload')!).value.trim();
	if (threadUpload !== '0' && threadUpload !== '1') {
		showError(`invalid Thread Upload value "${threadUpload}"`);
		return
	}
	const formData = new FormData();
	formData.set('hatena_id', hid);
	formData.set('blog_id', bid);
	formData.set('auto_upload', autoUpload);
	formData.set('thread_upload', threadUpload);
	const api = '/api/setting/blog';
	fetch(api, {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then(updateSettings)
	.then( () => {
		showMessage('saved blog settings!', false);
	})
	.catch( err => {
		showError(err);
	});
}

function updateSettings(settings: Settings) {
	const apisetting = document.getElementById('apisetting')!;
	const authorization = document.querySelector('#authorization > .status')!;
	const authorizeBtn = <HTMLButtonElement>document.querySelector('#authorization > form > button')!;
	if (settings.has_consumer) {
		apisetting.querySelector('.status')!.textContent = 'OK';
		(<HTMLInputElement>document.getElementById('ckey')!).value = settings.consumer_key;
		authorizeBtn.disabled = false;
	} else {
		apisetting.querySelector('.status')!.textContent = 'None';
		authorizeBtn.disabled = true;
	}
	authorization.innerHTML = '';
	if (settings.authorized) {
		authorization.textContent = 'OK';
	} else {
		authorization.textContent = 'None';
	}
	const blogLink = document.querySelector('#blog > form > .form span.decfont')!;
	blogLink.innerHTML = '';
	if (settings.hatena_blog !== null) {
		if (settings.hatena_blog.hatena_id !== '') {
			(<HTMLInputElement>document.getElementById('hid')!).value = settings.hatena_blog.hatena_id;
		}
		if (settings.hatena_blog.blog_id !== '') {
			(<HTMLInputElement>document.getElementById('bid')!).value = settings.hatena_blog.blog_id;
			const a = blogLink.appendChild(document.createElement('a'));
			const u = `https://${settings.hatena_blog.blog_id}/`;
			a.href = u;
			a.target = '_blank';
			a.rel = 'noopener noreferrer nofollow external';
			a.textContent = u;
		} else {
			blogLink.textContent = '-';
		}
		document.querySelector('#blog > .status')!.textContent = 'OK';
	} else {
		document.querySelector('#blog > .status')!.textContent = 'None';
		blogLink.textContent = '-';
	}
	if (settings.blog_settings !== null) {
		(<HTMLSelectElement>document.getElementById('autoupload')!)
			.value = settings.blog_settings.auto_upload ? '1' : '0';
		(<HTMLSelectElement>document.getElementById('threadupload')!)
			.value = settings.blog_settings.thread_upload ? '1' : '0';
	}
	if (settings.debug_settings === null) {
		document.getElementById('debugsetting')!.classList.add('toggleDisplayNone');
	} else {
		document.getElementById('debugsetting')!.classList.remove('toggleDisplayNone');
		(<HTMLInputElement>document.getElementById('dtuc')!)
			.value = settings.debug_settings.timeline_upload;
		(<HTMLSelectElement>document.getElementById('dunt')!)
			.value = settings.debug_settings.nontargets ? '1' : '0';
	}
}

function saveDebugSetting() {
	const dtuc = (<HTMLInputElement>document.getElementById('dtuc')!).value.trim();
	const nonTargets = (<HTMLSelectElement>document.getElementById('dunt')!).value.trim();
	if (nonTargets !== '0' && nonTargets !== '1') {
		showError(`invalid Uploaded Non-Targets value "${nonTargets}"`);
		return
	}
	const formData = new FormData();
	formData.set('timeline_upload', dtuc);
	formData.set('nontargets', nonTargets);
	fetch('/api/setting/debugmode', {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then(updateSettings)
	.then( () => {
		showMessage('saved debug-mode setting!', false);
	})
	.catch( err => {
		showError(err);
	});
}

function loadSettings() {
	const api = '/api/setting/get';
	fetch(api)
	.then(toJson)
	.then(updateSettings)
	.catch( err => {
		showError(err);
	});
}

function loadStatus(msg: boolean) {
	const api = '/api/status/get';
	fetch(api)
	.then(toJson)
	.then( (res: Status) => {
		if (res.system !== null) {
			const os = document.querySelectorAll('footer output');
			let rst = res.system.routine_start_time;
			if (rst === '') {
				rst = '-';
			} else {
				rst = toJpString(parseDateTime(rst))
					+ rst.slice(19, 23);
			}
			let ret = res.system.routine_end_time;
			if (ret === '') {
				ret = '-';
			} else {
				ret = toJpString(parseDateTime(ret))
					+ ret.slice(19, 23);
			}
			const cs = res.system.shutdown_token.split('');
			for (let i = 0; i < cs.length; i++) {
				const c = cs[i];
				const low = c.toLowerCase();
				if (c === low) {
					cs[i] = c.toUpperCase();
				} else {
					cs[i] = low;
				}
			}
			os[0].textContent = rst;
			os[1].textContent = ret;
			os[2].textContent = res.system.static_files;
			os[3].textContent = res.system.build_version;
			os[4].textContent = `${cs.join('')} (input swap-case token)`;
			(<HTMLInputElement>document.getElementById('shtk')!)
				.placeholder = res.system.shutdown_token;
		}
		if (msg) {
			showMessage('updated!', false);
		}
	})
	.catch( err => {
		showError(err);
	});
}

function shutdownServer() {
	const token = (<HTMLInputElement>document.getElementById('shtk')!).value;
	const formData = new FormData();
	formData.set('token', token);
	fetch('/api/shutdown', {
		method: 'POST',
		body: formData,
	})
	.then(toJson)
	.then( (res: { result: string; }) => {
		showMessage(`shutdown ${res.result}`, false);
	})
	.catch( err => {
		showError(err);
	});
}

////////////////
//
// 初期化処理
//
////////////////

document.querySelector('header > nav button.link')!
.addEventListener('click', () => {
	window.location.assign('/link');
});

document.querySelector('header > nav button.home')!
.addEventListener('click', () => {
	window.location.assign('/');
});

document.querySelector('header > nav button.misc')!
.addEventListener('click', () => {
	window.location.assign('/misc');
});

document.querySelector('#apisetting > form')!
.addEventListener('submit', e => {
	e.preventDefault();
	saveApiSetting();
});

document.querySelector('#authorization > form')!
.addEventListener('submit', e => {
	e.preventDefault();
	authorizeHatena();
});

document.querySelector('#blog > form')!
.addEventListener('submit', e => {
	e.preventDefault();
	saveBlogSetting();
});

document.querySelector('#debugsetting > form')!
.addEventListener('submit', e => {
	e.preventDefault();
	saveDebugSetting();
});

document.querySelector('footer > .status > button.updatestatus')!
.addEventListener('click', () => {
	document.querySelectorAll('footer output')
		.forEach( e => e.textContent = '' );
	loadStatus(true);
});

document.querySelector('footer > .status > button.shutdown')!
.addEventListener('click', shutdownServer);

window.addEventListener('load', () => {
	loadSettings();
	loadStatus(false);
});
