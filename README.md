# inwardly

独り言ツール  



ローカルホストでサーバー起動してブラウザから利用する   



#### 動作確認環境  
OS: Windows7 SP1 Starter  
ブラウザ: Vivaldi 3.6.2165.40 (Chrome/88.0.4324.186相当?)  
Goコンパイラ: go version go1.16 windows/386  



#### その他情報    
実行時使用メモリ: 推定50MB以上  
実行ファイルサイズ: 10MB以上  
ソースコードサイズ: 2MB以上  
ソースコードライセンス: MIT License  



#### インストール方法  
```bash
C:\> go install bitbucket.org/neetsdkasu/inwardly/inwardly_srv@latest
```
※インストール先はインストール環境のGoの設定に依存する(通常は`%GOPATH%\bin\`にインストールされると思われる)  


#### サーバー起動方法 
```bash
C:\> %GOPATH%\bin\inwardly_srv.exe
```
※これはインストール先がOSの環境変数のPATHに含まれている場合を想定  
※セキュリティソフト(ファイヤーウォールなど)の設定によっては正常に動作しない場合があるかも  


※inwardlyのデータの保存先を指定する場合（例えば`C:\foo\bar\`に保存する場合）  
```bash
C:\> %GOPATH%\bin\inwardly_srv.exe -home C:\foo\bar\
```


#### ブラウザでアクセス 
```
http://127.8.9.72:8972/
```

