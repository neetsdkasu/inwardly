# inwardly_wex2

ブラウザ拡張機能(WebExtension)  


ウェブサイトの"この記事をTweetでシェア"のボタンの遷移先URLをinwardlyへのページに改変するブラウザ拡張機能  



#### 動作確認環境  
OS: Windows7 SP1 Starter  
ブラウザ: Vivaldi 3.4.2066.76  


#### インストール  

※ブラウザVivaldiの場合  

　1. 拡張機能の設定ページを開く ( vivaldi://extensions/ )  
 2. デベロッパーモードに切り替える  
 3. "パッケージ化されてない拡張機能を読み込む"からinwardly_wex2のディレクトリを選択する  
 
