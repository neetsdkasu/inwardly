// script.js
// inwardly WebExtension 2
// redirect from sharing page to inwardly
// author: Leonardone

const host = '127.8.9.72';

const port = '8972';

const path = '/mention';

const baseUrl = `http://${host}:${port}${path}`;

const selector = 'a[href*="twitter.com/intent/"], a[href*="twitter.com/share"]';

document.querySelectorAll(selector).forEach(e => {
    e.href = baseUrl + (new URL(e.href)).search;
});

